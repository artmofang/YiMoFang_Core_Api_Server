<?php


//一维数组返回对应的键值字符串

function get_onearray_key_value($array){
	//定义返回
	$out_array=array();
	//定义key;
	$key='';
	//定义值
	$value='';
	//拼接相关
	foreach($array as $k=>$v)
	{
		$key.=$k.',';
		$value.='"'.$v.'",';
	}
	//字符截取
	$out_array['key']=substr($key,0,-1);
	$out_array['value']=substr($value,0,-1);
	return $out_array;

}

//二维数组转化
function get_twoarray_key_values($array){
	//定义返回
	$out_array=array();
//定义key;
	$key='';
//定义值
	$value='';
	foreach($array as $k=>$v)
	{
		foreach($v as $ke=>$var)
		{
			if($k==0){
				$key.=$ke.',';
			}
			$value.='"'.$var.'",';
		}
		$value=substr($value,0,-1);
		$value.='|';

	}
	//字符截取
	$out_array['key']=substr($key,0,-1);
	$out_array['value']=substr($value,0,-1);
	return $out_array;

}

/*数组转化inser语句单条语句
 *
 * @param unknown $table_name 表名
 * @param unknown $array 一位数组 数据键值对 建为key 值为value
 * ****/
function insertSql($table_name,$array){
	$key='';
	$value='';
	foreach($array as $k=>$v ){
		$key.=$k.',';
		
		$value.="'".$v."',";
	}
	$sql='insert into '.$table_name .' ('.substr($key,0,-1).') values ('.substr($value,0,-1).')';

	return $sql;


}

/*数组转化insert语句多条插入
 *
 *@param unknown $table_name 表名
 * @param unknown $array 2位数组 数据键值对 建为key 值为value
 *
 *
 */

function insertAllSql($table_name,$array){
	$key='';
	$value='';
	
	foreach($array as $k=>$v){
		$zhong='';
		foreach($v as $ke=>$va){
			if($k==0){
				$key.=$ke.',';
			}		
			$zhong.="'".$va."',";
		}
		$value.='('.substr($zhong,0,-1).'),';
	}
	$sql='insert into '.$table_name .' ('.substr($key,0,-1).') values' .substr($value,0,-1);
	return $sql;
}


function objectToArray(&$object) {
    $object =  json_decode( json_encode( $object),true);
    return  $object;
}


function deleteArrData($tmpArr,$dataArr){

	$newArr = $dataArr;
	
	foreach($tmpArr as $k=>$v){
		unset($newArr[$k]);
	}

	return $newArr;


}

































?>