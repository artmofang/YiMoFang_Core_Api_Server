<?php
/*
 * 将空字值转化为默认值
 * @params {$value} 实际结果
 * @params {$defaultValue} 默认结果
 * @return 返回处理后的结果
 */
function setDefaultValue($value,$defaultValue){
	if($value == ''){
       return $defaultValue;
	}else{
	   return $value;	
	}
}

/*
 * 判断一个数组是关联数组还是索引数组
 * @params {$array} 数组
 * @return true,是关联数组 false不是关联数组
 */
function is_assoc_array($array){
    return array_keys($array) !== range(0, count($array) - 1);
}
?>