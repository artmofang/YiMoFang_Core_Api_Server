<?php
namespace coreLib\AuthClass;
use coreLib\ErrorClass\Error;

/**
 * 验权操作类
 * @desc 用于验证请求服务器是否有权限访问核心层服务器
 **/

class Auth {

	private   $db;						 //数据库操作对象
	private   $route;					 //路由对象
	public    $requestServerToken;	     //请求的服务器token		

	/*
	 * 构造函数
	 * @params {$db} 数据库操作对象
	 * @return 没有返回值
	 */
	function __construct($db,$route){

		//数据库操作对象
		$this->db = $db;

		//路由操作对象
		$this->route = $route;

		if($GLOBALS['settings']['isAuthServer']){

			//验权操作
			$this->serverAuth();
		
		}

		


	}

	/*
	 * 验权操作方法
	 * @return 没有返回值
	 */
	function serverAuth(){

		//如果有则进行验证
		if($this->route->requestMethod == 'POST'){

			//拿出appid
			$appid = $this->route->requestServerAppId;

			//拿出服务器token
			$token = $this->route->requestServerToken;

			//查询redis中是否存在该appid的token缓存
			$this->db->redisDB->selectDb(2);

			//获取缓存服务器token
			$authResult = $this->db->redisDB->getData($token);

			//验证服务器token是否通过
			if(!$authResult){
				Error::Result("REQUEST_ERROR",$GLOBALS['errorMsg']['IllegalRequestError']);
			}

		}

	}

}
?>