<?php
namespace coreLib\ErrorClass;
use \coreLib\LogClass\Log;

/**
 * 错误处理类
 **/
class Error{

	  /*
 	   * 错误结果返回
 	   * @params {$type} 错误的类型
 	   * @params {$msg}	错误的内容
 	   * @return 没有返回值
 	   */	
	  static public function Result($type,$msg){

	  	 	//向请求端返回错误信息
	  	 	$errorData = array("success" => false,"result" => array("message" => $msg));
	  	 	echo json_encode($errorData);
	  	 	exit();
	  }

}