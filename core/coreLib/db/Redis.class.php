<?php
namespace coreLib\db\RedisClass;
use coreLib\ErrorClass\Error;

/**
 * redis数据库操作类
 **/
class Redis{

	public  $redisConn;	//数据库连接对象
	private $username;	//用户名
	private $password;	//密码
	private $port;		//端口号
	private $host;		//主机地址

	/*
	 * 构造函数
	 * @params {$setting} 数据库配置信息
	 * @return 没有返回值
	 */
	function __construct($setting){

		$this->username = $setting['username'];
		$this->password = $setting['password'];
		$this->port 	= $setting['port'];
		$this->host 	= $setting['host'];

		//创建redis数据库连接
		$redis = new \Redis();

		//连接redis数据库

		$redis->connect($this->host, $this->port);

		//判断程序是否运行
		if(!$redis->ping() == "+PONG"){
			Error::Result("DATABASE_ERROR","Redis_Error:已与redis数据库断开连接");
		}

		//数据库连接对象
		$this->redisConn = $redis;

	}

	/*
	 * 将数据存入redis数据库
	 */
	public function setData($key,$value){
		return $this->redisConn->set($key,$value);
	}

	/*
	 * 将数据取出
	 */
	public function getData($key){
		return $this->redisConn->get($key);
	}

	/*
	 * 设置过期时间
	 */
	public function setExpire($key,$expiration){
		return $this->redisConn->EXPIRE($key,$expiration);
	}

	/*
	 * 检查指定KEY是否存在
	 */
	public function keyExists($key){
		return $this->redisConn->exists($key);
	}

	/*
	 * 删除指定key
	 */
	public function deleteData($key){
		return $this->redisConn->del($key);
	}

	/*
	 * 切换数据库
	 */
	public function selectDb($index){
		return $this->redisConn->SELECT($index);
	}
	

}