<?php
namespace coreLib\DbClass;
use coreLib\ErrorClass\Error;
use coreLib\CacheClass\Cache;
use coreLib\db\MysqlClass\Mysql;
use coreLib\db\RedisClass\Redis;


/**
 * 数据库基类
 * @desc 主要用于对配置文件中的相关数据库进行连接操作
 **/
class Db{

	public  $mysqlDB;			 //mysql数据库操作对象
	public  $mongoDB;			 //mongodb数据库操作对象
	public  $redisDB;			 //redis数据库操作对象
	private $databaseSettings;   //数据库配置信息


	function __construct($settings){

		//取出数据库配置信息
		$this->databaseSettings = $settings;

		//查看是否有配置数据库信息
		if($this->databaseSettings == NULL){
			Error::Result("SYSTEM_ERROR",$GLOBALS['errorMsg']['databaseSettingError']);
		}

		//查看当前需要配置的数据库类别,获取到所有key
		$dbTypeArr = array_keys($this->databaseSettings);

		//循环创建需要的数据库对象
		for($i=0;$i<count($dbTypeArr);$i++){

			//获取当前数据库的配置
			$setting = $this->databaseSettings[$dbTypeArr[$i]];

			switch($dbTypeArr[$i]){

				case "Mysql":

					require_once("./core/coreLib/db/Mysql.class.php");
					$this->mysqlDB = new Mysql($setting);
					

				break;

				case "Redis":

					// require_once("./core/coreLib/db/Redis.class.php");
					// $this->redisDB = new Redis($setting);

				break;	

				default:
					Error::Result("SYSTEM_ERROR",$GLOBALS['errorMsg']['databaseTypeError']);
				break;
			}
		}
	}
}

