<?php
namespace coreLib\RequestClass;
use coreLib\ErrorClass\Error;
use coreLib\ResponseClass\Response;
use tool\RegexpClass\Regexp;
use coreLib\ToolClass\Tool;

/**
 * 请求处理类
 * @desc 对路由分析的接口进行请求处理，会寻找相应的义务处理文件
 **/

class Request{

	/*
	 * 构造函数
	 * @params {$route} 路由操作对象
	 * @params {$permissionm}	权限操作对象
	 * @params {$db}	数据库操作对象
	 * @params {$cache}	缓存操作对象
	 * @return 没有返回值
	 */
	function __construct($route,$permissionm,$db,$cache){

		//构建通用工具对象
		$tool = new Tool($db,$route->requestToken,$GLOBALS['settings']['tokenValidity']);

		//构建响应处理对象
		$response  = new Response();

		//正则处理对象
		$regexpObj = new Regexp();
		
		//请求接口必传参数验证
		$this->requestParamsValidation($route,$response);

		//如果配置读取缓存，则进行缓存读取操作
		if($GLOBALS['settings']['cache']['isReadCache']){
 

			//接口缓存数据调用
			$data = $cache->getCacheData();

			//判断是否有缓存数据，如果有则直接返回缓存数据
			if($data){
				$response->responseData(true,json_decode($data));
			}
		}
	
		
		//lib层加载
		if($route->pathInfoLibUrl!=false){


			include $route->pathInfoLibUrl;

			$model_name=$route->pathInfoName;

			$$model_name=new $model_name($db);
			//var_dump($check);
		}
		
	
		//调用接口文件
		require_once($route->reuqestFilePath);
		
	}

	/*
	 * 参数必传项验证
	 * @params {$route} 路由操作对象
	 * @params {$response} 返回结果操作对象
	 * @return 没有返回值
	 */
	protected function requestParamsValidation($route,$response){

		//判断接口参数是否满足条件
		$paramsValidation = $GLOBALS['interfaceParamsValidation'][$route->requestInterface];

		//判断是否需要验证		
		if($paramsValidation!=NULL){

		   //取出restful必填字段和验证条件
		   $restfulFieldArr     = array();
		   $restfulConditionArr = array();	
		   for($i=0;$i<count($paramsValidation['RESTFUL']);$i++){
		   	   $tempArr = explode("|",$paramsValidation['RESTFUL'][$i]);
		   	   array_push($restfulFieldArr,trim($tempArr[0]));
		   	   array_push($restfulConditionArr,trim($tempArr[1]));
		   }

		   //取出body必填字段和验证条件
		   $bodyFieldArr     = array();
		   $bodyConditionArr = array();	

		   for($i=0;$i<count($paramsValidation['BODY']);$i++){
		   	   $tempArr = explode("|",$paramsValidation['BODY'][$i]);
		   	   array_push($bodyFieldArr,trim($tempArr[0]));
		   	   array_push($bodyConditionArr,trim($tempArr[1]));
		   }

		 
		   $restfulValidation = $restfulFieldArr;
		   $bodyValidation    = $bodyFieldArr;
           
           //验证RESTFUL必填参数是否存在
           if($restfulValidation!=NULL){

           	  //取出当前传递的参数	
           	  $restfulKeyArr = $route->restfulParams==NULL?[]:array_keys($route->restfulParams);

           	  //获取未传递的restful参数
           	  $noRestFulParams = array_diff($restfulValidation,$restfulKeyArr);

           	  //如果有未填写的项，返回错误提示
			  if(count($noRestFulParams)>0){

                 //验证结果提示
			  	 $validationRestFulMessage = implode(",",$noRestFulParams);

			  	 //返回验证结果
			  	 $response->responseData(false,$GLOBALS['errorMsg']['restfulParamsError']['message'].":".$validationRestFulMessage);
			  
			  }

           }

           //restful必填参数进行格式验证
           for($i=0;$i<count($restfulFieldArr);$i++){

           		//取出正则验证规则
           		$pattern = $GLOBALS['regexpSettings'][$restfulConditionArr[$i]];
			    $restfulParamsValue = $route->restfulParams[$restfulFieldArr[$i]];
			
           		if($pattern){
           		    if($pattern == 'NOEMPTY'){
           		        if( empty($restfulParamsValue) )
           		        {
           		            $response->responseData(false,"参数:".$restfulFieldArr[$i]."不能为空");
           		        }
           		    }else{
           		        if(!preg_match($pattern, $restfulParamsValue)){
           		            $response->responseData(false,"参数:".$restfulFieldArr[$i]."格式不正确");
           		        }
           		    }
           		}else{
           		    $response->responseData(false,$GLOBALS['errorMsg']['noRegexpError']);
           		}
		   }	
		   
		//    var_dump($bodyFieldArr);
		//    var_dump($bodyConditionArr);


		   
		   //验证BODY必填参数是否存在
           if($bodyValidation!=NULL){


           	  //取出当前存在的body参数
				 $bodyKeyArr = $route->bodyParams==NULL?[]:array_keys($route->bodyParams);
				 
           	  //获取未传递的body参数
				 $noBodyParams = array_diff($bodyValidation,$bodyKeyArr);
				 
				 
			  if(count($noBodyParams)>0){

                 //验证结果提示
			  	 $validationBodyMessage = implode(",",$noBodyParams);

			  	 //返回验证结果
			  	 $response->responseData(false,$GLOBALS['errorMsg']['bodyParamsError']['message'].":".$validationBodyMessage);
			  
			  }
		   }
		   
		   

           //body必填参数进行格式验证
           for($i=0;$i<count($bodyFieldArr);$i++){

           		//取出正则验证规则
           		$pattern 			= $GLOBALS['regexpSettings'][$bodyConditionArr[$i]];
				$bodyParamsValue    = $route->bodyParams[$bodyFieldArr[$i]];
				   
			
           		if($pattern){
           			if($pattern == 'NOEMPTY'){
           			    if( empty($bodyParamsValue)){
           			        $response->responseData(false,"参数:".$bodyFieldArr[$i]."不能为空");
           			    }
           			}else{
				
           				if(!preg_match($pattern, $bodyParamsValue)){
							$response->responseData(false,"参数:".$bodyFieldArr[$i]."格式不正确");
						}
           			}
           		}else{
           			$response->responseData(false,$GLOBALS['errorMsg']['noRegexpError']);
           		}
           }

		}
	} 
}
