<?php
namespace coreLib\CacheClass;

/**
 * 缓存操作类
 **/
class Cache{

	public $cacheData;  //缓存数据
	public $route; 		//路由对象
	public $db;			//数据库操作对象
	
	/*
	 * 构造函数
	 * @params {$route} 路由操作对象
	 * @params {$db}	数据库操作对象	
	 * @return 没有返回值
	 */
	function __construct($route,$db){

		$this->route = $route;
		$this->db 	 = $db;

	}

	/*
	 * 生成缓存文件数据
	 * @params {$data} 需要被缓存的数据
	 * @return 没有返回值
	 */
	function createFileCacheData($data){


		//对数据进行文件缓存
		if($data){

			//获取生成的最终缓存文件路径
			$cachePath = $this->createCachaPath();

			//将需要缓存的文件转化为json
			$cacheJsonData = json_encode($data);

			//将数据写入文件
			file_put_contents($cachePath, $cacheJsonData);

		}

	} 

	//创建内存缓存
	function createMemoryCacheData($data){

		//对数据进行内存缓存
		if($data){

			//切换到第15个数据库，用来保存数据缓存
			$this->db->redisDB->selectDb(14);

			//生成内存缓存的key名
			$cacheKey = $this->createMemoryKeyName();

			//将需要缓存的文件转化为json
			$cacheJsonData = json_encode($data);

			//将缓存写入redis
			$this->db->redisDB->setData($cacheKey,$cacheJsonData);

			//查看缓存过期时间
			$cacheValidity  = $GLOBALS['settings']['cache']['cacheValidity'];

			//设置有效时间
			$this->db->redisDB->setExpire($cacheKey,$cacheValidity);

		}
	}

	/*
	 * 获取缓存数据
	 * @return 没有返回值
	 */
	public function getCacheData(){

		//判断该接口是否需要进行缓存读取
		if($this->route->requestMethod != 'GET'){
		   return false;	
		}

		//获取请求接口的缓存文件路径
		$cacheFilePath = $this->createCachaPath();

		//获取内存缓存key名
		$cacheMemoryKeyName = $this->createMemoryKeyName();

		//切换到第15个数据库，用来保存数据缓存
		$this->db->redisDB->selectDb(14);

		//首先判断是否有内存缓存
		if($data = $this->db->redisDB->getData($cacheMemoryKeyName)){

			//取出缓存数据
			$this->cacheData = $data;

			return $this->cacheData;
		}

		//判断文件是否存在
		if(file_exists($cacheFilePath)){

			//查看缓存过期时间
			$cacheValidity  = $GLOBALS['settings']['cache']['cacheValidity'];

			//获取文件创建时间
			$fileCreateTime = filectime($cacheFilePath);

			//判断是否过期
			if(($fileCreateTime + $cacheValidity) < time()){
				return false;
			} 	

			//取出缓存数据
			$this->cacheData = file_get_contents($cacheFilePath);
			
			return $this->cacheData;

		}

	}

	/*
	 * 生成接口缓存文件路径
	 * @return 缓存文件保存的完整路径
	 */
	public function createCachaPath(){

		//获取缓存数据存放目录
		$cacheRootPath = $GLOBALS['settings']['cache']['cacheFileDir'];

		//生成需要保存的文件路径
		$cacheFilePath = $this->route->requestUri;
		$cacheFilePath = str_replace("/","_",$this->route->requestUri);
			
		//最终缓存文件名称
		$rsCacheFile   = $cacheRootPath."c".$cacheFilePath.".json";

		return $rsCacheFile;

	}

	/*
	 * 生成内存缓存的key名
	 * @return 内存缓存的完整key名
	 */
	public function createMemoryKeyName(){

		//生成需要保存的文件路径
		$cacheKeyName = str_replace("/","_",$this->route->requestUri);

		//最终缓存文件名称
		$rsCacheKeyName   = "c".$cacheKeyName;

		return $rsCacheKeyName;

	}

}
?>