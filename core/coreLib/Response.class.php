<?php
namespace coreLib\ResponseClass;
use coreLib\ErrorClass\Error;

/**
 * 结果返回处理类
 * @desc 向请求端返回结果数据，并在结果返回时进行相关的缓存处理
 **/

class Response{

	/*
	 * 返回json数据类型的结果集
	 * @params {$isSuccess} true 成功后的结果，false 失败后的结果
	 * @params {$data}	返回的数据集，如果为NULL，可能为之需要返回一个成功的标志，没有需要返回的结果集
	 * @params {$cache}	缓存处理对象，如果未传递说明不需要进行缓存
	 * @return 没有返回值
	 */
	function responseData($isSuccess,$data=NULL,$cache=NULL){

		$newData = $data;

		//判断是成功返回还是失败返回
		if($isSuccess){

			//判断是否存在需要返回的数据内容
			if($newData==NULL){

				//判断返回的结果集是否为数组
				if(gettype($newData) == 'array'){
					$rsData = array("success"=>true,"result"=>array());
				}else{
					$rsData = array("success"=>true);
				}

			}else{

				//判断是否需要对数据进行缓存
				if($cache){
				   $this->dataCache($cache,$newData);
				}

				
				$rsData = array("success"=>true,"result"=>$newData);
			}


		}else{

			//判断是否有CODE，如果没有CODE说明是业务错误，业务错误给的CODE暂定为88888
			if(!isset($data['code'])){

				//保存消息信息
				$message = $newData;

				//构建新的返回消息
				$newData = array("code"=>"88888","message"=>$message);

			}

			$rsData = array("success"=>false,"result" =>$newData);
		}

		//判断是否需要保存接口运行信息
		if($GLOBALS['settings']['showInterfaceInfo']){

			//获取程序结束执行的时间
			$etime = microtime(true); 

			//计算运行时间 
			$execTime = ($etime - $GLOBALS['stime']);

			//在结果中添加运行信息
			$rsData['interfaceInfo'] = array(
				'runTime' => round($execTime,2).' 秒'
			);

		}

		//返回最终结果，JSON_UNESCAPED_SLASHES : Json不要编码Unicode
		echo json_encode($rsData,JSON_UNESCAPED_SLASHES);
		exit();
	}


	/*
	 * 缓存处理
	 * @params {$cacheObj}  缓存处理对象
	 * @params {$cacheData} 需要被缓存的数据
	 * @return 没有返回值
	 */
	function dataCache($cacheObj,$cacheData){

		//获取缓存配置信息
		$isCache = $GLOBALS['settings']['cache'];

		//判断是否需要进行缓存处理
		if($isCache){

			//查看是否进行文件缓存
			if($isCache['isFileCache']){

				//调用文件缓存方法
				$cacheObj->createFileCacheData($cacheData);

			}

			//查看是否进行内存缓存
			if($isCache['isMemoryCache']){

				//调用内存缓存方法
				$cacheObj->createMemoryCacheData($cacheData);

			}
		}
	}
}