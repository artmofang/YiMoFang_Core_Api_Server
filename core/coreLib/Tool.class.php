<?php
namespace coreLib\ToolClass;

use coreLib\db\RedisClass;
use coreLib\ErrorClass\Error;

//导入图片处理类库
require_once("tool/Image.class.php");

/**
 * 类库作用 :通用工具
 * 开发人员 :李蔚轩  
 **/
class Tool{

	private $token;
	private $tokenValidTime;
	private $db;

	public function __construct($db,$token,$tokenValidTime){

		$this->db 			  = $db;
		$this->token 		  = $token;
		$this->tokenValidTime = $tokenValidTime;

	}

	//更新token过期时间,一般在POST请求成功之后操作
	public function UPDATE_TOKEN(){

		//判断是否存在token
		if($this->token){

			//释放第一个存储过程的连接资源
			$this->db->mysqlDB->clearStoredResults();

			//构建更新token所属存储过程参数
			$proceArr = [
				$this->token,
				$this->tokenValidTime
			];

			//构建存储过程参数
			$this->db->mysqlDB->setProceduerParams($proceArr,"R");

			//执行更新存储过程
			$this->db->mysqlDB->execProceduer("EC_UPDATE_USER_TOKEN",$proceArr);

		}
	}

	public function FILE_MOVE($tempImageName){

		//判断是否有传入临时文件名，如果为真，开始临时目录移入操作
		if($tempImageName){

		  //获取文件目录存放文件
		  $rootUploadDir = $GLOBALS['settings']['fileUpload']['uploadPath'];

		  //获取当前的年月日
		  $nowYMD = date("Y/m/d"); 	

		  //拼接完整上传目录，并判端是否存在，不存在则创建
		  $newUploadDir = $rootUploadDir."/".$nowYMD."/";

		  //判断目录是否存在，不存在就创建
		  if(!file_exists($newUploadDir)){
			 mkdir($newUploadDir,0777,true);
		  }

		  //获取文件名
		  $fileName = basename($tempImageName);

		  //拼接最终文件路径名
		  $newFilePath = $newUploadDir.$fileName;

		  //将临时文件移动至新文件
		  $moveResult = rename($tempImageName,$newFilePath);

		  if(!$moveResult){
   			  Error::Result("BUSINESS_ERROR","文件信息保存失败");
		  }
		}
	}


	public function IMAGE_MOVE($tempImageName){

		//判断是否有传入临时文件名，如果为真，开始临时目录移入操作
		if($tempImageName){

		  //获取文件目录存放文件
		  $rootUploadDir = $GLOBALS['settings']['fileUpload']['uploadPath'];

		  //获取当前的年月日
		  $nowYMD = date("Y/m/d"); 	

		  //拼接完整上传目录，并判端是否存在，不存在则创建
		  $newUploadDir = $rootUploadDir."/".$nowYMD;

		  //判断目录是否存在，不存在就创建
		  if(!file_exists($newUploadDir)){
			 mkdir($newUploadDir,0777,true);
		  }

		  //获取文件名
		  $fileName = basename($tempImageName);

		  //拼接最终文件路径名
		  $newFilePath = $newUploadDir."/original_".$fileName;

		  //将临时文件移动至新文件
		  $moveResult = rename($tempImageName,$newFilePath);

		  if(!$moveResult){
   			  Error::Result("BUSINESS_ERROR","文件信息保存失败");
		  }

		  //拷贝一份图片，并命名为正常图(临时对应，以后会做专门的压缩大小后的正常图片)
		  @copy($newFilePath,$newUploadDir."/normal_".$fileName);

		  //查看是否需要生成缩略图
    	  $isThumb =  $GLOBALS['settings']['fileUpload']['isCreateThumb'];

    	  if($isThumb){

    		//生成缩略图
    		$this->createThumb($newFilePath,$newUploadDir,$fileName);

    	  }

		}

	}


	//生成缩略图
	public function createThumb($srcImage,$targetPath,$fileName){

		//实例化缩略图生成类
		$thumbObj = new \ThumbHandler();
		$thumbW = $GLOBALS['settings']['fileUpload']['thumbScaleW'];
		$thumbH = $GLOBALS['settings']['fileUpload']['thumbScaleH'];

		//设置原始图路径
		$thumbObj->setSrcImg($srcImage);

		//生成的缩略图完整路径
		$thumbPath = $targetPath."/thumb_".$fileName;

		//设置生成后的缩略图路径
		$thumbObj->setDstImg($thumbPath);

		//获取生成结果
		$thumbObj->createImg($thumbW,$thumbH);

	}
	
	/**
	 * 网站公用配置
	 * @param config_name 配置key
	 * @author comander
	 * @date 2016.11.18
	 */
	public function getWebSiteConfig( $keys ){
	    if( !$keys )
	    {
	        Error::Result("BUSINESS_ERROR", '参数传入错误');
	    }
	    
		$redis=$this->db->redisDB;
		$db=$this->db->mysqlDB;
		
		//判断redis 内是否有对应配置
		if($redis->keyExists('WEB_CONFIG'))
		{
		    //存在时直接调取
			$list=json_decode($redis->getData('WEB_CONFIG'),true);
		}
		else
		{
			$field='PARAM_KEY,PARAM_NAME,PARAM_VALUE';
			$resData = $db->field( $field )->selectTable( 'cl_biz_parameters' );
			
			//数组转型
			$list=[];
			foreach($resData as $val)
			{
				$list[$val['PARAM_KEY']]=$val;
			}
			
			//写入Redis
			$list && $redis->setData('WEB_CONFIG',json_encode($list));
		}
		//解析结果
		$result = [];
		foreach( $keys as $v )
		{
		    if( !empty( $list[$v] ) )
		    {
		        $result[$v] = $list[$v];
		    }
		    elseif( !empty( $v ) ) 
		    {
		        $result[$v] = '配置项 '.$v.' 不存在';
		    }
		}
		
		if( $result )
		{
            //返回配置
            return $result;
		}
		else 
		{
		    Error::Result("BUSINESS_ERROR", $key.' 配置项获取失败');
		}
	}




}