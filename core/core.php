<?php
use coreLib\RouteClass\Route;
use coreLib\AuthClass\Auth;
use coreLib\PermissonClass\Permisson;
use coreLib\RequestClass\Request;
use coreLib\DbClass\Db;
use coreLib\CacheClass\Cache;

//导入主配置文件
require_once("./config/setting.php");

//判断是否需要保存接口运行信息
if($GLOBALS['settings']['showInterfaceInfo']){

	//获取程序开始执行的时间,单位：毫秒	
	$GLOBALS['stime'] = microtime(true); 

}

// if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1'){
//     $GLOBALS['settings']['database']['Mysql']['dbname'] = "art_magic_cube_new_database";
// }else{
//     $GLOBALS['settings']['database']['Mysql']['dbname'] = "art_magic_cube_product";
// }

// echo $GLOBALS['settings']['database']['Mysql']['dbname'];
// echo "<hr/>";


//设置全局错误警告级别
$GLOBALS['settings']['debug'] == true ? error_reporting(E_ALL & ~E_NOTICE) : error_reporting(0);

//开启跨域设置
if($GLOBALS['settings']['isAccessControlAllowOrigin']){
   header('Access-Control-Allow-Origin: *');
   header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
   header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');
}

//导入公共函数库
require_once("./common/commonLib.php");

//导入错误处理类
require_once("./core/coreLib/Error.class.php");

//导入验权类
require_once("./core/coreLib/Auth.class.php");

//导入路由类
require_once("./core/coreLib/Route.class.php");

//导入权限处理类
require_once("./core/coreLib/Permission.class.php");

//构建请求处理类
require_once("./core/coreLib/Request.class.php");

//构建响应处理类
require_once("./core/coreLib/Response.class.php");

//缓存类
require_once("./core/coreLib/Cache.class.php");

//正则验证类
require_once("./tool/regexp.class.php");

//导入数据库操作类
require_once("./core/coreLib/Db.class.php");

//通用工具类
require_once("./core/coreLib/Tool.class.php");

//实例化数据库操作对象
$db = new Db($GLOBALS['settings']['database']);

//实例化路由操作对象
$routeObj = new Route($db);

//核心层访问验权操作
$auth = new Auth($db,$routeObj);

//构建缓存对象
$cache = new Cache($routeObj,$db);

//权限分析,获取该用户所拥有的权限信息...
$permissonObj = new Permisson($routeObj->visitorId);

//构建请求
$requestObj = new Request($routeObj,$permissonObj,$db,$cache);




