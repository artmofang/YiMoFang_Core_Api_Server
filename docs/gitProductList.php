<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/gitProductList.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons-halflings.css" />
		<link rel="stylesheet" type="text/css" href="css/pnotify.custom.min.css" />
		<link rel="stylesheet" href="css/ui-dialog.css">


		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
		<script type="text/javascript" src="js/dialog-plus-min.js"></script>
		<script type="text/javascript" src="js/pnotify.custom.min.js"></script>
		<script type="text/javascript" src="js/jsviews.js"></script>
		<script type="text/javascript" src="js/utils.js"></script>

	</head>
	<body>
		
		<div class="gitProductListBox">

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>安卓主app项目</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/andorid_app/</font></b>

			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>IOS主app项目</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/ios_app/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>身份验权服务器</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/authenticator_server.git</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>后台管理系统中间层服务器</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/ec_manager_web_middle_server.git/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>核心层接口服务器</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/ec.git/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>后台管理系统前端</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/ec_manager_web_server.git/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>商城前端中间层服务器</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/ec_web_server.git/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>中间层服务器通用库</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/middle_server_lib.git/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>移动App中间层服务器</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/app_middle_server.git/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>商城前端</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/sanbafule_web.git/</font></b>
			</div>

			<div class="gitItemBox">
				<span class="glyphicons glyphicons-globe-af"></span>
				<b>会员中心</b>
				<b style='margin-left:15px;'><font color=green>地址：</font><font color=red>git@120.27.4.237:/home/git/memberCenter.git/</font></b>
			</div>

				
			


		</div>

	</body>
<script>
	
</script>
</html>