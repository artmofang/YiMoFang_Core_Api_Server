<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
	</head>
	<body>	
		<div class="mainBox">

		</div>
	</body>
	<script>
		var docsArr = [
			{
				name:'核心层',
				desc:'',
				link:'http://localhost/YiMoFang_Core_Api_Server/docs/index.php'
			},
			{
				name:'微信端-中间层',
				desc:'',
				link:'http://localhost/YiMoFang_Core_Api_Server/docs/index.php?docs=art_weixin_api_docs'
			},
			{
				name:'App用户端-中间层',
				desc:'',
				link:'http://localhost/YiMoFang_Core_Api_Server/docs/index.php?docs=art_app_api_docs'
			},
			{
				name:'后台管理系统-中间层',
				desc:'',
				link:'http://localhost/YiMoFang_Core_Api_Server/docs/index.php?docs=art_manager_api_docs'
			}
		];

		//获取屏宽
		var screenWidth = $(window).width();

		//每行显示数量
		var rowCount = 4;

		//间隔
		var padding = 40;

		//每个div的宽
		var divWidth = screenWidth / rowCount;

		//创建div块
		for(var i=0;i<docsArr.length;i++){
			var oDiv = $("<div class='itemDiv'><a href='"+docsArr[i].link+"'><h1>"+docsArr[i].name+"</h1><div class='itemDivMain'>"+docsArr[i].desc+"</div></a></div>");
			oDiv.width(divWidth-padding*2);
			oDiv.css("padding",padding);
			$(".mainBox").append(oDiv);

			var sDiv = $("<div class='itemsDiv'></div>");
			sDiv.css("width","100%");
			sDiv.css("height","100%");
			oDiv.append(sDiv);
		}
	</script>
</html>


