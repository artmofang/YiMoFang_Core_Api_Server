<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/procedure.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons-halflings.css" />
		<link rel="stylesheet" type="text/css" href="css/pnotify.custom.min.css" />
		<link rel="stylesheet" href="css/ui-dialog.css">


		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
		<script type="text/javascript" src="js/dialog-plus-min.js"></script>
		<script type="text/javascript" src="js/pnotify.custom.min.js"></script>
		<script type="text/javascript" src="js/jsviews.js"></script>
		<script type="text/javascript" src="js/utils.js"></script>

	</head>
	<body>
	<!-- 加载中遮罩 -->
    <div class="maskLoadingBox" id="maskLoadingBox">
        <img src="./image/loading.gif" />
        <p>界面初始化中...</p>
    </div>
    <div class="databaseTableBox">
		<table class="myTable">
			<thead>
		      	<tr>
		      		<td width="5%">编号</td>
		      		<td>存储过程名称</td>
		      		<td>注释</td>
		      		<td>所属数据库</td>
		      		<td>最后修改时间</td>
		      		<td>操作</td>
		      	</tr>
		    </thead>
		    <tbody id="tbodybox">
		    	<script id="procedureListTemp" type="text/x-jsrender">
		    	{^{for result}}
		      	<tr name="{{:Name}}">
			      	<td>{{:#index + 1}}</td>
			      	<td>{{:Name}}</td>
			      	<td>{{:Comment}}</td>
			      	<td>{{:Db}}</td>
			      	<td>{{:Modified}}</td>
			      	<td class='actionTd'>
			      		<span class="glyphicons glyphicons-list-alt procedureDetail"></span>
			      	</td>
		      	</tr>
		      	{{/for}}
		      	</script>
		    </tbody>
		</table>
	</div>	


	<script id="procedureDetailTemp" type="text/x-jsrender">
		<pre id="proBox">{{:result}}</pre>
	</script>


</body>
<script>
	$(function(){

		//存储过程数据
		var procedureData = [];

		//获取列表模版
      	var procedureListTemp = $.templates("#procedureListTemp");

      	//获取列表模版
      	var procedureDetailTemp = $.templates("#procedureDetailTemp");

		//获取存储过程信息
		getDatabaseTable();

		//获取存储过程信息
		function getDatabaseTable(){

			//加载根菜单
	        $.getJSON("./handler/getProcedure.php",function(data){

	        	console.log(data);

	        	
	        	//清除进度条
            	$("#maskLoadingBox").hide();
	            
	            //判断是否返回错误
	            if(!data.success){
	               notifyShow('error', data.message);
	               return;
	            }

	            procedureData = {result : data.result};

	            //为模板绑定数据
	            procedureListTemp.link("#tbodybox",procedureData);

            	
	            
	        });

		}


		$(document).on("click",".procedureDetail",function(){

			//获取存储过程名称
			var proName = $(this).parent().parent().attr("name");

			var d = dialog({
			    	title  : '存储过程名 : ' + proName,
			    	onshow: function () {

			    		var that = this;

			    		//获取表结构详细
				        $.getJSON("./handler/getProcedureDetail.php",{"proName":proName},function(data){
				        	

				        	//判断是否返回错误
				            if(!data.success){
				               notifyShow('error', data.message);
				               return;
				            }

				            //获取出建表语句
				            var createPro = data.result[0]['Create Procedure'];

							var proHTML = procedureDetailTemp.render({result : createPro});

							that.content(proHTML);

						
				        });
			    		
			    	}	
				});

			d.showModal();

		});
		


	});
</script>
</html>