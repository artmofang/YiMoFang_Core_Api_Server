<?php
session_start();

echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'; 

//获取需要查看的文档类型
$docsName = empty($_GET['docs']) ? "art_core_api_docs" : $_GET['docs'];
switch($docsName){
	case 'art_weixin_api_docs':
		$_SESSION['docsName'] = "art_weixin_api_docs";
		$_SESSION['docsNameTitle'] = "微信端-中间层";
	break;
	case 'art_app_api_docs';
		$_SESSION['docsName'] = "art_app_api_docs";
		$_SESSION['docsNameTitle'] = "App用户端-中间层";
	break;
	case 'art_manager_api_docs':
		$_SESSION['docsName'] = "art_manager_api_docs";
		$_SESSION['docsNameTitle'] = "后台管理系统-中间层";
	break;
	default:
		$_SESSION['docsName'] = "art_core_api_docs";
		$_SESSION['docsNameTitle'] = "核心层";
	break;
}
?>