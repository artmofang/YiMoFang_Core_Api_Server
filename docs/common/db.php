<?php
require_once("../config/docsConfig.php");

//创建mysql数据库连接
$db = @new mysqli($docsSettings['host'],$docsSettings['username'],$docsSettings['password'],$docsSettings['dbname']);
if(mysqli_connect_error()){
	echo "Mysql_Error:".mysqli_connect_error();
	exit();
}

//设置编码
$db->query("SET NAMES UTF8");