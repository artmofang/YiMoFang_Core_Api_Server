<?php
include_once("./common/session.php");
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/docs.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
		<link rel="stylesheet" type="text/css" href="css/pnotify.custom.min.css" />

		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
		<script type="text/javascript" src="js/jquery.mCustomScrollbar.min.js"></script>
		<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
		<script type="text/javascript" src="js/pnotify.custom.min.js"></script>
		<script type="text/javascript" src="js/jsviews.js"></script>
		<script type="text/javascript" src="js/utils.js"></script>
	</head>
	<body>
		<!-- 加载中遮罩 -->
	    <div class="maskLoadingBox" id="maskLoadingBox">
	        <img src="./image/loading.gif" />
	        <p>界面初始化中...</p>
	    </div>

		<div class="docsBox">
			<div class="docsMenuBox">
				<h1><?php echo $_SESSION['docsNameTitle'];?><a href='http://localhost/API_SERVER/docs/managerInterface.php'><span class="glyphicons glyphicons-cogwheels" style="position:relative;top:7px;color:#fff;"></span></a></h1>

				<div class="searchBox">
					<input type="text" placeholder="接口名称/接口描述" id="searchInterface" />
				</div>
				<ul class="docsMenuUl">
					<script  id="menuTemp" type="text/x-jsrender">
						{^{for rootMenu}}
							<li class="menuLi">
								<a href="#">{{:name}}</a>
								<ul class="sonUl">
									{{for son}}
									<li id="{{:id}}">
										<p>{{:name}}</p>
										<p class="menuDesc">{{:path}}</p>
									</li>
									{{/for}}
								</ul>
							</li>
						{{/for}}
					</script>
				</ul>
			</div>
			<div class="docsContentBox">

				<div class="docsContentMain">
					<script  id="interfaceDetailTemp" type="text/x-jsrender">
							<h1 class="docsTitle">
								<span class="glyphicons glyphicons-bookmark"></span>
								{{:name}}
							</h1>	
							<div class="docsContentKey">
								<h2><span class="glyphicons glyphicons-arrow-down"></span>接口地址</h2>
								<div class="docsContentValue">
									<a href="#">{{:path}}</a>
								</div>
							</div>
							<div class="docsContentKey">
								<h2><span class="glyphicons glyphicons-arrow-down"></span>接口说明</h2>
								<div class="docsContentValue">
									<span>{{:description}}</span>
								</div>
							</div>
							<div class="docsContentKey">
								<h2><span class="glyphicons glyphicons-arrow-down"></span>请求类型</h2>
								<div class="docsContentValue">
									<span style="color:orange">{{if method == 'ALL'}} GET 或 POST {{else}} {{:method}} {{/if}}</span>
								</div>
							</div>
							<div class="docsContentKey">
								<h2><span class="glyphicons glyphicons-arrow-down"></span>所需权限</h2>
								<div class="docsContentValue">
									<span>无</span>
								</div>
							</div>
							<div class="docsContentKey">
								<h2><span class="glyphicons glyphicons-arrow-down"></span>RESTFul参数</h2>
								<div class="docsContentValue">
									<ul class="restfulUl">
										{{for params}}
											{{if type == "RESTFUL"}}
												<li>
													{{if is_must == "YES"}}
															<em class="must">(* 必须)</em>
													{{else}}
															<em class="noMust">(- 可选)</em>
													{{/if}}	
													<em class="key">{{:params_name}}</em>
													<em class="type">{{:params_type}}</em>
													<em class="desc">{{:params_description}}</em>
												</li>
											{{/if}}
										{{/for}}
									</ul>
								</div>
							</div>

							<div class="docsContentKey">
								<h2><span class="glyphicons glyphicons-arrow-down"></span>Body参数</h2>
								<div class="docsContentValue">
									<ul class="bodyUl">
										{{for params}}
											{{if type == "BODY"}}
												<li>
													{{if is_must == "YES"}}
															<em class="must">(* 必须)</em>
													{{else}}
															<em class="noMust">(- 可选)</em>
													{{/if}}	
													<em class="key">{{:params_name}}</em>
													<em class="type">{{:params_type}}</em>
													<em class="desc">{{:params_description}}</em>
												</li>
											{{/if}}
										{{/for}}
									</ul>
								</div>
							</div>
							
							<div class="docsContentKey">
								<h2><span class="glyphicons glyphicons-restart"></span>返回结果</h2>
								<div class="docsContentValue">
<pre>
{{:result}}
</pre>
								</div>
							</div>
				</script>
				</div>
				
			
				
				<div class="defaultContent">
					<span class="glyphicons glyphicons-book-open"></span>
					<p>你还未选择需要查看的接口</p>
				</div>
				

				<div class="loadingContent" id="loadingContent">
			        <img src="./image/loading.gif" />
			        <p>接口详细获取中...</p>
			    </div>


			</div>
		</div>
	</body>
	<script>
	$(window).load(function(){
      $(".docsMenuBox").mCustomScrollbar({theme:"rounded"});
    });

	$(function(){

		//上次访问的菜单hashId
		var lastHashId = window.location.hash;

		//获取hash详细
		var hashArr    = getHashValue(lastHashId);

		//根菜单数据
      	var menuData = {};

      	//根菜单模板
      	var menuTemp = $.templates("#menuTemp");

      	//接口详细数据
      	var interfaceDetailData = {};

      	//接口详细模板
      	var interfaceDetailTemp = $.templates("#interfaceDetailTemp");

      	//获取菜单信息
      	getMenuData();

      	//左侧菜单展开效果
      	leftMenuOpen();

      	//判断是否有上次访问的菜单记录
      	if(lastHashId){
      		getInterfaceDetailData({"menuId":hashArr[0]});
      	}

      	//鼠标移入
      	$(document).on("mouseover",".sonUl li",function(){

      		var scorllWidth = $(this)[0].scrollWidth;
      		var offsetWidth = $(this)[0].offsetWidth;  

      		//判断是否有文字超出
      		if(scorllWidth > offsetWidth){

      			$(".docsMenuBox").width(scorllWidth+15);
				$(".docsContentBox").stop(false,false).animate({"left":scorllWidth+15},200);

      		}

      		return false;

      	});

    	//鼠标移出
      	$(document).on("mouseleave",".docsMenuBox",function(){



      			//$(".docsMenuBox").css("width",240);
    
				$(".docsContentBox").stop(false,false).animate({"left":240},200);
				$(".docsMenuBox").stop(false,false).animate({"width":240},200);
		
				return false;

      	});


      	//打开接口详细
      	$(document).on("click",".sonUl li",function(){



      		//选中颜色
      		$(".sonUl li p").removeClass("selectedSonLi");
      		$(this).find("p").addClass("selectedSonLi");

      		//获取父元素索引
      		var _Findex = $(".menuLi").index($(this).parent().parent());

      		//子菜单索引
      		var _Sindex = $(this).parent().find("li").index(this);

      		//开启加载进度
      		startLoading();
      		
      		//获取接口ID
      		var menuid = $(this).attr("id");

      		//请求参数
      		var params = { "menuId" : menuid };

      		//记录hash
      		window.location.hash = menuid+"_"+_Findex+"_"+_Sindex;

      		//获取接口详细
      		getInterfaceDetailData(params);

      	});


      	var searchTimer = null;
      	$('#searchInterface').bind('keyup',function(event){  

      		var searchValue = $(this).val();

    			if(trim(searchValue).length <= 0){
    				clearTimeout(searchTimer);
				    $('.menuLi').show();
    				return;
      			}

	   		searchTimer = setTimeout(function(){
				$('.menuLi').hide();	
	   			$('.menuLi').filter(":contains("+searchValue+")").show();
	   		},200);

		});  

      	



      	/*
      	 * 获取菜单信息
      	 */
      	function getMenuData(){
      		//加载根菜单
	        $.getJSON("./handler/getMenu.php",function(data){
	            
	            //判断是否返回错误
	            if(!data.success){
	               notifyShow('error', data.message);
	               return;
	            }

	            //重构菜单数据
	   			menuData.rootMenu = [];
	            for(var i = 0;i<data.result.length;i++){

	            	if(data.result[i].fid == 0){
	            	   menuData.rootMenu.push(data.result[i]);
	            	}

	            }

	            for(var i =0;i<menuData.rootMenu.length;i++){
	            	menuData.rootMenu[i].son = [];
	            	for(var j=0;j<data.result.length;j++){
	            		if(menuData.rootMenu[i].id == data.result[j].fid){
	            		   menuData.rootMenu[i].son.push(data.result[j]);
	            		}
	            	}
	            }

	            //为模板绑定数据
	            menuTemp.link(".docsMenuUl",menuData);

	            //清除进度条
            	$("#maskLoadingBox").hide();

            	//根据上一次选择默认展开
            	if(hashArr){
            		menuDefaultOpen(hashArr);
            	}
            	
	            
	        });
      	}



      	//获取接口详细信息
		function getInterfaceDetailData(params){

				//获取接口详情
	      		$.getJSON("./handler/getInterfaceDetail.php",params,function(data){
	      		
	      			//判断是否返回错误
		            if(!data.success){
		               notifyShow('error', data.message);
		               return;
		            }

		            interfaceDetailData = data.result;
		            interfaceDetailTemp.link(".docsContentMain",interfaceDetailData)

		            endLoading();
		            $(".defaultContent").hide();

		            if(($(".restfulUl li").length) == 0){
		            	$(".restfulUl").html("<font color=#333>无</font>")
	      			}

					if(($(".bodyUl li").length) == 0){
						$(".bodyUl").html("<font color=#333>无</font>")
					}
	      		});

		}
		
	});

	/*
	 * 左侧菜单展开效果
	 */
	function leftMenuOpen(){
		$(document).on("click",".docsMenuUl .menuLi a",function(){

			 if($(this).next().is(":hidden")){
			 	$(this).next().slideDown(200);
			 }else{
			 	$(this).next().slideUp(200);
			 }
		});
	}; 

	//开始加载
	function startLoading(){

		//获取当前滚动高度
		var scrollTop = $(".docsContentBox").scrollTop();

		//设置top值
		$("#loadingContent").css("top",scrollTop);

      	//打开加载进度
      	$("#loadingContent").show();

      	//禁止滚动条滚动
      	$(".docsContentBox").css("overflow","hidden");
	}

	//结束加载
	function endLoading(){

		//关闭加载进度
      	$("#loadingContent").hide();

      	//打开滚动条滚动
      	$(".docsContentBox").css("overflow","auto");


	}

	//URL_hash解析函数,返回数组
	//[0] 查询详细所需id
	//[1] 根菜单索引
	//[2] 子菜单索引
	function getHashValue(hashValue){

		var newStr  = hashValue.substr(1,hashValue.length);
		var hashArr = newStr.split("_");
		
		return hashArr;
	}

	//菜单默认展开
	function menuDefaultOpen(hashV){
		$(".menuLi:eq("+hashV[1]+")").find(".sonUl").slideDown(200);
		$(".menuLi:eq("+hashV[1]+")").find(".sonUl li:eq("+hashV[2]+")").find("p").addClass("selectedSonLi");
	}	

	//字符串去除两边空格,原型方法扩展
	function trim(str){
		return str.replace(/(^\s*)|(\s*$)/g, "");
	}

	



	</script>
</html>