<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/databaseTable.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons-halflings.css" />
		<link rel="stylesheet" type="text/css" href="css/pnotify.custom.min.css" />
		<link rel="stylesheet" href="css/ui-dialog.css">


		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
		<script type="text/javascript" src="js/dialog-plus-min.js"></script>
		<script type="text/javascript" src="js/pnotify.custom.min.js"></script>
		<script type="text/javascript" src="js/jsviews.js"></script>
		<script type="text/javascript" src="js/utils.js"></script>

	</head>
	<body>
	<!-- 加载中遮罩 -->
    <div class="maskLoadingBox" id="maskLoadingBox">
        <img src="./image/loading.gif" />
        <p>界面初始化中...</p>
    </div>
    <div class="databaseTableBox">
		<table class="myTable">
			<thead>
		      	<tr>
		      		<td width="5%">编号</td>
		      		<td>表名</td>
		      		<td>注释</td>
		      		<td>存储引擎</td>
		      		<td>数据条数(约)</td>
		      		<td>默认字符集</td>
		      		<td>操作</td>
		      	</tr>
		    </thead>
		    <tbody id="tbodybox">
		    	<script id="databaseTableListTemp" type="text/x-jsrender">
		    	{^{for result}}
		      	<tr name="{{:Name}}">
			      	<td>{{:#index + 1}}</td>
			      	<td>{{:Name}}</td>
			      	<td>{{:Comment}}</td>
			      	<td>{{:Engine}}</td>
			      	<td>{{:Rows}}</td>
			      	<td>{{:Collation}}</td>
			      	<td class='actionTd'>
			      		<span class="glyphicons glyphicons-list-alt tableDetail"></span>
			      	</td>
		      	</tr>
		      	{{/for}}
		      	</script>
		    </tbody>
		</table>
	</div>	

	<script id="tableFieldListTemp" type="text/x-jsrender">
		<table class="myTable">
			<thead>
		      	<tr>
		      		<td>字段名称</td>
		      		<td>字段类型</td>
		      		<td>字段主键</td>
		      		<td>是否为NULL</td>
		      		<td>扩展属性</td>
		      		<td>注释</td>
		      	</tr>
		    </thead>
		    <tbody id="tbodyboxField">
		    	{^{for result}}
		      	<tr>
			      	<td>{{:Field}}</td>
			      	<td>{{:Type}}</td>
			      	<td>{{:Key}}</td>
			      	<td>{{:Null}}</td>
			      	<td>{{:Extra}}</td>
			      	<td>{{:Comment}}</td>
		      	</tr>
		      	{{/for}}
		    </tbody>
		</table>
	</script>
	
</body>
<script>
	$(function(){

		//表数据信息
		var tableData = [];

		//表中的列数据
		var tableFieldData = [];

		//获取数据库的所有表信息
		getDatabaseTable();

		//获取列表模版
      	var databaseTableListTemp = $.templates("#databaseTableListTemp");

      	//获取列表模版
      	var tableFieldListTemp = $.templates("#tableFieldListTemp");


		//获取数据库的所有表信息
		function getDatabaseTable(){
			//加载根菜单
	        $.getJSON("./handler/getDatabaseTable.php",function(data){

	        	console.log(data);

	        	//清除进度条
            	$("#maskLoadingBox").hide();
	            
	            //判断是否返回错误
	            if(!data.success){
	               notifyShow('error', data.message);
	               return;
	            }

	            var dataResult = [];

	            //将视图去除
	            for(var i = 0;i<data.result.length;i++){
	            	if(data.result[i].Comment != 'VIEW'){
	            		dataResult.push(data.result[i]);
	            	}
	            }

	            tableData = {result : dataResult};

	            //为模板绑定数据
	            databaseTableListTemp.link("#tbodybox",tableData);

            	
	            
	        });
		}


		//查看点击单表详细按钮时
		$(document).on("click",".tableDetail",function(){

			//获取当前选择的表名
			var tableName = $(this).parent().parent().attr("name");

			var d = dialog({
					width  : 1000,
			    	title  : '表名 : ' + tableName,
			    	onshow: function () {

			    		var that = this;

			    		//获取表结构详细
				        $.getJSON("./handler/getDatabaseTableDetail.php",{"tableName":tableName},function(data){
				        	

				        	//判断是否返回错误
				            if(!data.success){
				               notifyShow('error', data.message);
				               return;
				            }

				            tableFieldData = data.result;

							var tableHTML = tableFieldListTemp.render({result : tableFieldData});

							that.content(tableHTML);

						
				        });
			    		
			    	}	
				});

			d.showModal();

			



			
		

			
		});


	});
</script>
</html>