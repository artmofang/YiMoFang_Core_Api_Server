<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/addInterface.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="css/glyphicons.css" />
    <link rel="stylesheet" type="text/css" href="css/pnotify.custom.min.css" />

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jsviews.js"></script>
    <script type="text/javascript" src="js/pnotify.custom.min.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
	</head>
	<body>
    <!-- 加载中遮罩 -->
    <div class="maskLoadingBox" id="maskLoadingBox">
        <img src="./image/loading.gif" />
        <p>界面初始化中...</p>
    </div>

    <!-- 主内容区域 -->
		<div class="addMenuBox">
			<table class="table">
  				<tr>
  					<td width="8%" align="right">所属菜单 :</td>
  					<td width="10%">
  						<select class="form-control" id="rootMenuBox">
                <script  id="rootMenuTemp" type="text/x-jsrender">
                  <option value="no">--请选择--</option>
                  <option value="0">根菜单</option>
                  {{for result}}
                    <option value="{{:id}}">{{:name}}</option>
                  {{/for}}
                </script>
  						</select>
  					</td>
  					<td>
  						<input type="text" class="form-control rootMenuItem" placeholder="菜单名称" name="rootMenuInput" disabled="true" />
  					</td>
  					<td><button class="btn btn-default rootMenuItem" disabled="true" id="addRootMenuBtn">添加</button></td>
  				</tr>
  				<tr>
  					<td width="7%" align="right">接口名称 :</td>
  					<td></td>
  					<td width="93%">
  						<input type="text" class="form-control sonMenuItem empty" placeholder="接口名称" name="interfaceName" disabled="true"/>
  					</td>
  					<td></td>
  				</tr>
  				<tr>
  					<td width="7%" align="right">接口地址 :</td>
  					<td></td>
  					<td width="93%">
  						<input type="text" class="form-control sonMenuItem empty" placeholder="接口地址" name="interfaceAddress" disabled="true"/>
  					</td>
  					<td></td>
  				</tr>
  				<tr>
  					<td width="7%" align="right">接口说明 :</td>
  					<td></td>
  					<td width="93%">
  						<input type="text" class="form-control sonMenuItem empty" placeholder="接口说明" name="interfaceDesc" disabled="true"/>
  					</td>
  					<td></td>
  				</tr>
  				<tr>
  					<td width="7%" align="right">请求类型 :</td>
  					<td></td>
  					<td width="93%" style="font-size:12px;">
  						<input type="radio" name="requestMethod" value="POST" disabled="true" class="sonMenuItem">&nbsp;POST&nbsp;&nbsp;&nbsp;&nbsp;
  						<input type="radio" name="requestMethod" value="GET" disabled="true"  class="sonMenuItem">&nbsp;GET&nbsp;&nbsp;&nbsp;&nbsp;
                          <input type="radio" name="requestMethod" value="ALL" disabled="true"  class="sonMenuItem">&nbsp;ALL
  					</td>
  					<td></td>
  				</tr>
  				<tr>
  					<td width="7%" align="right">所需权限 :</td>
  					<td></td>
  					<td width="93%">
  						[暂无配置]
  					</td>
  					<td></td>
  				</tr>
  				<tr>
  					<td width="10%" align="right">RESTFul参数 : </td>
  					<td></td>
  					<td width="93%" id="restfulBox">
  						<input type="text" class="form-control sonMenuItem restfulParamsInput" placeholder="是否必须 ( YES/NO ) | 字段名 | 字段类型 ( String,Number,Array ) | 字段描述" disabled="true"  />
            </td>
  					<td>
              <span class="glyphicons glyphicons-plus-sign restfulAddParamsBtn" style="font-size:20px;cursor:pointer;"></span>
              <span class="glyphicons glyphicons-minus-sign restfulRemoveParamsBtn" style="font-size:20px;cursor:pointer;"></span>
            </td>
  				</tr>
  				<tr>
  					<td width="10%" align="right">body参数 :</td>
  					<td></td>
  					<td width="93%" id="bodyBox">
  						<input type="text" class="form-control sonMenuItem bodyParamsInput" placeholder="是否必须 ( YES/NO ) | 字段名 | 字段类型 ( String,Number,Array ) | 字段描述" disabled="true"  />
  					</td>
  					<td>
              <span class="glyphicons glyphicons-plus-sign bodyAddParamsBtn" style="font-size:20px;cursor:pointer;"></span>
              <span class="glyphicons glyphicons-minus-sign bodyRemoveParamsBtn" style="font-size:20px;cursor:pointer;"></span>
            </td>
  				</tr>
  				<tr>
  					<td width="7%" align="right">返回结果 :</td>
  					<td></td>
  					<td width="93%">
  						<textarea class="form-control sonMenuItem empty" style="height:300px;" placeholder="返回结果" disabled="true" name="resultValue"></textarea>
  					</td>
  					<td></td>
  				</tr>
  				<tr>
  					<td width="7%" align="right"></td>
  					<td></td>
  					<td width="93%">
  						<button class="btn btn-primary sonMenuItem" disabled="true" id="interfaceSubmit" >提交</button>
  					</td>
  					<td></td>
  				</tr>
			</table>
		</div>
	</body>
  <script>
  $(function(){

      //根菜单数据
      var rootMenuData = {};

      //根菜单模板
      var rootMenuTemp = $.templates("#rootMenuTemp");

      //最终提交结果
      var resultData = {};

      //当前选中的请求类型
      var methodValue = "GET";

      //初始化加载根菜单数据
      initRootMenu();

      //根菜单选择处理操作
      rootChangeAction();

      //根菜单创建操作
      addRootMenu();

      //参数添加按钮和减少按钮处理
      addAndRemoveParamsBtn();

      //接口创建按钮
      $("#interfaceSubmit").click(function(){

          //锁住提交按钮
          $(this).attr("disabled","disabled");
          $(this).text("处理中...");

          //验证操作
          if(!submitValidate()){return}

          //组装最终添加参数
          resultData.interfaceName    = $("input[name='interfaceName']").val();
          resultData.interfaceAddress = $("input[name='interfaceAddress']").val();
          resultData.interfaceDesc    = $("input[name='interfaceDesc']").val();
          resultData.methodValue      = methodValue;
          resultData.resultValue      = $("textarea[name='resultValue']").val();

          //参数支持的数据类型
          var paramsType = ["String","Number","Array","File","Float","Int"];

          //restful参数
          resultData.restfulArr = [];
          for(var i=0;i<$(".restfulParamsInput").length;i++){
             var paramsValue = $(".restfulParamsInput:eq("+i+")").val();
             if(paramsValue.length>0){

                  //判断格式是否正确
                  var n = paramsValue.split('|').length-1;
                  if(n!=3){
                     notifyShow('warning', "（第"+(i+1)+"个）restful参数 - 数量不正确");
                     continue;
                  }

                  //拆分参数
                  var paramsArr = paramsValue.split("|");

                  //拆分参数对象
                  var paramsRs = {};

                  //对每个参数进行验证 (参数一，是否必须传递)
                  paramsArr[0] = paramsArr[0].trim();
                  if(paramsArr[0] == ""){
                      notifyShow('warning', "（第"+(i+1)+"个）restful参数 - 请填写该参数是否必须传递");
                      continue;  
                  }else{
                      if(paramsArr[0] != "YES" && paramsArr[0] != "NO"){
                         notifyShow('warning', "（第"+(i+1)+"个）restful参数 - 是否需要进行传递参数填写错误（请填写YES/NO）");
                         continue; 
                      }
                  }
                  paramsRs.isMust = paramsArr[0];

                  //参数二，是否必须传递
                  paramsArr[1] = paramsArr[1].trim();
                  if(paramsArr[1]==""){
                      notifyShow('warning', "（第"+(i+1)+"个）restful参数 - 参数名不能为空");
                      continue;  
                  }
                  paramsRs.paramsName = paramsArr[1];

                  //参数三，字段类型
                  paramsArr[2] = paramsArr[2].trim();
                  if(paramsArr[2]==""){
                      notifyShow('warning', "（第"+(i+1)+"个）restful参数 - 参数类型不能为空");
                      continue;  
                  }else{

                      // if(paramsType.indexOf(paramsArr[2]) == -1){
                      //    notifyShow('warning', "（第"+(i+1)+"个）restful参数 - 参数类型填写错误");
                      //    continue; 
                      // }
                  }
                  paramsRs.paramsType = paramsArr[2];

                  //参数四，参数描述
                  paramsArr[3] = paramsArr[3].trim();
                  if(paramsArr[3]==""){
                      notifyShow('warning', "（第"+(i+1)+"个）restful参数 - 参数描述不能为空");
                      continue;  
                  }
                  paramsRs.paramsDesc = paramsArr[3];
                  resultData.restfulArr.push(paramsRs);

              }
          }

          //body参数
          resultData.bodyArr = [];
          for(var i=0;i<$(".bodyParamsInput").length;i++){
             var paramsValue = $(".bodyParamsInput:eq("+i+")").val();
             if(paramsValue.length>0){

                  //判断格式是否正确
                  var n = paramsValue.split('|').length-1;
                  if(n!=3){
                     notifyShow('warning', "（第"+(i+1)+"个）body参数数量不正确");
                     continue;
                  }

                  //拆分参数
                  var paramsArr = paramsValue.split("|");

                  //拆分参数对象
                  var paramsRs = {};

                  //对每个参数进行验证 (参数一，是否必须传递)
                  paramsArr[0] = paramsArr[0].trim();
                  if(paramsArr[0]==""){
                      notifyShow('warning', "（第"+(i+1)+"个）body参数 - 请填写该参数是否必须传递");
                      continue;  
                  }else{
                      if(paramsArr[0] != "YES" && paramsArr[0] != "NO"){
                         notifyShow('warning', "（第"+(i+1)+"个）body参数 - 是否需要进行传递参数填写错误（请填写YES/NO）");
                         continue; 
                      }
                  }
                  paramsRs.isMust = paramsArr[0];


                  //参数二，是否必须传递
                  paramsArr[1] = paramsArr[1].trim();
                  if(paramsArr[1]==""){
                      notifyShow('warning', "（第"+(i+1)+"个）body参数 - 参数名不能为空");
                      continue;  
                  }
                  paramsRs.paramsName = paramsArr[1];

                  //参数三，字段类型
                  paramsArr[2] = paramsArr[2].trim();
                  if(paramsArr[2]==""){
                      notifyShow('warning', "（第"+(i+1)+"个）body参数 - 参数类型不能为空");
                      continue;  
                  }else{

                      // if(paramsType.indexOf(paramsArr[2]) == -1){
                      //    notifyShow('warning', "（第"+(i+1)+"个）body参数 - 参数类型填写错误");
                      //    continue; 
                      // }
                  }
                  paramsRs.paramsType = paramsArr[2];

                  //参数四，参数描述
                  paramsArr[3] = paramsArr[3].trim();
                  if(paramsArr[3]==""){
                      notifyShow('warning', "（第"+(i+1)+"个）body参数 - 参数描述不能为空");
                      continue;  
                  }
                  paramsRs.paramsDesc = paramsArr[3];
                  resultData.bodyArr.push(paramsRs);

              }
          }

          //添加接口操作
          $.post("./handler/addInterface.php",resultData,function(data){

              console.log(data);

              //恢复按钮
              $("#interfaceSubmit").attr("disabled",false);
              $("#interfaceSubmit").text("保存");
             
              //判断是否返回错误
              if(!data.success){
                 notifyShow('error', data.message);
                 return;
              }

              notifyShow('success', "接口添加成功");

              //初始化界面
              pageInit(); 
              
   
          },'json');

      });


      




      /*
       * 初始化加载根菜单数据
       */
      function initRootMenu(){

        //加载根菜单
        $.getJSON("./handler/getRootMenu.php",function(data){
            
            //判断是否返回错误
            if(!data.success){
               notifyShow('error', data.message);
               return;
            }

            //获取根菜单数据
            rootMenuData = data.result;
            rootMenuTemp.link("#rootMenuBox",{result : rootMenuData});

            //清除进度条
            $("#maskLoadingBox").hide();
        });
      }

      /*
       * 根菜单选择处理操作
       */
      function rootChangeAction(){

          //选择所属菜单事件处理
          $("#rootMenuBox").change(function(){
              var menuValue = $(this).val();

              //选择选项时
              if(menuValue == "no"){

                 //禁止录入所有操作
                 $(".rootMenuItem").prop("disabled",true);
                 $(".sonMenuItem") .prop("disabled",true);
              
              //选择根菜单时候，打开根菜单创建操作   
              }else if(menuValue == "0"){

                 //打开根菜单创建操作
                 $(".rootMenuItem").prop("disabled",false);
                 $(".sonMenuItem").prop("disabled",true);

              //选择其他菜单时   
              }else{

                 //打开二级接口创建操作
                 $(".rootMenuItem").prop("disabled",true);
                 $(".sonMenuItem").prop("disabled",false);

                 //保存当前选中的菜单主ID
                 resultData.menuId = menuValue;

              }
          });
      }

      /*
       * 根菜单创建操作按钮
       */
      function addRootMenu(){

          $("#addRootMenuBtn").click(function(){
              var rootMenuName = $("input[name='rootMenuInput']").val();
              if(rootMenuName.length<=0){
                 notifyShow('warning', "请输入根菜单名称");
                 return;
              }

              $.getJSON("./handler/addRootMenu.php",{name:rootMenuName},function(data){
                  //判断是否返回错误
                  if(!data.success){
                    notifyShow('error', data.message);
                    return;
                  }

                  notifyShow('success',"根菜单添加成功,已刷新下拉选择列表");

                  //重新加载下拉列表
                  initRootMenu();

              });

          });

      }


      /*
       * 提交内容验证
       */
      function submitValidate(){

          //判断输入框是否为空
          var isEmpty = true;
          $(".empty").each(function(){

             var inputValue   = $(this).val();
             var defaultValue = $(this).attr("placeholder") || $(this).val();

             if(inputValue == ""){
                notifyShow('error', defaultValue+"不能为空");
                isEmpty = false;
             } 
          });

          if(!isEmpty){
            return false;
          }


          //判断请求类型是否选择
          var isChecked = false;
          $("input[name='requestMethod']").each(function(){
              if($(this).prop("checked")){
                 methodValue = $(this).val(); 
                 isChecked   = true;
              }
          });

          if(!isChecked){
             notifyShow('error', "至少需要选择一个请求类型");
             return false;
          }

          return true;

      } 

      /*
       * 参数添加按钮和减少按钮处理
       */
      function addAndRemoveParamsBtn(){

          //添加restful参数按钮
          $(".restfulAddParamsBtn").click(function(){
              var oInput = $(".restfulParamsInput:eq(0)").clone(true);
              $("#restfulBox").append(oInput);
          });
          $(".restfulRemoveParamsBtn").click(function(){
              if(($(".restfulParamsInput").length-1)<=0){
                notifyShow("warning", "当前已经是最后一个");
                return;
              }
              $(".restfulParamsInput:last").remove();
          });

          //添加和删除body参数按钮
          $(".bodyAddParamsBtn").click(function(){
              var oInput = $(".bodyParamsInput:eq(0)").clone(true);
              $("#bodyBox").append(oInput);
          });
          $(".bodyRemoveParamsBtn").click(function(){
              if(($(".bodyParamsInput").length-1)<=0){
                notifyShow("warning", "shang当前已经是最后一个");
                return;
              }
              $(".bodyParamsInput:last").remove();
          });
      }

      /*
       * 提交后页面初始化
       */
      function pageInit(){

          $(".sonMenuItem").not("input[name='requestMethod']").val("");

          //将参数输入归为1行
          $("#restfulBox").find("input").not(":first").remove();
          $("#bodyBox").find("input").not(":first").remove();

      }

  });
  
  //去除两端空格
  String.prototype.trim = function(){
　　    return this.replace(/(^\s*)|(\s*$)/g, "");
　}
  </script>
</html>