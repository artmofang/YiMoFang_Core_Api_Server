var notifyShow = function(type, msg) {

        var options = {
            title: false,
            text: msg,
            type: type,
            icon: false,
            animate_speed: 'fast',
            styling: 'bootstrap3'
        };

        var notify = new PNotify(options);
        return notify;
};