<?php
require_once("../common/db.php");

//接收需要添加的参数
$id      = $_POST['id'];
$field   = $_POST['field'];
$value   = $_POST['value'];
$menu_id = $_POST['menu_id'];

//拼接更新sql语句
$sql = "update interface_detail set {$field} = '{$value}' where id = {$id}";

//执行SQL
$db->query($sql);
if(($db->affected_rows)){

	//更新菜单表名称
	$sql = "update interface_menu set {$field} = '{$value}' where id = {$menu_id}";
	$db->query($sql);

	if(!($db->affected_rows)){
		echo json_encode(array("success"=>false,"message"=>"更新失败"));
    	exit();
	}
	
}

echo json_encode(array("success"=>true));