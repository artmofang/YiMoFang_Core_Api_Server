<?php
require_once("../common/db.php");

//获取根菜单名称
$rootMenuName = $_GET['name'];

//添加根菜单
$sql = "insert into interface_menu values(NULL,'{$rootMenuName}',0,'#')";
$result = $db->query($sql);
if(!$result){
   echo json_encode(array("success"=>false,"message"=>"根菜单添加失败"));
   exit();
}

if(mysqli_insert_id($db)){
   echo json_encode(array("success"=>true));
}

//断开连接
mysqli_close($db);
?>