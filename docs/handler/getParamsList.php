<?php
require_once("../common/db.php");

//接收需要添加的参数
$id   = $_GET['id'];
$type = $_GET['type'];

//查询restful参数列表
$sql = "select * from interface_params where detail_id = {$id} and type = '{$type}'";
$result = $db->query($sql);
if(!$result){	
   echo json_encode(array("success"=>false,"message"=>"数据库查询失败"));
   exit();
}

//获取数据
$resultData = array();
while($row = mysqli_fetch_assoc($result)){
	$resultData[] = $row;
}

echo json_encode(array("success"=>true,"result"=>$resultData));