<?php
require_once("../common/db.php");

//获取参数
$interfaceId = $_GET['menuId'];

//查询接口详细
$sql    = "select * from interface_detail where menu_id = {$interfaceId} order by id"; 
$result = $db->query($sql);
if(!$result){
   echo json_encode(array("success"=>false,"message"=>"接口详情获取失败"));
   exit();
}

//获取接口详细数据
$resultData = array();
while($row = mysqli_fetch_assoc($result)){
	$resultData = $row;
}

//获取接口参数数据
if(!$resultData['id']){
   echo json_encode(array("success"=>false,"message"=>"接口详情获取失败"));
   exit();
}

//格式化结果数据
$pregStr = preg_replace('/#(.*)#/','<font color=#999>//\\1</font>',$resultData['result']);
$pregStr = preg_replace('/(String|Boolean|Double|Number)/','<font color=red>\\1</font>',$pregStr);
$resultData['result'] = $pregStr;

//查询接口参数shang
$sql = "select * from interface_params where detail_id = {$resultData['id']} order by id";
$result = $db->query($sql);
if(!$result){
   echo json_encode(array("success"=>false,"message"=>"接口详情获取失败"));
   exit();
}

//获取接口详细数据
$paramsData = array();
while($row = mysqli_fetch_assoc($result)){
	$paramsData[] = $row;
}

//组装数据
$resultData['params'] = $paramsData;

//返回结果
echo json_encode(array("success"=>true,"result"=>$resultData));

//断开连接
mysqli_close($db);