<?php
require_once("../common/db.php");

//接收表名
$tableName = $_GET['tableName'];


$sql = "SHOW FULL FIELDS FROM ".$tableName." from ECommerce";

$result = $db->query($sql);
if(!$result){
   echo json_encode(array("success"=>false,"message"=>"数据库查询失败"));
   exit();
}

//获取数据
$resultData = array();
while($row = mysqli_fetch_assoc($result)){
	$resultData[] = $row;
}

//返回结果
echo json_encode(array("success"=>true,"result"=>$resultData));

//断开连接
mysqli_close($db);