<?php
require_once("../common/db.php");

//获取根菜单id
$rootMenuId = $_GET['rootMenuId'];

//查询该根菜单下的子菜单id
$sql = "select * from interface_menu where fid = {$rootMenuId}"; 
$result = $db->query($sql);
if(!$result){
   echo json_encode(array("success"=>false,"message"=>"数据库查询失败"));
   exit();
}

//获取数据
$resultData = array();
while($row = mysqli_fetch_assoc($result)){
	$resultData[] = $row;
}

//拼接in语句所需id串
$inStr = "";
for($i = 0;$i<count($resultData);$i++){
	$inStr .= $resultData[$i]['id'].",";
}
$inStr = rtrim($inStr,",");

if(empty($inStr)){
   echo json_encode(array("success"=>false,"message"=>"该根菜单下还未创建子菜单"));
   exit();
}

//查询子菜单列表数据
$sql = "select * from interface_detail where menu_id in ({$inStr})"; 
$result = $db->query($sql);
if(!$result){	
   echo json_encode(array("success"=>false,"message"=>"数据库查询失败"));
   exit();
}

//获取数据
$resultData = array();
while($row = mysqli_fetch_assoc($result)){
	$resultData[] = $row;
}

//查询每条接口的参数数量
for($i=0;$i<count($resultData);$i++){
	$detail_id = $resultData[$i]['id'];
	$sql = "select type,count(type) as num from interface_params where detail_id = {$detail_id} group by type";
	$result = $db->query($sql);
	$resultData[$i]['bodyCount']    = 0;
	$resultData[$i]['restfulCount'] = 0;
	while($row = mysqli_fetch_assoc($result)){
		if($row['type'] == 'BODY'){
			$resultData[$i]['bodyCount'] = $row['num'];
		}else{
			$resultData[$i]['restfulCount'] = $row['num'];
		}
	}
}


//返回结果
echo json_encode(array("success"=>true,"result"=>$resultData));

//断开连接
mysqli_close($db);

