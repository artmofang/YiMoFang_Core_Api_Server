<?php
require_once("../common/db.php");

$proName = $_GET['proName'];

//查询根菜单列表
$db->query('use ECommerce');
$sql    = "show create procedure ".$proName;

$result = $db->query($sql);
if(!$result){
   echo json_encode(array("success"=>false,"message"=>"数据库查询失败"));
   exit();
}

//获取数据
$resultData = array();
while($row = mysqli_fetch_assoc($result)){
	$resultData[] = $row;
}

//返回结果
echo json_encode(array("success"=>true,"result"=>$resultData));

//断开连接
mysqli_close($db);