<?php
/*
 * 工具类请求错误信息
 */
$GLOBALS['toolErrorMsg'] = array(

    //图片上传请求方式错误
	"uploadMethodError" => array(
		"code"    => "10000",
		"message" => "图片上传请求方式错误"
	),

	//上传文件过大
	"uploadFileMaxSizeError" => array(
		"code"    => "10001",
		"message" => "上传文件过大"
	),

	//文件类型不符
	"uploadFileTypeError" => array(
		"code"    => "10002",
		"message" => "上传文件的类型不符合"
	),

	//文件无写入权限
	"writeDirError" => array(
		"code"    => "10003",
		"message" => "上传目录无写入权限"
	),

	//图片上传失败
	"uploadError" => array(
		"code"    => "10004",
		"message" => "图片/文件上传失败"
	),

	//CURL模块未安装或未启用
	"curlInstallError" => array(
		"code"	  => "10005",
		"message" => "你还未启用curl模块"
	),

	//CURL发送请求时的URL格式不正确
	"curlUrlError" => array(
		"code"	  => "10006",
		"message" => "发送请求的URL格式不正确"
	),

	

);