<?php
/*
 * 核心错误消息配置数组
 */
$GLOBALS['errorMsg'] = array(

	//请求方式错误
	"methodError" => array(
		"code" 	  => "00001",
		"message" => "不支持的请求方式"
	),

	//请求接口空错误
	"interfaceEmptyError" => array(
		"code" 	  => "00002",
		"message" => "接口地址为空"
	),

	//请求接口未找到
	"interfaceNoneError" => array(
		"code" 	  => "00003",
		"message" => "请求的接口不存在"
	),

	//参数中包含了非法key
	"paramsFormatError" => array(
		"code" 	  => "00004",
		"message" => "参数的格式不正确"
	),

	//接口文件未找到
	"interfaceFileNoneError" => array(
		"code" 	  => "00005",
		"message" => "接口文件未找到"
	),

	//未找到的接口权限模式
	"interfaceProvilegeError" => array(
		"code" 	  => "00006",
		"message" => "该接口验证权限未配置"
	),

	//未配置数据库
	"databaseSettingError" => array(
		"code" 	  => "00007",
		"message" => "未配置数据库信息"
	),

	//未找到满足他条件的数据库类型
	"databaseTypeError" => array(
		"code" 	  => "00008",
		"message" => "未找到该数据库类型"
	),

	//restful参数传递有误
	"restfulParamsError" => array(
		"code" 	  => "00009",
		"message" => "缺少restful参数必传项"
	),

	//body参数传递有误
	"bodyParamsError" => array(
		"code" 	  => "00010",
		"message" => "缺少body参数必传项"
	),

	//未找到匹配的验证规则
	"noRegexpError" => array(
		"code" 	  => "00011",
		"message" => "请检查参数字段的正则配置规则是否存在"
	),

	//该接口请求方式错误
	"noMethodError" => array(
		"code" 	  => "00012",
		"message" => "该接口使用的请求方式错误"
	),

	//未传递用户token
	"noTokenError" => array(
		"code" 	  => "00013",
		"message" => "缺少token验证参数"
	),

	//存储过程调用失败
	"noProceduerError" => array(
		"code" 	  => "00014",
		"message" => "存储过程调用失败，请看该存储过程是否存在"
	),

	//存储过程参数类型
	"noProceduerActionTypeParamsError" => array(
		"code" 	  => "00015",
		"message" => "存储过程操作类型参数错误"
	),

	//存储过程参数格式错误
	"noProceduerParamsFormatError" => array(
		"code" 	  => "00016",
		"message" => "存储过程参数格式传递错误"
	),

	//存储过程参数键值对应数量不正确
	"noProceduerParamsCountError" => array(
		"code" 	  => "00017",
		"message" => "存储过程参数键值对应数量不正确"
	),

	//未被开放的可以访问核心层的服务器地址
	"noServerAuthorizationError" => array(
		"code" 	  => "00018",
		"message" => "该主机未被进行授权访问"
	),

	//非法的请求，没有在验权服务器中获取到访问令牌
	"IllegalRequestError" => array(
		"code" 	  => "00019",
		"message" => "该请求非法，没有访问令牌"
	)


);
