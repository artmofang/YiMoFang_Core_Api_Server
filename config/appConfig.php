<?php
/*
 * 应用基本配置数组
 */




$databaseName = "";
if(isset($_SERVER['HTTP_X_SERVICE_TYPE']) && $_SERVER['HTTP_X_SERVICE_TYPE'] == 'product'){
	$databaseName = "art_magic_cube_product";
}else{
	$databaseName = "art_magic_cube_new_database";
}

$GLOBALS['settings'] = array(

	//设置开发模式
	"debug"	=> false,

	//接口结果中是否返回本次接口的执行信息,包括执行时间,内存占用等
	"showInterfaceInfo" => false,

	//是否开启物理删除
	"isPhysicallyDelete" => false,

	//是否开启跨域请求
	"isAccessControlAllowOrigin" => true,

	//设置允许的请求模式
	"method" => array("GET","POST","ALL"),

	//token令牌的有效时限 (单位：秒,默认为一周)
	"tokenValidity" => 60 * 60 * 24 * 7,

	//默认查询时分页显示数量
	"pageLimit" => 20,

	//是否在核心层验权请求服务器身份
	"isAuthServer" => false, 

	//是否开启接口白名单验证
	"isWhiteAuth" => false,
	
	//数据库相关配置，将所有出现在配置项中的数据库进行连接以及操作对象的创建
	"database" => array(

		//Mysql相关配置项
		"Mysql" => array(
			"username" => "liweixuan",
			"password" => "12345678",
			"port"     => 3306,
			"host"     => "rm-2ze1kg3ip763wer90o.mysql.rds.aliyuncs.com",
			"dbname"   => "art_magic_cube_product", //art_magic_cube_new_database _new_database   art_magic_cube_product
			"prefix"   => "art"
		),

		//Rdis相关配置项
		"Redis" => array(
			"username" => "",
			"password" => "",
			"port"     => 6379,
			"host"     => "127.0.0.1"
		)

	),

	//文件上传相关配置项
	"fileUpload"  => array(

		//最大文件上传值(byte),默认2M
		"maxSize" => "2000000",

		//允许上传的图像文件类型
		"fileType" => array(
			'image/jpg',  
		    'image/jpeg',  
		    'image/png',  
		    'image/pjpeg',  
		    'image/gif',  
		    'image/bmp',  
		    'image/x-png' 
		),	

		//文件临时存放目录
		"uploadTempPath" => "temp",
		
		//上传图片的根目录
		"uploadPath" => "files",
		
		//是否生成缩略图
		"isCreateThumb" => true,

		//缩略图的宽
		"thumbScaleW" => "150",

		//缩略图的高
		"thumbScaleH" => "150"

	),

	//短信相关配置
	"sms" => array(

		//短信验证码位数
		"maxLength" => 6,

		//短信验证码过期时间(默认10分钟)
		"validTime" => 60*60*24
	),

	//缓存相关配置
	"cache" => array(

		//缓存文件存放目录
		"cacheFileDir"  => "cache/",

		//是否进行文件缓存
		"isFileCache"   => false,

		//是否进行内存缓存
		"isMemoryCache" => false,

		//缓存的过期时间
		"cacheValidity" => 60*60,

		//是否读取缓存
		"isReadCache"   => false

	)
);
