<?php
/*
 * 接口白名单配置
 * 针对请求服务器地址进行验证
 * 如果通过了接口白名单，则不在对本次请求进行服务器白名单验证
 * 接口白名单优先级高于服务器白名单
 */


//不需要进行服务器白名单验证的接口白名单
$GLOBALS['interfaceName'] = array(
	
);

//允许访问核心层的server地址与端口号
$GLOBALS['serverIP'] = array(
	"http://127.0.0.1:3000",
	"http://localhost:3000",
	"http://localhost:3100",
	"http://127.0.0.1:3300",
	"http://localhost:3300",
	"http://127.0.0.1:3100",
	"http://127.0.0.1:3500",
	"http://192.168.1.112:3400",
	"http://localhost",
	"http://127.0.0.1"
);

?>