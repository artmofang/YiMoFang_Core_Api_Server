<?php

/*
 * 接口参数验证
 */
$GLOBALS['interfaceParamsValidation'] = array(

    /******* 通用工具 - 开始 ******/

     //发送短信
    "/common/sendCodeSms" => array(  
        "BODY" => array(
            'phone  | NORMAL',
            'type   | NORMAL'
        )
    ),  

    "/common/validationSmsCode" => array(  
        "BODY" => array(
            'phone  | NORMAL',
            'code   | NORMAL'
        )
    ),  

    /******* 通用工具 - 结束 ******/

    /******* 卞光洋 - 开始 ******/
    "/consult/add_organization_consult_user"=>array (
      "BODY"=>array (
          'aocu_name             | NORMAL',
          'aocu_type             | NORMAL',
          'aocu_member           | NORMAL',
          'aocu_phone            | NUMBER',
          'aocu_address          | NORMAL',
      )
    ),
    "/consult/update_organization_consult_user"=>array (
        "BODY"=>array (
            'aocu_id             | NUMBER',

        )
    ),
    "/organization/delete_organization_consult_user" => array(
        "BODY" => array(
            'aocu_id          | NUMBER',
        )
    ),

    "/organization/add_organization_activity_template" => array(
        "BODY" => array(
            'oat_type         | NUMBER',
            'oat_name         | NORMAL',
            'oat_title_image  | NORMAL',
            'oat_cid          | NUMBER',
        )
    ),

    "/discounts_activity/update_discounts_activity_config" => array(
        "BODY" => array(
            'dac_id          | NUMBER',
        )
    ),
    "/organization/delete_organization_discounts_activity_content" => array(
        "BODY" => array(
            'dac_oaid          | NUMBER',
        )
    ),

    "/organization/add_organization_discounts_activity_template_config" => array(
        "BODY" => array(
            'dat_name           | NORMAL',
            'dat_title_image    | NORMAL',
            'dat_category_text  | NORMAL',
            'dat_user_count     | NORMAL',
            'dat_old_price      | NORMAL',
            'dat_new_price      | NORMAL',
            'dat_oatid          | NUMBER'
        )
    ),

    "/organization/add_organization_discounts_activity_content" => array(
        "BODY" => array(
            'dac_oaid           | NUMBER',
            'dac_type           | NUMBER',
            'dac_order          | NUMBER',
            'dac_content        | NORMAL',

        )
    ),
    "/organization/update_organization_discounts_activity_content" => array(
        "BODY" => array(
            'dac_id           | NUMBER'
        )
    ),
    "/organization/add_organization_discounts_activity_template_content" => array(
        "BODY" => array(
            'dac_oaid           | NUMBER',
            'dac_type           | NUMBER',
            'dac_order          | NUMBER',
            'dac_content        | NORMAL',

        )
    ),
    "/organization/add_bulk_purchase_activity_content" => array(
        "BODY" => array(
            'bpac_oaid           | NUMBER',
            'bpac_type           | NUMBER',
            'bpac_order          | NUMBER',
            'bpac_content        | NORMAL',

        )
    ),

    "/organization/bulk_purchase_create_group" => array(
        "BODY" => array(
            'u_id           | NORMAL',
            'oa_id          | NORMAL',
            'order_NO       | NORMAL',
            'min_count      | NORMAL',
            'is_validation  | NORMAL',
        )
    ),

    "/organization/bulk_purchase_activity_join" => array(
        "BODY" => array(
            'u_id           | NORMAL',
            'oa_id          | NORMAL',
            'f_id           | NORMAL',
            'order_NO       | NORMAL',
            'min_count      | NORMAL',
            'is_validation  | NORMAL',
        )
    ),

    "/organization/bulk_purchase_activity_fast_join" => array(
        "BODY" => array(
            'u_id           | NORMAL',
            'oa_id          | NORMAL',
            'order_NO       | NORMAL',
            'min_count      | NORMAL',
            'is_validation  | NORMAL',
        )
    ),

    "/organization/bulk_purchase_activity_solitaier_join" => array(
        "BODY" => array(
            'u_id           | NORMAL',
            'oa_id          | NORMAL',
            'order_NO       | NORMAL',
            'is_validation  | NORMAL',
        )
    ),


    "/organization/delete_bulk_purchase_activity_content" => array(
        "BODY" => array(
            'bpac_oaid          | NUMBER'
        )
    ),
    
    '/organization/update_bulk_purchase_activity_config' => array(
        "BODY" => array(
            'bpac_oaid | NUMBER',
        )
    ),
    "/organization/add_bargain_activity_content" => array(
        "BODY" => array(
            'bc_oaid           | NUMBER',
            'bc_type           | NUMBER',
            'bc_order          | NUMBER',
            'bc_content        | NORMAL',

        )
    ),
    '/organization/delete_bargain_activity_content' => array(
        "BODY" => array(
            'bc_oaid | NUMBER',
        )
    ),
    '/organization/add_bargain_activity_config' => array(
        "BODY" => array(
            'bc_oaid | NUMBER',
            'bc_most_price | MONEY',
            'bc_least_price | MONEY',
            'bc_most_cut_price | MONEY',
            'bc_least_cut_price | MONEY'
        )
    ),
    '/organization/update_bargain_activity_config' => array(
        "BODY" => array(
            'bc_id | NUMBER',
        )
    ),
    "/organization/add_vote_activity_content" => array(
        "BODY" => array(
            'vac_oaid           | NUMBER',
            'vac_type           | NUMBER',
            'vac_order          | NUMBER',
            'vac_content        | NORMAL',

        )
    ),
    '/organization/delete_vote_activity_content' => array(
        "BODY" => array(
            'vac_oaid | NUMBER',
        )
    ),
    '/organization/add_vote_activity_config' => array(
        "BODY" => array(
            'bc_oaid | NUMBER',
            'bc_most_price | MONEY',
            'bc_least_price | MONEY',
            'bc_most_cut_price | MONEY',
            'bc_least_cut_price | MONEY'
        )
    ),
    '/organization/update_vote_activity_config' => array(
        "BODY" => array(
            'vac_id | NUMBER',
        )
    ),
    "/vote_activity/add_vote_activity_reward" => array(
        "BODY" => array(
            'vr_oaid                | NUMBER',
            'vr_rank                | NUMBER',
            'vr_ranknum             | NUMBER',
            'vr_name                | NORMAL',
            'vr_coupon_id           | NORMAL',
            'vr_coupon_type         | NORMAL',

        )
    ),
    '/organization/delete_vote_activity_reward' => array(
        "BODY" => array(
            'vr_oaid | NUMBER',
        )
    ),
    '/oganization/add_leaflet_activity_config'=>array (
        "BODY" => array(
            'lac_oaid               | NUMBER',
            'lac_front_bg_image     | NORMAL',
            'lac_reverse_bg_image   | NORMAL',
            'lac_title              | NORMAL',
            'lac_content            | NORMAL',
            'lac_style_url          | NORMAL',
            'lac_logo               | NORMAL',
            'lac_qr_code            | NORMAL',
            'lac_address            | NORMAL',
            'lac_phone              | NORMAL',
        )
    ),
    '/oganization/update_leaflet_activity_config'=>array (
        "BODY" => array(
            'lac_id               | NUMBER'
        )
    ),
    '/oganization/add_h5scene_activity_content'=>array (
        "BODY" => array(
            'hc_oaid               | NUMBER',
            'hc_type               | NUMBER',
            'hc_bgimage            | NORMAL',
            'hc_title              | NORMAL',
            'hc_image              | NORMAL',
            'hc_order              | NUMBER',
        )
    ),
    '/oganization/delete_h5scene_activity_content'=>array (
        "BODY" => array(
            'hc_oaid              | NUMBER'
        )
    ),
    '/oganization/add_h5scene_activity_config'=>array (
        "BODY" => array(
            'hc_oaid                    | NUMBER',
            'hc_style_url               | NORMAL',
        )
    ),
    '/oganization/update_h5scene_activity_config'=>array (
        "BODY" => array(
            'lac_id               | NUMBER'
        )
    ),

    /******* 卞光洋 - 结束 ******/

    /******* 李蔚轩(机构ERP相关) - 开始 ******/
    "/erpStudent/add" => array(
        "BODY" => array(
            'os_name  | NORMAL'
        )
    ),

    "/erpStudent/update" => array(
        "BODY" => array(
            'os_id  | NUMBER'
        )
    ), 

    "/erpStudent/delete" => array(
        "BODY" => array(
            'os_id  | NUMBER'
        )
    ), 

    "/erpUser/searchRole" => array(
        "RESTFUL" => array(
            'our_ouid  | NUMBER'
        )
    ), 

    

    /******* 李蔚轩(机构ERP相关) - 结束 ******/


    /******* 李蔚轩 - 开始 ******/

    "/organization/search_name_or_address_or_tags" => array(
        "BODY" => array(
            'keywords | NORMAL'
        )
    ),

    "/bargain/help_onself_bargain_action" => array(
        "BODY" => array(
            'bj_id | NORMAL',
            'oa_id | NORMAL',
            'u_id  | NORMAL'
        )
    ),

    "/bargain/join_bargain_activity" => array(
        "BODY" => array(
            'oa_id   | NORMAL',
            'u_id | NORMAL',
            'bj_phone | NORMAL'
        )
    ),
    
    '/bargain/help_bargain_action' => array(
        "BODY" => array(
            'bj_id   | NORMAL',
            'oa_id   | NORMAL',
            'cut_uid | NORMAL'
        )
    ),

    '/organization/add' => array(
        "BODY" => array(
            'o_name  | NORMAL'
        )
    ),
    '/organization/update' => array(
        "BODY" => array(
            'o_id | NUMBER'
        )
    ),
    '/organization/delete' => array(
        "BODY" => array(
            'o_id | NUMBER'
        )
    ),

    "/forum/participation_activity" => array(
        "BODY" => array(
            'fauj_faid     | NUMBER',
            'fauj_uid      | NUMBER',
            'fauj_name     | NOEMPTY',
            'fauj_phone    | NOEMPTY',
            'fauj_is_share | NUMBER'
        )
    ),

    "/forum/user_participation_activity" => array(
        "BODY" => array(
            'fauj_faid     | NUMBER',
            'fauj_uid      | NUMBER'
        )
    ),

    '/discounts_activity/update_discounts_activity_content' =>array(
        "BODY" => array(
            'dac_id | NORMAL'
        )
    ),

    '/discounts_activity/delete_discounts_activity_content' =>array(
        "BODY" => array(
            'dac_id | NORMAL'
        )
    ),

    '/discounts_activity/add_discounts_activity_content' =>array(
        "BODY" => array(
            'dac_oaid    | NORMAL',
            'dac_type    | NORMAL',
            'dac_order   | NORMAL',
            'dac_content | NORMAL',
        )
    ),

    "/cash_coupon/add_cash_coupon" => array(
        "BODY" => array(
            'cc_name | NORMAL',
            'cc_price | NORMAL',
            'cc_start_time | NORMAL',
            'cc_end_time | NORMAL',
            'cc_oid | NORMAL'
        )
    ),

    /******* 李蔚轩 - 结束 ******/

    /***************仲成飞参数验证 开始******************/
    '/forum/add_category' => array(
        "BODY" => array(
            'fc_name| NORMAL',
            'fc_path| NORMAL'
        )
    ),
    '/forum/delete_category' => array(
        "BODY" => array(
            'fc_id | NUMBER'
        )
    ),
    '/forum/update_category' => array(
        "BODY" => array(
            'fc_id | NUMBER'
        )
    ),
    '/forum/add_content' => array(
        "BODY" => array(
            'fc_fcid | NUMBER',
            'fc_uid | NUMBER',
            'fc_title | NORMAL',
            'fc_utype | NUMBER ',
            'fc_type | NUMBER',
            'fc_is_grown | NUMBER',
            'fc_tags | NORMAL',
            'fc_fcfid | NUMBER',
        )
    ),
    '/forum/delete_content' => array(
        "BODY" => array(
            'fc_id | NUMBER'
        )
    ),
    '/forum/update_content' => array(
        "BODY" => array(
            'fc_id | NUMBER'
        )
    ),
    '/forum/add_content_anser_comment' => array(
        "BODY" => array(
            'fcac_fcaid | NUMBER',
            'fcac_content | NORMAL',
            'fcac_uid | NUMBER'
        )
    ),
    '/forum/delete_content_anser_comment' => array(
        "BODY" => array(
            'fcac_id | NUMBER'
        )
    ),


    '/banner/add_banner' => array(
        "BODY" => array(
            'b_image | NORMAL',
            'b_blid | NUMBER',
            'b_name | NORMAL',
        )
    ),

    '/banner/delete_banner' => array(
        "BODY" => array(
            'b_id | NUMBER',
        )
    ),
    '/banner/update_banner' => array(
        "BODY" => array(
            'b_id | NUMBER',
        )
    ),
    '/banner/add_banner_location' => array(
        "BODY" => array(
            'bl_name | NORMAL',
            'bl_type | NUMBER',
        )
    ),

    '/banner/update_banner_location' => array(
        "BODY" => array(
            'bl_id | NUMBER',
        )
    ),
    '/banner/delete_banner_location' => array(
        "BODY" => array(
            'bl_id | NUMBER',
        )
    ),

    '/bulk_purchase/add_bulk_purchase_activity_config' => array(
        "BODY" => array(
            'bpac_name | NORMAL',
            'bpac_desc | NORMAL',
            'bpac_old_price | NORMAL',
            'bpac_new_price | NORMAL',
            'bpac_min_count | NUMBER',
            'bpac_open_price | NORMAL',
            'bpac_participation_price | NORMAL',
        )
    ),
    '/bulk_purchase/delete_bulk_purchase_activity_config' => array(
        "BODY" => array(
            'bpac_id | NUMBER',
        )
    ),
    '/bulk_purchase/update_bulk_purchase_activity_config' => array(
        "BODY" => array(
            'bpac_id | NUMBER',
        )
    ),
    '/bulk_purchase/add_bulk_purchase_activity_user' => array(
        "BODY" => array(
            'bpau_uid | NUMBER',
            'bpau_oaid | NUMBER',
            'bpau_fid | NUMBER',
        )
    ),
    '/bulk_purchase/delete_bulk_purchase_activity_user' => array(
        "BODY" => array(
            'bpau_id | NUMBER',
        )
    ),
    '/bulk_purchase/update_bulk_purchase_activity_user' => array(
        "BODY" => array(
            'bpau_id | NUMBER',
        )
    ),

    '/cash_conpon/add_cash_conpon' => array(
        "BODY" => array(
            'cc_name | NORMAL',
            'cc_price | NORMAL',
            'cc_start_time | NUMBER',
            'cc_end_time | NUMBER',
            'cc_oid | NUMBER',
            'cc_type | NUMBER',
            'cc_max_count | NUMBER',
        )
    ),
    
    '/cash_coupon/delete_cash_coupon' => array(
        "BODY" => array(
            'cc_id | NUMBER',
        )
    ),
    '/cash_coupon/update_cash_coupon' => array(
        "BODY" => array(
            'cc_id | NUMBER',
        )
    ),

    '/category/add_category' => array(
        "BODY" => array(
            'c_fid | NUMBER',
            'c_name | NORMAL',
            'c_path | NORMAL',
        )
    ),
    '/category/delete_category' => array(
        "BODY" => array(
            'cid | NUMBER',
        )
    ),
    '/category/update_category' => array(
        "BODY" => array(
            'cid | NUMBER',
        )
    ),
    '/category/delete_course_coupon' => array(
        "BODY" => array(
            'cc_id | NUMBER',
        )
    ),
    '/category/update_course_coupon' => array(
        "BODY" => array(
            'cc_id | NUMBER',
        )
    ),
    '/course_coupon/add_course_coupon' => array(
        "BODY" => array(
            'cc_name | NORMAL',
            'cc_start_time | NUMBER',
            'cc_end_time | NUMBER',
            'cc_oid | NUMBER',
            'cc_ocid | NUMBER',
            'cc_max_count | NUMBER',
        )
    ),
    
    '/discounts_activity/add_discounts_activity_config' =>array(
        "BODY" => array(
            'dac_max_count | NORMAL',
            'dac_old_price | MONEY',
            'dac_new_price | MONEY',
            'dac_oaid      | NUMBER',
            'dac_address   | NORMAL'
        )
    ),
    '/discounts_activity/delete_discounts_activity_config' =>array(
        "BODY" => array(
            'dac_id | NUMBER',
        )
    ),
    '/discounts_activity/update_discounts_activity_config' =>array(
        "BODY" => array(
            'dac_id | NUMBER',
        )
    ),
    '/discounts_activity/add_discounts_activity_user' =>array(
        "BODY" => array(
            'dau_uid | NUMBER',
            'dau_oaid | NUMBER',
            'dau_phone | NORMAL',
            'dau_name | NORMAL',
        )
    ),
    '/discounts_activity/delete_discounts_activity_user' =>array(
        "BODY" => array(
            'dau_id | NUMBER',
        )
    ),
    '/discounts_activity/update_discounts_activity_user' =>array(
        "BODY" => array(
            'dau_id | NUMBER',
        )
    ),
    '/exchange_coupon/add_exchange_coupon'=>array(
        "BODY" => array(
            'ec_name | NORMAL',
            'ec_start_time | NUMBER',
            'ec_end_time | NUMBER',
            'ec_type | SWITCH',
            'ec_oid | NUMBER',
            'ec_max_count | NUMBER',
            'ec_pid | NORMAL',
        )
    ),
    '/exchange_coupon/update_exchange_coupon'=>array(
        "BODY" => array(
            'ec_id | NUMBER',
        )
    ),
    '/exchange_coupon/delete_exchange_coupon'=>array(
        "BODY" => array(
            'ec_id | NUMBER',
        )
    ),
    '/intelligent_user/update_intelligent_user'=>array(
        "BODY" => array(
            'iuc_id | NUMBER',
        )
    ),
    '/intelligent_user/add_intelligent_user'=>array(
        "BODY" => array(
            'iuc_uid | NUMBER',
            'iuc_invite_count | NUMBER',
            'iuc_browse_count | NUMBER',
            'iuc_collect_count | NUMBER',
            'iuc_fans_count | NUMBER',
            'iuc_article_count | NUMBER',
        )
    ),
    '/vote_activity/add_vote_activity_config'=>array(
        "BODY" => array(
            'vac_type                       |NUMBER',
            'vac_oaid                       |NUMBER',
//            'is_vote_verify                 |NUMBER',
//            'is_vote_limit                  |NUMBER',
//            'is_vote_most_count_every_hour  |NUMBER',
        )
    ),
    '/vote_activity/delete_vote_activity_config'=>array(
        "BODY" => array(
            'vac_id | NUMBER',
        )
    ),
    '/vote_activity/update_vote_activity_config'=>array(
        "BODY" => array(
            'vac_id | NUMBER',
        )
    ),
    '/vote_activity/add_vote_activity_user'=>array(
        "BODY" => array(
            'vau_uid | NUMBER',
            'vau_name | NORMAL',
            'vau_oaid | NUMBER',
            'vau_content | NORMAL',
            'vau_declaration | NORMAL',
            'vau_phone | NORMAL',
        )
    ),
    '/vote_activity/delete_vote_activity_user'=>array(
        "BODY" => array(
            'vau_id | NUMBER',
        )
    ),
    '/vote_activity/update_vote_activity_user'=>array(
        "BODY" => array(
            'vau_id | NUMBER',
        )
    ),
    '/vote_activity/add_vote_activity_user_record'=>array(
        "BODY" => array(
            'vaur_uid | NUMBER',
            'vaur_vote_uid | NUMBER',
            'vaur_oaid | NUMBER',
        )
    ),
    '/withdrawal/add_withdrawal'=>array(
        "BODY" => array(
            'w_oid | NUMBER',
            'w_organization_name | NORMAL',
            'w_house_name | NORMAL',
            'w_bank_number | NORMAL',
            'w_bank_name | NORMAL',
            'w_bank_branch | NORMAL',
            'w_bank_phone | NORMAL',    
            'w_withdrawal_amount | MONEY',
            'w_pay_amount | MONEY',
            'w_uid | NUMBER',
        )
    ),
    '/withdrawal/withdrawal_balance'=>array(
        "BODY" => array(
            'wdr_oid   | NUMBER'
        )
    ),
    '/withdrawal/delete_withdrawal'=>array(
        "BODY" => array(
            'w_id | NUMBER',
        )
    ),
    '/withdrawal/update_withdrawal'=>array(
        "BODY" => array(
            'w_id | NUMBER',
        )
    ),
    '/withdrawal/add_withdrawal_record'=>array(
        "BODY" => array(
            'wdr_oid | NUMBER',
            'wdr_money | MONEY',
            'wdr_desc | NORMAL',
            'wdr_practical_money | MONEY'
        )
    ),
    '/withdrawal/delete_withdrawal_record'=>array(
        "BODY" => array(
            'wr_id | NUMBER',
        )
    ),
    '/forum/add_activity'=>array(
        "BODY" => array(
            'fa_title | NORMAL',
            'fa_content | NORMAL',
            'fa_start_time | NUMBER',
            'fa_end_time | NUMBER',
            'fa_type | NUMBER',
            'fa_fid | NUMBER',
            'fa_price | MONEY',
            'fa_rule | NUMBER',
            'fa_read_score | NUMBER',
            'fa_share_score | NUMBER',
            'fa_join_score | NUMBER',
        )
    ),
    '/forum/delete_activity'=>array(
        "BODY" => array(
            'fa_id | NUMBER',
        )
    ),
    '/forum/update_activity'=>array(
        "BODY" => array(
            'fa_id | NUMBER',
        )
    ),
    '/forum/add_activity'=>array(
        "BODY" => array(
            'fac_faid | NUMBER',
            'fac_uid | NUMBER',
            'fac_content | NORMAL',
            'fac_reply_uid | NUMBER',
            'fac_is_reply | NUMBER',
            'fac_is_author_commnet | NUMBER',
            'fac_zan_count | NUMBER ',
        )
    ),

    '/organization/add_organization_category'=>array(
        "BODY" => array(
            'oc_oid | NUMBER',
            'oc_cid | NUMBER',
        )
    ),

    '/organization/delete_organization_category'=>array(
        "BODY" => array(
            'oc_id | NUMBER',
        )
    ),
    '/organization/update_organization_category'=>array(
        "BODY" => array(
            'oc_id | NUMBER',
        )
    ),
    '/forum/add_tags'=>array(
        "BODY" => array(
            'ft_name | NORMAL',
        )
    ),
    '/forum/delete_tags'=>array(
        "BODY" => array(
            'ft_id | NUMBER',
        )
    ),
    '/forum/update_tags'=>array(
        "BODY" => array(
            'ft_id | NUMBER',
        )
    ),
    '/vote_activity/add_vote'=>array(
        "BODY" => array(
            'vaur_uid | NUMBER',
            'vaur_vote_uid | NUMBER',
            'vaur_oaid | NUMBER',
        )
    ),
    '/wechatpay/update_order_info'=>array(
        "BODY" => array(
            'out_trade_no | NORMAL',
            'transaction_id | NORMAL',
            'balance_pay | MONEY',
            'uid | NUMBER',
            'fid | NUMBER',
        )
    ),
    //添加平台优惠券
    '/platform_coupon/add_platform_coupon'=>array(
        "BODY" => array(
            'pc_type | NUMBER',
            'pc_price | MONEY',
            'pc_name | NORMAL',
            'pc_stock | NUMBER',
            'pc_start_time | NUMBER',
            'pc_end_time | NUMBER',
        )
    ),
    //删除平台优惠券
    '/platform_coupon/delete_platform_coupon'=>array(
        "BODY" => array(
            'pc_id | NUMBER',
        )
    ),
    //修改平台优惠券
    '/platform_coupon/update_platform_coupon'=>array(
        "BODY" => array(
            'pc_id | NUMBER',
        )
    ),
    //领取平台优惠券
    '/platform_coupon/receive_platform_coupon'=>array(
        "BODY" => array(
            'pc_id | NUMBER',
            'pc_type | NUMBER',
            'ucc_uid | NUMBER',
        )
    ),
    //领取平台优惠券
    '/platform_coupon/receive_new_bag'=>array(
        "BODY" => array(
            'ucc_uid | NUMBER',
        )
    ),
    //领取限时补贴
    '/platform_coupon/receive_limit_time_bag'=>array(
        "BODY" => array(
            'ucc_uid | NUMBER',
        )
    ),
    //财务管理 订单总额
    '/order/order_sum'=>array(
        "BODY" => array(
            'o_oid | NUMBER',
        )
    ),
    //财务管理 订单列表
    '/order/order_no_pay'=>array(
        "BODY" => array(
            'o_oid | NUMBER',
            'year | NUMBER',
            'month | NUMBER',
            'type | NUMBER',
            'skip | NUMBER',
            'limit | NUMBER',
        )
    ),

    //退款申请提交参数验证
    '/order/submit_apply_refund'=>array(
        "BODY" => array(
            'of_refund_cash | MONEY',
            'of_refund_reason | NORMAL',
            'of_dec | NORMAL',
            'of_oid | NUMBER',
            'of_ono | NORMAL'
    )
    ),
    //财务管理已验证的订单
    '/order/order_yes_validation'=>array(
        "BODY" => array(
            'o_oid | NUMBER',
        )
    ),
    //财务管理未验证的订单
    '/order/order_no_validation'=>array(
        "BODY" => array(
            'o_oid | NUMBER',

        )
    ),
    //提交对订单的评价接口
    '/order/submit_comment'=>array(
        "BODY" => array(
            'o_id | NUMBER',
            'o_type | NUMBER',
            'o_oid | NUMBER',
            'o_oac_id | NUMBER',
            'o_uid | NUMBER',
            'level | NUMBER',
            'content | NORMAL'
    
        )
    ),
    //查询机构后台订单列表加时间条件
    '/organization_master/search_order_by_time'=>array(
        "BODY" => array(
            'o_oid | NUMBER',
            'year | NUMBER',
            'mouth | NUMBER',
        )
    ),
    //查询机构后台订单列表加时间条件
    '/forum/help_activity_join_user'=>array(
        "BODY" => array(
            'join_uid | NUMBER',
            'help_uid | NUMBER',
            'activity_id | NUMBER',
            'type | NUMBER',
        )
    ),
    //添加红包活动内容
    '/organization/add_red_bag_activity_content'=>array(
        "BODY" => array(
            'rba_oaid | NUMBER',
            'rba_type | NUMBER',
            'rba_cid | NUMBER',
        )
    ),
    //修改红包活动内容
    '/organization/update_red_bag_activity_content'=>array(
        "BODY" => array(
            'rba_id | NUMBER',
        )
    ),
    //删除红包活动内容
    '/organization/delete_red_bag_activity_content'=>array(
        "BODY" => array(
            'rba_oaid | NUMBER',
        )
    ),
    //参加优惠活动
    '/organization/discounts_activity_join_free'=>array(
        "BODY" => array(
            '_u_id | NUMBER',
            '_a_id | NUMBER',
            '_o_id | NUMBER',
        )
    ),
    //添加锁信息
    '/bulk_purchase/add_bulk_purchase_lock'=>array(
        "BODY" => array(
            'bpl_oaid | NUMBER',
            'bpl_fid | NUMBER',
            'order_NO | NUMBER',
        )
    ),
    //添加锁信息
    '/bulk_purchase/update_bulk_purchase_lock'=>array(
        "BODY" => array(
            'bpl_id | NUMBER',
        
        )
    ),
    //参加优惠活动
    '/vote_activity/search_vote_num'=>array(
        "BODY" => array(
            'uid | NUMBER',
            'oaid | NUMBER',
            
        )
    ),
    //参加约课
    '/user/add_appointment_record'=>array(
        "BODY" => array(
            'ar_uid | NUMBER',
            'ar_name | NORMAL',
            'ar_phone | NORMAL',
            'ar_cid | NUMBER',
            'ar_oid | NUMBER',
    
        )
    ),
    //编辑约课记录
    '/user/update_appointment_record'=>array(
        "BODY" => array(
            'ar_id | NUMBER'
        )
    ),

    '/organization_master/add_activity_coupon'=>array(
        "BODY" => array(
            'ac_oid | NUMBER',
            'ac_oaid | NUMBER',
            'ac_type | NUMBER',
            'ac_fid | NUMBER',
            'ac_oatype | NUMBER',
        )
    ),
    //删除机构发放优惠券记录
    '/organization_master/delete_activity_coupon'=>array(
        "BODY" => array(
            'ac_oaid | NUMBER',
        )
    ),
    //添加用户浏览活动记录
    '/user/add_activity_browse_record'=>array(
        "BODY" => array(
            'abr_oaid | NUMBER',
            'abr_oid | NUMBER',
            'abr_uid | NUMBER',
        )
    ),
    //更新用户浏览活动记录
    '/user/update_activity_browse_record'=>array(
        "BODY" => array(
            'abr_id | NUMBER',
        )
    ),
    //添加用户消息
    '/organization/add_user_information'=>array(
        "BODY" => array(
            'ui_uid | NUMBER',
            'ui_activity_name | NORMAL',
            'ui_activtiy_type | NUMBER',
            'ui_activity_fid | NUMBER',
            'ui_type | NUMBER',
            'ui_oid | NUMBER',
        )
    ),
    //添加微传单内容
    '/organization/add_leaflet_activity_content'=>array(
        "BODY" => array(
            'latc_oaid | NUMBER',
            'latc_type | NORMAL',
            'latc_bgimage | NORMAL',
            'latc_content | NORMAL',
        )
    ),
    '/organization/delete_leaflet_activity_content'=>array(
        "BODY" => array(
            'latc_oaid | NUMBER',
        )
    ),
    '/organization_video/add_organization_video_content'=>array(
        "BODY" => array(
            'ovc_ovid | NUMBER',
            'ovc_type | NUMBER',
            'ovc_order | NUMBER',
            'ovc_content | NORMAL',
        )
    ),
    '/organization_video/update_organization_video_content'=>array(
        "BODY" => array(
            'ovc_id | NUMBER',
        )
    ),
    '/organization_video/delete_organization_video_content'=>array(
        "BODY" => array(
            'ovc_ovid | NUMBER',
        )
    ),

    '/organization_video/add_organization_video_discuss'=>array(
        "BODY" => array(
            'ovd_uid | NUMBER',
            'ovd_ovid | NUMBER',
            'ovd_content | NORMAL',
            'ovd_level | NUMBER',
        )
    ),
    '/organization_video/update_organization_video_discuss'=>array(
        "BODY" => array(
            'ovd_id | NUMBER',
        )
    ),
    '/organization_video/delete_organization_video_discuss'=>array(
        "BODY" => array(
            'ovd_id | NUMBER',
        )
    ),

    '/organization_video/add_organization_video_series'=>array(
        "BODY" => array(
            'ovs_ovid | NUMBER',
//            'ovs_title | NORMAL',
//            'ovs_order | NUMBER',
//            'ovs_video_address | NORMAL',
//            'ovs_video_time | NORMAL',
        )
    ),
    '/organization_video/update_organization_video_series'=>array(
        "BODY" => array(
            'ovs_id | NUMBER',
        )
    ),
    '/organization_video/delete_organization_video_series'=>array(
        "BODY" => array(
        )
    ),
    
    
    /***************仲成飞参数验证 结束******************/

    /**************  ice_start  **************/

    //用户相关
    '/user/add' => array(
        "BODY" => array(
            'u_username | NORMAL',
            "u_password | NORMAL"
        )
    ),
    '/user/update' => array(
        "BODY" => array(
            'u_username | NORMAL'
        )
    ),
    '/user/update_user' => array(
        "BODY" => array(
            'u_id | NUMBER'
        )
    ),
    '/user/delete' => array(
        "BODY" => array(
            'u_id | NUMBER'
        )
    ),
    '/user/add_cash_coupon' => array(
        "BODY" => array(
            'ucc_uid | NUMBER',
        )
    ),
    '/user/delete_cash_coupon' => array(
        "BODY" => array(
            'ucc_id | NUMBER'
        )
    ),
    '/user/update_cash_coupon' => array(
        "BODY" => array(
            'ucc_id | NUMBER',
        )
    ),
    '/user/add_collect' => array(
        "BODY" => array(
            'uc_uid | NUMBER',
            'uc_fid | NUMBER',
            'uc_type | NUMBER'
        )
    ),
    '/user/delete_collect' => array(
        "BODY" => array(
            'uc_id | NUMBER'
        )
    ),
    '/user/add_course_coupon' => array(
        "BODY" => array(
            'ucc_uid | NUMBER'
        )
    ),
    '/user/delete_course_coupon' => array(
        "BODY" => array(
            'ucc_id | NUMBER'
        )
    ),
    '/user/update_course_coupon' => array(
        "BODY" => array(
            'ucc_id | NUMBER',
            'ucc_is_used | NUMBER'
        )
    ),
    '/user/add_exchange_coupon' => array(
        "BODY" => array(
            'uec_uid | NUMBER',
            'uec_ecid | NUMBER'
        )
    ),
    '/user/delete_exchange_coupon' => array(
        "BODY" => array(
            'uec_id | NUMBER'
        )
    ),
    '/user/update_exchange_coupon' => array(
        "BODY" => array()
    ),

    '/user/add_token' => array(
        "BODY" => array(
            'ut_token | NORMAL',
            'ut_username | NORMAL'
        )
    ),
    '/user/add_bank_card' => array(
        "BODY" => array(
            'ubc_uid | NUMBER',
            'ubc_house_name | NORMAL',
            'ubc_bank_number | NUMBER',
        )
    ),
    '/user/delete_bank_card' => array(
        "BODY" => array(
            'ubc_id | NUMBER',
        )
    ),
    '/user/update_bank_card' => array(
        "BODY" => array(
            'ubc_id | NUMBER',
        )
    ),
    //机构相关
    '/organization/add_organization_activity' => array(
        "BODY" => array(
            'oa_title           | NORMAL',
            'oa_type            | NUMBER',
            'oa_oid             | NUMBER',
            'oa_start_time      | NUMBER',
            'oa_end_time        | NUMBER'
        )
    ),
    '/organization/delete_organization_activity' => array(
        "BODY" => array(
            'oa_id | NUMBER',
        )
    ),
    '/organization/update_organization_activity' => array(
        "BODY" => array(
            'oa_id | NUMBER',
        )
    ),
    '/organization/add_activity_comment' => array(
        "BODY" => array(
            'oac_oaid | NUMBER',
            'oac_content | NORMAL',
            'oac_uid | NUMBER',
        )
    ),
    '/organization/delete_activity_comment' => array(
        "BODY" => array(
            'oac_id | NUMBER',
        )
    ),
    '/organization/update_activity_comment' => array(
        "BODY" => array(
            'oac_id | NUMBER',
        )
    ),
    '/organization/add_organization_banner' => array(
        "BODY" => array(
//            'ob_title | NORMAL',
            'ob_oid | NUMBER',
            'ob_image_url | NORMAL',
        )
    ),
    '/organization/delete_organization_banner' => array(
        "BODY" => array(
            'ob_id | NUMBER',
        )
    ),
    '/organization/update_organization_banner' => array(
        "BODY" => array(
            'ob_id | NUMBER',
        )
    ),
    '/organization/add_organization_comment' => array(
        "BODY" => array(
            'oc_oid | NUMBER',
            'oc_uid | NUMBER',
            'oc_score_level | NUMBER',
            'oc_content | NORMAL',
        )
    ),
    '/organization/delete_organization_comment' => array(
        "BODY" => array(
            'oc_id | NUMBER',
        )
    ),
    '/organization/update_organization_comment' => array(
        "BODY" => array(
            'oc_id | NUMBER',
        )
    ),
    '/organization/add_organization_course' => array(
        "BODY" => array(
            'oc_title | NORMAL',
            'oc_old_price | NORMAL',
            'oc_new_price | NORMAL',
            'oc_course_count | NUMBER',
            'oc_count | NUMBER',
            'oc_oid | NUMBER',
            'oc_cid | NUMBER',
            'oc_type | NUMBER',
            'oc_appointment | NORMAL',
            'oc_switching | NORMAL',
            'oc_refund | NORMAL',
            'oc_other_info | NORMAL',
            'oc_prompt | NORMAL',
            'oc_age | NORMAL',
            'oc_base | NORMAL',
            'oc_desc | NORMAL',
            'oc_title_image | NORMAL',
            'oc_content | NORMAL',
        )
    ),
    '/organization/delete_organization_course' => array(
        "BODY" => array(
            'oc_id | NUMBER',
        )
    ),
    '/organization/update_organization_course' => array(
        "BODY" => array(
            'oc_id | NUMBER',
        )
    ),
    '/organization/add_organization_photo' => array(
        "BODY" => array(
            'op_fid | NUMBER',
            'op_image_url | NORMAL',
            'op_title | NORMAL',
            'op_oid | NUMBER',
        )
    ),
    '/organization/delete_organization_photo' => array(
        "BODY" => array(
            'op_id | NUMBER',
        )
    ),
    '/organization/update_organization_photo' => array(
        "BODY" => array(
            'op_id | NUMBER',
        )
    ),
    '/organization/add_organization_teacher' => array(
        "BODY" => array(
            'ot_name | NORMAL',
            'ot_oid | NUMBER',
        )
    ),
    '/organization/delete_organization_teacher' => array(
        "BODY" => array(
            'ot_id | NUMBER',
        )
    ),
    '/organization/update_organization_teacher' => array(
        "BODY" => array(
            'ot_id | NUMBER',
        )
    ),
    '/location/add' => array(
        "BODY" => array(
            'l_fid | NUMBER',
            'l_name | NORMAL',
            'l_type | NUMBER',
        )
    ),
    '/location/delete' => array(
        "BODY" => array(
            'l_id | NUMBER',
        )
    ),
    '/location/update' => array(
        "BODY" => array(
            'l_id | NUMBER',
        )
    ),
    '/forum/add_content_zan_record' => array(
        "BODY" => array(
            'fczr_fcid | NUMBER',
            'fczr_uid | NUMBER',
        )
    ),
    '/forum/delete_content_zan_record' => array(
        "BODY" => array(
            'fczr_id | NUMBER',
        )
    ),
    '/forum/update_content_zan_record' => array(
        "BODY" => array(
            'fczr_id | NUMBER',
        )
    ),
    '/forum/add_content_share_record' => array(
        "BODY" => array(
            'fcsr_fcid | NUMBER',
            'fcsr_uid | NUMBER',
        )
    ),
    '/forum/delete_content_share_record' => array(
        "BODY" => array(
            'fcsr_id | NUMBER',
        )
    ),
    '/forum/add_content_comment_zan_record' => array(
        "BODY" => array(
            'fcczr_fcid | NUMBER',
            'fcczr_uid | NUMBER',
        )
    ),
    '/forum/delete_content_comment_zan_record' => array(
        "BODY" => array(
            'fcczr_id | NUMBER',
        )
    ),
    '/forum/add_forum_content_comment' => array(
        "BODY" => array(
            'fcc_fcid | NUMBER',
            'fcc_uid | NUMBER',
            'fcc_content | NORMAL',
        )
    ),
    '/forum/delete_forum_content_comment' => array(
        "BODY" => array(
            'fcc_id | NUMBER',
        )
    ),
    '/forum/add_content_answer_zan_record' => array(
        "BODY" => array(
            'fcazr_fcaid | NUMBER',
            'fcazr_uid | NUMBER',
        )
    ),
    '/forum/update_forum_content_comment' => array(
        "BODY" => array(
            'fcc_id | NUMBER',
        )
    ),
    '/forum/delete_content_answer_zan_record' => array(
        "BODY" => array(
            'fcazr_id | NUMBER',
        )
    ),
    '/forum/update_content_answer_zan_record' => array(
        "BODY" => array(
            'fcazr_id | NUMBER',
        )
    ),
    '/forum/add_content_answer_share_record' => array(
        "BODY" => array(
            'fcasr_fcaid | NUMBER',
            'fcasr_uid | NUMBER',
        )
    ),
    '/forum/delete_content_answer_share_record' => array(
        "BODY" => array(
            'fcsr_id | NUMBER',
        )
    ),
    '/forum/add_content_answer' => array(
        "BODY" => array(
            'fca_uid | NUMBER',
            'fcc_fcid | NUMBER',
            'fca_content | NORMAL',
        )
    ),
    '/forum/delete_content_answer' => array(
        "BODY" => array(
            'fca_id | NUMBER',
        )
    ),
    '/forum/update_content_answer' => array(
        "BODY" => array(
            'fca_id | NUMBER',
        )
    ),
    '/prize/add_prize' => array(
        "BODY" => array(
            'p_name | NORMAL',
            'p_price | MONEY',
            'p_title_image | NORMAL',
            'p_type | NUMBER',
            'p_oid | NUMBER',
        )
    ),
    '/prize/delete_prize' => array(
        "BODY" => array(
            'p_id | NUMBER',
        )
    ),
    '/prize/update_prize' => array(
        "BODY" => array(
            'p_id | NUMBER',
        )
    ),
    '/video/add_video_course' => array(
        "BODY" => array(
            'vc_oid | NUMBER',
            'vc_tittle | NORMAL',
        )
    ),
    '/video/delete_video_course' => array(
        "BODY" => array(
            'vc_id | NUMBER',
        )
    ),
    '/video/update_video_course' => array(
        "BODY" => array(
            'vc_id | NUMBER',
        )
    ),

    '/order/add_order' => array(
        "BODY" => array(
            'o_oid | NUMBER',
            'o_uid | NUMBER',
//            'o_name | NORMAL',
            'o_phone | NUMBER',
//            'o_age | NUMBER',
//            'o_sex | NUMBER',
//            'o_address | NORMAL',
            'o_type | NUMBER',
            'o_aoc_id | NUMBER',
            'o_money | MONEY',
            'o_pay_money | MONEY',
           // 'o_aoc_id | NUMBER',
           // 'o_preferential_money | MONEY',
            'o_wechat_pay | MONEY',
            'o_balance_pay | MONEY',
            'o_course_name | NORMAL',
            'o_course_pic | NORMAL',
           // 'o_organization_name | NORMAL',
            'o_old_price | MONEY',
            'o_new_price | MONEY',
            //'o_activity_type | NUMBER',
            'o_pay_type | NUMBER'
            
           
        )
    ),

    '/bargain/delete_bargain_activity_config' => array(
        "BODY" => array(
            'bac_id | NUMBER'
        )
    ),
    '/bargain/update_bargain_activity_config' => array(
        "BODY" => array(
            'bac_id | NUMBER'
        )
    ),
    '/bargain/add_bargain_join' => array(
        "BODY" => array(
            'bj_uid | NUMBER',
            'bj_oaid | NUMBER'
        )
    ),
    '/bargain/delete_bargain_join' => array(
        "BODY" => array(
            'bj_id | NUMBER'
        )
    ),
    '/bargain/update_bargain_join' => array(
        "BODY" => array(
            'bj_id | NUMBER'
        )
    ),
    '/bargain/add_bargain_record' => array(
        "BODY" => array(
            'br_fid | NUMBER',
            'br_cut_id | NUMBER',
            'br_cut_price | MONEY',
        )
    ),
    '/bargain/delete_bargain_record' => array(
        "BODY" => array(
            'br_id | NUMBER'
        )
    ),
    '/bargain/update_bargain_record' => array(
        "BODY" => array(
            'br_id | NUMBER'
        )
    ),
    '/user/add_user_browse_record' => array(
        "BODY" => array(
            'ubr_browse_type | NUMBER',
            'ubr_fid | NUMBER',
            'ubr_uid | NUMBER',
        )
    ),
    '/user/delete_user_browse_record' => array(
        "BODY" => array(
            'ubr_id | NUMBER'
        )
    ),
    '/user/update_user_browse_record' => array(
        "BODY" => array(
            'ubr_id | NUMBER'

        )
    ),
    '/forum/add_activity_comment' => array(
        "BODY" => array(
            'fac_faid | NUMBER',
            'fac_uid | NUMBER',
            'fac_content | NORMAL',
            'fac_fid | NORMAL'
        )
    ),
    '/forum/add_activity_zan_record' => array(
        "BODY" => array(
            'fazr_faid | NUMBER',
            'fazr_uid | NUMBER'
        )
    ),
    '/forum/delete_activity_zan_record' => array(
        "BODY" => array(
            'fazr_id | NUMBER'
        )
    ),
    '/forum/update_forum_recommend' => array(
        "BODY" => array(
            'fac_id | NUMBER'
        )
    ),
    '/forum/add_activity_comment_zan_record' => array(
        "BODY" => array(
            'faczr_uid | NUMBER',
            'faczr_fid | NUMBER',
        )
    ),
    '/forum/delete_activity_comment_zan_record' => array(
        "BODY" => array(
            'faczr_id | NUMBER',
        )
    ),'/forum/update_activity_comment_zan_record' => array(
        "BODY" => array(
            'faczr_id | NUMBER',
        )
    ),
    '/forum/add_content_answer_comment_zan_record' => array(
        "BODY" => array(
            'fcaczr_fcacid | NUMBER',
            'fcaczr_uid | NUMBER',
        )
    ),
    '/forum/delete_content_answer_comment_zan_record' => array(
        "BODY" => array(
            'fcaczr_id | NUMBER',
        )
    ),
    '/forum/update_content_answer_comment_zan_record' => array(
        "BODY" => array(
            'fcaczr_id | NUMBER',
        )
    ),
    '/platform/add_platform_notification' => array(
        "BODY" => array(
            'pn_content | NORMAL',
            'pn_title | NORMAL',
            'pn_type | NUMBER',
        )
    ),
    '/platform/delete_platform_notification' => array(
        "BODY" => array(
            'pn_id | NUMBER',
        )
    ),
    '/platform/update_platform_notification' => array(
        "BODY" => array(
            'pn_id | NUMBER',
        )
    ),
    '/organization/add_organization_course_content' => array(
        "BODY" => array(
            'occ_ocid | NUMBER',
            'occ_content | NORMAL',
        )
    ),
    '/organization/delete_organization_course_content' => array(
        "BODY" => array(
            'occ_id | NUMBER',
        )
    ),
    '/organization/update_organization_course_content' => array(
        "BODY" => array(
            'occ_id | NUMBER',
        )
    ),
    '/organization_master/order_verification' => array(
        "BODY" => array(
            '_o_no | NORMAL',
            '_cn_number | NORMAL',
            '_oid | NORMAL',
        )
    ),
    '/organization_master/order_refund' => array(
        "BODY" => array(
            '_o_id | NUMBER',
            '_o_no | NUMBER',
            '_uid | NUMBER',
        )
    ),
    '/organization_master/agree_refund_order' => array(
        "BODY" => array(
            '_o_id | NUMBER',
            '_o_no | NUMBER',
            '_uid | NUMBER',
        )
    ),
    '/organization/add_organization_finance_info' => array(
        "BODY" => array(
            'ofi_oid | NUMBER',
            'ofi_account_type | NUMBER',
            'ofi_legal | NORMAL',
            'ofi_account_name | NORMAL',
            'ofi_bank | NORMAL',
            'ofi_bank_number | NORMAL',
            'ofi_prove_image | NORMAL',
            'ofi_branch | NORMAL',
        )
    ),
    '/organization/update_organization_finance_info' => array(
        "BODY" => array(
            'ofi_id | NUMBER',
        )
    ),
    '/organization/delete_organization_finance_info' => array(
        "BODY" => array(
            'ofi_id | NUMBER',
        )
    ),
    '/forum/add_forum_invite_log' => array(
        "BODY" => array(
            'fil_uid | NUMBER',
            'fil_ficid | NUMBER',
            'fil_fcid | NUMBER',
            'fil_fid | NUMBER',
        )
    ),
    '/order/add_consume_num' => array(
        "BODY" => array(
            'cn_uid | NUMBER',
            'cn_oid | NUMBER',
            'cn_coid | NUMBER',
            'cn_ono | NUMBER',
        )
    ),
    '/order/delete_consume_num' => array(
        "BODY" => array(
            'cn_id | NUMBER',
        )
    ),
    '/order/update_consume_num' => array(
        "BODY" => array(
            'cn_id | NUMBER',
        )
    ),
    '/order/add_order_refund' => array(
        "BODY" => array(
            'of_oid | NUMBER',
            'of_refund_cash | MONEY',
        )
    ),
    '/order/delete_order_refund' => array(
        "BODY" => array(
            'of_id | NUMBER',
        )
    ),
    '/order/update_order_refund' => array(
        "BODY" => array(
            'of_id | NUMBER',
        )
    ),
    '/organization/add_organization_comment_zan_record' => array(
        "BODY" => array(
            'oczr_ocid | NUMBER',
            'oczr_uid | NUMBER',
        )
    ),
    '/organization/delete_organization_comment_zan_record' => array(
        "BODY" => array(
            'oczr_id | NUMBER'
        )
    ),
    '/organization/update_organization_comment_zan_record' => array(
        "BODY" => array(
            'oczr_id | NUMBER'
        )
    ),
    '/user/update_organization_concern' => array(
        "BODY" => array(
            'uoc_id | NUMBER'
        )
    ),
    '/forum/add_master_concern' => array(
        "BODY" => array(
            'mc_master_id | NUMBER',
            'mc_uid | NUMBER',
        )
    ),
    '/forum/delete_master_concern' => array(
        "BODY" => array(
            'mc_id | NUMBER',
        )
    ),
    '/forum/update_master_concern' => array(
        "BODY" => array(
            'mc_id | NUMBER',
        )
    ),
    '/forum/update_master' => array(
        "BODY" => array(
            'm_uid | NUMBER',
        )
    ),
    '/forum/search_content_and_organization' => array(
        "BODY" => array(
            'search | NUMBER',
            'art_category | NUMBER',
            'page | NUMBER',
            'limit | NUMBER',
        )
    ),
    '/user/add_organization_course_browsing_history' => array(
        "BODY" => array(
            'uocbh_ocid | NUMBER',
            'uocbh_uid | NUMBER',
        )
    ),
    '/user/update_organization_course_browsing_history' => array(
        "BODY" => array(
            'uocbh_id | NUMBER',
        )
    ),
    '/user/delete_organization_course_browsing_history' => array(
        "BODY" => array(
            'uocbh_id | NUMBER',
        )
    ),
    '/user/add_organization_activity_browsing_history' => array(
        "BODY" => array(
            'ubh_oaid | NUMBER',
            'ubh_uid | NUMBER',
        )
    ),
    '/user/update_organization_activity_browsing_history' => array(
        "BODY" => array(
            'ubh_id | NUMBER',
        )
    ),
    '/user/delete_organization_activity_browsing_history' => array(
        "BODY" => array(
            'ubh_id | NUMBER',
        )
    ),
    '/order/balance_refund' => array(
        "BODY" => array(
            '_uid | NUMBER',
            '_blance_pay_money | MONEY',
        )
    ),
    '/organization/add_activity_comment_zan' => array(
        "BODY" => array(
            'oacz_oacid | NUMBER',
            'oacz_uid | NUMBER',
        )
    ),
    '/organization/delete_activity_comment_zan' => array(
        "BODY" => array(
            'oacz_id | NUMBER',
        )
    ),
    '/organization/update_activity_comment_zan' => array(
        "BODY" => array(
            'oacz_id | NUMBER',
        )
    ),
    '/forum/forum_search_by_yourself' => array(
        "BODY" => array(
            'search | NUMBER',
            'text | NORMAL',
            'page | NUMBER',
            'limit | NUMBER',
            'u_id | NUMBER',
        )
    ),
    '/forum/invite_answer' => array(
        "BODY" => array(
            'category_type | NUMBER',
            'skip | NUMBER',
            'limit | NUMBER',
        )
    ),
    '/forum/add_forum_activity_appointment' => array(
        "BODY" => array(
            'faa_faid | NUMBER',
            'faa_uid | NUMBER',
            'faa_phone | NUMBER',
        )
    ),
    '/forum/delete_forum_activity_appointment' => array(
        "BODY" => array(
            'faa_id | NUMBER',
        )
    ),
    '/forum/update_forum_activity_appointment' => array(
        "BODY" => array(
            'faa_id | NUMBER',
        )
    ),
    '/organization/add_organization_user' => array(
        "BODY" => array(
            'ou_oid | NUMBER',
            'ou_uid | NUMBER',
            'ou_phone | NORMAL',
            'ou_name | NORMAL',
        )
    ),
    '/organization/delete_organization_user' => array(
        "BODY" => array(
            'ou_id | NUMBER'
        )
    ),
    '/organization/update_organization_user' => array(
        "BODY" => array(
            'ou_id | NUMBER'
        )
    ),

    '/organization/add_activity_music' => array(
        "BODY" => array(
            'am_tag | NUMBER',
            'am_url | NORMAL',
        )
    ),
    '/organization/delete_activity_music' => array(
        "BODY" => array(
            'am_id | NUMBER'
        )
    ),
    '/organization/update_activity_music' => array(
        "BODY" => array(
            'am_id | NUMBER'
        )
    ),
    '/organization/add_activity_skin' => array(
        "BODY" => array(
            'as_bg_style | NORMAL',
            'as_border_style | NORMAL',
            'as_header_style | NORMAL',
            'as_footer_style | NORMAL',
        )
    ),
    '/organization/delete_activity_skin' => array(
        "BODY" => array(
            'as_id | NUMBER'
        )
    ),
    '/organization/update_activity_skin' => array(
        "BODY" => array(
            'as_id | NUMBER'
        )
    ),



    //oa
    '/oaDemand/add_demand' => array(
        "BODY" => array(
            'd_content | NORMAL',
            'd_create_user | NUMBER',
        )
    ),
    '/oaDemand/delete_demand' => array(
        "BODY" => array(
            'd_id | NUMBER',
        )
    ),
    '/oaDemand/update_demand' => array(
        "BODY" => array(
            'd_id | NUMBER',
        )
    ),
    '/oaDemand/add_demand_distribution' => array(
        "BODY" => array(
            'dd_did | NUMBER',
            'dd_uid | NUMBER',
            'dd_end_time | NUMBER',
        )
    ),
    '/oaDemand/delete_demand_distribution' => array(
        "BODY" => array(
            'dd_id | NUMBER',
        )
    ),
    '/oaDemand/update_demand_distribution' => array(
        "BODY" => array(
            'dd_id | NUMBER',
        )
    ),
    '/oaDemand/add_demand_remarks' => array(
        "BODY" => array(
            'dr_did | NUMBER',
            'dr_remark | NORMAL',
            'dr_user | NUMBER',
        )
    ),
    '/oaDemand/delete_demand_remarks' => array(
        "BODY" => array(
            'dr_id | NUMBER',
        )
    ),
    '/oaDemand/update_demand_remarks' => array(
        "BODY" => array(
            'dr_id | NUMBER',
        )
    ),
    '/oaDemand/add_demand_working_hours' => array(
        "BODY" => array(
            'dwh_uid | NUMBER',
            'dwh_date | NUMBER'
        )
    ),
    '/oaDemand/delete_working_hours' => array(
        "BODY" => array(
            'dwh_id | NUMBER',
        )
    ),
    '/oaDemand/update_working_hours' => array(
        "BODY" => array(
            'dwh_id | NUMBER',
        )
    ),
    '/oaUser/delete_user' => array(
        "BODY" => array(
            'u_id | NUMBER',
        )
    ),
    '/oaUser/update_user' => array(
        "BODY" => array(
            'u_id | NUMBER',
        )
    ),


    '/oaDemand/add_demand_enclosure' => array(
        "BODY" => array(
            'de_did | NUMBER',
            'de_file_type | NUMBER',
            'de_file_size | NORMAL',
            'de_enclosure | NORMAL'
        )
    ),
    '/oaDemand/delete_demand_enclosure' => array(
        "BODY" => array(
            'de_id | NUMBER',
        )
    ),
    '/oaDemand/update_demand_enclosure' => array(
        "BODY" => array(
            'de_id | NUMBER',
        )
    ),

    '/oaDemand/add_demand_remarks_enclosure' => array(
        "BODY" => array(
            'dre_drid | NUMBER',
            'dre_file_type | NUMBER',
            'dre_file_size | NORMAL',
            'dre_enclosure | NORMAL'
        )
    ),
    '/oaDemand/delete_demand_remarks_enclosure' => array(
        "BODY" => array(
            'dre_id | NUMBER',
        )
    ),
    '/oaDemand/update_demand_remarks_enclosure' => array(
        "BODY" => array(
            'dre_id | NUMBER',
        )
    ),
    '/organization/add_activity_cover' => array(
        "BODY" => array(
            'ac_name | NORMAL',
            'ac_bg_image | NORMAL',
            'ac_image_text | NORMAL',
            'ac_image_text_style | NORMAL',
            'ac_thumbnail | NORMAL',
            'ac_tag | NUMBER',
        )
    ),
    '/organization/update_art_product_order' =>array(
        "BODY" => array(
            'apo_no | NORMAL'
        )
    ),
    '/organization/add_art_product_order'=>array(
        "BODY" => array(
            'apo_no | NORMAL',
            'apo_name | NORMAL',
            'apo_old_price | NORMAL',
            'apo_new_price | NORMAL',
            'apo_real_pay | NORMAL',
        )
    ),
    '/organization/delete_activity_cover' => array(
        "BODY" => array(
            'ac_id | NUMBER',
        )
    ),
    '/organization/update_activity_cover' => array(
        "BODY" => array(
            'ac_id | NUMBER',
        )
    ),
    '/organization/add_organization_tools_apply' => array(
        "BODY" => array(
            'ota_name | NORMAL',
            'ota_type | NORMAL',
            'ota_member | NORMAL',
            'ota_phone | NORMAL',
            'ota_address | NORMAL',
        )
    ),
    '/organization/delete_organization_tools_apply' => array(
        "BODY" => array(
            'ota_id | NUMBER',
        )
    ),
    '/organization/update_organization_tools_apply' => array(
        "BODY" => array(
            'ota_id | NUMBER',
        )
    ),
    '/message/add_message_push_record' => array(
        "BODY" => array(
            'mpr_message | NORMAL',
            'mpr_sum | NUMBER',
            'mpr_arrive | NUMBER',
            'mpr_fail | NUMBER',
            'mpr_title | NORMAL',
            'mpr_user | NUMBER',
        )
    ),
    '/message/delete_message_push_record' => array(
        "BODY" => array(
            'mpr_id | NUMBER',
        )
    ),
    '/organization/add_organization_activity_browse_record' => array(
        "BODY" => array(
            'oabr_oaid | NUMBER',
            'oabr_uid | NUMBER',
            'oabr_oid | NUMBER',
        )
    ),
    '/organization/delete_organization_activity_browse_record' => array(
        "BODY" => array(
            'oabr_id | NUMBER',
        )
    ),
    '/organization/update_organization_activity_browse_record' => array(
        "BODY" => array(
            'oabr_id | NUMBER',
        )
    ),
    '/organization/add_organization_message_push_record' => array(
        "BODY" => array(
            'omrp_title | NUMBER',
            'omrp_content | NORMAL',
            'ompr_oaid | NUMBER',
            'ompr_oid | NUMBER',
        )
    ),
    '/organization/delete_organization_message_push_record' => array(
        "BODY" => array(
            'ompr_id | NUMBER',
        )
    ),
    '/organization_video/add_organization_video' => array(
        "BODY" => array(
            'ov_skin_id | NUMBER',
            'ov_title_image | NORMAL',
            'ov_title_imgae_th | NORMAL',
//            'ov_title_image_text | NORMAL',
//            'ov_title_image_text_style | NUMBER',
            'ov_title | NORMAL',
            'ov_old_price | NORMAL',
            'ov_now_price | NORMAL',
            'ov_category_text | NORMAL',
            'ov_course_type | NUMBER',
//          'ov_tags | NORMAL',
            'ov_oid | NUMBER',
        )
    ),
    '/organization_video/delete_organization_video' => array(
        "BODY" => array(
            'ov_id | NUMBER',
        )
    ),
    '/organization_video/update_organization_video' => array(
        "BODY" => array(
            'ov_id | NUMBER',
        )
    ),
    '/organization_video/add_organization_video_buy_record' => array(
        "BODY" => array(
            'ovbr_uid | NUMBER',
            'ovbr_ovid | NUMBER',
            'ovbr_oid | NUMBER',
            'ovbr_order_id | NUMBER',
        )
    ),
    '/organization_video/delete_organization_video_buy_record' => array(
        "BODY" => array(
            'ovbr_id | NUMBER',
        )
    ),
    '/organization_video/update_organization_video_buy_record' => array(
        "BODY" => array(
            'ovbr_id | NUMBER',
        )
    ),


    //oa

    /**************  ice_end  **************/

    /** oa ***/
    "/oaUser/add" => array(
        "BODY" => array(
            'u_username | NORMAL',
            'u_password | NORMAL',
            'u_phone | NORMAL',
            'u_age | NORMAL',
            'u_realname | NORMAL',
            'u_sex | NORMAL',
            'u_role_id | NORMAL'
        )
    )


);
?>
