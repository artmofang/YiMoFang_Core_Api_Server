<?php

/*
 * 接口级权限,及请求方式配置
 * 值为：
 * 1.是否需要进行token验证（no-不需要 yes-需要）
 * 2.请求方式
 */
$GLOBALS['interfacePrivilege'] = array(

    /******* 通用工具 - 开始 ******/
    "/common/sendCodeSms"       => "no|ALL",   //发送验证短信
    "/common/validationSmsCode" => "no|ALL",   //验证短信码是否正确
    /******* 通用工具 - 结束 ******/

    /******* 李蔚轩(机构ERP相关) - 开始 ******/
    "/erpUser/search"       => "no|ALL",   //获取用户信息
    "/erpUser/searchRole"   => "no|ALL",   //获取用户所拥有的角色
    "/erpPermission/search" => "no|ALL",   //获取用户所能使用的接口列表
    "/erpStudent/add"       => "no|POST",  //新增学员
    "/erpStudent/search" => "no|ALL",   //查询学员
    "/erpStudent/update" => "no|POST",  //更新学员信息
    "/erpStudent/delete" => "no|POST",  //删除学员

    "/oaUser/organizationStructureSearch" => "no|ALL",  //获取用户所在节点
    "/oaRole/organizationStructureSearch" => "no|ALL",  //获取节点所拥有的角色
    /******* 李蔚轩(机构ERP相关) - 开始 ******/

    /******* 卞光洋 - 开始 ******/
    //客户咨询服务
    "/consult/add_organization_consult_user" => "no|POST",
    "/consult/delete_organization_consult_user" => "no|POST",
    "/consult/update_organization_consult_user" => "no|POST",
    "/consult/search_organization_consult_user" => "no|ALL",


    //活动模板
    "/organization/add_organization_activity_template" => "no|POST",
    "/organization/delete_organization_activity_template" => "no|POST",
    "/organization/update_organization_activity_template" => "no|POST",
    "/organization/search_organization_activity_template" => "no|ALL",

    //促销模板
    "/organization/add_organization_discounts_activity_template" => "no|POST",
    "/organization/delete_organization_discounts_activity_template" => "no|POST",
    "/organization/update_organization_discounts_activity_template" => "no|POST",
    "/organization/search_organization_discounts_activity_template" => "no|ALL",

    //促销配置表
    "/discounts_activity/add_discounts_activity_config" => "no|POST",// 添加优惠活动
    '/discounts_activity/delete_discounts_activity_config'=>"no|POST",// 删除优惠活动
    '/discounts_activity/update_discounts_activity_config'=>"no|POST",// 修改优惠活动
    '/discounts_activity/search_discounts_activity_config'=>"no|POST",// 修改优惠活动
    "/organization/search_organization_discounts_activity_config" => "no|ALL",// 查询优惠活动
    
    //拼团锁
    "/bulk_purchase/add_bulk_purchase_lock" => "no|POST",
    '/bulk_purchase/delete_bulk_purchase_lock'=>"no|POST",
    '/bulk_purchase/update_bulk_purchase_lock'=>"no|POST",
    "/bulk_purchase/search_bulk_purchase_lock" => "no|ALL",

    //促销模板内容表
    "/organization/add_organization_discounts_activity_template_content" => "no|POST",
    "/organization/delete_organization_discounts_activity_template_content" => "no|POST",
    "/organization/update_organization_discounts_activity_template_content" => "no|POST",
    "/organization/search_organization_discounts_activity_template_content" => "no|ALL",

    //促销内容表
    "/organization/add_organization_discounts_activity_content" => "no|POST",
    "/organization/delete_organization_discounts_activity_content" => "no|POST",
    "/organization/update_organization_discounts_activity_content" => "no|POST",
    "/organization/search_organization_discounts_activity_content" => "no|ALL",

    //拼团模板
    "/organization/add_bulk_purchase_activity_template" => "no|POST",
    "/organization/delete_bulk_purchase_activity_template" => "no|POST",
    "/organization/update_bulk_purchase_activity_template" => "no|POST",
    "/organization/search_bulk_purchase_activity_template" => "no|ALL",

    //拼团模板内容
    "/organization/add_bulk_purchase_activity_template_content" => "no|POST",
    "/organization/delete_bulk_purchase_activity_template_content" => "no|POST",
    "/organization/update_bulk_purchase_activity_template_content" => "no|POST",
    "/organization/search_bulk_purchase_activity_template_content" => "no|ALL",

    //拼团内容
    "/organization/add_bulk_purchase_activity_content" => "no|POST",
    "/organization/delete_bulk_purchase_activity_content" => "no|POST",
    "/organization/update_bulk_purchase_activity_content" => "no|POST",
    "/organization/search_bulk_purchase_activity_content" => "no|ALL",

    //拼团配置表
    '/organization/add_bulk_purchase_activity_config'=>"no|POST",// 添加拼团活动
    '/bulk_purchase/delete_bulk_purchase_activity_config'=>"no|POST",// 删除拼团活动
    '/organization/update_bulk_purchase_activity_config'=>"no|POST",// 更新拼团活动
    '/bulk_purchase/search_bulk_purchase_activity_config'=>"no|ALL",// 查询拼团活动

    //拼团用户
    '/bulk_purchase/add_bulk_purchase_activity_user'=>"no|POST",// 添加拼团参与用户
    '/bulk_purchase/update_bulk_purchase_activity_user'=>"no|POST",// 修改拼团参与用户
    '/bulk_purchase/delete_bulk_purchase_activity_user'=>"no|POST",// 删除拼团参与用户
    '/bulk_purchase/search_bulk_purchase_activity_user'=>"no|ALL",// 查询拼团参与用户
    '/bulk_purchase/search_bulk_purchase_activity_user_info'=>"no|ALL",// 查询拼团参与用户,带拼团信息

    //砍价模板
    "/organization/add_bargain_activity_template" => "no|POST",
    "/organization/delete_bargain_activity_template" => "no|POST",
    "/organization/update_bargain_activity_template" => "no|POST",
    "/organization/search_bargain_activity_template" => "no|ALL",

    //砍价模板内容
    "/organization/add_bargain_activity_template_content" => "no|POST",
    "/organization/delete_bargain_activity_template_content" => "no|POST",
    "/organization/update_bargain_activity_template_content" => "no|POST",
    "/organization/search_bargain_activity_template_content" => "no|ALL",

    //砍价内容
    "/organization/add_bargain_activity_content" => "no|POST",
    "/organization/delete_bargain_activity_content" => "no|POST",
    "/organization/update_bargain_activity_content" => "no|POST",
    "/organization/search_bargain_activity_content" => "no|ALL",

    //砍价配置
    "/organization/add_bargain_activity_config" => "no|POST",                        //机构砍价活动配置添加
    "/organization/delete_bargain_activity_config" => "no|POST",                    //机构砍价活动配置删除
    "/organization/search_bargain_activity_config" => "no|ALL",                     //机构砍价活动配置查询
    "/organization/update_bargain_activity_config" => "no|POST",                    //机构砍价活动配置更新

    //砍价参与
    "/bargain/add_bargain_join" => "no|POST",                                    //砍价活动参与者添加
    "/bargain/delete_bargain_join" => "no|POST",                                //机构砍价活动参与者删除
    "/bargain/search_bargain_join" => "no|ALL",                                 //机构砍价活动参与者查询
    "/bargain/search_bargain_join_user_info" => "no|ALL",                      //机构砍价活动参与者查询(带用户信息)
    "/bargain/update_bargain_join" => "no|POST",                                //机构砍价活动参与者更新

   


    //砍价记录
    "/bargain/add_bargain_record" => "no|POST",                                 //机构砍价活动砍价记录添加
    "/bargain/delete_bargain_record" => "no|POST",                              //机构砍价活动砍价记录删除
    "/bargain/search_bargain_record" => "no|ALL",                               //机构砍价活动砍价记录查询
    "/bargain/search_bargain_record_user_info" => "no|ALL",                               //机构砍价活动砍价记录查询（带砍价用户的信息）
    "/bargain/update_bargain_record" => "no|POST",                              //机构砍价活动砍价记录更新

    //投票模板
    "/organization/add_vote_activity_template" => "no|POST",
    "/organization/delete_vote_activity_template" => "no|POST",
    "/organization/update_vote_activity_template" => "no|POST",
    "/organization/search_vote_activity_template" => "no|ALL",

    //投票模板内容
    "/organization/add_vote_activity_template_content" => "no|POST",
    "/organization/delete_vote_activity_template_content" => "no|POST",
    "/organization/update_vote_activity_template_content" => "no|POST",
    "/organization/search_vote_activity_template_content" => "no|ALL",

    //投票内容
    "/organization/add_vote_activity_content" => "no|POST",
    "/organization/delete_vote_activity_content" => "no|POST",
    "/organization/update_vote_activity_content" => "no|POST",
    "/organization/search_vote_activity_content" => "no|ALL",

    //投票配置
    '/vote_activity/add_vote_activity_config' =>"no|POST",//添加投票活动
    '/vote_activity/update_vote_activity_config' =>"no|POST",//修改投票活动
    '/vote_activity/delete_vote_activity_config' =>"no|POST",//删除投票活动
    '/vote_activity/search_vote_activity_config' =>"no|ALL",//查询投票活动

    //投票参加
    '/vote_activity/add_vote_activity_user' =>"no|POST",//添加投票参与用户
    '/vote_activity/search_vote_activity_user' =>"no|ALL",//查询投票参与用户
    '/vote_activity/search_vote_activity_user_and_activity_info' =>"no|ALL",//查询投票参与用户（包含活动信息）
    '/vote_activity/user_integral_ranking' =>"no|ALL",//查询用户在投票活动中的名次
    '/vote_activity/update_vote_activity_user' =>"no|POST",//更新投票参与用户
    '/vote_activity/delete_vote_activity_user' =>"no|POST",//删除投票参与用户

    //投票记录
    '/vote_activity/add_vote_activity_user_record' =>"no|POST",//添加投票记录
    '/vote_activity/search_vote_activity_user_record' =>"no|ALL",//查询投票记录
    '/vote_activity/update_vote_activity_user_record' =>"no|POST",//更新投票记录
    '/vote_activity/search_vote_activity_record' =>"no|ALL",//查询投票记录

    //投票奖励表
    '/vote_activity/add_vote_activity_reward' =>"no|POST",//添加投票记录
    '/vote_activity/search_vote_activity_reward' =>"no|ALL",//查询投票记录
    '/vote_activity/update_vote_activity_reward' =>"no|POST",//更新投票记录
    '/vote_activity/delete_vote_activity_reward' =>"no|ALL",//查询投票记录

    //传单模板
    "/organization/add_leaflet_activity_template" => "no|POST",
    "/organization/delete_leaflet_activity_template" => "no|POST",
    "/organization/update_leaflet_activity_template" => "no|POST",
    "/organization/search_leaflet_activity_template" => "no|ALL",

    //传单内容
    "/organization/add_leaflet_activity_config" => "no|POST",
    "/organization/delete_leaflet_activity_config" => "no|POST",
    "/organization/update_leaflet_activity_config" => "no|POST",
    "/organization/search_leaflet_activity_config" => "no|ALL",

    "/organization/search_leaflet_activity_template_content" => "no|ALL",

    //h5场景模板内容
    "/organization/add_h5scene_activity_template_content" => "no|POST",
    "/organization/delete_h5scene_activity_template_content" => "no|POST",
    "/organization/update_h5scene_activity_template_content" => "no|POST",
    "/organization/search_h5scene_activity_template_content" => "no|ALL",

    //h5场景配置内容
    "/organization/add_h5scene_activity_content" => "no|POST",
    "/organization/delete_h5scene_activity_content" => "no|POST",
    "/organization/update_h5scene_activity_content" => "no|POST",
    "/organization/search_h5scene_activity_content" => "no|ALL",

    //h5场景模板
    "/organization/add_h5scene_activity_template" => "no|POST",
    "/organization/delete_h5scene_activity_template" => "no|POST",
    "/organization/update_h5scene_activity_template" => "no|POST",
    "/organization/search_h5scene_activity_template" => "no|ALL",

    //h5场景配置
    "/organization/add_h5scene_activity_config" => "no|POST",
    "/organization/delete_h5scene_activity_config" => "no|POST",
    "/organization/update_h5scene_activity_config" => "no|POST",
    "/organization/search_h5scene_activity_config" => "no|ALL",

    //红包活动
    '/organization/add_red_bag_activity_content'   =>"no|POST",//增加红包活动内容
    '/organization/update_red_bag_activity_content'=>"no|POST",//修改红包活动内容
    '/organization/search_red_bag_activity_content'=>"no|ALL",//查询红包活动内容
    '/organization/delete_red_bag_activity_content'=>"no|POST",//删除红包活动内容
    /******* 卞光洋 - 结束 ******/

    /******* 李蔚轩 - 开始 ******/

    "/organization/add_art_strategic_partner_recruiting_record" => "no|ALL",  //新增战略合作申请


    "/organization/search_art_product_user_read_record" => "no|ALL",      //查询产品用户记录表
    "/organization/update_art_product_user_read_record" => "no|ALL",      //更新产品用户记录表
    "/organization/add_art_product_user_read_record" => "no|ALL",      //新增产品用户记录表


    "/organization/search_art_product_order" => "no|ALL",      //查询机构购买艺魔方的订单列表
    "/organization/add_art_product_order" => "no|POST",      //新增艺魔方产品订单
    "/organization/update_art_product_order" => "no|POST",      //更新艺魔方产品订单
    "/organization/search_art_product" => "no|ALL",         //更新艺魔方产品
    "/organization/search_art_peoduct_record" => "no|ALL",         //获取机构购买产品的记录
    "/organization/add_art_peoduct_record" => "no|POST",         //新增机构购买产品的记录
    "/organization/update_art_peoduct_record" => "no|POST",         //更新机构购买产品的记录

    "/organization/add" => "no|POST",      //新增机构
    "/organization/delete" => "no|POST",   //删除机构
    "/organization/update" => "no|POST",   //更新机构
    "/organization/search" => "no|ALL",    //查询机构
    "/organization/search_near" => "no|ALL",    //根据坐标查询附近的机构
    "/organization/search_organization_sort" => "no|ALL",    //智能排序

    "/oaMenu/search"        => "no|ALL",          //查询菜单
    "/oaUser/search"        => "no|ALL",          //查询员工 
    "/oaUser/add"           => "no|POST",         //新增员工
    "/oaRole/search"        => "no|ALL",          //查询角色
    "/oaPermissions/search" => "no|ALL",          //查询权限

    "/forum/participation_activity" => "no|POST",  //参与论坛活动
    "/forum/user_participation_activity" => "no|POST",  //参与有奖论坛活动参加
    "/forum/search_content_and_userinfo" => "no|ALL",           //查询论坛内容(带发布用户的相关信息)
    "/forum/search_content_and_userinfo" => "no|ALL",   //查询论坛内容(带发布用户的相关信息)
    "/user/search_organization_activity_browsing" => "no|ALL",   //查询用户机构活动浏览记录
    "/user/search_organization_course_browsing" => "no|ALL",   //查询用户机构课程浏览记录
    "/user/search_art_user_user_teacher_info" => "no|ALL",              //用户身份为教师带教师信息
    "/forum/search_forum_activity_comment" => "no|ALL",                 //用户身份为教师带教师信息
    "/forum/search_forum_activity_join_user" => "no|ALL",               //论坛活动参与者用户信息

    "/bargain/search_bargain_join_and_activity_info" => "no|ALL",               //机构砍价活动参与者查询(带活动信息)
    "/user/search_organization_concern_info" => "no|ALL",                        //查询用户关注机构(带机构信息)
    "/forum/search_question_concern_info" => "no|ALL",                        //查询论坛问题关注记录(带问题信息)
    "/user/search_user_answer_info"=>"no|ALL",//查询个人中心我的回答
    "/bargain/help_bargain_action"=>"no|ALL",//帮好友砍价操作
    "/bargain/join_bargain_activity"=>"no|ALL",//参与砍价活动
    "/bargain/help_onself_bargain_action"=>"no|ALL",//自己给自己砍价
    "/organization/search_organization_activity_h5_config_info" => "no|ALL", //查询海报活动列表信息（包含扩展表信息）
    "/organization/search_organization_activity_leaflet_config_info" => "no|ALL", //查询传单活动列表信息（包含扩展表信息）
    /******* 李蔚轩 - 结束 ******/


    /*******仲成飞 -开始*********/
    "/forum/search_category" => "no|ALL",   //查询论坛分类
    "/forum/add_category" => "no|POST",     //新增论坛分类
    "/forum/delete_category" => "no|POST",  //删除论坛分类
    "/forum/update_category" => "no|POST",  //修改论坛分类

    "/forum/add_content" => "no|POST",        //新增论坛内容
    "/forum/search_content" => "no|ALL",     //查询论坛内容
    "/forum/delete_content" => "no|POST",     //删除论坛内容
    "/forum/update_content" => "no|POST",     //更新论坛内容

    "/forum/add_tags" => "no|POST",     //新增论坛标签内容
    "/forum/search_tags" => "no|ALL",     //查询论坛标签
    "/forum/delete_tags" => "no|POST",     //删除论坛标签
    "/forum/update_tags" => "no|POST",     //更新论坛标签


    "/forum/add_content_anser_comment"=>"no|POST", //添加问题回答的评论
    "/forum/delete_content_anser_comment"=>"no|POST", //删除问题回答的评论
    "/forum/search_content_anser_comment"=>"no|ALL", //查询问题回答的评论
    "/forum/update_content_anser_comment"=>"no|POST", //更新问题回答的评论
    
    '/banner/add_banner'=>"no|POST",// 添加广告
    '/banner/delete_banner'=>"no|POST",// 删除广告
    '/banner/search_banner'=>"no|ALL",// 搜索广告
    '/banner/update_banner'=>"no|POST",// 修改广告
    
    '/banner/add_banner_location'=>"no|POST",// 添加广告位
    '/banner/update_banner_location'=>"no|POST",// 更新广告位
    '/banner/delete_banner_location'=>"no|POST",// 删除广告位
    '/banner/search_banner_location'=>"no|ALL",// 查询广告位
    


    "/video/search_video_course_user_collect" => "no|ALL",                                   //查询视频课（用户收藏）

    '/cash_coupon/add_cash_coupon'=>"no|POST",// 添加代金券信息
    '/cash_coupon/update_cash_coupon'=>"no|POST",// 更新代金券信息
    '/cash_coupon/delete_cash_coupon'=>"no|POST",// 删除代金券信息
    '/cash_coupon/search_cash_coupon'=>"no|ALL",// 查询代金券信息

    "/user/organization_course_collect" => "no|ALL",                                 //用户收藏机构课程
    '/category/add_category'=>"no|POST",// 添加艺术类别
    '/category/delete_category'=>"no|POST",// 删除艺术类别
    '/category/search_category'=>"no|ALL",// 查询艺术类别
    '/category/update_category'=>"no|POST",// 修改艺术类别

    '/course_coupon/add_course_coupon'=>"no|POST",// 添加约课券
    '/course_coupon/delete_course_coupon'=>"no|POST",// 删除约课券
    '/course_coupon/search_course_coupon'=>"no|ALL",// 查询约课券
    '/course_coupon/update_course_coupon'=>"no|POST",// 修改约课券

    '/discounts_activity/add_discounts_activity_content'=>"no|POST",// 添加优惠活动内容
    '/discounts_activity/delete_discounts_activity_content'=>"no|POST",// 删除优惠活动内容
    '/discounts_activity/update_discounts_activity_content'=>"no|POST",// 修改优惠活动内容
    '/discounts_activity/search_discounts_activity_content'=>"no|ALL",// 查询优惠活动内容

    '/discounts_activity/add_discounts_activity_user'=>"no|POST",// 添加机构优惠活动参与用户
    '/discounts_activity/delete_discounts_activity_user'=>"no|POST",// 删除机构优惠活动参与用户
    '/discounts_activity/update_discounts_activity_user'=>"no|POST",// 修改机构优惠活动参与用户
    '/discounts_activity/search_discounts_activity_user'=>"no|ALL",// 查询机构优惠活动参与用户
    
    '/exchange_coupon/add_exchange_coupon' =>"no|POST",//添加兑换券
    '/exchange_coupon/delete_exchange_coupon' =>"no|POST",//删除兑换券
    '/exchange_coupon/update_exchange_coupon' =>"no|POST",//修改兑换券
    '/exchange_coupon/search_exchange_coupon' =>"no|ALL",//查询兑换券
    
    '/intelligent_user/search_intelligent_user' =>"no|ALL",//查询达人用户
    '/intelligent_user/add_intelligent_user' =>"no|POST",//添加达人用户
    '/intelligent_user/update_intelligent_user' =>"no|POST",//修改达人用户

    
    '/withdrawal/add_withdrawal'=>"no|POST",//添加提现信息
    '/withdrawal/delete_withdrawal'=>"no|POST",//删除提现信息
    '/withdrawal/update_withdrawal'=>"no|POST",//修改提现信息
    '/withdrawal/search_withdrawal'=>"no|POST",//查询提现信息
    
    '/withdrawal/withdrawal_balance'=>"no|POST",//获取机构的可体现余额
    '/withdrawal/add_withdrawal_record'=>"no|POST",//增加提现信息
    '/withdrawal/delete_withdrawal_record'=>"no|POST",//删除提现信息
    '/withdrawal/search_withdrawal_record'=>"no|ALL",//查询提现信息

    '/forum/add_activity'=>"no|POST",//增加论坛活动信息
    '/forum/delete_activity'=>"no|POST",//删除论坛活动信息
    '/forum/search_activity'=>"no|ALL",//查询论坛活动信息
    '/forum/update_activity'=>"no|POST",//修改论坛活动信息

    '/forum/search_forum_invite_content'=>"no|ALL",//查询问题邀请信息
    '/forum/add_forum_invite_content'=>"no|POST",//新增问题邀请信息
    '/forum/search_forum_invite_log'=>"no|ALL",//查询邀请记录
    '/forum/add_forum_invite_log'=>"no|POST",//新增邀请记录

    '/forum/add_activity_comment'=>"no|POST",//增加论坛活动评论信息
    '/forum/delete_activity_comment'=>"no|POST",//删除论坛活动评论信息
    '/forum/search_activity_comment'=>"no|ALL",//查询论坛活动评论信息
    '/forum/update_activity_comment'=>"no|POST",//更新论坛活动评论信息

    '/organization/add_organization_category'=>"no|POST",//增加论坛活动评论信息
    '/organization/update_organization_category'=>"no|POST",//修改论坛活动评论信息
    '/organization/search_organization_category'=>"no|ALL",//查询论坛活动评论信息
    '/organization/delete_organization_category'=>"no|POST",//删除论坛活动评论信息

    '/platform_coupon/add_platform_coupon'=>"no|POST",       //增加平台优惠券
    '/platform_coupon/update_platform_coupon'=>"no|POST",    //修改平台优惠券
    '/platform_coupon/search_platform_coupon'=>"no|ALL",     //查询平台优惠券
    '/platform_coupon/delete_platform_coupon'=>"no|POST",    //删除平台优惠券
    '/platform_coupon/receive_platform_coupon'=>"no|POST",    //领取平台优惠券
    '/platform_coupon/receive_new_bag'=>"no|ALL",    //领取新手礼包
    '/platform_coupon/receive_limit_time_bag'=>"no|ALL",    //领取新手礼包

    '/forum/search_user_content'=>"no|ALL",//查询艺论坛内容列表
    '/forum/search_user_comment'=>"no|ALL",//查询艺论坛评论列表
    "/forum/search_answer_userinfo"=>"no|ALL",//查询艺论坛问题回答列表
    
    "/forum/search_answer_comment_userinfo"=>"no|ALL",//查询艺论坛问题回答的评论
    "/forum/search_master"=>"no|ALL",//查询达人信息
    "/forum/search_invite_content"=>"no|ALL",//查询邀请回答的问题信息
    "/forum/search_invite_log"=>"no|ALL",//查询邀请记录
    "/forum/search_master_user_info"=>"no|ALL",//查询达人列表
    "/organization/search_help_activity_list"=>"no|ALL",//查询助力活动参赛者得票排名
    "/vote_activity/add_vote"=>"no|POST",//投票功能接口
    "/wechatpay/update_order_info"=>"no|POST", //支付成功后更新状态
    "/user/user_platform_course_coupon"=>"no|ALL", //查询用户获得的平台约课券
    "/user/user_platform_cash_coupon"=>"no|ALL",   //查询用户获得的平台代金券
    "/order/order_sum"=>"no|POST",                  //财务管理订单总额
    "/order/order_no_pay"=>"no|POST",                  //财务管理订单总额
  

    "/order/submit_apply_refund"=>"no|POST",      //退款申请提交
    "/order/order_yes_validation"=>"no|POST",                  //财务管理已验证的订单
    "/order/order_no_validation"=>"no|POST",                  //财务管理未验证的订单
    "/order/search_order_refund"=>"no|ALL",                  //查询退款订单信息
    "/order/submit_comment"=>"no|POST",                  //提交对订单的评论接口
    "/forum/search_forum_user_activity_info"=>"no|ALL",           //提交对订单的评论接口
    "/organization_master/search_order_by_time"=>"no|POST",      //查询机构后台订单列表加时间条件
    "/sms/search_sms"=>"no|ALL",                                    //查询短信验证码
    "/organization/search_vote_join_user_info"=>"no|ALL",       //机构端查询参加投票活动的参加人列表

    "/organization_master/refund_order_list"=>"no|ALL",          //查询短信验证码

    "/organization/search_name_or_address_or_tags"=>"no|ALL",   //模糊查询机构信息（通过名称，地址，标签类别）
    "/forum/help_activity_join_user"=>"no|POST",   //机构活动阅读助力  转发助力接口
    "/user/search_organization_course_comment"=>"no|ALL",         //查询用户评论机构课程的评论列表
    "/user/search_question_answer"=>"no|ALL",         //查询用户论坛互动（机构课程）
    "/user/search_organization_activity_comment"=>"no|ALL",         //查询用户论坛互动(机构活动)
    "/organization_master/search_invite_count"=>"no|ALL",         //查询机构被邀请的总数
    "/organization/discounts_activity_join_free"=>"no|ALL",         //查询机构被邀请的总数
    "/vote_activity/search_vote_num"=>"no|POST",                      //查询某个投票活动多少人给我投过票
    "/organization/browse_count_setInc"=>"no|POST",                //活动浏览数加1
    "/user/add_appointment_record"=>"no|POST",                      //添加预约课程记录
    "/organization/search_appointment_record"=>"no|ALL",                    //查询预约课程记录
    "/user/update_appointment_record"=>"no|POST",                    //查询预约课程记录
    "/user/search_user_cash_coupon"=>"no|ALL",                      //查询自己拥有的代金券
    //"/organization/search_appointment_record"=>"no|ALL",                    //查询预约课程记录
    "/order/search_user_order"=>"no|ALL",                    //查询用户订单
    "/organization/search_redbag_cash_coupon"=>"no|ALL",                    //红包活动
    "/organization_master/search_template_scence"=>"no|ALL",                    //查询场景
    "/organization/search_organization_location_user"=>"no|ALL",                    //查询机构信息（用户 区域名称 视图）
    "/organization/search_organization_location"=>"no|ALL",                    //查询机构信息（用户 区域名称 视图）
    "/organization/search_near_organization"=>"no|ALL",                    //查询附近机构  （组合）
    "/organization/search_organization_activity_discounts_price"=>"no|ALL",                    //查询促销小于一百元
    "/organization_video/search_organization_video_discuss_fix_user"=>"no|ALL",                    //查询视频课程评论记录与用户表视图
    "/organization_video/search_organization_video_buy_record_fix_user"=>"no|ALL",                    //查询视频课程购买记录与用户表视图

    //营销招生主表的增删改查
    "/organization/search_market_main"=>"no|ALL",  //查询营销招生主表
    //营销招生附件表增删改查
    //"/organization/search_enclosure"=>"no|ALL",  //查询营销招生附件表
    //营销招生营销内参增删改查
    "/organization/search_reference"=>"no|ALL", //查询营销内参
    //活动优惠券记录
    "/organization_master/add_activity_coupon"=>"no|POST",
    "/organization_master/delete_activity_coupon"=>"no|POST",
    "/organization_master/search_activity_coupon"=>"no|ALL",
    "/organization_master/search_coupon_activity"=>"no|ALL",
    
    //活动浏览记录
    "/user/add_activity_browse_record"=>"no|POST",
    "/user/update_activity_browse_record"=>"no|POST",
    "/user/search_activity_browse_record"=>"no|ALL",
    "/user/delete_activity_browse_record"=>"no|POST",
    //查询机构管理员
    "/user/searchOrganizationUser"=>"no|ALL",
    "/organization_master/searchUserInformation"=>"no|ALL",
    "/organization/add_user_information"=>"no|POST",
    //添加微传单内容
    "/organization/add_leaflet_activity_content"=>"no|POST",
    "/organization/search_leaflet_activity_content"=>"no|ALL",
    "/organization/delete_leaflet_activity_content"=>"no|ALL",

    '/organization_video/add_organization_video_content'=>"no|POST",//增加视频课程内容表
    '/organization_video/delete_organization_video_content'=>"no|POST",//删除视频课程内容表
    '/organization_video/search_organization_video_content'=>"no|ALL",//查询视频课程内容表
    '/organization_video/update_organization_video_content'=>"no|POST",//更新视频课程内容表

    '/organization_video/add_organization_video_discuss'=>"no|POST",//增加视频课程评论表
    '/organization_video/delete_organization_video_discuss'=>"no|POST",//删除视频课程评论表
    '/organization_video/search_organization_video_discuss'=>"no|ALL",//查询视频课程评论表
    '/organization_video/update_organization_video_discuss'=>"no|POST",//更新视频课程评论表

    '/organization_video/add_organization_video_series'=>"no|POST",//增加视频课程评论表
    '/organization_video/delete_organization_video_series'=>"no|POST",//删除视频课程评论表
    '/organization_video/search_organization_video_series'=>"no|ALL",//查询视频课程评论表
    '/organization_video/update_organization_video_series'=>"no|POST",//更新视频课程评论表
    
    /*******仲成飞 -结束*********/


    /**********  ice_start  **********/

    "/user/search" => "no|ALL",                                             //用户信息查询
    "/user/delete" => "no|POST",                                            //用户删除
    "/user/update" => "no|POST",                                            //用户信息更新
    "/user/update_user" => "no|POST",                                      //用户信息更新
    "/user/add" => "no|POST",                                               //新增用户

    "/user/add_cash_coupon" => "no|POST",                                   //用户新增代金券
    "/user/search_cash_coupon" => "no|ALL",                                 //用户代金券查询
    "/user/delete_cash_coupon" => "no|POST",                                //用户代金券删除
    "/user/update_cash_coupon" => "no|POST",                                //用户代金券更新

    "/user/add_collect" => "no|POST",                                       //添加收藏
    "/user/search_collect" => "no|ALL",                                     //收藏查询
    "/user/delete_collect" => "no|POST",                                    //删除收藏

    "/user/add_course_coupon" => "no|POST",                                 //约课卡券新增
    "/user/delete_course_coupon" => "no|POST",                              //约课卡券删除
    "/user/search_course_coupon" => "no|ALL",                               //用户约课卡券查询
    "/user/update_course_coupon" => "no|POST",                              //约课卡券更新

    "/user/add_exchange_coupon" => "no|POST",                               //添加兑换券
    "/user/delete_exchange_coupon" => "no|POST",                           //删除兑换券
    "/user/search_exchange_coupon" => "no|ALL",                            //查询兑换券
    "/user/update_exchange_coupon" => "no|POST",                           //更新兑换券

    "/user/add_token" => "no|POST",                                         //新增token
    "/user/search_token" => "no|POST",                                      //查询token

    "/user/add_bank_card" => "no|POST",                                     //添加银行卡
    "/user/delete_bank_card" => "no|POST",                                  //删除银行卡
    "/user/search_bank_card" => "no|ALL",                                   //查询银行卡
    "/user/update_bank_card" => "no|POST",                                  //更新银行卡

    "/user/add_user_browse_record" => "no|POST",                            //添加收藏记录
    "/user/delete_user_browse_record" => "no|POST",                         //删除收藏记录
    "/user/search_user_browse_record" => "no|ALL",                          //查询收藏记录
    "/user/update_user_browse_record" => "no|POST",                         //更新收藏记录

    "/user/add_organization_concern" => "no|POST",                          //添加用户关注机构
    "/user/delete_organization_concern" => "no|POST",                       //删除用户关注机构
    "/user/search_organization_concern" => "no|ALL",                        //查询用户关注机构

    "/user/add_organization_images" => "no|POST",                           //添加机构轮播
    "/user/delete_organization_images" => "no|POST",                        //删除机构轮播
    "/user/search_organization_images" => "no|ALL",                         //查询机构轮播
    "/user/update_organization_images" => "no|POST",                        //更新机构轮播

    "/user/add_teacher_info" => "no|POST",                                  //添加身份为教师的用户信息
    "/user/delete_teacher_info" => "no|POST",                               //删除身份为教师的用户信息
    "/user/search_teacher_info" => "no|ALL",                                //查询身份为教师的用户信息
    "/user/update_teacher_info" => "no|POST",                               //更新身份为教师的用户信息

    "/organization/add_organization_activity" => "no|POST",                 //机构活动添加
    "/organization/delete_organization_activity" => "no|POST",             //机构活动删除
    "/organization/search_organization_activity" => "no|ALL",              //机构活动查询
    "/organization/update_organization_activity" => "no|POST",             //机构活动信息更新

    "/organization/search_organization_activity_info" => "no|ALL",         //机构活动查询带机构坐标
    "/organization/search_organization_activity_near_info" => "no|ALL",   //机构活动查询附近的机构


    "/organization/add_activity_comment" => "no|POST",                     //机构活动回复添加
    "/organization/delete_activity_comment" => "no|POST",                 //机构活动回复删除
    "/organization/search_activity_comment" => "no|ALL",                  //机构活动回复查询
    "/organization/update_activity_comment" => "no|POST",                 //机构活动回复信息更新

    "/organization/add_organization_banner" => "no|POST",                 //机构广告添加
    "/organization/delete_organization_banner" => "no|POST",              //机构广告删除
    "/organization/search_organization_banner" => "no|ALL",               //机构广告查询
    "/organization/update_organization_banner" => "no|POST",              //机构广告信息更新

    "/organization/add_organization_comment" => "no|POST",                //机构评论添加
    "/organization/delete_organization_comment" => "no|POST",             //机构评论删除
    "/organization/search_organization_comment" => "no|ALL",              //机构评论查询
    "/organization/update_organization_comment" => "no|POST",             //机构评论更新

    //view_start
    "/organization/search_art_organization_comment_info" => "no|ALL",   //机构评论回复带评论者回复者信息
    "/user/search_art_user_user_teacher_info" => "no|ALL",               //用户身份为教师带教师信息
    "/forum/search_forum_activity_comment" => "no|ALL",                   //查询论坛活动评论
    "/forum/search_forum_activity_join_user" => "no|ALL",                 //论坛活动参与者用户信息
    //view_end

    "/organization/add_organization_course" => "no|POST",                //机构课程添加
    "/organization/delete_organization_course" => "no|POST",             //机构课程删除
    "/organization/search_organization_course" => "no|ALL",              //机构课程查询
    "/organization/search_organization_course_info" => "no|ALL",              //机构课程查询(带机构信息)

    "/organization/update_organization_course" => "no|POST",             //机构课程更新

    "/organization/add_organization_photo" => "no|POST",                  //机构相册添加
    "/organization/delete_organization_photo" => "no|POST",               //机构相册删除
    "/organization/search_organization_photo" => "no|ALL",                //机构相册查询
    "/organization/update_organization_photo" => "no|POST",               //机构相册更新

    "/location/search" => "no|ALL",                                         //省市区查询
    "/location/add" => "no|POST",                                           //省市区添加
    "/location/delete" => "no|POST",                                        //省市区删除
    "/location/update" => "no|POST",                                        //省市区更新

    /*423.pm*/
    "/forum/add_content_zan_record" => "no|POST",                           //添加论坛内容点赞记录
    "/forum/delete_content_zan_record" => "no|POST",                        //删除论坛内容点赞记录
    "/forum/search_content_zan_record" => "no|ALL",                         //查询论坛内容点赞记录
    "/forum/update_content_zan_record" => "no|POST",                        //更新论坛内容点赞记录

    "/forum/add_content_share_record" => "no|POST",                         //添加论坛内容转发记录
    "/forum/delete_content_share_record" => "no|POST",                      //删除论坛内容转发记录
    "/forum/search_content_share_record" => "no|ALL",                       //查询论坛内容转发记录

    "/forum/add_content_comment_zan_record" => "no|POST",                   //添加论坛文章点赞记录
    "/forum/delete_content_comment_zan_record" => "no|POST",               //删除论坛文章点赞记录
    "/forum/search_content_comment_zan_record" => "no|ALL",                //查询论坛文章点赞记录
    "/forum/update_content_comment_zan_record" => "no|POST",               //更新论坛文章点赞记录
    
    "/forum/add_forum_content_comment" => "no|POST",                        //添加论坛内容文章评论
    "/forum/delete_forum_content_comment" => "no|POST",                     //删除论坛内容文章评论
    "/forum/search_forum_content_comment" => "no|ALL",                      //查询论坛内容文章评论
    "/forum/update_forum_content_comment" => "no|POST",                     //更新论坛内容文章评论

    "/forum/add_content_answer_zan_record" => "no|POST",                    //添加论坛回答点赞记录
    "/forum/delete_content_answer_zan_record" => "no|POST",                 //删除论坛回答点赞记录
    "/forum/search_content_answer_zan_record" => "no|ALL",                  //查询论坛回答点赞记录
    "/forum/update_content_answer_zan_record" => "no|POST",                  //更新论坛回答点赞记录

    "/forum/add_content_answer_share_record" => "no|POST",                  //添加论坛回答转发记录
    "/forum/delete_content_answer_share_record" => "no|POST",               //删除论坛回答转发记录
    "/forum/search_content_answer_share_record" => "no|ALL",                //查询论坛回答转发记录

    "/forum/add_activity_zan_record" => "no|POST",                            //添加论坛活动点赞记录
    "/forum/delete_activity_zan_record" => "no|POST",                         //删除论坛活动点赞记录
    "/forum/search_activity_zan_record" => "no|ALL",                          //查询论坛活动点赞记录
    "/forum/update_activity_zan_record" => "no|POST",                         //更新论坛活动点赞记录

    "/forum/add_content_answer" => "no|POST",                                 //添加论坛内容问复
    "/forum/delete_content_answer" => "no|POST",                              //删除论坛内容问复
    "/forum/search_content_answer" => "no|ALL",                               //查询论坛内容问复
    "/forum/update_content_answer" => "no|POST",                              //更新论坛内容问复

    "/forum/add_activity_comment_zan_record" => "no|POST",                   //添加论坛评论点赞记录
    "/forum/delete_activity_comment_zan_record" => "no|POST",                //删除论坛评论点赞记录
    "/forum/search_activity_comment_zan_record" => "no|ALL",                 //查询论坛评论点赞记录
    "/forum/update_activity_comment_zan_record" => "no|POST",                //更新论坛评论点赞记录

    "/prize/add_prize" => "no|POST",                                            //添加奖品
    "/prize/delete_prize" => "no|POST",                                         //删除奖品
    "/prize/search_prize" => "no|ALL",                                          //查询奖品
    "/prize/update_prize" => "no|POST",                                         //更新奖品

    "/video/add_video_course" => "no|POST",                                     //添加视频课
    "/video/delete_video_course" => "no|POST",                                  //删除视频课
    "/video/search_video_course" => "no|ALL",                                   //查询视频课
    "/video/update_video_course" => "no|POST",                                  //更新视频课

    "/video/add_video_course_list" => "no|POST",                                //添加视频课视频
    "/video/delete_video_course_list" => "no|POST",                            //删除视频课视频
    "/video/search_video_course_list" => "no|ALL",                             //查询视频课视频
    "/video/update_video_course_list" => "no|POST",                            //更新视频课视频

    "/video/add_video_course_comment" => "no|POST",                            //添加视频课评论
    "/video/delete_video_course_comment" => "no|POST",                        //删除视频课评论
    "/video/search_video_course_comment" => "no|ALL",                         //查询视频课评论
    "/video/update_video_course_comment" => "no|POST",                        //更新视频课评论


    "/order/add_order" => "no|POST",                                            //生成订单
    "/order/delete_order" => "no|POST",                                         //删除订单
    "/order/search_order" => "no|ALL",                                          //查询订单
    "/order/update_order" => "no|POST",                                         //更新订单

    "/order/add_verification_code" => "no|POST",                               //生成消费码
    "/order/delete_verification_code" => "no|POST",                            //删除消费码
    "/order/search_verification_code" => "no|ALL",                             //查询消费码
    "/order/update_verification_code" => "no|POST",                            //更新消费码


    "/forum/search_activity_user_join" => "no|ALL",                            //查询论坛活动参与用户
    "/forum/user_integral_ranking" => "no|ALL",                                 //查询论坛活动用户积分排名
    "/forum/user_forum_activity_integral_detail" => "no|ALL",                 //查询参与机构活动用户获取积分详情
    "/forum/forum_activity_helper_info" => "no|ALL",                           //查询参与机构活动用户助力者信息
    "/organization/bulk_purchase_activity_info" => "no|ALL",                  //查询机构拼团活动信息
    "/organization/activity_comment_info" => "no|ALL",                         //查询机构拼团活动评论信息
    "/organization/bulk_purchase_join_user" => "no|ALL",                       //查询机构拼团活动参团信息
    "/organization/organization_acivity_orgnization" => "no|ALL",             //查询机构拼团活动关联机构信息
    "/organization/organization_activity_detail_info" => "no|ALL",            //查询机构拼团活动详细信息
    "/organization/organization_video_course" => "no|ALL",                     //查询机构视频课
    "/organization/question_organization_answer" => "no|ALL",                  //查询机构被邀请回答的问题
    "/organization/invite_log_user" => "no|ALL",                                //查询邀请机构回答的用户信息


    "/organization/video_course_comment_user" => "no|ALL",                     //查询视频课回复信息
    "/organization/art_content_answer" => "no|ALL",                             //查询文章机构回复数量
    "/cash_coupon/coupon_of_user" => "no|ALL",                                  //查询用户所持代金券
    "/order/search_order_verification" => "no|ALL",                             //查询订单以及消费码信息
    "/user/user_organization_collect" => "no|ALL",                              //用户收藏机构查询
    "/user/user_organization_activity_collect" => "no|ALL",                    //用户收藏机构活动查询
    "/user/organization_activity_collect" => "no|ALL",                          //用户收藏机构活动带区域查询
    "/user/forum_content_user_collect" => "no|ALL",                             //用户收藏论坛文章带用户信息
    "/user/forum_activity_collect" => "no|ALL",                                 //用户收藏论坛活动
    "/user/user_cash_coupon" => "no|ALL",                                       //用户所持代金券带机构信息
    "/user/user_course_coupon" => "no|ALL",                                     //用户所持约课券带机构信息
    "/user/user_exchange_coupon" => "no|ALL",                                   //用户所持兑换券带机构信息
    "/user/user_browse_course" => "no|ALL",                                     //用户浏览课程记录带机构信息
    "/user/user_browse_activity" => "no|ALL",                                   //用户浏览活动记录带机构信息

    "/organization/add_help_activity_sign_up" => "no|POST",                     //机构助力活动报名添加
    "/organization/delete_help_activity_sign_up" => "no|POST",                 //机构助力活动报名删除
    "/organization/search_help_activity_sign_up" => "no|ALL",                  //机构助力活动报名查询
    "/organization/update_help_activity_sign_up" => "no|POST",                 //机构助力活动报名信息更新

    "/organization/add_help_activity_help_record" => "no|POST",                //机构助力活动助力记录添加
    "/organization/delete_help_activity_help_record" => "no|POST",            //机构助力活动助力记录删除
    "/organization/search_help_activity_help_record" => "no|ALL",             //机构助力活动助力记录查询
    "/organization/update_help_activity_help_record" => "no|POST",            //机构助力活动助力记录信息更新
    //
    "/organization/add_organization_finance_info" => "no|POST",                //添加机构财务信息
    "/organization/delete_organization_finance_info" => "no|POST",            //删除机构财务信息
    "/organization/search_organization_finance_info" => "no|ALL",             //查询机构财务信息
    "/organization/update_organization_finance_info" => "no|POST",            //更新机构财务信息



    "/forum/update_forum_recommend" => "no|POST",                              //论坛活动评论更新

    "/bargain/organization_bargain_list" => "no|ALL",                           //机构砍价活动列表

    "/forum/add_content_answer_comment_zan_record"=>"no|POST",                //添加问题回答评论点赞记录
    "/forum/delete_content_answer_comment_zan_record"=>"no|POST",             //删除问题回答评论点赞记录
    "/forum/search_content_answer_comment_zan_record"=>"no|ALL",              //查询问题回答评论点赞记录
    "/forum/update_content_answer_comment_zan_record"=>"no|POST",             //更新问题回答评论点赞记录

    "/platform/add_platform_notification" => "no|POST",                        //添加平台消息通知
    "/platform/delete_platform_notification" => "no|POST",                     //删除平台消息通知
    "/platform/search_platform_notification" => "no|ALL",                      //查询平台消息通知
    "/platform/update_platform_notification" => "no|POST",                     //更新平台消息通知

    "/organization/add_organization_course_content" => "no|POST",             //添加课程内容
    "/organization/delete_organization_course_content" => "no|POST",          //删除课程内容
    "/organization/search_organization_course_content" => "no|ALL",           //查询课程内容
    "/organization/update_organization_course_content" => "no|POST",          //更新课程内容

    "/forum/add_question_concern" => "no|POST",                                  //添加论坛问题关注记录
    "/forum/delete_question_concern" => "no|POST",                               //删除论坛问题关注记录
    "/forum/search_question_concern" => "no|ALL",                                //查询论坛问题关注记录
    "/forum/update_question_concern" => "no|POST",                               //更新论坛问题关注记录

    "/forum/forum_question_invite" => "no|ALL",                                  //机构后台邀请回答问题列表
    "/forum/forum_question_invite_user" => "no|ALL",                            //机构后台邀请回答者

    "/organization_master/order_verification" => "no|POST",                     //验证订单更新订单状态
    "/organization_master/order_refund" => "no|POST",                           //订单退款审核
    "/organization_master/agree_refund_order" => "no|POST",                           //机构审核成功

    "/organization/discounts_activity_join" => "no|POST",                       //促销活动参加
    "/organization/bulk_purchase_create_group" => "no|POST",                    //拼团活动开团
    "/organization/bulk_purchase_activity_join" => "no|POST",                   //拼团活动参团
    "/organization/bulk_purchase_activity_fast_join" => "no|POST",                   //一键拼团参加
    "/organization/bulk_purchase_activity_solitaier_join" => "no|POST",                   //参与拼团接龙

    "/organization/bulk_purchase_activity_join_info" => "no|POST",  //查询拼团用户（包含用户信息,和卷码信息）
    "/organization/bulk_purchase_activity_join_user" => "no|POST",  //查询拼团用户（包含用户信息）

    "/order/add_consume_num" => "no|POST",                                       //生成消费码
    "/order/delete_consume_num" => "no|POST",                                    //删除消费码
    "/order/search_consume_num" => "no|ALL",                                     //查询消费码
    "/order/update_consume_num" => "no|POST",                                    //更新消费码

    "/order/add_order_refund" => "no|POST",                                      //生成退款信息
    "/order/delete_order_refund" => "no|POST",                                   //删除退款信息
    "/order/update_order_refund" => "no|POST",                                   //更新退款信息

    "/bulk_purchase/bulk_purchase_join_member"  => "no|ALL",                   //查询拼团活动配置名额


    "/bulk_purchase/bulk_purchase_order"  => "no|ALL",                          //查询拼团订单

    "/forum/update_article_browse_times"  => "no|ALL",                          //更新文章浏览数


    "/organization/add_organization_comment_zan_record" => "no|POST",        //添加课程评论点赞记录
    "/organization/delete_organization_comment_zan_record" => "no|POST",     //删除课程评论点赞记录
    "/organization/search_organization_comment_zan_record" => "no|ALL",      //查询课程评论点赞记录
    "/organization/update_organization_comment_zan_record" => "no|POST",     //更新课程评论点赞记录

    "/forum/add_master_concern" => "no|POST",                                   //添加关注达人记录
    "/forum/delete_master_concern" => "no|POST",                                //删除关注达人记录
    "/forum/search_master_concern" => "no|ALL",                                 //查询关注达人记录
    "/forum/update_master_concern" => "no|POST",                                //更新关注达人记录

    "/user/update_collect" => "no|POST",                                        //更新收藏

    "/user/update_organization_concern" => "no|POST",                          //更新用户关注机构

    "/forum/update_master" => "no|POST",                                        //更新关注达人记录

    "/forum/search_content_and_organization" => "no|ALL",                     //查询论坛内容带机构信息

    "/forum/forum_activity_fix_organization" => "no|ALL",                     //查询论坛活动带机构信息

    '/organization/add_organization_view_message_record'=>"no|POST",        //增加机构查看平台消息记录
    '/organization/update_organization_view_message_record'=>"no|POST",     //修改机构查看平台消息记录
    '/organization/search_organization_view_message_record'=>"no|ALL",      //查询机构查看平台消息记录
    '/organization/delete_organization_view_message_record'=>"no|POST",     //删除机构查看平台消息记录

    '/user/add_organization_course_browsing_history'=>"no|POST",            //增加用户浏览课程记录
    '/user/update_organization_course_browsing_history'=>"no|POST",         //修改用户浏览课程记录
    '/user/search_organization_course_browsing_history'=>"no|ALL",          //查询用户浏览课程记录
    '/user/delete_organization_course_browsing_history'=>"no|POST",         //删除用户浏览课程记录

    '/user/add_organization_school_browsing_history'=>"no|POST",            //增加用户浏览校区记录
    '/user/update_organization_school_browsing_history'=>"no|POST",         //修改用户浏览校区记录
    '/user/search_organization_school_browsing_history'=>"no|ALL",          //查询用户浏览校区记录
    '/user/delete_organization_school_browsing_history'=>"no|POST",         //删除用户浏览校区记录

    '/user/add_organization_activity_browsing_history'=>"no|POST",          //增加用户浏览活动记录
    '/user/update_organization_activity_browsing_history'=>"no|POST",       //修改用户浏览活动记录
    '/user/search_organization_activity_browsing_history'=>"no|ALL",        //查询用户浏览活动记录
    '/user/delete_organization_activity_browsing_history'=>"no|POST",       //删除用户浏览活动记录

    "/forum/forum_search_by_yourself" => "no|POST",                             //论坛搜索
    "/forum/forum_search_by_yourself_heat" => "no|POST",                       //论坛搜索页热门搜索

    "/forum/art_invite_log_content" => "no|ALL",                                //邀请记录关联内容

    "/order/balance_refund" => "no|POST",                                       //余额退款

    "/organization/add_activity_comment_zan" => "no|POST",                     //机构活动评论点赞添加
    "/organization/delete_activity_comment_zan" => "no|POST",                 //机构活动评论点赞删除
    "/organization/search_activity_comment_zan" => "no|ALL",                  //机构活动评论点赞查询
    "/organization/update_activity_comment_zan" => "no|POST",                 //机构活动评论点赞信息更新

    "/forum/search_forum_content_answer_user" => "no|ALL",                    //查询用户所回答的论坛问题信息
    "/forum/forum_question_answer_user_collect" => "no|ALL",                  //查询用户所收藏的回答
    "/forum/forum_content_category_detail" => "no|ALL",                        //查询用户发表文章分类详情


    "/forum/invite_answer" => "no|POST",                                         //查询用户发表文章分类详情
    "/forum/search_master_info" => "no|ALL",                                     //查询达人信息
    "/order/order_fix_refund" => "no|ALL",                                       //查询退款订单信息

    '/forum/add_forum_activity_appointment'=>"no|POST",                        //添加活动预约记录
    '/forum/update_forum_activity_appointment'=>"no|POST",                     //修改活动预约记录
    '/forum/search_forum_activity_appointment'=>"no|ALL",                      //查询活动预约记录
    '/forum/delete_forum_activity_appointment'=>"no|POST",                     //删除活动预约记录

    '/organization/add_organization_user'=>"no|POST",                           //添加机构管理员
    '/organization/update_organization_user'=>"no|POST",                        //修改机构管理员
    '/organization/search_organization_user'=>"no|ALL",                         //查询机构管理员
    '/organization/delete_organization_user'=>"no|POST",                        //删除机构管理员

    '/organization/search_discounts_activity'=>"no|ALL",                        //查询促销活动

    '/forum/aa_test_skip_and_limit' => "no|ALL",                                 //分页查询测试
    '/forum/history_activity' => "no|ALL",                                        //历史活动
    '/organization/search_order_fix_consume' => "no|ALL",                       //查询订单+验证码视图
    '/organization/search_organization_user_fix_user' => "no|ALL",             //查询机构用户+用户表视图

    '/withdrawal/search_withdrawal_info_by_different_ways' => "no|ALL",        //查询机构提现信息
    '/order/search_order_by_different_ways' => "no|ALL",                        //查询订单列表
    '/forum/search_question_fix_user' => "no|ALL",                              //查询问题表+用户表art_question_fix_user
    '/forum/search_question_fix_user_push' => "no|ALL",                         //查询问题表+用户表art_question_fix_user_push
    '/forum/search_invite_organization' => "no|ALL",                            //查询/搜索机构列表
    '/forum/search_question_by_user' => "no|ALL",                                //用户查询问题
    '/forum/search_question_concern_fix_content' => "no|ALL",                   //用户关注问题

    '/organization/video_course_fix_organization' => "no|ALL",                   //视频课表+机构表（机构视频课列表）
    '/organization/search_organization_activity_only' => "no|ALL",                //查询机构活动
    //    '/excel_update/search_aa' => "no|ALL",                   //查询aa表数据
//        '/excel_update/search_aaupload_organization' => "no|ALL",                   //查询aa表数据
//    '/excel_update/update_ab' => "no|POST",                   //更新ab表数据

    '/oaDemand/add_demand' => "no|POST",                                            //添加需求
    '/oaDemand/delete_demand' => "no|POST",                                         //删除需求
    '/oaDemand/search_demand' => "no|ALL",                                         //查询需求
    '/oaDemand/update_demand' => "no|POST",                                         //更新需求

    '/oaDemand/add_demand_distribution' => "no|POST",                              //添加分配
    '/oaDemand/delete_demand_distribution' => "no|POST",                           //删除分配
    '/oaDemand/search_demand_distribution' => "no|ALL",                           //查询分配
    '/oaDemand/update_demand_distribution' => "no|POST",                           //更新分配

    '/oaDemand/add_demand_remarks' => "no|POST",                                     //添加备注
    '/oaDemand/delete_demand_remarks' => "no|POST",                                //删除备注
    '/oaDemand/search_demand_remarks' => "no|ALL",                                 //查询备注
    '/oaDemand/update_demand_remarks' => "no|POST",                                //更新备注

    '/oaDemand/add_demand_working_hours' => "no|POST",                             //添加工作时间
    '/oaDemand/delete_demand_working_hours' => "no|POST",                          //删除工作时间
    '/oaDemand/search_demand_working_hours' => "no|ALL",                           //查询工作时间
    '/oaDemand/update_demand_working_hours' => "no|POST",                          //更新工作时间


    '/oaDemand/add_demand_enclosure' => "no|POST",                                  //添加附件
    '/oaDemand/delete_demand_enclosure' => "no|POST",                               //删除附件
    '/oaDemand/search_demand_enclosure' => "no|ALL",                                //查询附件
    '/oaDemand/update_demand_enclosure' => "no|POST",                               //更新附件

    '/oaDemand/add_demand_remarks_enclosure' => "no|POST",                         //添加备注附件
    '/oaDemand/delete_demand_remarks_enclosure' => "no|POST",                      //删除备注附件
    '/oaDemand/search_demand_remarks_enclosure' => "no|ALL",                       //查询备注附件
    '/oaDemand/update_demand_remarks_enclosure' => "no|POST",                      //更新备注附件

    '/oaUser/search_user' => "no|ALL",                                              //查询员工
    '/oaUser/delete_user' => "no|POST",                                              //删除员工
    '/oaUser/update_user' => "no|POST",                                              //更新员工

    '/oaDemand/oa_demand_fix_user' => "no|ALL",                                    //需求表关联用户表
    '/oaDemand/oa_demand_distribution_fix_user' => "no|ALL",                      //分配表关联用户表

    '/oaDemand/search_history_demand' => "no|ALL",                                 //查询历史需求
    '/oaDemand/oa_demand_distribution_fix_demand' => "no|ALL",                   //分配表关联需求表
    '/oaDemand/oa_demand_remarks_fix_user' => "no|ALL",                         //备注表关联用户表

    "/organization/add_activity_music" => "no|POST",                               //添加活动音乐
    "/organization/delete_activity_music" => "no|POST",                            //删除活动音乐
    "/organization/search_activity_music" => "no|ALL",                             //查询活动音乐
    "/organization/update_activity_music" => "no|POST",                            //更新活动音乐

    "/organization/add_activity_skin" => "no|POST",                               //添加活动皮肤
    "/organization/delete_activity_skin" => "no|POST",                            //删除活动皮肤
    "/organization/search_activity_skin" => "no|ALL",                             //查询活动皮肤
    "/organization/update_activity_skin" => "no|POST",                            //更新活动皮肤
    
    "/organization/add_activity_cover" => "no|ALL",                            //添加活动封面图
    "/organization/search_activity_cover" => "no|ALL",                            //查询活动封面图
    "/organization/delete_activity_cover" => "no|ALL",                            //删除活动封面图
    "/organization/update_activity_cover" => "no|ALL",                            //更新活动封面图
    "/organization/bulk_purchase_sign" => "no|ALL",                            //拼团报名成员

    "/organization/add_organization_tools_apply" => "no|POST",                //添加机构工具申请使用记录
    "/organization/delete_organization_tools_apply" => "no|POST",             //删除机构工具申请使用记录
    "/organization/search_organization_tools_apply" => "no|ALL",              //查询机构工具申请使用记录
    "/organization/update_organization_tools_apply" => "no|POST",             //更新机构工具申请使用记录

    "/message/add_message_push_record" => "no|POST",                            //添加消息推送记录
    "/message/delete_message_push_record" => "no|POST",                         //删除消息推送记录
    "/message/search_message_push_record" => "no|ALL",                          //查询消息推送记录

    "/message/message_push_record_fix_user" => "no|ALL",                         //消息推送记录+user

    "/organization/add_organization_activity_browse_record" => "no|POST",      //添加活动浏览记录
    "/organization/delete_organization_activity_browse_record" => "no|POST",   //删除活动浏览记录
    "/organization/search_organization_activity_browse_record" => "no|ALL",    //查询活动浏览记录
    "/organization/update_organization_activity_browse_record" => "no|POST",   //更新活动浏览记录

    "/organization/add_organization_message_push_record" => "no|POST",          //添加消息推送记录
    "/organization/delete_organization_message_push_record" => "no|POST",       //删除消息推送记录
    "/organization/search_organization_message_push_record" => "no|ALL",        //查询消息推送记录

    "/organization/search_activity_browse_record_fix_user" => "no|ALL",        //查询消息推送记录+user

    "/organization_video/add_organization_video" => "no|POST",                  //添加视频课
    "/organization_video/delete_organization_video" => "no|POST",               //删除视频课
    "/organization_video/search_organization_video" => "no|ALL",                //查询视频课
    "/organization_video/update_organization_video" => "no|POST",               //更新视频课

    "/organization_video/add_organization_video_buy_record" => "no|POST",                  //添加视频课购买记录
    "/organization_video/delete_organization_video_buy_record" => "no|POST",               //删除视频课购买记录
    "/organization_video/search_organization_video_buy_record" => "no|ALL",                //查询视频课购买记录
    "/organization_video/update_organization_video_buy_record" => "no|POST",               //更新视频课购买记录

    "/product_version/search" => "no|ALL",                //查询产品版本信息
    /**********  ice_end  **********/
    
    
    /**********  臧 **********/
    "/product_record/searchProductRecord" => "no|ALL",                     //查询机构购买产品记录
  
    /**********  臧 **********/


);


















