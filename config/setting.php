<?php
//应用配置文件
require_once("./config/appConfig.php");

//核心文件错误消息配置
require_once("./config/message/errorMessage.php");

//工具类错误消息配置
require_once("./config/message/toolMessage.php");

//接口级权限配置文件
require_once("./config/interfacePrivilege.php");

//接口参数验证配置文件
require_once("./config/interfaceParamsValidation.php");

//正则配置
require_once("./config/regexpConfig.php");

//三方配置文件
require_once("./config/third/thirdConfig.php");

//导入接口白名单配置文件
require_once("./config/interfaceWhitelists.php");