<?php
namespace tool\NetworkClass;
use coreLib\ErrorClass\Error;
use coreLib\ResponseClass\Response;

class Network {

	public  $rsData;   //请求结果
	public  $curl;	   //网络请求对象

	function __construct(){

		//判断是否开启了CURL模块
		if(!extension_loaded('curl')){
			$response->requestData(false,$GLOBALS['toolErrorMsg']['curlInstallError']);
		}

		//初始化操作对象
		$this->curl   = curl_init();
		
   		//设置头文件的信息作为数据流输出
    	curl_setopt($this->curl, CURLOPT_HEADER, 0);

    	//设置获取的信息以文件流的形式返回，而不是直接输出。
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

    	//不输出BODY体的信息
    	// curl_setopt($this->curl, CURLOPT_NOBODY, 1);

	}

	/*
	 * GET请求
	 */
	public function GET($url,$returnType="json"){

		//判断URL格式是否正确
		if(!$this->checkUrl($url)){
			$response->requestData(false,$GLOBALS['toolErrorMsg']['curlUrlError']);
		}
	
		//设置地址
		curl_setopt($this->curl, CURLOPT_URL, $url);

		$this->rsData = curl_exec($this->curl);

		if($returnType == "json"){
    		return $this->rsData;
    	}else{
    		return $this->rsData;
    	}
	}

	/*
	 * POST请求
	 */
	public function POST($url,$data,$returnType="json"){

		//判断URL格式是否正确
		if(!$this->checkUrl($url)){
		   $response->requestData(false,$GLOBALS['toolErrorMsg']['curlUrlError']);
		}

		//设置地址
		curl_setopt($this->curl, CURLOPT_URL, $url);

		//设置post方式提交
    	@curl_setopt($this->curl, CURLOPT_POST, 1);

    	//设置参数
    	@curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);

    	$this->rsData = curl_exec($this->curl);

    	if($returnType == "json"){
    		return json_decode($this->rsData,true);
    	}else{
    		return $this->rsData;
    	}
		
	}



	/*
	 * 验证请求方式是否正确
	 */
	protected function checkUrl($url){
		$pattern='/^(http|https):\/\/[a-zA-Z0-9]*/'; 
		if (!@preg_match($pattern,$url)) {
			return false;
		}
		return true;
	}


	/*
	 * 析构函数
	 */
	function __destruct(){

		//关闭URL请求
    	curl_close($this->curl);
	}


}

?>