<?php
namespace tool\UploadClass;
use coreLib\ErrorClass\Error;

//导入图片处理类库
require_once("tool/Image.class.php");

/*
 * 文件上传类
 */

class Upload {
 
	private $file;						//上传的文件对象


	public  $fileType;					//文件上传的类型
	public  $uploadError;				//错误返回结果
	public  $uploadResult = array();   	//最终结果返回
	public  $isCut;						//是否需要剪裁
	public  $cutData;					//剪裁值

	function __construct($fileType,$file,$isCut,$cutData){
		
		$this->fileType = $fileType;
		$this->file  	= $file;
		$this->isCut 	= $isCut;
		$this->cutData  = $cutData;

	}

	/*
	 * 执行上传操作	
	 */
	public function uploadAction(){

		//判断是否存在需要上传的文件对象
		if(!$this->isExistsFile()){
		   return false;	
		}

		//检查文件大小
		if(!$this->isMaxSize()){
		   return false;
		}

		//检查文件类型
		if(!$this->isFileType()){
		   return false;	
		}

		//将文件上传值指定的上传目录
		if(!$this->moveUpload()){
		   return false;
		}



		return true;

	}

	/*
	 * 判断是否存在需要上传的文件
	 */
	protected function isExistsFile(){

		//判断是否为合法请求
		if($_SERVER['REQUEST_METHOD'] != 'POST'){  

		   //判断是否存在该上传文件	
		   if (!is_uploaded_file($this->file["file"]['tmp_name'])){
		   	   $this->uploadError = $GLOBALS['toolErrorMsg']['uploadMethodError'];
		   	   return false;
		   }
		}

		return true;

	}

	/*
	 * 检查文件大小
	 */
	protected function isMaxSize(){

		$uploadMaxSize = $GLOBALS['settings']['fileUpload']['maxSize'];
		$fileSize      = $this->file['file']['size'];
		
		if($uploadMaxSize < $fileSize){
		   $this->uploadError = $GLOBALS['toolErrorMsg']['uploadFileMaxSizeError'];	
		   return false;
		}

		return true;
	}

	/*
	 * 检查文件类型
	 */
	protected function isFileType(){

		//判断此处为图像上传还是文件上传
		if($this->fileType == 'IMAGE'){

			$uploadFileType = $GLOBALS['settings']['fileUpload']['fileType'];

		 	if(!in_array($this->file['file']['type'], $uploadFileType)){
		 		$this->uploadError = $GLOBALS['toolErrorMsg']['uploadFileTypeError'];	
		 		return false;
		 	}

		 	return true;

		}else if($this->fileType == 'FILE'){
		 	return true;
		} 
	}

	/*
	 * 将文件上传值指定的上传目录
	 */
	protected function moveUpload(){

		//文件上传临时目录
		$rootUploadDir = $GLOBALS['settings']['fileUpload']['uploadTempPath'];

		//检查上传目录是否有写入权限
		if(!is_writable($rootUploadDir)){
			$this->uploadError = $GLOBALS['toolErrorMsg']['writeDirError'];	
			return false;
		}
		//获取文件后缀
		$pathInfo = pathinfo($this->file['file']['name']);  
	    $fileType   = $pathInfo['extension'];  

	    //当前的时间戳
	    $nowTime = time();

	    //生成随机六位数字符串
	    $randStr = rand_str();

	    //完整文件名
	    $newFileName = $nowTime."-".$randStr.".".$fileType;

	    //新生成的文件名
	    $newFilePath = $rootUploadDir ."/".$newFileName;

	    if(!move_uploaded_file($this->file['file']['tmp_name'],$newFilePath)){  
        	$this->uploadError = $GLOBALS['toolErrorMsg']['uploadError'];	
       	 	return false;
    	}  

    	//查看是否需要剪裁图片
    	if($this->isCut){

    	  $x = intval($this->cutData['x']);
    	  $y = intval($this->cutData['y']);
    	  $w = intval($this->cutData['w']);
    	  $h = intval($this->cutData['h']);

    	  //实例化缩略图生成类
		  $t = new \ThumbHandler();

		  //生成裁剪后图
		  $t->setSrcImg($newFilePath);
		  $t->setCutType(2);				//指明为手工裁切
		  $t->setSrcCutPosition($x, $y);	//源图起点坐标
		  $t->setRectangleCut($w, $h);		//裁切尺寸
		  $t->setDstImg($newFilePath);
		  $t->createImg($w,$h);

    	}

    	//上传成功后保存上传成功后的文件名
    	$this->uploadResult['original'] = $newFilePath;
		

		return true;
	}


}
