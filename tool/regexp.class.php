<?php
namespace tool\RegexpClass;
use coreLib\ErrorClass\Error;
use coreLib\ResponseClass\Response;

/*
 * 正则验证
 */

class Regexp {

	//验证restful参数格式是否正确
	function restFulV($response,$route,$paramName,$rule){

		//取出正则验证规则
   		$pattern 	 = $GLOBALS['regexpSettings'][$rule];
   		$paramsValue = $route->restfulParams[$paramName];

   		if($pattern && $paramsValue){
   			if(!preg_match($pattern, $paramsValue)){
				  $response->responseData(false,"参数:".$paramName."格式不正确");
			   }
   		}
   		return $paramsValue;
	}

	//验证body参数格式是否正确
	function bodyV($response,$route,$paramName,$rule){

		//取出正则验证规则
   		$pattern 	 = $GLOBALS['regexpSettings'][$rule];
   		$paramsValue = $route->bodyParams[$paramName];


   		if($pattern && $paramsValue){
   			if(!preg_match($pattern, $paramsValue)){
				  $response->responseData(false,"参数:".$paramName."格式不正确");
			   }
   		}

   		return $paramsValue;

	}

	//验证图片类型数据
	function imageV($response,$route,$paramName){

		//获取图片数据路径
		$paramsValue = $route->bodyParams[$paramName];
			
		//$response->responseData(false,"参数:".$paramsValue);
		
		//默认返回结果
		$imageResult = array(
			"tempImagePath"   	=> "",
		 	"normalImagePath" 	=> "",
		 	"originalImagePath" => "",
		 	"thumbImagePath"    => ""  
		);

		//如果未更新图片信息，直接返回默认数组内容
		if(!$paramsValue){
			return $imageResult;
		}
		
		

		//判断该图片是否存在于服务器中
		if(!file_exists($paramsValue)){
		   	$response->responseData(false,"参数:".$paramName."图片不存在".$paramsValue);
		}

		//获取文件最终存放路径
	  	$rootUploadDir = $GLOBALS['settings']['fileUpload']['uploadPath'];

	  	//获取当前的年月日
	  	$nowYMD = date("Y/m/d"); 	

	  	//获取文件名
	  	$fileName = basename($paramsValue);		

	  	//拼接最终文件路径名
	 	$newFilePath = $rootUploadDir."/".$nowYMD."/";

	 	//返回最终各个类型文件图片存放路径
	 	$imageResult['tempImagePath'] 	  = $paramsValue;
	 	$imageResult['normalImagePath']   = $newFilePath."normal_".$fileName;
	 	$imageResult['originalImagePath'] = $newFilePath."original_".$fileName;
	 	$imageResult['thumbImagePath'] 	  = $newFilePath."thumb_".$fileName;

	 	//返回最终结果
	 	return $imageResult;


	}


	//验证普通文件类型数据
	function fileV($response,$route,$paramName){

		//获取文件数据路径
		$paramsValue = $route->bodyParams[$paramName];

		if($paramsValue) {

			//默认返回结果
			$fileResult = array(
				"tempImagePath" => "",
			 	"filePath" 		=> ""
			);


			//判断该文件是否存在于服务器中
			if(!file_exists($paramsValue)){
			   	$response->responseData(false,"参数:".$paramName."文件不存在");
			}

			//获取文件最终存放路径
		  	$rootUploadDir = $GLOBALS['settings']['fileUpload']['uploadPath'];

		  	//获取当前的年月日
		  	$nowYMD = date("Y/m/d"); 	

		  	//获取文件名
		  	$fileName = basename($paramsValue);		

		  	//拼接最终文件路径名
		 	$newFilePath = $rootUploadDir."/".$nowYMD."/";

		 	//返回最终各个类型文件图片存放路径
		 	$fileResult['tempImagePath'] = $paramsValue;
		 	$fileResult['filePath'] 	 = $newFilePath.$fileName;

		 	//返回最终结果
		 	return $fileResult;

		}
	}

}
?>