<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:19
 * name:机构砍价活动配置更新
 * url:/bargain/update_bargain_activity_config
 */

//获取参数
$aocu_id       = $route->bodyParams["aocu_id"];  //主键ID

//更新条件
$whereArr = [
    "aocu_id" => $aocu_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_consult_user",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );