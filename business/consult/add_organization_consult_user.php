<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 11:33
 * name:机构砍价活动配置添加
 * url:/bargain/add_bargain_activity_config
 */

$aocu_name    = $route->bodyParams['aocu_name'];                                 //机构名称
$aocu_type    = $route->bodyParams['aocu_type'];                                 //机构类型
$aocu_member  = $route->bodyParams['aocu_member'];                               //联系人名称
$aocu_phone   = $route->bodyParams['aocu_phone'];                                //联系电话
$aocu_address = $route->bodyParams['aocu_address'];                              //联系地址

//写入数组
$insertArr = [
    'aocu_name'        => $aocu_name,
    'aocu_type'        => $aocu_type,
    'aocu_member'      => $aocu_member,
    'aocu_phone'       => $aocu_phone,
    'aocu_address'     => $aocu_address,
    'aocu_create_time' => time(),

];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_consult_user",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );