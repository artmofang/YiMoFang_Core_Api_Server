<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:15
 * name:机构砍价活动配置删除
 * url:/bargain/delete_bargain_activity_config
 */

//获取参数
$aocu_id = $route->bodyParams['aocu_id'];//主键ID

//拼接删除条件
$whereArr = [
    'aocu_id'=>$aocu_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("organization_consult_user",$whereArr,false);

//返回结果
$response->responseData(true,$reData);