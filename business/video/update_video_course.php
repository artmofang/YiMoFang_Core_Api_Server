<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 10:05
 * name:更新视频课
 * url:/video/update_video_course
 */

//获取参数

$vc_id       = $route->bodyParams["vc_id"];  //主键ID

//更新条件
$whereArr = [
    "vc_id" => $vc_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("video_course",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );