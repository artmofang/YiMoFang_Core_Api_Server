<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 11:11
 * name:添加视频课评论
 * url:/video/add_video_course_comment
 */

//获取参数
$vcm_uid                = $route->bodyParams["vcm_uid"];                                            //评论用户ID
$vcm_comment_level      = $route->bodyParams["vcm_comment_level"];                                 //星评
$vcm_comment_content    = $route->bodyParams["vcm_comment_content"];                               //评论内容

//写入数组
$insertArr = [
    "vcm_uid"                 => $vcm_uid,
    "vcm_comment_level"      => $vcm_comment_level,
    "vcm_comment_content"    => $vcm_comment_content,
    "vcm_comment_time"       => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("video_course_comment",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );

