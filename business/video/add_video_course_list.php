<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 10:58
 * name:添加视频课视频
 * url:/video/add_video_course_list
 */

//获取参数
$vcl_vc_id             = $route->bodyParams["vcl_vc_id"];                                     //关联视频课ID
$vcl_video             = $route->bodyParams["vcl_video"];                                     //视频地址
$vcl_video_tittle      = $route->bodyParams["vcl_video_tittle"];                             //视频标题
$vcl_video_time        = $regexpObj->bodyV($response,$route,'vcl_video_time','NORMAL');      //视频时长


//写入数组
$insertArr = [
    "vcl_vc_id"            => $vcl_vc_id,
    "vcl_video"            => $vcl_video,
    "vcl_video_tittle"    => $vcl_video_tittle,
    "vcl_video_time"      => $vcl_video_time,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("video_course_list",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );