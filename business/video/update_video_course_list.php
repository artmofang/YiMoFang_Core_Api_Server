<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 11:05
 * name:更新视频课视频
 * url:/video/update_video_course_list
 */

//获取参数

$vcl_id       = $route->bodyParams["vcl_id"];  //主键ID

//更新条件
$whereArr = [
    "vcl_id" => $vcl_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("video_course_list",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );