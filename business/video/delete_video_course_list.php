<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 11:02
 * name:删除视频课视频
 * url:/video/delete_video_course_list
 */

//获取参数

$vcl_id = $route->bodyParams["vcl_id"];  //主键ID

$whereArr = [
    "vcl_id" => $vcl_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("video_course_list",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );