<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 11:45
 * name:更新视频课评论
 * url:/video/update_video_course_comment
 */

//获取参数

$vcm_id       = $route->bodyParams["vcm_id"];  //主键ID

//更新条件
$whereArr = [
    "vcm_id" => $vcm_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("video_course_comment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );