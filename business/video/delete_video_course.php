<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 9:59
 * name:删除视频课
 * url:/video/delete_video_course
 */

//获取参数

$vc_id = $route->bodyParams["vc_id"];  //主键ID

$whereArr = [
    "vc_id" => $vc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("video_course",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );