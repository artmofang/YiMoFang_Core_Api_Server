<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/28
 * Time: 11:18
 * name:删除视频课评论
 * url:/video/delete_video_course_comment
 */

//获取参数

$vcm_id = $route->bodyParams["vcm_id"];  //主键ID

$whereArr = [
    "vcm_id" => $vcm_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("video_course_comment",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );