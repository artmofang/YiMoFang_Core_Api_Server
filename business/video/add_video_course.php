<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 19:40
 * name:添加视频课
 * url:/video/add_video_course
 */

//获取参数
$vc_tittle          = $route->bodyParams["vc_tittle"];                                      //课程标题
$vc_oid             = $route->bodyParams["vc_oid"];                                         //关联机构ID
$vc_course_detail   = $regexpObj->bodyV($response,$route,'vc_course_detail','NORMAL');      //课程详情


//写入数组
$insertArr = [
    "vc_tittle"            => $vc_tittle,
    "vc_oid"               => $vc_oid,
    "vc_course_detail"    => $vc_course_detail,
    "vc_create_time"      => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("video_course",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );