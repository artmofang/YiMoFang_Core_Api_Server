<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 20:47
 * 添加优惠活动
 */
//获取参数
$dac_old_price = $route->bodyParams['dac_old_price'];//原价
$dac_new_price = $route->bodyParams['dac_new_price'];//现价
$dac_max_count = $route->bodyParams['dac_max_count'];//最大人数
$dac_address   = $route->bodyParams['dac_address'];//地址
$dac_oaid = $route->bodyParams['dac_oaid'];//关联机构的id
$dac_coupon_type = $route->bodyParams['dac_coupon_type'];
$dac_coupon_id = $route->bodyParams['dac_coupon_id'];
$dac_prompt = $regexpObj->bodyV($response,$route,'dac_prompt','NORMAL'); //温馨提示。。。

//写入数组
$insertArr = [
    'dac_max_count'=>$dac_max_count,
    'dac_old_price'=>$dac_old_price,
    'dac_new_price'=>$dac_new_price,
    'dac_address'=>$dac_address,
    'dac_oaid'=>$dac_oaid,
    'dac_coupon_type'=>$dac_coupon_type,
    'dac_prompt'=>$dac_prompt,
    'dac_coupon_id'=>$dac_coupon_id
];

//执行写入语句
$rsData = $db->mysqlDB->insert("discounts_activity_config",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );