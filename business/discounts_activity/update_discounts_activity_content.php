<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 21:30
 * 修改优惠活动
 */
$dac_id = $route->bodyParams["dac_id"];

//更新条件
$whereArr = [
    "dac_id" => $dac_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("discounts_activity_content",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );