<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 21:51
 * 添加机构优惠活动参与用户
 * url：/discounts_activity/add_discounts_activity_user
 */
//获取参数
$dau_uid = $route->bodyParams['dau_uid'];//用户id
$dau_oaid = $route->bodyParams['dau_oaid'];//活动ID
$dau_phone = $route->bodyParams['dau_phone'];//用户电话
$dau_name = $route->bodyParams['dau_name'];//用户姓名

//写入数组
$insertArr = [
    'dau_uid'=>$dau_uid,
    'dau_oaid'=>$dau_oaid,
    'dau_phone'=>$dau_phone,
    'dau_name'=>$dau_name,
    'dau_create_time'=>time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("discounts_activity_user",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );