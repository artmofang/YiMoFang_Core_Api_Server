<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 21:42\
 * 查询优惠活动信息
 */
//获取字段筛选参数
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){
    
    unset($route->restfulParams['count']);
    
    //获取到记录的总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count('discounts_activity_config');
    
}

//根据相应的条件查询
$rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("discounts_activity_config");

//拼接获得数据
$rs = array( "count" => $sum , "data" => $rsData );


//返回成功结果
$response->responseData( true, $rs );