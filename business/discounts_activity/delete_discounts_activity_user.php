<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 21:58
 * 删除机构优惠活动参与用户
 */
$dau_id = $route->bodyParams['dau_id'];

//拼接删除条件
$whereArr = [
    'dau_id'=>$dau_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("discounts_activity_user",$whereArr,false);

//返回结果
$response->responseData(true,$reData);