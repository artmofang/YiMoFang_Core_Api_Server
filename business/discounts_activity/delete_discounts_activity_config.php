<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 21:27
 * 删除优惠活动
 */
$dac_id = $route->bodyParams['dac_id'];

//拼接删除条件
$whereArr = [
    'dac_id'=>$dac_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("discounts_activity_config",$whereArr,false);

//返回结果
$response->responseData(true,$reData);