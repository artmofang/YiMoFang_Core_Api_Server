<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 20:47
 * 添加优惠活动
 */
//获取参数
$dac_oaid = $route->bodyParams['dac_oaid'];//关联的活动ID
$dac_type = $route->bodyParams['dac_type'];//类型
$dac_order = $route->bodyParams['dac_order'];//排序
$dac_content = $route->bodyParams['dac_content'];//内容

//写入数组
$insertArr = [
    'dac_oaid'=>$dac_oaid,
    'dac_type'=>$dac_type,
    'dac_order'=>$dac_order,
    'dac_content'=>$dac_content
];

//执行写入语句
$rsData = $db->mysqlDB->insert("discounts_activity_content",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );