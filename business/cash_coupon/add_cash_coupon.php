<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 16:39
 */
//获取参数
$cc_name = $route->bodyParams['cc_name'];//代金券名称
$cc_price = $route->bodyParams['cc_price'];//代金券价格
$cc_start_time = $route->bodyParams['cc_start_time'];//代金券有效期开始时间
$cc_end_time = $route->bodyParams['cc_end_time'];//代金券有效期结束时间
$cc_oid = $route->bodyParams['cc_oid'];//所属机构id
$cc_type = $route->bodyParams['cc_type'];//代金卷使用范围 0-通用 1-机构课程代金卷 2-机构活动代金卷

$cc_max_count = $regexpObj->bodyV($response,$route,'cc_max_count','NUMBER');//最大领取数
$cc_fid  = $regexpObj->bodyV($response,$route,'cc_fid','NORMAL');//如果type类型为1或2，fid为相应的主键ID
$cc_max_price  = $regexpObj->bodyV($response,$route,'cc_max_price','NORMAL');//满多少钱可使用，0为不限制
$cc_desc  = $regexpObj->bodyV($response,$route,'cc_desc','NORMAL');//劵说明
$cc_create_time = time();//创建时间


//写入数组
$insertArr = [
    'cc_name'=>$cc_name,
    'cc_price'=>$cc_price,
    'cc_start_time'=>$cc_start_time,
    'cc_end_time'=>$cc_end_time,
    'cc_oid'=>$cc_oid,
    'cc_type'=>$cc_type,
    'cc_max_count'=>$cc_max_count,
    'cc_received_count'=>$cc_received_count,
    'cc_fid'=>$cc_fid,
    'cc_used_count'=>$cc_used_count,
    'cc_max_price'=>$cc_max_price,
    'cc_desc'=>$cc_desc,
    'cc_create_time'=>$cc_create_time,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("cash_coupon",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );