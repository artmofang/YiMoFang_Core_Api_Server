<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 18:06
 */
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){
    
    unset($route->restfulParams['count']);
    
    //获取到记录的总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count('cash_coupon');
    
}


if(isset($route->restfulParams['is_all'])){
    unset($route->restfulParams['is_all']);
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("cash_coupon");
    $rs = array( "count" => $sum , "data" => $rsData );
}else{
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("cash_coupon");
    $arr = array();
    for($i = 0;$i<count($rsData);$i++){
        if($rsData[$i]["cc_end_time"]>time()){
            array_push($arr,$rsData[$i]);
        }
    }
    $rs = array( "count" => $sum , "data" => $arr );
}
//根据相应的条件查询


//拼接获得数据



//返回成功结果
$response->responseData( true, $rs );