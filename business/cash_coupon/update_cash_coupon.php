<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 18:48
 */
$cc_id = $route->bodyParams["cc_id"];

//更新条件
$whereArr = [
    "cc_id" => $cc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("cash_coupon",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );