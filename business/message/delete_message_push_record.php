<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/10
 * Time: 10:07
 * name:删除消息推送记录
 * url:/message/delete_message_push_record
 */

//获取参数
$mpr_id = $route->bodyParams['mpr_id'];

//拼接删除条件
$whereArr = [
    'mpr_id'=>$mpr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_message_push_record",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);