<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/10
 * Time: 9:51
 * name：添加消息推送记录
 * url:/message/add_message_push_record
 */

//获取参数
$mpr_message = $route->bodyParams['mpr_message'];//消息内容
$mpr_sum = $route->bodyParams['mpr_sum'];//发送总次数
$mpr_arrive = $route->bodyParams['mpr_arrive'];//发送成功次数
$mpr_fail = $route->bodyParams['mpr_fail'];//发送失败次数
$mpr_title = $route->bodyParams['mpr_title'];//标题
$mpr_user = $route->bodyParams['mpr_user'];//操作者id

//写入数组
$insertArr = [
    "mpr_message"  => $mpr_message,
    "mpr_sum"      => $mpr_sum,
    "mpr_arrive"   => $mpr_arrive,
    "mpr_fail"     => $mpr_fail,
    "mpr_title"    => $mpr_title,
    "mpr_user"     => $mpr_user,
    "mpr_create_time" => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_message_push_record",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );