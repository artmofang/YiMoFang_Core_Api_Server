<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/19
 * Time: 12:01
 */

$fic_cid      = $route->bodyParams["fic_cid"];
$fic_buid  = $route->bodyParams["fic_buid"];
$fic_invited_type  = $regexpObj->bodyV($response,$route,'fic_invited_type','NORMAL');//被邀请者的类型 0-机构 1-达人

//写入数组
$insertArr = [
    "fic_cid"           => $fic_cid,
    "fic_invited_type"  => $fic_invited_type,
    "fic_buid"          => $fic_buid
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_invite_content",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );