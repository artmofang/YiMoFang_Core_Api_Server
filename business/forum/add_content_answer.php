<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 18:21
 * name:添加论坛内容问答
 * url:/forum/add_content_answer
 */

//获取参数
$fca_uid      = $route->bodyParams['fca_uid'];                               //回答人ID
$fca_content  = $route->bodyParams['fca_content'];  //回答文字内容
$fcc_fcid     = $route->bodyParams['fcc_fcid'];  //对应问题ID
$fca_images   = $regexpObj->bodyV($response,$route,'fca_images','NORMAL');   //回复的图片内容使用 | 隔开

//写入数组
$insertArr = [
    'fca_uid'          => $fca_uid,
    'fca_content'      => $fca_content,
    'fca_images'       => $fca_images,
    'fcc_fcid'         => $fcc_fcid,
    'fca_create_time'  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_answer",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );