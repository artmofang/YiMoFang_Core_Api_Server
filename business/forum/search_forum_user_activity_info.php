<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/20
 * Time: 14:35
 * 查询用户参加的活动列表 以及活动信息
 */
//获取字段筛选参数
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数


if(isset($route->restfulParams['count'])){
    
    unset($route->restfulParams['count']);
    
    //删除limit之前先行查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_forum_user_activity_info",true);
    
    //删除不能与count同时使用的条件
    unset($route->restfulParams['skip']);
    unset($route->restfulParams['limit']);
    
    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count("art_forum_user_activity_info",true);
    
    
}

//根据条件做相应查询
if(!$rsData){
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_forum_user_activity_info",true);
}
//拼接后
$rs = array( "count" => $sum , "data" => $rsData);

foreach ($rs['data'] as $k=>$v){
    $sql = "SELECT * FROM (SELECT fauj.fauj_faid,fauj.fauj_uid,fauj.fauj_integral,@rownum := @rownum + 1 AS rownum
    FROM
    (
        SELECT
            fauj_uid,
            fauj_integral,
						fauj_faid
        FROM
            `art_forum_activity_user_join`
				WHERE
						fauj_faid = ".$v['fa_id']."
        ORDER BY
            fauj_integral DESC
    ) AS fauj,
    (SELECT @rownum := 0) r) as d where fauj_uid =".$v['fauj_uid'];
    
    $re=$db->mysqlDB->query($sql);
    
    $rs['data'][$k]['rownum'] = $re[0]['rownum'];
}

//返回成功结果
$response->responseData( true, $rs );