<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/3
 * Time: 11:38
 * 查询回答列表
 */
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){
    
    unset($route->restfulParams['count']);
    
    //获取到记录的总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count('art_forum_answer_user_info',true);
    
}

//根据相应的条件查询
$rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_forum_answer_user_info",ture);

//拼接获得数据
$rs = array( "count" => $sum , "data" => $rsData );


//返回成功结果
$response->responseData( true, $rs );