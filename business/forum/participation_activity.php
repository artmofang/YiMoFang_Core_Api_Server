<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 11:58
 * 参与论坛活动
 */

//获取参数信息
$fauj_faid      = $route->bodyParams['fauj_faid'];           //论坛活动ID
$fauj_uid       = $route->bodyParams['fauj_uid'];            //参与人ID
$fauj_name      = $route->bodyParams['fauj_name'];           //参与人名字
$fauj_phone     = $route->bodyParams['fauj_phone'];          //参与人手机号码
$fauj_is_share  = $route->bodyParams['fauj_is_share'];       //是否为分享参与
$fauj_share_uid = $regexpObj->bodyV($response,$route,'fauj_share_uid','NUMBER'); //分享者ID

//构造存储过程参数
$proceArr = [
    $fauj_faid,
    $fauj_uid,
    $fauj_name,
    $fauj_phone,
    $fauj_is_share,
    setDefaultValue($fauj_share_uid,0)
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_USER_JOIN_FORUM_ACTIVITY", $proceArr);


//返回成功结果
$response->responseData( true);


