<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 15:43
 * name:更新论坛文章点赞记录
 * url:/forum/update_content_comment_zan_record
 */

$fcczr_id = $route->bodyParams["fcczr_id"];

//更新条件
$whereArr = [
    "fcczr_id" => $fcczr_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content_comment_zan_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );