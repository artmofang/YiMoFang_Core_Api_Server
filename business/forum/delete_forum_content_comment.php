<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:29
 * name:删除论坛内容文章评论
 * url:/forum/delete_forum_content_comment
 */

//获取参数

$fcc_id = $route->bodyParams["fcc_id"];  //主键ID

$whereArr = [
    "fcc_id" => $fcc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("forum_content_comment",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );