<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:18
 * url:/forum/delete_content_comment_zan_record
 * name:删除论坛文章点赞记录
 */

//获取参数值
$fcczr_id = $route->bodyParams['fcczr_id'];

//拼接删除条件
$whereArr = [
    'fcczr_id'=>$fcczr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_comment_zan_record",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);