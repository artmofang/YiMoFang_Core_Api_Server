<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/19
 * Time: 12:01
 */

$fil_uid      = $route->bodyParams["fil_uid"];//邀请人的用户id
$fil_ficid  = $route->bodyParams["fil_ficid"];//关联的邀请表的主键id
$fil_fcid  = $route->bodyParams["fil_fcid"];//问题ID
$fil_fid  = $route->bodyParams["fil_fid"];//对应被邀请者的id
$fil_invite_type  = $regexpObj->bodyV($response,$route,'fil_invite_type','NUMBER');//被邀请者的类型 0-机构 1-达人

//写入数组
$insertArr = [
    "fil_uid"  => $fil_uid,
    "fil_ficid" => $fil_ficid,
    "fil_fcid" => $fil_fcid,
    "fil_invite_type" => $fil_invite_type,
    "fil_fid" => $fil_fid,
    "fil_create_time" => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_invite_log",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );