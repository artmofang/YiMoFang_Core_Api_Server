<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 19:53
 * name:更新文章浏览数
 * url:/forum/update_article_browse_times
 */

//获取参数
$fc_id = $route->bodyParams["fc_id"];


$sql = "select fc_browse_count from art_forum_content where fc_id = $fc_id";

$rs  = $db->mysqlDB->query($sql);

$browse_times=intval($rs[0]["fc_browse_count"])+1;

//更新条件
$whereArr = [
    "fc_id" => $fc_id,
];
$updateData = [
    "fc_browse_count" => $browse_times
];
//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content",$whereArr,$updateData);

//返回成功结果
$response->responseData( true, $rsData );