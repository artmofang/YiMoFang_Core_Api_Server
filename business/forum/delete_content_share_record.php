<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:07
 * name:删除论坛内容转发记录
 * url:/forum/delete_content_share_record
 */
//获取参数值
$fcsr_id = $route->bodyParams['fcsr_id'];

//拼接删除条件
$whereArr = [
    'fcsr_id'=>$ffcsr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_share_record",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);