<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/12
 * Time: 16:59
 * name:删除论坛活动点赞记录
 * url:/forum/delete_activity_zan_record
 */

$fazr_id = $route->bodyParams['fazr_id'];

//拼接删除条件
$whereArr = [
    'fazr_id'=>$fazr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_activity_zan_record",$whereArr,false);

//返回结果
$response->responseData(true,$reData);