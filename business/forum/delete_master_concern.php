<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 22:32
 * name:删除关注达人记录
 * url:/forum/delete_master_concern
 */

//获取参数值
$mc_id = $route->bodyParams['mc_id'];

//拼接删除条件
$whereArr = [
    'mc_id'=>$mc_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("master_concern",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);