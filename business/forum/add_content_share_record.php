<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:05
 * name:添加论坛内容转发记录
 * url:/forum/add_content_share_record
 */

//获取参数
$fcsr_fcid              = $route->bodyParams["fcsr_fcid"];      //论坛文章ID
$fcsr_uid               = $route->bodyParams["fcsr_uid"];       //用户ID

//写入数组
$insertArr = [
    "fcsr_fcid"          => $fcsr_fcid,
    "fcsr_uid"           => $fcsr_uid,
    "fcsr_create_time"  => time(),

];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_share_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );