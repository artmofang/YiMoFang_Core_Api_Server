<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 14:38
 * name:更新论坛回答点赞记录
 * url:/forum/update_content_answer_zan_record
 */

$fcazr_id = $route->bodyParams["fcazr_id"];

//更新条件
$whereArr = [
    "fcazr_id" => $fcazr_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content_answer_zan_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );