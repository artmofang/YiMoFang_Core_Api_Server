<?php
//获取参数
$fc_id = $route->bodyParams["fc_id"];

//更新条件
$whereArr = [
    "fc_id" => $fc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_category",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );