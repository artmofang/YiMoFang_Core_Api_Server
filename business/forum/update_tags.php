<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 11:29
 * 更新文章标签
 */
//获取参数
$ft_id = $route->bodyParams["ft_id"];

//更新条件
$whereArr = [
    "ft_id" => $ft_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_tags",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );