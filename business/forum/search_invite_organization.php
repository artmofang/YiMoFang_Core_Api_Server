<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/6
 * Time: 16:03
 * name:查询/搜索机构列表
 * url:/forum/search_invite_organization
 */

$search = $route->bodyParams['o_name'];//机构名称

$skip = $route->bodyParams['skip'];//第几条

$limit = $route->bodyParams['limit'];//查询多少条

$fc_id = $route->bodyParams['fc_id'];

$u_id = $route->bodyParams['u_id'];

$search_condition = "LIKE '%$search%'";

if( $skip!='' && $limit!=''){

    $sql = "SELECT `o_uid` as `o_id`,`o_logo`,`o_name`,`o_desc`,`o_invite_count` FROM `art_organization` WHERE `o_name` $search_condition LIMIT $skip,$limit";

    $masterSql = "SELECT `m_uid` AS `o_id`,`u_header_url` AS `o_logo`,`m_nickname` AS `o_name`,`m_sign` AS `o_desc`,`m_invitation` AS `o_invite_count` FROM `v_art_forum_master` WHERE `m_nickname` $search_condition LIMIT $skip,$limit";

}else{

    $sql = "SELECT `o_uid` as `o_id`,`o_logo`,`o_name`,`o_desc`,`o_invite_count` FROM `art_organization` WHERE `o_name`  $search_condition LIMIT 20";

    $masterSql = "SELECT `m_uid` AS `o_id`,`u_header_url` AS `o_logo`,`m_nickname` AS `o_name`,`m_sign` AS `o_desc`,`m_invitation` AS `o_invite_count` FROM `v_art_forum_master`  WHERE `m_nickname` $search_condition LIMIT 20";
}

$re = $db->mysqlDB->query($sql);

$master_re = $db->mysqlDB->query($masterSql);


if($fc_id!="" && $u_id!=""){

    foreach ($re as $k =>$v){

        $o_id = $v['o_id'];

        $inviteInfo = "SELECT `fil_id` FROM `art_forum_invite_log` WHERE `fil_uid` = $u_id AND `fil_fcid` = $fc_id AND `fil_fid` = $o_id AND `fil_invite_type` = 0";

        $exist = $db->mysqlDB->query($inviteInfo);

        if($exist){

            $re[$k]['is_invited']= 1;

            $re[$k]['fil_invite_type']= 0;

        }else{

            $re[$k]['is_invited']= 0;

            $re[$k]['fil_invite_type']= 0;

        }
    }

    foreach ($master_re as $k =>$v){

        $o_id = $v['o_id'];

        $inviteInfo = "SELECT `fil_id` FROM `art_forum_invite_log` WHERE `fil_uid` = $u_id AND `fil_fcid` = $fc_id AND `fil_fid` = $o_id AND `fil_invite_type` = 1";

        $exist = $db->mysqlDB->query($inviteInfo);

        if($exist){

            $master_re[$k]['is_invited']= 1;

            $master_re[$k]['fil_invite_type']= 1;

        }else{

            $master_re[$k]['is_invited']= 0;

            $master_re[$k]['fil_invite_type']= 1;

        }
        array_push($re,$master_re[$k]);
    }
}

$rsData = array("data" => $re);

$response->responseData( true, $rsData );

