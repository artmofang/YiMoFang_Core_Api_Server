<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 20:41
 * name:删除论坛问题关注记录
 * url:/forum/delete_question_concern
 */

//获取参数值
$fqc_id = $route->bodyParams['fqc_id'];

//拼接删除条件
$whereArr = [
    'fqc_id'=>$fqc_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_question_concern",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);