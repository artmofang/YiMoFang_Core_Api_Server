<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/26
 * Time: 16:04
 * name:更新论坛活动评论信息
 * url:/forum/update_activity_comment
 */

//获取参数
$fac_id = $route->bodyParams["fac_id"];

//更新条件
$whereArr = [
    "fac_id" => $fac_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_activity_comment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );