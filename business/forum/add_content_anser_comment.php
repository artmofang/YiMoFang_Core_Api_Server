<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/17
 * Time: 11:19
 */
//获取参数
$fcac_fcaid             = $route->bodyParams['fcac_fcaid'];//关联内容的回答/评论的ID
$fcac_content           = $route->bodyParams['fcac_content'];//评论内容
$fcac_uid               = $route->bodyParams['fcac_uid'];//评论人id
$fcac_reply_uid         = $route->bodyParams['fcac_reply_uid'];//被回复人ID
$fcac_is_reply          = $route->bodyParams['fcac_is_reply'];//是否为一次回复
$fcac_is_author_commnet = $regexpObj->bodyV($response, $route, 'fcac_is_author_commnet', 'NUMBER');//是否为作者回复
$fcac_fid = $regexpObj->bodyV($response, $route, 'fcac_fid', 'NUMBER');
//写入数组
$insertArr = [
    'fcac_fcaid' => $fcac_fcaid,
    'fcac_content' => $fcac_content,
    'fcac_uid' => $fcac_uid,
    'fcac_reply_uid' => $fcac_reply_uid,
    'fcac_is_reply' => $fcac_is_reply,
    'fcac_is_author_commnet' => $fcac_is_author_commnet,
    'fcac_create_time ' => time(),
    'fcac_fid' => $fcac_fid,
];

//执行写入语句
$rsData = $db->mysqlDB->insert('forum_content_anser_comment', $insertArr);

//获取返回值
$response->responseData(true, $rsData);
