<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/26
 * Time: 15:50
 * name:删除论坛评论点赞记录
 * url:/forum/delete_activity_comment_zan_record
 */

$faczr_id = $route->bodyParams['faczr_id'];

//拼接删除条件
$whereArr = [
    'faczr_id'=>$faczr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_activity_comment_zan_record",$whereArr,false);

//返回结果
$response->responseData(true,$reData);