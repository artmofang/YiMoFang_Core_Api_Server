<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/15
 * Time: 22:25
 * name:更新问题回答的评论
 * url:/forum/update_content_anser_comment
 */

//获取参数
$fcac_id = $route->bodyParams["fcac_id"];

//更新条件
$whereArr = [
    "fcac_id" => $fcac_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content_anser_comment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );