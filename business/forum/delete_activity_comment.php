<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 17:39
 * 删除论坛活动评论信息
 */
$fac_id = $route->bodyParams['fac_id'];

//拼接删除条件
$whereArr = [
    'fac_id'=>$fac_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_activity_comment",$whereArr,false);

//返回结果
$response->responseData(true,$reData);
