<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/16
 * Time: 21:22
 */
$fc_fcid = $route->bodyParams['fc_fcid'];                                               //所属分类id
$fc_uid = $route->bodyParams['fc_uid'];                                                 //所属用户id
$fc_title = $route->bodyParams['fc_title'];                                             //标题
$fc_content = $regexpObj->bodyV($response,$route,'fc_content','NORMAL');                //内容
$fc_type = $route->bodyParams['fc_type'];                                               //内容类型 0-艺问答 1-谈艺谈
$fc_utype = $route->bodyParams['fc_utype'];                                               //发布文章或问题的用户类型  0 平台  1机构  2用户
$fc_is_grown = $route->bodyParams['fc_is_grown'];                                       //0-儿童 1-成人
$fc_tags = $route->bodyParams['fc_tags'];                                                //文章或问题标签
$fc_fcfid = $route->bodyParams['fc_fcfid'];                                                //所属分类的父类ID
$fc_images  = $regexpObj->bodyV($response,$route,'fc_images','NORMAL');                     //标题图片用 |隔开
$fc_create_time = time();

//写入数组
$insertArr = [
    'fc_fcid'=> $fc_fcid,
    'fc_tags'=>$fc_tags,
    'fc_uid'=>$fc_uid,
    'fc_title'=>$fc_title,
    'fc_content'=>$fc_content,
    'fc_type'=>$fc_type,
    'fc_utype'=>$fc_utype,
    'fc_images'=>$fc_images,
    'fc_fcfid'=>$fc_fcfid,
    'fc_create_time '=>$fc_create_time,
    'fc_is_grown '=>$fc_is_grown,
    'is_delete'=> $is_delete,
];

//执行写入语句
$rsData = $db->mysqlDB->insert('forum_content',$insertArr);

////返回成功结果
$response->responseData( true, $rsData );
