<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/12
 * Time: 20:54
 * name:回答评论+用户+机构表
 * url:/forum/search_comment_fix_user_fix_organization
 */

//获取字段筛选参数
$fields = $route->bodyParams['fields'];

$sum = 0;

//判断是否需要查询总记录数


if(isset($route->restfulParams['count'])){

    unset($route->restfulParams['count']);

    //删除limit之前先行查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_comment_fix_user_fix_organization",true);

    //删除不能与count同时使用的条件
    unset($route->restfulParams['skip']);
    unset($route->restfulParams['limit']);

    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count("art_comment_fix_user_fix_organization",true);

}

//根据条件做相应查询
if(!$rsData){
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_comment_fix_user_fix_organization",true);
}


//拼接后
$rs = array( "count" => $sum , "data" => $rsData);
//返回成功结果
$response->responseData( true, $rs );