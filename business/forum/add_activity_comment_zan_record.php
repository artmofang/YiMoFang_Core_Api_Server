<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/14
 * Time: 11:52
 * name:添加论坛评论点赞记录
 * url:/forum/add_activity_comment_zan_record
 */

//获取参数
$faczr_uid               = $route->bodyParams['faczr_uid'];               //点赞用户ID
$faczr_fid               = $route->bodyParams['faczr_fid'];               //被点赞用户ID

//写入数组
$insertArr = [
    'faczr_uid'                  => $faczr_uid,
    'faczr_fid'                  => $faczr_fid,
    'faczr_zan_time'            => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_activity_comment_zan_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );