<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:43
 * name：删除论坛回答点赞记录
 * url:/forum/delete_content_answer_zan_record
 */

//获取参数值
$fcazr_id = $route->bodyParams['fcazr_id'];

//拼接删除条件
$whereArr = [
    'fcazr_id'=>$fcazr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_answer_zan_record",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);