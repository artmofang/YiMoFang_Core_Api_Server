<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:40
 * name:添加论坛回答点赞记录
 * url:/forum/add_content_answer_zan_record
 */

//获取参数
$fcazr_fcaid          = $route->bodyParams["fcazr_fcaid"];              //问题回答ID
$fcazr_uid            = $route->bodyParams["fcazr_uid"];                //用户ID


//写入数组
$insertArr = [
    "fcazr_fcaid"           => $fcazr_fcaid,
    "fcazr_uid"             => $fcazr_uid,
    "fcazr_create_time"      => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_answer_zan_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );