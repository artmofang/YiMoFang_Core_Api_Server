<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/23
 * Time: 21:42
 */

//获取参数信息
$join_uid      = $route->bodyParams['join_uid'];           //参与者的id
$help_uid      = $route->bodyParams['help_uid'];          //助力者id
$activity_id   = $route->bodyParams['activity_id'];          //活动id
$type          = $route->bodyParams['type'];                //助力方式   0-阅读 1-转发

//构造存储过程参数
$proceArr = [
    $join_uid,
    $help_uid,
    $activity_id,
    $type
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_HELP_FORUM_ACTIVITY", $proceArr);


//返回成功结果
$response->responseData(true);