<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 22:29
 * name:添加关注达人记录
 * url:/forum/add_master_concern
 */

//获取参数
$mc_master_id   = $route->bodyParams["mc_master_id"];   //达人ID
$mc_uid         = $route->bodyParams["mc_uid"];         //用户ID
$mc_type        = $regexpObj->bodyV($response,$route,'mc_type','NORMAL');//活动评论数


//写入数组
$insertArr = [
    "mc_master_id"    => $mc_master_id,
    "mc_uid"           => $mc_uid,
    "mc_type"          => $mc_type,
    "mc_create_time"  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("master_concern",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );