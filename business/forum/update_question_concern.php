<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 20:43
 * name:更新论坛问题关注记录
 * url:/forum/update_question_concern
 */

$fqc_id = $route->bodyParams["fqc_id"];

//更新条件
$whereArr = [
    "fqc_id" => $fqc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_question_concern",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );