<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/30
 * Time: 21:04
 * name:论坛搜索
 * url:/forum/forum_search_by_yourself
 */
$search_aim = $route->bodyParams['search'];  //1-全部 2-机构 3-一问答 4-谈一谈

$text = $route->bodyParams['text'];  //查询条件

$page = $route->bodyParams['page'];//页数

$limit = $route->bodyParams['limit']; //每页显示条数

$u_id = $route->bodyParams['u_id']; //当前用户id

//查询是否有搜索记录

$is_raked = "SELECT  `frc_id`,`frc_rake_times` FROM `art_forum_rake_record` WHERE `frc_text` = '$text'";

$re = $db->mysqlDB->query($is_raked);

$rsData = array("data" => $re);

if ($re) {

    //有搜索记录，更新该搜索文字的搜索次数

    $frc_id = $rsData["data"][0]['frc_id'];

    $addRakeTimes = "UPDATE `art_forum_rake_record` SET `frc_rake_times` = `frc_rake_times`+1 WHERE `frc_id` = '$frc_id' ";

    $re = $db->mysqlDB->query($addRakeTimes);

} else {

    //无搜索记录，添加搜索记录

    $addRecord = "INSERT INTO `art_forum_rake_record` (`frc_text`) VALUES ('$text')";

    $re = $db->mysqlDB->query($addRecord);

}

if ($search_aim == 1) {
    //搜索全部
    if($page==0){
        $all_master_one = "SELECT  `m_id`,`u_id`,`u_type`,`u_nickname`,`u_oid`,`u_header_url`,`m_sign`  FROM `v_art_forum_search` WHERE `u_nickname` LIKE  '%$text%' LIMIT 1";
    }

    if ($page!=="" && $limit) {

        $all_forum_content = "SELECT  `fc_id`,`fc_title`,`fc_content`,`fc_images`,`fc_type`,`fc_browse_count`,`fc_answer_count`,`fc_create_time`,`fc_uid` FROM `art_forum_content` WHERE `fc_title` LIKE  '%$text%'  LIMIT $page,$limit";

    } else {

        $all_forum_content = "SELECT  `fc_id`,`fc_title`,`fc_content`,`fc_images`,`fc_type`,`fc_browse_count`,`fc_answer_count`,`fc_create_time`,`fc_uid` FROM `art_forum_content` WHERE `fc_title` LIKE  '%$text%'";

    }

    $result_master = $db->mysqlDB->query($all_master_one);

    $result_forum_content = $db->mysqlDB->query($all_forum_content);

    $masterInfo = array("all_master" => $result_master);

    $master_id = $masterInfo["all_master"][0]['m_id'];

    //查询是否已关注当前达人
    if($master_id!=''){
        $concernInfo = "SELECT  `mc_id` FROM `art_master_concern` WHERE `mc_master_id`= '$master_id' AND `mc_uid`= '$u_id'AND is_delete = 0";

        $is_concern = $db->mysqlDB->query($concernInfo);
        if (!empty($is_concern)) {

            $result_master["is_concern"] = 1;

        } else {

            $result_master["is_concern"] = 0;

        }
    }

    $rsData = array("all_master" => $result_master, "all_forum_content" => $result_forum_content);

    $response->responseData(true, $rsData);

} elseif ($search_aim == 2) {
    //搜索用户
    if ($page!=="" && $limit) {

        $master = "SELECT  `m_id`,`u_id`,`u_type`,`u_nickname`,`u_oid`,`m_sign`,`u_header_url`  FROM `v_art_forum_search` WHERE `u_nickname`  LIKE  '%$text%'  LIMIT $page,$limit";

    } else {

        $master = "SELECT  `m_id`,`u_id`,`u_type`,`u_nickname`,`u_oid`,`m_sign`,`u_header_url`  FROM `v_art_forum_search` WHERE `u_nickname`  LIKE  '%$text%'   ";

    }

    $masterResult = $db->mysqlDB->query($master);

    $masterInfo = array("all_master" => $masterResult);

    $masterId = array();

    for ($i = 0; $i < count($masterResult); $i++) {

        array_push($masterId, $masterInfo["all_master"][$i]['m_id']);

    }

    for ($i = 0; $i < count($masterId); $i++) {

        $concernInfo = "SELECT  `mc_id` FROM `art_master_concern` WHERE `mc_master_id`= '$masterId[$i]' AND `mc_uid`= '$u_id'AND `is_delete`= 0";

        $is_concern = $db->mysqlDB->query($concernInfo);

        if ($is_concern) {

            $concern = array("is_concern" => 1);

            array_push($masterInfo["all_master"][$i], $concern);

        } else {

            $concern = array("is_concern" => 0);

            array_push($masterInfo["all_master"][$i], $concern);
        }

    }

    $response->responseData(true, $masterInfo);

} else {
    if ($search_aim == 3) {
        //搜索问题
        if ($page!=="" && $limit) {

            $sql = "SELECT  `fc_id`,`fc_title`,`fc_content`,`fc_images`,`fc_type`,`fc_answer_count`,`fc_create_time`,`fc_uid` FROM `art_forum_content` WHERE `fc_type` = 0  AND `fc_title` LIKE  '%$text%'  LIMIT $page,$limit ";

        } else {

            $sql = "SELECT  `fc_id`,`fc_title`,`fc_content`,`fc_images`,`fc_type`,`fc_answer_count`,`fc_create_time`,`fc_uid` FROM `art_forum_content` WHERE `fc_type` = 0  AND `fc_title` LIKE  '%$text%' ";

        }

    } elseif ($search_aim == 4) {
        //搜索文章
        if ($page!=="" && $limit) {

            $sql = "SELECT  `fc_id`,`fc_title`,`fc_content`,`fc_images`,`fc_type`,`fc_browse_count`,`fc_create_time`,`fc_uid` FROM `art_forum_content` WHERE `fc_type` = 1  AND `fc_title` LIKE  '%$text%'  LIMIT $page,$limit ";

        } else {

            $sql = "SELECT  `fc_id`,`fc_title`,`fc_content`,`fc_images`,`fc_type`,`fc_browse_count`,`fc_create_time`,`fc_uid` FROM `art_forum_content` WHERE `fc_type` = 1  AND `fc_title` LIKE  '%$text%' ";

        }

    }

    $re = $db->mysqlDB->query($sql);

    $rsData = array("all_forum_content" => $re);

//返回成功结果
    $response->responseData(true, $rsData);
}

