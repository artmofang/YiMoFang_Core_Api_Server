<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:24
 * name:添加论坛内容文章评论
 * url:/forum/add_forum_content_comment
 */

//获取参数
$fcc_fcid               = $route->bodyParams["fcc_fcid"];                                           //所属的论坛内容ID
$fcc_content            = $route->bodyParams["fcc_content"];                                        //回复内容
$fcc_uid                = $route->bodyParams["fcc_uid"];                                            //回复人ID
$fcc_reply_uid          = $regexpObj->bodyV($response,$route,'fcc_reply_uid','NORMAL');             //被回复人ID
$fcc_fid                = $regexpObj->bodyV($response,$route,'fcc_fid','NORMAL');                    //所属的评论ID
$fcc_is_reply           = $regexpObj->bodyV($response,$route,'fcc_is_reply','NORMAL');              //是否为一次回复
$fcc_is_author_commnet  = $regexpObj->bodyV($response,$route,'fcc_is_author_commnet','NORMAL');    //是否为作者回复


//写入数组
$insertArr = [
    "fcc_fcid"                 => $fcc_fcid,
    "fcc_content"              => $fcc_content,
    "fcc_uid"                  => $fcc_uid,
    "fcc_reply_uid"            => setDefaultValue($fcc_reply_uid,0),
    "fcc_is_reply"             => setDefaultValue($fcc_is_reply,0),
    "fcc_is_author_commnet"    => setDefaultValue($fcc_is_author_commnet,0),
    "fcc_create_time"          => time(),
    "fcc_fid"                  => setDefaultValue($fcc_fid,0)
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_comment",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );