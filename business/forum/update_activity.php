<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 17:11
 * 修改艺论坛活动信息
 */
$fa_id = $route->bodyParams["fa_id"];

//更新条件
$whereArr = [
    "fa_id" => $fa_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_activity",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );