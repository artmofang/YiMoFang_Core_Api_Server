<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/19
 * Time: 11:21
 * name:添加活动预约记录
 * url:/forum/add_forum_activity_appointment
 */

//获取参数
$faa_faid       = $route->bodyParams["faa_faid"];       //活动ID
$faa_uid        = $route->bodyParams["faa_uid"];         //用户ID
$faa_phone        = $route->bodyParams["faa_phone"];    //用户电话


$route->restfulParams['is_delete'] = 0;

$sql = "SELECT * FROM art_forum_activity_appointment WHERE `faa_faid` = $faa_faid AND `faa_uid` = $faa_uid";

$is_exist = $db->mysqlDB->query($sql);
//var_dump($is_exist);
if(!$is_exist){
    //写入数组
    $insertArr = [
        "faa_faid"    => $faa_faid,
        "faa_uid"     => $faa_uid,
        "faa_phone"   => $faa_phone,
    ];

//执行写入语句
    $rsData = $db->mysqlDB->insert("forum_activity_appointment",$insertArr);

//返回成功结果
    $response->responseData( true, $rsData );

}else{

    $response->responseData( false, "您已预约过了" );

}
