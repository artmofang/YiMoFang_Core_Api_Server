<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 18:32
 * name:删除论坛内容问答
 * url:/forum/delete_content_answer
 */

//获取参数值
$fca_id = $route->bodyParams['fca_id'];//主键ID

//拼接删除条件
$whereArr = [
    'fca_id'=>$fca_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_answer",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);