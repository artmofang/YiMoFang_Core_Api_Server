<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/15
 * Time: 19:44
 * name:更新论坛内容文章评论
 * url:/forum/update_forum_content_comment
 */

$fcc_id = $route->bodyParams["fcc_id"];

//更新条件
$whereArr = [
    "fcc_id" => $fcc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content_comment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );