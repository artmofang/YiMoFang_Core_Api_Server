<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:53
 * name:删除论坛回答转发记录
 * url:/forum/delete_content_answer_share_record
 */

$fcasr_id = $route->bodyParams['fcasr_id'];

//拼接删除条件
$whereArr = [
    'fcasr_id'=>$fcasr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_answer_share_record",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);