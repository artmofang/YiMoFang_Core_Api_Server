<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 11:06
 * 添加论坛文章标签
 */
//获取参数
$ft_name             = $route->bodyParams["ft_name"];                                           //标签名称
$ft_create_time      =time();


//写入数组
$insertArr = [
    "ft_name"                 => $ft_name,
    "ft_create_time"         => $ft_create_time
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_tags",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );