<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:16
 * name:添加论坛文章点赞记录
 * url:/forum/add_content_comment_zan_record
 */

//获取参数
$fcczr_fcid              = $route->bodyParams["fcczr_fcid"];      //论坛内容ID
$fcczr_uid               = $route->bodyParams["fcczr_uid"];       //用户ID

//写入数组
$insertArr = [
    "fcczr_fcid"          => $fcczr_fcid,
    "fcczr_uid"           => $fcczr_uid,
    "fcczr_create_time"  => time(),

];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_comment_zan_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );