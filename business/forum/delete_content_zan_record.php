<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 14:49
 * name:删除论坛内容点赞记录
 * url:/forum/delete_content_zan_record
 */

//获取参数值
$fczr_id = $route->bodyParams['fczr_id'];

//拼接删除条件
$whereArr = [
    'fczr_id'=>$fczr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_zan_record",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);