<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 16:17
 * name:删除问题回答评论点赞记录
 * url:/forum/delete_content_answer_comment_zan_record
 */

//获取参数值
$fcaczr_id = $route->bodyParams['fcaczr_id'];//主键ID

//拼接删除条件
$whereArr = [
    'fcaczr_id'=>$fcaczr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_answer_comment_zan_record",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);