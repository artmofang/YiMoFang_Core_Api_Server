<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 18:38
 * name:更新论坛内容问答
 * url:/forum/update_content_answer
 */

//获取参数
$fca_id = $route->bodyParams["fca_id"];

//更新条件
$whereArr = [
    "fca_id" => $fca_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content_answer",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );