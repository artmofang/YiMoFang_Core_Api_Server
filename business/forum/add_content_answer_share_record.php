<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 17:52
 * name:添加论坛回答转发记录
 * url:/forum/add_content_answer_share_record
 */

//获取参数
$fcasr_fcaid          = $route->bodyParams["fcasr_fcaid"];              //关联论坛回答ID
$fcasr_uid            = $route->bodyParams["fcasr_uid"];                //用户ID


//写入数组
$insertArr = [
    "fcasr_fcaid"           => $fcasr_fcaid,
    "fcasr_uid"             => $fcasr_uid,
    "fcasr_create_time"    => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_answer_share_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );

