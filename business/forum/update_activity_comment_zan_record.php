<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/26
 * Time: 15:51
 * name:更新论坛评论点赞记录
 * url:/forum/update_activity_comment_zan_record
 */

//获取参数
$faczr_id = $route->bodyParams["faczr_id"];

//更新条件
$whereArr = [
    "faczr_id" => $faczr_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_activity_comment_zan_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );