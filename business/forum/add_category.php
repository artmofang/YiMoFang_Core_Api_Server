<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/16
 * Time: 20:01
 */
//获取参数
$fc_name = $route->bodyParams['fc_name'];
$fc_path = $route->bodyParams['fc_path'];
$fc_fid  = $regexpObj->bodyV($response,$route,'fc_fid','NORMAL');
$fc_icon  = $regexpObj->bodyV($response,$route,'fc_icon','NORMAL');
$fc_order = $regexpObj->bodyV($response,$route,'fc_order','NORMAL');
$fc_desc   = $regexpObj->bodyV($response,$route,'fc_desc','NORMAL');
$is_home_category   = $regexpObj->bodyV($response,$route,'is_home_category','SWITCH');//是否为首页分类
$fc_article_count   = $regexpObj->bodyV($response,$route,'fc_article_count','NORMAL');
$fc_concern_count   = $regexpObj->bodyV($response,$route,'fc_concern_count','NORMAL');
$is_delete  = $regexpObj->bodyV($response,$route,'is_delete','NORMAL');
$is_view  = $regexpObj->bodyV($response,$route,'is_view','SWITCH');

//写入数组
$insertArr = [
   'fc_name'=> $fc_name,
   'fc_path'=> $fc_path,
    'fc_fid'=> $fc_fid,
    'fc_icon'=>$fc_icon,
    'fc_order'=>$fc_order,
    'fc_desc'=>$fc_desc,
    'fc_article_count'=>$fc_article_count,
    'fc_concern_count'=>$fc_concern_count,
    'is_delete'=>$is_delete,
    'is_view'=>$is_view,
    'is_home_category'=>$is_home_category,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_category",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );