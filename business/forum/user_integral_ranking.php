<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/4
 * Time: 9:11
 * name:论坛活动用户积分的排名
 * url:/forum/user_integral_ranking
 */

$fauj_uid = $route->bodyParams['fauj_uid'];  //参与者ID

$sql = "SELECT * FROM (SELECT fauj.fauj_uid,fauj.fauj_integral,@rownum := @rownum + 1 AS rownum
FROM
    (
        SELECT
            fauj_uid,
            fauj_integral
        FROM
            `art_forum_activity_user_join` 
        ORDER BY
            fauj_integral DESC
    ) AS fauj,
    (SELECT @rownum := 0) r) as d where fauj_uid = $fauj_uid";

$re=$db->mysqlDB->query($sql);

$res=array("data"=>$re);

$response->responseData(true, $res);