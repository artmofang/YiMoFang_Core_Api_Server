<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/12
 * Time: 16:50
 * name:添加论坛活动点赞记录
 * url:/forum/add_activity_zan_record
 */

//获取参数
$fazr_faid              = $route->bodyParams["fazr_faid"];          //论坛内容ID
$fazr_uid               = $route->bodyParams["fazr_uid"];         //用户ID

//写入数组
$insertArr = [
    "fazr_faid"           => $fazr_faid,
    "fazr_uid"            => $fazr_uid,
    "fazr_create_time"   => time(),

];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_activity_zan_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );

