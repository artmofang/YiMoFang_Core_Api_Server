<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 14:36
 * name：添加论坛内容点赞记录
 * url:/forum/add_content_zan_record
 */

//获取参数
$fczr_fcid              = $route->bodyParams["fczr_fcid"];      //论坛内容
$fczr_uid               = $route->bodyParams["fczr_uid"];       //用户ID

//写入数组
$insertArr = [
    "fczr_fcid"          => $fczr_fcid,
    "fczr_uid"           => $fczr_uid,
    "fczr_create_time"  => time(),

];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_zan_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );