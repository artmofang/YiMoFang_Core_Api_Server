<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 17:16
 * 添加论坛活动评论信息
 */
//获取参数
$fac_faid               = $route->bodyParams['fac_faid'];//论坛活动id
$fac_uid                = $route->bodyParams['fac_uid'];//评论人id
$fac_content            = $route->bodyParams['fac_content'];//评论内容
$fac_reply_uid          = $route->bodyParams['fac_reply_uid'];//被回复人的id
$fac_is_reply           = $route->bodyParams['fac_is_reply'];//是否为一次回复 0否 1是

$fac_create_time        = time ();//创建时间
if($fac_is_reply==1){
    $fac_fid            = $route->bodyParams['fac_fid'];//回复评论的父ID
}else{
    $fac_fid            = $regexpObj->bodyV($response,$route,'fac_fid','NUMBER');
}


//写入数组
$insertArr = [
    'fac_faid'                  => $fac_faid,
    'fac_uid'                   => $fac_uid,
    'fac_content'               => $fac_content,
    'fac_reply_uid'             => $fac_reply_uid,
    'fac_is_reply'              => $fac_is_reply,
    'fac_create_time'           => $fac_create_time,
    "fac_fid"                    =>$fac_fid
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_activity_comment",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );