<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 16:21
 * name:更新问题回答评论点赞记录
 * url:/forum/update_content_answer_comment_zan_record
 */

$fcaczr_id = $route->bodyParams["fcaczr_id"];

//更新条件
$whereArr = [
    "fcaczr_id" => $fcaczr_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content_answer_comment_zan_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );