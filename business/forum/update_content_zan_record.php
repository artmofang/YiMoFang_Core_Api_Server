<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 15:25
 * name:更新论坛内容点赞记录
 * url:/forum/update_content_zan_record
 */

$fczr_id = $route->bodyParams["fczr_id"];

//更新条件
$whereArr = [
    "fczr_id" => $fczr_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_content_zan_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );