<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 17:06
 * 删除艺论坛活动信息
 */
$fa_id = $route->bodyParams['fa_id'];

//拼接删除条件
$whereArr = [
    'fa_id'=>$fa_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_activity",$whereArr,false);

//返回结果
$response->responseData(true,$reData);