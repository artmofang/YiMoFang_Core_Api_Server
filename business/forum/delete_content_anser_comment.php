<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/17
 * Time: 11:53
 */
//获取参数值
$fcac_id = $route->bodyParams['fcac_id'];

//拼接删除条件
$whereArr = [
    'fcac_id'=>$fcac_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("forum_content_anser_comment",$whereArr,false);

//返回结果
$response->responseData(true,$rsData);