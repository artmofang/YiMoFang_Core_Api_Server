<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 20:36
 * name:添加论坛问题关注记录
 * url:/forum/add_question_concern
 */

//获取参数
$fqc_fcid = $route->bodyParams["fqc_fcid"];   //问题ID
$fqc_uid  = $route->bodyParams["fqc_uid"];    //用户ID


//写入数组
$insertArr = [
    "fqc_fcid"          => $fqc_fcid,
    "fqc_uid"           => $fqc_uid,
    "fqc_create_time"  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_question_concern",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );