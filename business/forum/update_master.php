<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 22:53
 * name:更新达人信息
 * url:/forum/update_master
 */

$m_uid = $route->bodyParams["m_uid"];

//更新条件
$whereArr = [
    "m_uid" => $m_uid
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_master",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );