<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 22:34
 * name:更新关注达人记录
 * url:/forum/update_master_concern
 */

$mc_id = $route->bodyParams["mc_id"];

//更新条件
$whereArr = [
    "mc_id" => $mc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("master_concern",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );