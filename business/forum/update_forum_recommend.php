<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/14
 * Time: 11:33
 * name:论坛活动评论更新
 * url:/forum/update_forum_recommend
 */

$fac_id = $route->bodyParams["fac_id"];

//更新条件
$whereArr = [
    "fac_id" => $fac_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_activity_comment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );