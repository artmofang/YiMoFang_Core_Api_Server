<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/19
 * Time: 11:45
 * name:修改活动预约记录
 * url:/forum/update_forum_activity_appointment
 */

$faa_id = $route->bodyParams["faa_id"];

//更新条件
$whereArr = [
    "faa_id" => $faa_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("forum_activity_appointment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );