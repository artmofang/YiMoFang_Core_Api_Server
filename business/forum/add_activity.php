<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 11:58
 * 添加艺论坛活动信息
 */
//获取参数
$fa_title = $route->bodyParams['fa_title'];//活动标题
$fa_content = $route->bodyParams['fa_content'];//活动内容
$fa_start_time = $route->bodyParams['fa_start_time'];//活动开始时间
$fa_end_time = $route->bodyParams['fa_end_time'];//活动结束时间
$fa_type = $route->bodyParams['fa_type'];//活动的发布人类型 0-平台 1-机构等等
$fa_fid = $route->bodyParams['fa_fid'];//'如果非平台发布的活动，则关联机构或用户的ID
$fa_price = $route->bodyParams['fa_price'];//'活动参与的价格
$fa_rule = $route->bodyParams['fa_rule'];//'活动玩法 0-参与有奖 1-热度得奖
$fa_read_score = $route->bodyParams['fa_read_score'];//'阅读一次所加分数
$fa_share_score = $route->bodyParams['fa_share_score'];//'转发一次所获得的分数
$fa_join_score = $route->bodyParams['fa_join_score'];//'参与一次所获得的分数

$fa_browse_count = $regexpObj->bodyV($response,$route,'fa_browse_count','NUMBER');//活动浏览数
$fa_share_count  = $regexpObj->bodyV($response,$route,'fa_share_count','NUMBER');//活动转发数
$fa_join_count  = $regexpObj->bodyV($response,$route,'fa_join_count','NUMBER');//活动参与数
$fa_collect_count = $regexpObj->bodyV($response,$route,'fa_collect_count','NORMAL');//活动收藏数
$fa_comment_count = $regexpObj->bodyV($response,$route,'fa_comment_count','NORMAL');//活动评论数
$fa_create_time = time ();//创建时间

//写入数组
$insertArr = [
    'fa_title'=> $fa_title,
    'fa_content'=> $fa_content,
    'fa_start_time'=> $fa_start_time,
    'fa_end_time'=> $fa_end_time,
    'fa_type'=> $fa_type,
    'fa_fid'=> $fa_fid,
    'fa_price'=> $fa_price,
    'fa_rule'=> $fa_rule,
    'fa_read_score'=> $fa_read_score,
    'fa_share_score'=> $fa_share_score,
    'fa_join_score'=> $fa_join_score,
    'fa_join_count'=> $fa_join_count,
    'fa_browse_count'=> $fa_browse_count,
    'fa_share_count'=> $fa_share_count,
    'fa_collect_count'=> $fa_collect_count,
    'fa_comment_count'=> $fa_comment_count,
    'fa_create_time'=> $fa_create_time,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_activity",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );