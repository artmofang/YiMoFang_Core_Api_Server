<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 16:13
 * name:添加问题回答评论点赞记录
 * url:/forum/add_content_answer_comment_zan_record
 */

//获取参数
$fcaczr_fcacid      = $route->bodyParams['fcaczr_fcacid'];                       //关联问题回答的回复ID
$fcaczr_uid         = $route->bodyParams['fcaczr_uid'];                          //点赞用户ID

//写入数组
$insertArr = [
    'fcaczr_fcacid'        => $fcaczr_fcacid,
    'fcaczr_uid'           => $fcaczr_uid,
    'fcaczr_create_time'  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("forum_content_answer_comment_zan_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );