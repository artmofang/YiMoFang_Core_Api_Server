<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/17
 * Time: 12:02
 */
//获取参数
$b_image = $route->bodyParams['b_image'];
$b_blid = $route->bodyParams['b_blid'];

$b_url = $regexpObj->bodyV($response,$route,'b_url','NORMAL');
$b_name = $regexpObj->bodyV($response,$route,'b_name','NORMAL');
$b_sort = $regexpObj->bodyV($response,$route,'b_sort','NORMAL');
$is_delete = $regexpObj->bodyV($response,$route,'is_delete','NORMAL');
$is_open = $regexpObj->bodyV($response,$route,'is_open','SWITCH');
$b_create_time = time();

//写入数组
$insertArr = [
    'b_image'=>$b_image,
    'b_blid'=>$b_blid,
    'b_url' => $b_url,
    'is_delete'=>$is_delete,
    'is_open'=>$is_open,
    'b_create_time'=>$b_create_time,
    'b_sort'=>$b_sort,
    'b_name'=>$b_name
];
//执行写入语句
$rsData = $db->mysqlDB->insert("banner",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );