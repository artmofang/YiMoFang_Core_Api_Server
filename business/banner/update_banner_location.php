<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/17
 * Time: 16:21
 */
$bl_id = $route->bodyParams["bl_id"];

//更新条件
$whereArr = [
    "bl_id" => $bl_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("banner_location",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );