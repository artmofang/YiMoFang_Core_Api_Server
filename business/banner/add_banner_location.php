<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/17
 * Time: 15:33
 */
//获取参数
$bl_name = $route->bodyParams['bl_name'];
$bl_type = $route->bodyParams['bl_type'];
$is_delete = $regexpObj->bodyV($response,$route,'is_delete','NORMAL');

//写入数组
$insertArr = [
    'bl_name'=>$bl_name,
    'bl_type'=>$bl_type
];

//执行写入语句
$rsData = $db->mysqlDB->insert('banner_location',$insertArr);

//成功返回结果
$response->responseData( true, $rsData );