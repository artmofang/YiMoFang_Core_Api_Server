<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/17
 * Time: 15:16
 */
$b_id = $route->bodyParams["b_id"];

//更新条件
$whereArr = [
    "b_id" => $b_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("banner",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );