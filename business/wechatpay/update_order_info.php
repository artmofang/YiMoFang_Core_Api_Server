<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/15
 * Time: 19:35
 */

//获取参数信息
$o_no       = $route->bodyParams['out_trade_no'];                      //订单编号
$o_we_chat_no = $route->bodyParams['transaction_id'];                 //微信订单编号
$balance_pay = $route->bodyParams['balance_pay'];                 //余额支付价格
$uid = $route->bodyParams['uid'];                                  //用户id
$fid = $route->bodyParams['fid'];                                  //如果为拼团活动，这个参数是该团的主键活id


//构造存储过程参数
$proceArr = [
    $o_no,
    $o_we_chat_no,
    $balance_pay,
    $uid,
    $fid
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_FINISH_PAY", $proceArr);

//返回成功结果
$response->responseData( true);
