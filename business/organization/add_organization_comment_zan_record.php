<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 20:43
 * name:添加课程评论点赞记录
 * url:/organization/add_organization_comment_zan_record
 */

$oczr_ocid         = $route->bodyParams["oczr_ocid"]; //关联评论的ID
$oczr_uid          = $route->bodyParams["oczr_uid"];  //点赞用户ID
//写入数组
$insertArr = [
    "oczr_ocid"              => $oczr_ocid,
    "oczr_uid"               => $oczr_uid ,
    "oczr_create_time"      => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_comment_zan_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);