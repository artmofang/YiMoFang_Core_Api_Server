<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/22
 * Time: 20:52
 */
//获取参数
$hc_oaid = $route->bodyParams["hc_oaid"];//主键ID

$whereArr = [
    "hc_oaid" => $hc_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("h5scene_content",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );