<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/2
 * Time: 20:19
 * name:机构活动评论点赞信息更新
 * url:/organization/update_activity_comment_zan
 */

//获取参数
$oacz_id = $route->bodyParams["oacz_id"];//主键ID

//更新条件
$whereArr = [
    "oacz_id" => $oacz_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_activity_comment_zan",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );