<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/7
 * Time: 16:09
 * name:机构助力活动报名信息更新
 * url:/organization/update_help_activity_sign_up
 */

//获取参数
$hasu_id = $route->bodyParams["hasu_id"];//主键ID

//更新条件
$whereArr = [
    "hasu_id" => $hasu_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("help_activity_sign_up",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );