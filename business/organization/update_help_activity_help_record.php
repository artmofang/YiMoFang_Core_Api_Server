<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/7
 * Time: 16:41
 * name:机构助力活动助力记录信息更新
 * url:/organization/update_help_activity_help_record
 */

//获取参数
$hahr_id = $route->bodyParams["hahr_id"];//主键ID

//更新条件
$whereArr = [
    "hahr_id" => $hahr_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );