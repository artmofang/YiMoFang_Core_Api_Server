<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/17
 * Time: 20:48
 */

$dac_oaid    = $route->bodyParams["dac_oaid"];//关联的模板促销活动ID
$dac_type    = $route->bodyParams["dac_type"];//内容类型 0-文本 1-图片 2-视频
$dac_order   = $route->bodyParams["dac_order"];//排序
$dac_content = $route->bodyParams["dac_content"];//内容
//写入数组
$insertArr = [
    "dac_oaid"    => $dac_oaid,
    "dac_type"    => $dac_type,
    "dac_order"   => $dac_order,
    "dac_content" => $dac_content,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("discounts_activity_content",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);