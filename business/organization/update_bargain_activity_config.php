<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/20
 * Time: 20:56
 */
$bc_id = $route->bodyParams["bc_id"];

//更新条件
$whereArr = [
    "bc_id" => $bc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("bargain_config",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );