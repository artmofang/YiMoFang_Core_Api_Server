<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 17:48
 * name:机构评论更新
 * url:/organization/update_organization_comment
 */
//获取参数
$oc_id = $route->bodyParams["oc_id"];//主键ID

//更新条件
$whereArr = [
    "oc_id" => $oc_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_comment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );