<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 16:45
 * name:更新活动皮肤
 * url:/organization/update_activity_skin
 */

//获取参数
$as_id = $route->bodyParams["as_id"];//主键ID

//更新条件
$whereArr = [
    "as_id" => $as_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("activity_skin",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );