<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/14
 * Time: 15:58
 * name:删除消息推送记录
 * url:/organization/delete_organization_message_push_record
 */

//获取参数
$ompr_id = $route->bodyParams["ompr_id"];//主键ID

$whereArr = [
    "ompr_id" => $ompr_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_message_push_record",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );