<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/20
 * Time: 21:02
 */
//获取参数
$bc_oaid = $route->bodyParams["bc_oaid"];//主键ID

$whereArr = [
    "bc_oaid" => $bc_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("bargain_content",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );