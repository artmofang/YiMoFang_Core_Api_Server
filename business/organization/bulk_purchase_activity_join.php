<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/21
 * Time: 17:10
 * name:拼团活动参团
 * url:/organization/bulk_purchase_activity_join
 */
error_reporting(0);
//获取参数信息
$u_id          = $route->bodyParams['u_id'];   //用户ID
$oa_id         = $route->bodyParams['oa_id'];   //活动ID
$f_id          = $route->bodyParams['f_id'];   //对应开团记录的主键ID
$order_NO      = $route->bodyParams['order_NO'];   //订单号
$min_count     = $route->bodyParams['min_count'];   //成团人数
$is_validation = $route->bodyParams['is_validation'];   //是否验证

//构造存储过程参数
$proceArr = [
    $u_id,
    $oa_id,
    $f_id,
    $order_NO,
    $min_count,
    $is_validation
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_BULK_PURCHASE_ACTIVITY_JOIN", $proceArr , "DATA");

//返回成功结果
$response->responseData( true , $data);