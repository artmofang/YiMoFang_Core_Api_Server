<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/21
 * Time: 12:21
 * name:修改机构管理员
 * url:/organization/update_organization_user
 */

//获取参数
$ou_id = $route->bodyParams["ou_id"];//主键ID

//更新条件
$whereArr = [
    "ou_id" => $ou_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_user",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );