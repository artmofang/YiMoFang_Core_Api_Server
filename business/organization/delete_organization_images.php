<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 12:03
 * name:机构轮播删除
 * url：/user/delete_organization_images
 */
//获取参数
$oi_id = $route->bodyParams["oi_id"];//主键ID

$whereArr = [
    "oi_id" => $oi_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_images",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );