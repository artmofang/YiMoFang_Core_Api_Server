<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/26
 * Time: 21:36
 * name:修改机构查看平台消息记录
 * url:/organization/update_organization_view_message_record
 */

//获取参数
$ovmr_id = $route->bodyParams["ovmr_id"];//主键ID

//更新条件
$whereArr = [
    "ovmr_id" => $ovmr_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_view_message_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );