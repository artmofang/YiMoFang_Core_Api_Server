<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 18:42
 * name:机构相册删除
 * url:/organization/delete_organization_photo
 */
//获取参数
$op_id = $route->bodyParams["op_id"];//主键ID

$whereArr = [
    "op_id" => $op_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_photo",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );