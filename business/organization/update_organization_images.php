<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 12:06
 * name:机构轮播更新
 * url:/user/update_organization_images
 */
//获取参数
$oi_id = $route->bodyParams["oi_id"];//主键ID

//更新条件
$whereArr = [
    "oi_id" => $oi_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_images",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );