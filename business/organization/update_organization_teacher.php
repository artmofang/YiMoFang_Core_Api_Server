<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 20:24
 * name:机构教师更新
 * url:/organization/update_organization_teacher
 */
//获取参数
$ot_id = $route->bodyParams["ot_id"];//主键ID

//更新条件
$whereArr = [
    "ot_id" => $ot_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_teacher",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );