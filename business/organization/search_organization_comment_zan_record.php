<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 20:50
 * name:查询课程评论点赞记录
 * url:/organization/search_organization_comment_zan_record
 */

//获取字段筛选参数
$fields = $route->bodyParams['fields'];

//默认条件
//$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){

    unset($route->restfulParams['count']);
    //删除limit之前先行查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("organization_comment_zan_record");

    //删除不能与count同时使用的条件
    unset($route->restfulParams['skip']);
    unset($route->restfulParams['limit']);
    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count("organization_comment_zan_record");


}
//根据条件做相应查询
if(!$rsData){
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("organization_comment_zan_record");
}

//拼接后
$rs = array( "count" => $sum , "data" => $rsData );


//返回成功结果
$response->responseData( true, $rs );