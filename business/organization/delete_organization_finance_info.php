<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/19
 * Time: 10:33
 * name:删除财务账户信息
 * url:/organization/delete_organization_finance_info
 */

$ofi_id = $route->bodyParams["ofi_id"];//主键ID

$whereArr = [
    "ofi_id" => $ofi_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_finance_info",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );