<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/26
 * Time: 23:45
 */
//获取参数
$rba_oaid = $route->bodyParams["rba_oaid"];//主键ID

$whereArr = [
    "rba_oaid" => $rba_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("red_bag_activity_content",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );