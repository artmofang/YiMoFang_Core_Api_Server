<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 16:37
 * name:删除活动音乐
 * url:/organization/delete_activity_music
 */

//获取参数
$am_id = $route->bodyParams["am_id"];//主键ID

$whereArr = [
    "am_id" => $am_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("activity_music",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );