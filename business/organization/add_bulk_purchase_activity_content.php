<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 12:31
 */
$bpac_oaid    = $route->bodyParams["bpac_oaid"];//关联的模板拼团活动ID
$bpac_type    = $route->bodyParams["bpac_type"];//内容类型 0-文本 1-图片 2-视频
$bpac_order   = $route->bodyParams["bpac_order"];//排序
$bpac_content = $route->bodyParams["bpac_content"];//内容
//写入数组
$insertArr = [
    "bpac_oaid"    => $bpac_oaid,
    "bpac_type"    => $bpac_type,
    "bpac_order"   => $bpac_order,
    "bpac_content" => $bpac_content,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("bulk_purchase_activity_content",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);