<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/19
 * Time: 10:57
 * name:更新财务账户信息
 * url:/organization/update_organization_finance_info
 */

//获取参数
$ofi_id = $route->bodyParams["ofi_id"];//主键ID

//更新条件
$whereArr = [
    "ofi_id" => $ofi_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_finance_info",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );