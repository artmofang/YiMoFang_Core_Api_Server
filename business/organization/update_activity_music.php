<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 16:39
 * name:更新活动音乐
 * url:/organization/update_activity_music
 */

//获取参数
$am_id = $route->bodyParams["am_id"];//主键ID

//更新条件
$whereArr = [
    "am_id" => $am_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("activity_music",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );