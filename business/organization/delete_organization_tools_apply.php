<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/9
 * Time: 16:31
 * name:删除机构工具申请使用记录
 * url:/organization/delete_organization_tools_apply
 */

$ota_id = $route->bodyParams["ota_id"];//主键ID

$whereArr = [
    "ota_id" => $ota_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_tools_apply",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );