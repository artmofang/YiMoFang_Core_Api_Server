<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/21
 * Time: 15:34
 * name:促销活动参加
 * url:/organization/discounts_activity_join
 */

//获取参数信息
$_u_id      = $route->bodyParams['_u_id'];   //用户ID
$_a_id      = $route->bodyParams['_a_id'];   //活动ID

//构造存储过程参数
$proceArr = [
    $_u_id,
    $_a_id,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_DISCOUTS_JOIN", $proceArr);

//返回成功结果
$response->responseData( true);