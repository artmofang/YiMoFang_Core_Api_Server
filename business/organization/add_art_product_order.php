<?php
//获取参数
$apo_name           = $route->bodyParams["apo_name"];
$apo_old_price      = $route->bodyParams["apo_old_price"];
$apo_new_price      = $route->bodyParams["apo_new_price"];
$apo_real_pay       = $route->bodyParams["apo_real_pay"];
$apo_pay_type       = $route->bodyParams["apo_pay_type"];
$apo_no             = $route->bodyParams["apo_no"];
$apo_contact        = $route->bodyParams["apo_contact"];
$apo_phone          = $route->bodyParams["apo_phone"];
$apo_status         = $route->bodyParams["apo_status"];
$apo_product_type   = $route->bodyParams["apo_product_type"];
$apo_buy_time       = $route->bodyParams["apo_buy_time"];
$apo_product_image  = $route->bodyParams["apo_product_image"];
$apo_campus_name    = $route->bodyParams["apo_campus_name"];
$apo_apid           = $route->bodyParams["apo_apid"];

$apo_oid            = $regexpObj->bodyV($response,$route,'apo_oid','NORMAL');
$apo_provinces      = $regexpObj->bodyV($response,$route,'apo_provinces','NORMAL');
$apo_city           = $regexpObj->bodyV($response,$route,'apo_city','NORMAL');
$apo_area           = $regexpObj->bodyV($response,$route,'apo_area','NORMAL');
$apo_provinces_name = $regexpObj->bodyV($response,$route,'apo_provinces_name','NORMAL');
$apo_city_name      = $regexpObj->bodyV($response,$route,'apo_city_name','NORMAL');
$apo_area_name      = $regexpObj->bodyV($response,$route,'apo_area_name','NORMAL');
$apo_address        = $regexpObj->bodyV($response,$route,'apo_address','NORMAL');
$apo_invite_code    = $regexpObj->bodyV($response,$route,'apo_invite_code','NORMAL');
$apo_school_count   = $regexpObj->bodyV($response,$route,'apo_school_count','NORMAL');
$apo_type           = $regexpObj->bodyV($response,$route,'apo_type','NORMAL');

$apo_openid         = $regexpObj->bodyV($response,$route,'apo_openid','NORMAL');
$apo_service_status = $regexpObj->bodyV($response,$route,'apo_service_status','NORMAL');
$apo_mcid           = $regexpObj->bodyV($response,$route,'apo_mcid','NORMAL');
$apo_osid           = $regexpObj->bodyV($response,$route,'apo_osid','NORMAL');
$apo_nickname       = $regexpObj->bodyV($response,$route,'apo_nickname','NORMAL');

//写入数组
$insertArr = [
    "apo_name"           => $apo_name,
    "apo_old_price"      => $apo_old_price,
    "apo_new_price"      => $apo_new_price,
    "apo_real_pay"       => $apo_real_pay,
    "apo_pay_type"       => $apo_pay_type,
    "apo_oid"            => $apo_oid,
    "apo_no"             => $apo_no,
    "apo_contact"        => $apo_contact,
    "apo_phone"          => $apo_phone,
    "apo_status"         => $apo_status,
    "apo_product_type"   => $apo_product_type,
    "apo_buy_time"       => $apo_buy_time,
    "apo_product_image"  => $apo_product_image,
    "apo_create_time"    => time(),
    "apo_campus_name"    => $apo_campus_name,
    "apo_provinces"      => $apo_provinces,
    "apo_city"           => $apo_city,
    "apo_area"           => $apo_area,
    "apo_address"        => $apo_address,
    "apo_apid"           => $apo_apid,
    "apo_provinces_name" => $apo_provinces_name,
    "apo_city_name"      => $apo_city_name,
    "apo_area_name"      => $apo_area_name,
    "apo_invite_code"    => $apo_invite_code,
    "apo_school_count"   => $apo_school_count,
    "apo_type"           => $apo_type,
    "apo_openid"         => $apo_openid,
    "apo_service_status" => $apo_service_status,
    "apo_mcid"           => $apo_mcid,
    "apo_osid"           => $apo_osid,
    "apo_nickname"       => $apo_nickname,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("art_product_order",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);