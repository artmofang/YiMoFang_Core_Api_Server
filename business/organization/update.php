<?php
//获取参数
$o_id = $route->bodyParams["o_id"];//主键ID

//更新条件
$whereArr = [
    "o_id" => $o_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );