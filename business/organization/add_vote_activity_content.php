<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 14:08
 */
$vac_oaid    = $route->bodyParams["vac_oaid"];//关联的模板拼团活动ID
$vac_type    = $route->bodyParams["vac_type"];//内容类型 0-文本 1-图片 2-视频
$vac_order   = $route->bodyParams["vac_order"];//排序
$vac_content = $route->bodyParams["vac_content"];//内容
//写入数组
$insertArr = [
    "vac_oaid"    => $vac_oaid,
    "vac_type"    => $vac_type,
    "vac_order"   => $vac_order,
    "vac_content" => $vac_content,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("vote_activity_content",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);