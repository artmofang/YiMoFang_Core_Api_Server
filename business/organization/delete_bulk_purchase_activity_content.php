<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/20
 * Time: 21:04
 */
//获取参数
$bpac_oaid = $route->bodyParams["bpac_oaid"];//主键ID

$whereArr = [
    "bpac_oaid" => $bpac_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("bulk_purchase_activity_content",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );