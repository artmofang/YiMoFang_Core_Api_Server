<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 16:43
 * name:删除活动皮肤
 * url:/organization/delete_activity_skin
 */

//获取参数
$as_id = $route->bodyParams["as_id"];//主键ID

$whereArr = [
    "as_id" => $as_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("activity_skin",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );