<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 18:38
 * name:机构相册添加
 * url:/organization/add_organization_photo
 */
//获取参数
$op_fid         = $route->bodyParams["op_fid"];                                   //相册父ID 0-相册 其他为相册中的图片的父ID
$op_image_url   = $route->bodyParams["op_image_url"];                            //图片地址
$op_title       = $route->bodyParams["op_title"];                                //图片标题 fid为0时为相册名称
$op_oid         = $route->bodyParams["op_oid"];                                   //关联机构ID
$op_order       = $regexpObj->bodyV($response,$route,'op_order','NUMBER');        //排序

//写入数组
$insertArr = [
    "op_fid"         => $op_fid,
    "op_image_url"  => $op_image_url,
    "op_title"       => $op_title,
    "op_oid"         => $op_oid,
    "op_order"       => $op_order,
    "op_create_time"=> time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_photo",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);