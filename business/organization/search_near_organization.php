<?php

//获取字段筛选参数
$fields = "*";
if(isset($route->bodyParams['fields'])){
    $fields = $route->bodyParams['fields'];
}

//默认条件
$route->restfulParams['is_delete'] = 0;

$lng = $route->restfulParams['lng'];
$lat = $route->restfulParams['lat'];
$dis = $route->restfulParams['dis'];

$skip = 0;
if(isset($route->restfulParams['skip'])){
    $skip = $route->restfulParams['skip'];
}

$limit = 20;
if(isset($route->restfulParams['limit'])){
    $limit = $route->restfulParams['limit'];
}

$area = "";
if($route->restfulParams['o_area'] != ""){
    $area .= " and o_area = ".$route->restfulParams['o_area']." ";
}

$keywords = "";
if($route->restfulParams['keywords'] != ""){
    $keywordsArr = explode("-", $route->restfulParams['keywords']);
    $keywords .= " and ".$keywordsArr[0]." like '%".$keywordsArr[1]."%' ";
}
//$condition = "distance ASC";
if(isset($route->restfulParams['sort'])){
    $sort = $route->restfulParams['sort'];
    if($sort == 1){
        $condition = "o_course_sales DESC";
    }elseif ($sort == 2){
        $condition = "o_browse_times DESC";
    }elseif($sort == 3){
        $condition = "distance ASC";
    }
}
// o_provinces = 2471 ,o_provinces
$sql = "SELECT
$fields,(
    6371 * acos (
      cos ( radians($lat) )
      * cos( radians( o_latitude ) )
      * cos( radians( o_longitude ) - radians($lng) )
      + sin ( radians($lat) )
      * sin( radians( o_latitude ) )
    )
) AS distance
FROM art_organization WHERE o_provinces = 2471 $area $keywords
HAVING distance < $dis
ORDER BY $condition LIMIT $skip,$limit;";

//echo $sql;
$re= $db->mysqlDB->query($sql);

$res = array( "data" => $re ,"sql"=>$sql);

//返回成功结果
$response->responseData( true,$res);