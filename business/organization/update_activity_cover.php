<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/3
 * Time: 22:05
 * name:更新活动封面图
 * url:/organization/update_activity_cover
 */

//获取参数
$ac_id = $route->bodyParams["ac_id"];//主键ID

//更新条件
$whereArr = [
    "ac_id" => $ac_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("activity_cover",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );