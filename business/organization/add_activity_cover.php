<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/3
 * Time: 22:00
 * name:添加活动封面图
 * url:/organization/add_activity_cover
 */

//获取参数
$ac_name              = $route->bodyParams["ac_name"];                       //封面名称
$ac_bg_image          = $route->bodyParams["ac_bg_image"];                   //封面背景图
$ac_image_text        = $route->bodyParams["ac_image_text"];                 //背景上的文字块
$ac_image_text_style  = $route->bodyParams["ac_image_text_style"];          //背景上的文字块样式
$ac_thumbnail         = $route->bodyParams["ac_thumbnail"];                  //缩略图
$ac_tag               = $regexpObj->bodyV($response,$route,'ac_tag','NUMBER'); //风格 0-美术 1-音乐 2-舞蹈 3-书法。。。


//写入数组
$insertArr = [
    "ac_name"               => $ac_name,
    "ac_bg_image"          => $ac_bg_image,
    "ac_image_text"        => $ac_image_text,
    "ac_image_text_style" => $ac_image_text_style,
    "ac_thumbnail"         => $ac_thumbnail,
    "ac_create_time"       =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("activity_cover",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);