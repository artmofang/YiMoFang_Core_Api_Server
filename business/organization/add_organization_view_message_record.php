<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/26
 * Time: 21:31
 * name:增加机构查看平台消息记录
 * url:/organization/add_organization_view_message_record
 */

$ovmr_oid    = $route->bodyParams["ovmr_oid"];//机构ID
$ovmr_pnid    = $route->bodyParams["ovmr_pnid"];//平台消息ID
//写入数组
$insertArr = [
    "ovmr_oid"    => $ovmr_oid,
    "ovmr_pnid"    => $ovmr_pnid,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_view_message_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);