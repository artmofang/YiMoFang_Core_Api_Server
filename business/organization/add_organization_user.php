<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/21
 * Time: 12:02
 * name:添加机构管理员
 * url:/organization/add_organization_user
 */

//获取参数
$ou_oid   = $route->bodyParams["ou_oid"];                              //关联机构ID
$ou_uid   = $route->bodyParams["ou_uid"];                              //关联用户ID
$ou_phone = $route->bodyParams["ou_phone"];                            //电话
$ou_name  = $route->bodyParams["ou_name"];                             //姓名
$ou_role  = $regexpObj->bodyV($response,$route,'ou_role','NORMAL');    //角色名称

//写入数组
$insertArr = [
    "ou_oid"             => $ou_oid,
    "ou_uid"             => $ou_uid,
    "ou_phone"           => $ou_phone,
    "ou_name"            => $ou_name,
    "ou_role"            => $ou_role,
    "ou_create_time"    => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_user",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);