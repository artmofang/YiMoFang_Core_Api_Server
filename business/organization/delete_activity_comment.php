<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 13:10
 */
//获取参数
$oac_id = $route->bodyParams["oac_id"];//主键ID

$whereArr = [
    "oac_id" => $oac_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_activity_comment",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );