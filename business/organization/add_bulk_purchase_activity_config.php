<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 14:13
 */
$bpac_oaid                    = $route->bodyParams["bpac_oaid"];                //关联的模板拼团活动ID
$bpac_old_price               = $route->bodyParams["bpac_old_price"];           //原价
$bpac_open_price              = $route->bodyParams["bpac_open_price"];          //开团价格
$bpac_participation_price     = $route->bodyParams["bpac_participation_price"]; //参团价格
$bpac_min_count               = $route->bodyParams["bpac_min_count"];           //几人团

$bc_course_image        = $regexpObj->bodyV($response, $route, 'bc_course_image', 'NORMAL');
$bc_course_title        = $regexpObj->bodyV($response, $route, 'bc_course_title', 'NORMAL');
$bc_course_price        = $regexpObj->bodyV($response, $route, 'bc_course_price', 'NORMAL');
$bc_course_count        = $regexpObj->bodyV($response, $route, 'bc_course_count', 'NORMAL');
$bc_course_desc         = $regexpObj->bodyV($response, $route, 'bc_course_desc', 'NORMAL');

$bpac_appointment_info        = $regexpObj->bodyV($response, $route, 'bpac_appointment_info', 'NORMAL');//预约信息
$bpac_class_adjust_info       = $regexpObj->bodyV($response, $route, 'bpac_class_adjust_info', 'NORMAL');//调课说明
$bpac_refund_info             = $regexpObj->bodyV($response, $route, 'bpac_refund_info', 'NORMAL');//退款说明
$bpac_prompt             = $regexpObj->bodyV($response, $route, 'bpac_prompt', 'NORMAL');//退款说明
$bpac_other_info              = $regexpObj->bodyV($response, $route, 'bpac_other_info', 'NORMAL');//其他说明
$bpac_builder_coupon_id       = $regexpObj->bodyV($response, $route, 'bpac_builder_coupon_id', 'NORMAL');//团长奖励优惠券id
$bpac_builder_coupon_type     = $regexpObj->bodyV($response, $route, 'bpac_builder_coupon_type', 'NORMAL');//团长奖励优惠券类型
$bpac_builder_coupon_name     = $regexpObj->bodyV($response, $route, 'bpac_builder_coupon_name', 'NORMAL');//团长奖励优惠券名字
$bpac_participant_coupon_id   = $regexpObj->bodyV($response, $route, 'bpac_participant_coupon_id', 'NORMAL');//参团长奖励优惠券id
$bpac_participant_coupon_type = $regexpObj->bodyV($response, $route, 'bpac_participant_coupon_type', 'NORMAL');//参团奖励优惠券类型
$bpac_participant_coupon_name = $regexpObj->bodyV($response, $route, 'bpac_participant_coupon_name', 'NORMAL');//参团奖励优惠券名字
$bpac_type = $regexpObj->bodyV($response, $route, 'bpac_type', 'NORMAL');
$bpac_max_user_count = $regexpObj->bodyV($response, $route, 'bpac_max_user_count', 'NORMAL');


//写入数组
$insertArr = [
    "bpac_oaid"                    => $bpac_oaid,
    "bpac_old_price"               => $bpac_old_price,
    "bpac_type"=>$bpac_type,
    "bpac_open_price"              => $bpac_open_price,
    "bpac_participation_price"     => $bpac_participation_price,
    "bpac_min_count"               => $bpac_min_count,
    "bpac_appointment_info"        => $bpac_appointment_info,
    "bpac_class_adjust_info"       => $bpac_class_adjust_info,
    "bpac_refund_info"             => $bpac_refund_info,
    "bpac_other_info"              => $bpac_other_info,
    "bpac_prompt"                  => $bpac_prompt,
    "bpac_max_user_count"          => $bpac_max_user_count,
    "bpac_builder_coupon_id"       => $bpac_builder_coupon_id,
    "bpac_builder_coupon_type"     => $bpac_builder_coupon_type,
    "bpac_builder_coupon_name"     => $bpac_builder_coupon_name,
    "bpac_participant_coupon_id"   => $bpac_participant_coupon_id,
    "bpac_participant_coupon_type" => $bpac_participant_coupon_type,
    "bpac_participant_coupon_name" => $bpac_participant_coupon_name,
    "bc_course_image" => $bc_course_image,
    "bc_course_title" => $bc_course_title,
    "bc_course_price" => $bc_course_price,
    "bc_course_count" => $bc_course_count,
    "bc_course_desc" => $bc_course_desc,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("bulk_purchase_activity_config",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);