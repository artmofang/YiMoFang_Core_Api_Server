<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/14
 * Time: 11:49
 * name:删除活动浏览记录
 * url:/organization/delete_organization_activity_browse_record
 */

//获取参数
$oabr_id = $route->bodyParams["oabr_id"];//主键ID

$whereArr = [
    "oabr_id" => $oabr_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_activity_browse_record",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );