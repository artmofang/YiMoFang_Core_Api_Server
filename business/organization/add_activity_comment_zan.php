<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/2
 * Time: 20:15
 * name:机构活动评论点赞添加
 * url:/organization/add_activity_comment_zan
 */

//获取参数
$oacz_oacid                   = $route->bodyParams["oacz_oacid"];      //关联活动评论ID
$oacz_uid                    = $route->bodyParams["oacz_uid"];       //用户ID


//写入数组
$insertArr = [
    "oacz_oacid"         => $oacz_oacid,
    "oacz_uid"           => $oacz_uid,
    "oacz_create_time"  =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_activity_comment_zan",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);