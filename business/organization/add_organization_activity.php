<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 10:42
 * name:机构活动添加
 * url:/organization/add_organization_activity
 */
//获取参数
$min_count         = $route->bodyParams["min_count"];                                 //拼团成团人数
$open_price         = $route->bodyParams["open_price"];                                 //开团价格
$oa_title         = $route->bodyParams["oa_title"];                                 //活动的标题
$oa_type          = $route->bodyParams["oa_type"];                                  //活动的类型 0-拼团 1-投票 2-砍价 3-H5海报 4-助力 5-促销活动 6-红包活动
$oa_oid           = $route->bodyParams["oa_oid"];                                   //活动所属的机构
//$oa_category_text = $route->bodyParams["oa_category_text"];                         //活动分类字段
$oa_start_time    = $route->bodyParams["oa_start_time"];                            //开始时间
$oa_end_time      = $route->bodyParams["oa_end_time"];                              //结束时间
$oa_share_title   = $regexpObj->bodyV($response, $route, 'oa_share_title', 'NORMAL');  //分享标题
$oa_share_content = $regexpObj->bodyV($response, $route, 'oa_share_content', 'NORMAL');//分享内容
$oa_share_pic     = $regexpObj->bodyV($response, $route, 'oa_share_pic', 'NORMAL');    //分享图片
$oa_ocid          = $regexpObj->bodyV($response, $route, 'oa_ocid', 'NORMAL');         //关联的课程ID
$oa_remark        = $regexpObj->bodyV($response, $route, 'oa_remark', 'NORMAL');    //活动的备注
$oa_role          = $regexpObj->bodyV($response, $route, 'oa_role', 'NORMAL');      //活动规则
$oa_reward        = $regexpObj->bodyV($response, $route, 'oa_reward', 'NORMAL');    //活动奖励信息
$oa_explain       = $regexpObj->bodyV($response, $route, 'oa_explain', 'NORMAL');   //活动的简短描述
$oa_title_image   = $regexpObj->bodyV($response, $route, 'oa_title_image', 'NORMAL');//活动标题图
$oa_is_end        = $regexpObj->bodyV($response, $route, 'oa_is_end', 'NORMAL');    //活动是否结束
$oa_order         = $regexpObj->bodyV($response, $route, 'oa_order', 'NORMAL');     //活动排序
$oa_category_text = $regexpObj->bodyV($response, $route, 'oa_category_text', 'NORMAL');     //活动分类字段

$oa_title_image_text = $regexpObj->bodyV($response, $route, 'oa_title_image_text', 'NORMAL');     //上课时间
$oa_title_image_text_style = $regexpObj->bodyV($response, $route, 'oa_title_image_text_style', 'NORMAL');     //上课时间
$oa_skin_id = $regexpObj->bodyV($response, $route, 'oa_skin_id', 'NUMBER');     //上课时间
$oa_music_id = $regexpObj->bodyV($response, $route, 'oa_music_id', 'NUMBER');     //上课时间

$oa_class_time = $regexpObj->bodyV($response, $route, 'oa_class_time', 'NORMAL');     //上课时间
$oa_class_duration = $regexpObj->bodyV($response, $route, 'oa_class_duration', 'NORMAL');     //课程时长
$activity_new_price = $regexpObj->bodyV($response, $route, 'activity_new_price', 'NORMAL');     //课程现价
$oa_class_apply = $regexpObj->bodyV($response, $route, 'oa_class_apply', 'NORMAL');     //适用课程
$oa_class_people = $regexpObj->bodyV($response, $route, 'oa_class_people', 'NORMAL');     //适用人群
$oa_class_basics = $regexpObj->bodyV($response, $route, 'oa_class_basics', 'NORMAL');     //适用基础
$oa_class_phone = $regexpObj->bodyV($response, $route, 'oa_class_phone', 'NORMAL');     //联系电话
$oa_thumbnail = $regexpObj->bodyV($response, $route, 'oa_thumbnail', 'NORMAL');     //联系电话
$oa_title_image_type = $regexpObj->bodyV($response, $route, 'oa_title_image_type', 'NUMBER');     //背景图类型  0 自定义 1 模板

$most_price = $regexpObj->bodyV($response, $route, 'temp_most_price', 'NORMAL');     //最高价格
$least_price = $regexpObj->bodyV($response, $route, 'temp_least_price', 'NORMAL');     //最低价格


$oa_is_home = 0;
if($oa_type == 5){
    $oa_is_home = 1;
}
//查询机构名称
$sql = "select o_name from art_organization where o_id = $oa_oid";

$re = $db->mysqlDB->query($sql);
$organization_name = $re[0]['o_name'];
if($oa_type == 2){
	$oa_share_title = "【".$organization_name."】".$oa_category_text . "原价".$most_price."元，最低可砍价".$least_price."元";
	$oa_share_content="好课不容错过，快来参与吧。";
}else if($oa_type == 1){
	$oa_share_title = "我正在参加【" . $oa_title . "】，快来给我投一票吧！";
	$oa_share_content="我离冠军还差你一票。";
}else if($oa_type == 5){
	$oa_share_title = "【".$organization_name."】" . $oa_category_text  . explode("|",$oa_class_duration)[0] . "节仅需".$activity_new_price."元";
	$oa_share_content="好课不容错过，快来参与吧。";
}else if($oa_type == 0){
	$oa_share_title = "【".$organization_name."】" . $oa_category_text  . $min_count . "人拼仅需".$open_price."元";
	$oa_share_content="好课不容错过，快来参与吧。";
}else{
	$oa_share_title = "【".$organization_name."】".$oa_title . explode("|",$oa_class_duration)[0] . "节仅需".$activity_new_price."元";
	$oa_share_content="好课不容错过，快来参与吧。";
}

//写入数组
$insertArr = [
    "oa_title"          => $oa_title,
    "oa_type"           => $oa_type,
    "oa_oid"            => $oa_oid,
    "oa_category_text"  => $oa_category_text,
    "oa_start_time"     => $oa_start_time,
    "oa_end_time"       => $oa_end_time,
    "oa_remark"         => $oa_remark,
    "oa_explain"        => $oa_explain,
    "oa_title_image"    => $oa_title_image,
    "oa_create_time"    => time(),
    "oa_is_home"        => $oa_is_home,
    "oa_share_title"    => $oa_share_title,
    "oa_share_content"  => $oa_share_content,
    "oa_share_pic"      => $oa_title_image,
    "oa_is_end"         => $oa_is_end,
    "oa_order"          => $oa_order,
    "oa_reward"         => $oa_reward,
    "oa_role"           => $oa_role,
    "oa_ocid"           => $oa_ocid,

    "oa_title_image_text"           => $oa_title_image_text,
    "oa_title_image_text_style"    => $oa_title_image_text_style,
    "oa_skin_id"                    => $oa_skin_id,
    "oa_music_id"                   => $oa_music_id,

    "oa_class_time"                => $oa_class_time,
    "oa_class_duration"            => $oa_class_duration,
    "oa_class_apply"               => $oa_class_apply,
    "oa_class_people"              => $oa_class_people,
    "oa_class_basics"              => $oa_class_basics,
    "oa_class_phone"               => $oa_class_phone,
    "oa_thumbnail"                 => $oa_thumbnail,
    "oa_title_image_type"                 => $oa_title_image_type,
    
    
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_activity",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);