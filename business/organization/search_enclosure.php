<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/24
 * Time: 16:13
 * 查询营销招生附件表
 */
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){
    
    unset($route->restfulParams['count']);
    //删除limit之前先行查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("enclosure");
    
    //删除不能与count同时使用的条件
    unset($route->restfulParams['skip']);
    unset($route->restfulParams['limit']);
    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count("enclosure");
}
//根据条件做相应查询
if(!$rsData){
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("enclosure");
}

//拼接后
$rs = array( "count" => $sum , "data" => $rsData );
