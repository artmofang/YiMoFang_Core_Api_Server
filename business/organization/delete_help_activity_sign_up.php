<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/7
 * Time: 15:37
 * name:机构助力活动报名删除
 * url:/organization/delete_help_activity_sign_up
 */

//获取参数
$hasu_id = $route->bodyParams["hasu_id"];//主键ID

$whereArr = [
    "hasu_id" => $hasu_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("help_activity_sign_up",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );