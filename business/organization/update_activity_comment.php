<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 13:12
 * name:机构活动回复信息更新
 * url:/organization/update_activity_comment
 */
//获取参数
$oac_id = $route->bodyParams["oac_id"];//主键ID

//更新条件
$whereArr = [
    "oac_id" => $oac_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_activity_comment",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );