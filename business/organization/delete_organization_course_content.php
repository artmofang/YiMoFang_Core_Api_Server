<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 17:45
 * name:删除课程内容
 * url:/organization/delete_organization_course_content
 */

$occ_id = $route->bodyParams["occ_id"];//主键ID

$whereArr = [
    "occ_id" => $occ_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_course_content",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );