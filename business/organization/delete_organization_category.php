<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 18:50
 * 删除分类信息
 */
$oc_id = $route->bodyParams["oc_id"];//主键ID

$whereArr = [
    "oc_id" => $oc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_category",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );