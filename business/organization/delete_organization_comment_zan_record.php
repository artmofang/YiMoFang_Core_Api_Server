<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 20:46
 * name:删除课程评论点赞记录
 * url:/organization/delete_organization_comment_zan_record
 */

//获取参数
$oczr_id = $route->bodyParams["oczr_id"];//主键ID

$whereArr = [
    "oczr_id" => $oczr_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_comment_zan_record",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );