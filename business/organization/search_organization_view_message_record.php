<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/26
 * Time: 21:38
 * name:查询机构查看平台消息记录
 * url:/organization/search_organization_view_message_record
 */

//获取字段筛选参数
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){

    unset($route->restfulParams['count']);
    //删除limit之前先行查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("organization_view_message_record");

    //删除不能与count同时使用的条件
    unset($route->restfulParams['skip']);
    unset($route->restfulParams['limit']);
    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count("organization_view_message_record");


}
//根据条件做相应查询
if(!$rsData){
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("organization_view_message_record");
}

//拼接后
$rs = array( "count" => $sum , "data" => $rsData );


//返回成功结果
$response->responseData( true, $rs );

