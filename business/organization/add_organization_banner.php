<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 14:05
 * name:机构广告添加
 * url:/organization/add_organization_banner
 */
//获取参数
//$ob_title     = $route->bodyParams["ob_title"];                             //广告标题
$ob_oid       = $route->bodyParams["ob_oid"];                               //关联的机构ID
$ob_image_url = $route->bodyParams["ob_image_url"];                        //图片地址
$ob_url       = $regexpObj->bodyV($response,$route,'ob_url','NORMAL');      //广告点击后的地址
$ob_order     = $regexpObj->bodyV($response,$route,'ob_order','NUMBER');    //排序
$ob_title     = $regexpObj->bodyV($response,$route,'ob_title','NUMBER');    //排序



//写入数组
$insertArr = [
    "ob_title"          => $ob_title,
    "ob_oid"            => $ob_oid,
    "ob_image_url"     => $ob_image_url,
    "ob_url"            => $ob_url,
    "ob_order"          => $ob_order,
    "ob_create_time"   => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_banner",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);