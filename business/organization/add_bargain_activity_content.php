<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 14:06
 */
$bc_oaid    = $route->bodyParams["bc_oaid"];//关联的模板拼团活动ID
$bc_type    = $route->bodyParams["bc_type"];//内容类型 0-文本 1-图片 2-视频
$bc_order   = $route->bodyParams["bc_order"];//排序
$bc_content = $route->bodyParams["bc_content"];//内容
//写入数组
$insertArr = [
    "bc_oaid"    => $bc_oaid,
    "bc_type"    => $bc_type,
    "bc_order"   => $bc_order,
    "bc_content" => $bc_content,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("bargain_content",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);