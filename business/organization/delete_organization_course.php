<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 18:33
 * name:机构课程删除
 * url:/organization/delete_organization_course
 */
$oc_id = $route->bodyParams["oc_id"];//主键ID

$whereArr = [
    "oc_id" => $oc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_course",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );