<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/14
 * Time: 11:45
 * name:添加活动浏览记录
 * url:/organization/add_organization_activity_browse_record
 */

$oabr_oaid = $route->bodyParams["oabr_oaid"];//活动ID
$oabr_uid  = $route->bodyParams["oabr_uid"];// 用户ID
$oabr_oid  = $route->bodyParams["oabr_oid"];// 活动所属机构ID

//写入数组
$insertArr = [
    "oabr_oaid"         => $oabr_oaid,
    "oabr_uid"          => $oabr_uid ,
    "oabr_oid"          => $oabr_oid,
    "oabr_create_time" => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_activity_browse_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);