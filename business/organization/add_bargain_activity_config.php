<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 11:33
 * name:机构砍价活动配置添加
 * url:/bargain/add_bargain_activity_config
 */

$bc_oaid               = $route->bodyParams['bc_oaid'];                                        //关联活动ID
$bc_most_price         = $route->bodyParams['bc_most_price'];                                 //最高价
$bc_least_price        = $route->bodyParams['bc_least_price'];                                //最低价
$bc_most_cut_price     = $route->bodyParams['bc_most_cut_price'];                             //最大减额
$bc_least_cut_price    = $route->bodyParams['bc_least_cut_price'];                            //最低减额
$bc_interval_cut_time  = $regexpObj->bodyV($response,$route,'bc_interval_cut_time','NORMAL'); //距离再次砍价间隔时间，单位小时
$bc_invite_reward      = $regexpObj->bodyV($response,$route,'bc_invite_reward','NORMAL');     //邀请一个用户所得奖金
$bc_appointment_info   = $regexpObj->bodyV($response,$route,'bc_appointment_info','NORMAL');  //预约信息
$bc_class_adjust_info  = $regexpObj->bodyV($response,$route,'bc_class_adjust_info','NORMAL'); //调课说明
$bc_refund_info        = $regexpObj->bodyV($response,$route,'bc_refund_info','NORMAL');       //退款说明
$bc_other_info         = $regexpObj->bodyV($response,$route,'bc_other_info','NORMAL');        //其他说明
$bc_cozy_tips         = $regexpObj->bodyV($response,$route,'bc_cozy_tips','NORMAL');           //温馨提示
$bc_preferential         = $regexpObj->bodyV($response,$route,'bc_preferential','NORMAL');           ////门店优惠信息每条以 | 隔开
$bc_get_reward         = $regexpObj->bodyV($response,$route,'bc_get_reward','NORMAL');           ////领取奖励方式
$bc_course_title          = $regexpObj->bodyV($response,$route,'bc_course_title','NORMAL');    //拼团活动所对应的课程名称
$bc_course_image          = $regexpObj->bodyV($response,$route,'bc_course_image','NORMAL');    //拼团活动对应的课程图片
$bc_course_price          = $regexpObj->bodyV($response,$route,'bc_course_price','NORMAL');    //拼团活动对应的课程的i价格
$bc_course_count          = $regexpObj->bodyV($response,$route,'bc_course_count','NORMAL');    //拼团活动对应的课程数量
$bc_max_user_count          = $regexpObj->bodyV($response,$route,'bc_max_user_count','NORMAL');    //砍价活动的最大人数限制  0为不限制
$bc_coupon_id          = $regexpObj->bodyV($response,$route,'bc_coupon_id','NORMAL');    //砍价活动领取奖励id
$bc_coupon_type          = $regexpObj->bodyV($response,$route,'bc_coupon_type','NORMAL');    //获得奖励的类型 0代金券 1约课券 2兑换券

//写入数组
$insertArr = [
    'bc_oaid'               => $bc_oaid,
    'bc_most_price'         => $bc_most_price,
    'bc_least_price'        => $bc_least_price,
    'bc_most_cut_price'     => $bc_most_cut_price,
    'bc_least_cut_price'    => $bc_least_cut_price,
    'bc_interval_cut_time'  => $bc_interval_cut_time,
    'bc_invite_reward'      => $bc_invite_reward,
    'bc_appointment_info'   => $bc_appointment_info,
    'bc_class_adjust_info'  => $bc_class_adjust_info,
    'bc_refund_info'        => $bc_refund_info,
    'bc_other_info'         => $bc_other_info,
    'bc_cozy_tips'          => $bc_cozy_tips,
    'bc_course_title'       => $bc_course_title,
    'bc_course_image'       => $bc_course_image,
    'bc_course_price'       => $bc_course_price,
    'bc_course_count'       => $bc_course_count,
    'bc_get_reward'       => $bc_get_reward,
    'bc_preferential'       => $bc_preferential,
    'bc_max_user_count'       => $bc_max_user_count,
    'bc_coupon_id'       => $bc_coupon_id,
    'bc_coupon_type'       => $bc_coupon_type,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("bargain_config",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );