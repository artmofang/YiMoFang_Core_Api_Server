<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/17
 * Time: 14:14
 * 添加用户消息记录
 */

$ui_uid    = $route->bodyParams["ui_uid"];//用户id
$ui_activity_name    = $route->bodyParams["ui_activity_name"];//活动或者课程名称
$ui_activtiy_type   = $route->bodyParams["ui_activtiy_type"];//'活动类型  0-拼团 1-投票 2-砍价 3-H5海报 4-传单 5-促销活动  6-红包活动 7-微传单'',',
$ui_activity_fid    = $route->bodyParams["ui_activity_fid"];//'如果是拼团活动    0 普通 1 阶梯 2接龙',
$ui_type    = $route->bodyParams["ui_type"];//'0 课程 1活动 2约课',
$ui_oid    = $route->bodyParams["ui_oid"];//机构id
//写入数组
$insertArr = [
    "ui_uid"               => $ui_uid,
    "ui_activity_name"    => $ui_activity_name,
    "ui_activtiy_type"    => $ui_activtiy_type,
    "ui_activity_fid"     => $ui_activity_fid,
    "create_time"          =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_information",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);