<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/26
 * Time: 23:43
 * 更新红包活动的内容
 */
//获取参数
$rba_id = $route->bodyParams["rba_id"];//主键ID

//更新条件
$whereArr = [
    "rba_id" => $rba_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("red_bag_activity_content",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );