<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 16:41
 * name:添加活动皮肤
 * url:/organization/add_activity_skin
 */

//获取参数
$as_bg_style  = $route->bodyParams["as_bg_style"];      //背景图
$as_border_style  = $route->bodyParams["as_border_style"]; //内容框样式
$as_header_style  = $route->bodyParams["as_header_style"]; //头部样式
$as_footer_style  = $route->bodyParams["as_footer_style"]; //尾部样式


//写入数组
$insertArr = [
    "as_bg_style"       => $as_bg_style,
    "as_border_style"  => $as_border_style,
    "as_header_style"  => $as_header_style,
    "as_footer_style"  => $as_footer_style,
    "as_create_time"   =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("activity_skin",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);