<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/26
 * Time: 23:34
 * 添加红包活动内容
 */


$rba_oaid    = $route->bodyParams["rba_oaid"];//关联的机构活动id
$rba_type    = $route->bodyParams["rba_type"];//红包类型  0 - 约课券  1 - 代金券
$rba_cid    = $route->bodyParams["rba_cid"];//关联的代金券或者约课券id
//写入数组
$insertArr = [
    "rba_oaid"    => $rba_oaid,
    "rba_type"    => $rba_type,
    "rba_cid"    => $rba_cid,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("red_bag_activity_content",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);