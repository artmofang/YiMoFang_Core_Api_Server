<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/26
 * Time: 21:39
 * name:删除机构查看平台消息记录
 * url:/organization/delete_organization_view_message_record
 */

//获取参数
$ovmr_id = $route->bodyParams["ovmr_id"];//主键ID

$whereArr = [
    "ovmr_id" => $ovmr_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_view_message_record",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );