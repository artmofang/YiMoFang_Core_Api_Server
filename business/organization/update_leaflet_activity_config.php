<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/21
 * Time: 21:39
 */
//获取参数
$lac_id = $route->bodyParams["lac_id"];//主键ID

//更新条件
$whereArr = [
    "lac_id" => $lac_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("leaflet_activity_config",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );