<?php

//获取参数
$apur_oid  = $route->bodyParams["apur_oid"];      
$apur_apid  = $route->bodyParams["apur_apid"]; 
$apur_end_time  = $route->bodyParams["apur_end_time"]; 


//写入数组
$insertArr = [
    "apur_oid"       => $apur_oid,
    "apur_apid"  => $apur_apid,
    "apur_end_time"  => $apur_end_time,
    "apur_create_time"  => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("art_product_use_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);