<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 11:40
 */
//获取参数
$oac_oaid                   = $route->bodyParams["oac_oaid"];                                             //关联活动ID
$oac_content                = $route->bodyParams["oac_content"];                                          //评论内容
$oac_uid                    = $route->bodyParams["oac_uid"];                                              //评论的用户
$oc_is_organization_commnet = $regexpObj->bodyV($response,$route,'oc_is_organization_commnet','NUMBER');//是否为机构评论。。。
$oac_reply_uid = $regexpObj->bodyV($response,$route,'oac_reply_uid','NUMBER');                          //被评论用户ID。。。
$oac_is_reply = $regexpObj->bodyV($response,$route,'oac_is_reply','NUMBER');                            //是否为一次回复。。。
$oac_fid = $regexpObj->bodyV($response,$route,'oac_fid','NUMBER');                            //是否为一次回复。。。


//写入数组
$insertArr = [
    "oac_oaid"                      => $oac_oaid,
    "oac_content"                   => $oac_content,
    "oac_uid"                       => $oac_uid,
    "oac_reply_uid"                => $oac_reply_uid,
    "oac_is_reply"                 => $oac_is_reply,
    "oac_fid"                       => $oac_fid,
    "oc_is_organization_commnet"  => $oc_is_organization_commnet,
    "oac_comment_time"             =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_activity_comment",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);