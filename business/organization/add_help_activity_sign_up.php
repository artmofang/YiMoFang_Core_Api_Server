<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/7
 * Time: 14:48
 * name:机构活动回复添加
 * url:/organization/add_help_activity_sign_up
 */

//获取参数
$hasu_uid           = $route->bodyParams["hasu_uid"];           //关联用户ID
$hasu_name          = $route->bodyParams["hasu_name"];         //报名用户名称
$hasu_mobile        = $route->bodyParams["hasu_mobile"];       //报名用户电话
$hasu_age           = $route->bodyParams["hasu_age"];          //报名用户年龄
$hasu_declaration   = $route->bodyParams["hasu_declaration"]; //用户宣言
$hasu_oaid          = $route->bodyParams["hasu_oaid"];         //关联活动ID


//写入数组
$insertArr = [
    "hasu_uid"          => $hasu_uid,
    "hasu_name"         => $hasu_name,
    "hasu_mobile"       => $hasu_mobile,
    "hasu_age"          => $hasu_age,
    "hasu_declaration" => $hasu_declaration,
    "hasu_oaid"         => $hasu_oaid,
    "hasu_sign_time"    =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("help_activity_sign_up",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);