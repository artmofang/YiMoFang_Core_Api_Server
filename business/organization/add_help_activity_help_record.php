<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/7
 * Time: 16:25
 * name:机构助力活动助力记录添加
 * url:/organization/add_help_activity_help_record
 */

//获取参数
$hahr_uid       = $route->bodyParams["hahr_uid"];          //参赛者ID
$hahr_helper_id = $route->bodyParams["hahr_helper_id"];   //父id，如果为0，则为参赛者
$hahr_oaid      = $route->bodyParams["hahr_oaid"];         //活动ID


//写入数组
$insertArr = [
    "hahr_uid"                      => $hahr_uid,
    "hahr_helper_id"               => $hahr_helper_id,
    "hahr_oaid"                     => $hahr_oaid,
    "hahr_help_time"               =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("help_activity_help_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);