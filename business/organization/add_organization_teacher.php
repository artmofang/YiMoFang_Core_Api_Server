<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 18:54
 * name:机构教师添加
 * url:/organization/add_organization_teacher
 */
//获取参数
$ot_name       = $route->bodyParams["ot_name"];                                   //教师名称
$ot_oid        = $route->bodyParams["ot_oid"];                                    //关联的机构ID
$ot_header_url = $regexpObj->bodyV($response,$route,'ot_header_url','NORMAL');   //教师头像
$ot_sex        = $regexpObj->bodyV($response,$route,'ot_sex','NUMBER');           //性别 0-女 1-男 2-保密
$ot_school     = $regexpObj->bodyV($response,$route,'ot_school','NORMAL');        //毕业学校
$ot_desc       = $regexpObj->bodyV($response,$route,'ot_desc','NORMAL');          //教师描述
$ot_age        = $regexpObj->bodyV($response,$route,'ot_age','NUMBER');           //教师年龄
$ot_seniority  = $regexpObj->bodyV($response,$route,'ot_seniority','NORMAL');    //教龄

//写入数组
$insertArr = [
    "ot_name"            => $ot_name,
    "ot_oid"             => $ot_oid,
    "ot_header_url"     => $ot_header_url,
    "ot_sex"             => $ot_sex,
    "ot_school"          => $ot_school,
    "ot_desc"            => $ot_desc,
    "ot_age"             => $ot_age,
    "ot_seniority"      => $ot_seniority,
    "ot_create_time"    => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_teacher",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);