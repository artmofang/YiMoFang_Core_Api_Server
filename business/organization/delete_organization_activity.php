<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 11:10
 * name:机构活动删除
 */
//获取参数
$oa_id = $route->bodyParams["oa_id"];//主键ID

$whereArr = [
    "oa_id" => $oa_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_activity",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );