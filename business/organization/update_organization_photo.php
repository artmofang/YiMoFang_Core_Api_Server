<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 18:48
 * name:机构相册更新
 * url:/organization/update_organization_photo
 */
//获取参数
$op_id = $route->bodyParams["op_id"];//主键ID

//更新条件
$whereArr = [
    "op_id" => $op_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_photo",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );