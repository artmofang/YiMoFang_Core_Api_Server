<?php

//获取参数
$apo_no = $route->bodyParams["apo_no"];//订单编号

//更新条件
$whereArr = [
    "apo_no" => $apo_no
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("art_product_order",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );