<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/9
 * Time: 16:18
 * name:添加机构工具申请使用记录
 * url:/organization/add_organization_tools_apply
 */

//获取参数
$ota_name     = $route->bodyParams["ota_name"];                           //机构名称
$ota_type     = $route->bodyParams["ota_type"];                           //机构类型
$ota_member   = $route->bodyParams["ota_member"];                         //机构联系人
$ota_phone    = $route->bodyParams["ota_phone"];                          //机构联系方式
$ota_address  = $route->bodyParams["ota_address"];                        //机构地址
$ota_block    = $regexpObj->bodyV($response,$route,'ota_block','NORMAL'); //街道信息

//写入数组
$insertArr = [
    "ota_name"           => $ota_name,
    "ota_type"           => $ota_type,
    "ota_member"         => $ota_member,
    "ota_phone"          => $ota_phone,
    "ota_address"        => $ota_address,
    "ota_block"          => $ota_block,
    "ota_create_time"   => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_tools_apply",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);