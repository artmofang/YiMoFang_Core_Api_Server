<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/2
 * Time: 20:17
 * name:机构活动评论点赞删除
 * url:/organization/delete_activity_comment_zan
 */

//获取参数
$oacz_id = $route->bodyParams["oacz_id"];//主键ID

$whereArr = [
    "oacz_id" => $oacz_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_activity_comment_zan",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );