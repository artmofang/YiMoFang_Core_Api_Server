<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 11:57
 * name:添加机构轮播
 * url:add_organization_images
 */
//获取参数
$oi_url         = $route->bodyParams["oi_url"];                                   //点击地址
$oi_image       = $route->bodyParams["oi_image"];                                 //图片
$oi_title       = $route->bodyParams["oi_title"];                                 //标题
$oi_oid         = $route->bodyParams["oi_oid"];                                   //关联机构ID
$oi_order       = $regexpObj->bodyV($response,$route,'oi_order','NUMBER');        //排序

//写入数组
$insertArr = [
    "oi_url"          => $oi_url,
    "oi_image"        => $oi_image,
    "oi_title"        => $oi_title,
    "oi_oid"          => $oi_oid,
    "oi_order"        => $oi_order,
    "oi_create_time" => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_images",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);