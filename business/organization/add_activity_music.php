<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 16:33
 * name:添加活动音乐
 * url:/organization/add_activity_music
 */

//获取参数
$am_tag  = $route->bodyParams["am_tag"];      //音乐风格 0-活泼 1-典雅 2-激动 2-安静
$am_url  = $route->bodyParams["am_url"];       //音乐链接


//写入数组
$insertArr = [
    "am_tag"            => $am_tag,
    "am_url"            => $am_url,
    "am_create_time"   =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("activity_music",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);