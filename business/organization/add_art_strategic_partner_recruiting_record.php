<?php
//获取参数
$sprr_company_name = $route->bodyParams["sprr_company_name"];
$sprr_concat      = $route->bodyParams["sprr_concat"];
$sprr_phone      = $route->bodyParams["sprr_phone"];
$sprr_pid       = $route->bodyParams["sprr_pid"];
$sprr_cid       = $route->bodyParams["sprr_cid"];
$sprr_aid       = $route->bodyParams["sprr_aid"];

//写入数组
$insertArr = [
    "sprr_company_name" => $sprr_company_name,
    "sprr_concat"       => $sprr_concat,
    "sprr_phone"         => $sprr_phone,
    "sprr_pid"             => $sprr_pid,
    "sprr_create_time"    => time(),
    "sprr_cid"    => $sprr_cid,
    "sprr_aid"    => $sprr_aid,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("strategic_partner_recruiting_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);