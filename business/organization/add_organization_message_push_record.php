<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/14
 * Time: 15:48
 * name:添加消息推送记录
 * url:/organization/add_organization_message_push_record
 */

//获取参数
$omrp_title   = $route->bodyParams["omrp_title"];     //标题
$omrp_content = $route->bodyParams["omrp_content"];  //内容
$ompr_oaid    = $route->bodyParams["ompr_oaid"];     //活动ID
$ompr_oid    = $route->bodyParams["ompr_oid"];      //机构ID
$ompr_arrive_count = $route->bodyParams["ompr_arrive_count"];      //机构ID
$ompr_fail_count    = $route->bodyParams["ompr_fail_count"];      //机构ID
$ompr_type    = $route->bodyParams["ompr_type"];      //机构ID

//写入数组
$insertArr = [
    "omrp_title"        => $omrp_title,
    "omrp_content"      => $omrp_content,
    "ompr_oaid"         => $ompr_oaid,
    "ompr_oid"          => $ompr_oid,
    "ompr_arrive_count" => $ompr_arrive_count,
    "ompr_fail_count"   => $ompr_fail_count,
    "ompr_type"         => $ompr_type,
    "ompr_create_time" => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_message_push_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);