<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/22
 * Time: 20:53
 */
$hc_oaid       = $route->bodyParams["hc_oaid"];//关联的模板拼团活动ID
$hc_style_url  = $route->bodyParams["hc_style_url"];//风格地址文件
$hc_bgmusicurl = $regexpObj->bodyV($response, $route, 'hc_bgmusicurl', 'NORMAL');//背景音乐地址
$hc_ispay      = $regexpObj->bodyV($response, $route, 'hc_ispay', 'NORMAL');//0-不支付，1-支付
$hc_paymoney   = $regexpObj->bodyV($response, $route, 'hc_paymoney', 'NORMAL');//支付金额
//写入数组
$insertArr = [
    "hc_oaid"       => $hc_oaid,
    "hc_style_url"  => $hc_style_url,
    "hc_bgmusicurl" => $hc_bgmusicurl,
    "hc_ispay"      => $hc_ispay,
    "hc_paymoney"   => $hc_paymoney,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("h5scene_config",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);