<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 11:16
 */
//获取参数
$oa_id = $route->bodyParams["oa_id"];//主键ID

//更新条件
$whereArr = [
    "oa_id" => $oa_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_activity",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );