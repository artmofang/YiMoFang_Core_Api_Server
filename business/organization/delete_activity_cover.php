<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/3
 * Time: 22:04
 * name:删除活动封面图
 * url:/organization/delete_activity_cover
 */

//获取参数
$ac_id = $route->bodyParams["ac_id"];//主键ID

$whereArr = [
    "ac_id" => $ac_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("activity_cover",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );