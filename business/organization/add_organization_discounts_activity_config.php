<?php

$dat_name              = $route->bodyParams["dat_name"];//活动名称
$dat_title_image       = $route->bodyParams["dat_title_image"];//活动标题图
$dat_category_text     = $route->bodyParams["dat_category_text"];//活动分类信息
$dat_user_count        = $route->bodyParams["dat_user_count "];//活动人数
$dat_old_price         = $route->bodyParams["dat_old_price"];//活动原价
$dat_new_price         = $route->bodyParams["dat_new_price"];//活动现价
$dat_oatid             = $route->bodyParams["dat_oatid"];//活动备注
$dat_note              = $regexpObj->bodyV($response, $route, 'dat_note', 'NORMAL');  //活动备注
$dat_coupon_type       = $regexpObj->bodyV($response, $route, 'dat_coupon_type', 'NORMAL');  //代金劵名称
$dat_cash_coupon_money = $regexpObj->bodyV($response, $route, 'dat_cash_coupon_money', 'NORMAL');  //代金劵金额

//写入数组
$insertArr = [
    "dat_name"          => $dat_name,
    "dat_title_image"   => $dat_title_image,
    "dat_category_text" => $dat_category_text,
    "dat_user_count"    => $dat_user_count,
    "dat_old_price"     => $dat_user_count,
    "dat_new_price"     => $dat_new_price,
    "dat_oatid"         => $dat_oatid,
    "dat_note"          => setDefaultValue($dat_note, ""),
    "dat_coupon_type"   => setDefaultValue($dat_coupon_type, ""),
    "dat_coupon_id"     => setDefaultValue($dat_coupon_id, 0),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("discounts_activity_template",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);