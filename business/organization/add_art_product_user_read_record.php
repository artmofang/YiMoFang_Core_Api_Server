<?php
//获取参数
$apurr_openid           = $route->bodyParams["apurr_openid"];
$apurr_nickname      = $route->bodyParams["apurr_nickname"];
$apurr_header      = $route->bodyParams["apurr_header"];
$apurr_uid       = $route->bodyParams["apurr_uid"];
$apurr_status       = $route->bodyParams["apurr_status"];
$apuur_mcid       = $route->bodyParams["apuur_mcid"];

//写入数组
$insertArr = [
    "apurr_openid"           => $apurr_openid,
    "apurr_nickname"       => $apurr_nickname,
    "apurr_header"         => $apurr_header,
    "apurr_uid"             => $apurr_uid,
    "apuur_mcid"             => $apuur_mcid,
    "apurr_create_time"    => time(),
    "apurr_status"    => $apurr_status,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("art_product_user_read_record",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);