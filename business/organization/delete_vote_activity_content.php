<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/20
 * Time: 21:47
 */
//获取参数
$vac_oaid = $route->bodyParams["vac_oaid"];//主键ID

$whereArr = [
    "vac_oaid" => $vac_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("vote_activity_content",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );