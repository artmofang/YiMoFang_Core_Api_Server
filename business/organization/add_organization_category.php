<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 18:38
 * 添加机构分类
 */
//获取参数
$oc_oid       = $route->bodyParams["oc_oid"];                             //机构id
$oc_cid       = $route->bodyParams["oc_cid"];                               //分类ID




//写入数组
$insertArr = [
    "oc_oid"          => $oc_oid,
    "oc_cid"            => $oc_cid,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_category",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);