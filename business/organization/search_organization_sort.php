<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9
 * Time: 18:16
 */


//获取字段筛选参数
$fields = "*";
if(isset($route->bodyParams['fields'])){
    $fields = $route->bodyParams['fields'];
}

//默认条件
$route->restfulParams['is_delete'] = 0;

$lng = $route->restfulParams['lng'];
$lat = $route->restfulParams['lat'];
$sort = $route->restfulParams['sort'];
if($sort == 1){
    $condition = "o_course_sales";
}elseif ($sort == 2){
    $condition = "o_browse_times";
}
$skip = 0;
if(isset($route->restfulParams['skip'])){
    $skip = $route->restfulParams['skip'];
}

$limit = 20;
if(isset($route->restfulParams['limit'])){
    $limit = $route->restfulParams['limit'];
}


// o_provinces = 2471 ,o_provinces
$sql = "SELECT
$fields,FORMAT((
    6371 * acos (
      cos ( radians($lat) )
      * cos( radians( o_latitude ) )
      * cos( radians( o_longitude ) - radians($lng) )
      + sin ( radians($lat) )
      * sin( radians( o_latitude ) )
    )
),1) AS distance
FROM art_organization WHERE o_provinces = 2471
ORDER BY $condition desc LIMIT $skip,$limit;";


$re= $db->mysqlDB->query($sql);

$res = array( "data" => $re);

//返回成功结果
$response->responseData( true,$res);