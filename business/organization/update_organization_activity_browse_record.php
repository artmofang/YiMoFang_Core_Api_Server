<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/14
 * Time: 14:47
 * name:更新活动浏览记录
 * url:/organization/update_organization_activity_browse_record
 */

//获取参数
$oabr_id = $route->bodyParams["oabr_id"];//主键ID

//更新条件
$whereArr = [
    "oabr_id" => $oabr_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_activity_browse_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );