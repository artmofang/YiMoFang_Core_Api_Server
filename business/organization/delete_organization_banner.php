<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 16:12
 * name:机构广告删除
 * url:/organization/delete_organization_banner
 */
//获取参数
$ob_id = $route->bodyParams["ob_id"];//主键ID

$whereArr = [
    "ob_id" => $ob_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_banner",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );