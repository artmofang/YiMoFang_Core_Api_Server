<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 17:50
 * name:更新课程内容
 * url:/organization/update_organization_course_content
 */

//获取参数
$occ_id = $route->bodyParams["occ_id"];//主键ID

//更新条件
$whereArr = [
    "occ_id" => $occ_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_course_content",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );