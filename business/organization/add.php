<?php
//获取参数
$o_name           = $route->bodyParams["o_name"];                                   //机构名称
$o_logo           = $regexpObj->bodyV($response,$route,'o_logo','NORMAL');          //机构Logo
$o_slogan         = $regexpObj->bodyV($response,$route,'o_slogan','NORMAL');        //机构宣传语
$o_desc           = $regexpObj->bodyV($response,$route,'o_desc','NORMAL');          //机构描述
$o_address        = $regexpObj->bodyV($response,$route,'o_address','NORMAL');       //机构详细地址
$o_phone          = $regexpObj->bodyV($response,$route,'o_phone','NORMAL');       //机构详细地址
$o_provinces      = $regexpObj->bodyV($response,$route,'o_provinces','NORMAL');     //所在省份
$o_city           = $regexpObj->bodyV($response,$route,'o_city','NORMAL');          //所属城市
$o_area           = $regexpObj->bodyV($response,$route,'o_area','NORMAL');          //所在区域
$o_longitude      = $regexpObj->bodyV($response,$route,'o_longitude','NORMAL');     //经度
$o_latitude       = $regexpObj->bodyV($response,$route,'o_latitude','NORMAL');      //纬度
$o_status         = $regexpObj->bodyV($response,$route,'o_status','NORMAL');        //机构状态 0-未审核 1-已审核
$o_star_level     = $regexpObj->bodyV($response,$route,'o_star_level','NORMAL');    //好评-平均分
$o_tags           = $regexpObj->bodyV($response,$route,'o_tags','NORMAL');          //机构标签用 | 隔开
$o_label          = $regexpObj->bodyV($response,$route,'o_label','NORMAL');          //机构标签用 | 隔开 例如：名师执教|环境优美
$o_uid            = $regexpObj->bodyV($response,$route,'o_uid','NUMBER');          //机构标签用 | 隔开 例如：名师执教|环境优美
$o_new_desc       = $regexpObj->bodyV($response,$route,'o_new_desc','NUMBER');          //机构标签用 | 隔开 例如：名师执教|环境优美
$o_establish_time = $regexpObj->bodyV($response,$route,'o_establish_time','NUMBER');
$o_student_num    = $regexpObj->bodyV($response,$route,'o_student_num','NUMBER');
//写入数组
$insertArr = [
    "o_name"           => $o_name,
    "o_logo"           => $o_logo,
    "o_slogan"         => $o_slogan,
    "o_desc"           => $o_desc,
    "o_phone"          => $o_phone,
    "o_address"        => $o_address,
    "o_provinces"      => $o_provinces == NULL ? 2471 : $o_provinces,
    "o_city"           => $o_city == NULL ? 2472: $o_city,
    "o_area"           => $o_area == NULL ? 2481 : $o_area,
    "o_longitude"      => $o_longitude,
    "o_latitude"       => $o_latitude,
    "o_status"         => $o_status,
    "o_star_level"     => $o_star_level,
    "o_tags"           => $o_tags,
    "o_create_time"    => time(),
    "o_label"          => $o_label,
    "o_uid"            => $o_uid,
    "o_new_desc"       => $o_new_desc,
    "o_establish_time" => $o_establish_time,
    "o_student_num"    => $o_student_num,
];


//执行写入语句
$rsData = $db->mysqlDB->insert("organization",$insertArr);



//返回成功结果
$response->responseData( true,  $rsData);
