<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 18:56
 * 更新机构分类信息
 */

//获取参数
$oc_id = $route->bodyParams["oc_id"];//主键ID

//更新条件
$whereArr = [
    "oc_id" => $oc_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_category",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );
