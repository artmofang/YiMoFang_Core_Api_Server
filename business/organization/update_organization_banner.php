<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 16:36
 * name:机构广告信息更新
 * url:/organization/update_organization_banner
 */
//获取参数
$ob_id = $route->bodyParams["ob_id"];//主键ID

//更新条件
$whereArr = [
    "ob_id" => $ob_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_banner",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );