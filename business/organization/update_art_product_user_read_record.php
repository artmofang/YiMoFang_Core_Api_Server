<?php

//获取参数
$apurr_id = $route->bodyParams["apurr_id"];

//更新条件
$whereArr = [
    "apurr_id" => $apurr_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("art_product_user_read_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );