<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/22
 * Time: 20:01
 */
$hc_oaid    = $route->bodyParams["hc_oaid"];//关联的模板拼团活动ID
$hc_type    = $route->bodyParams["hc_type"];//内容类型 0-文本 1-图片 2-视频
$hc_bgimage = $route->bodyParams["hc_bgimage"];//排序
$hc_title   = $route->bodyParams["hc_title"];//内容
$hc_image   = $route->bodyParams["hc_image"];//内容
$hc_order   = $route->bodyParams["hc_order"];//内容
//写入数组
$insertArr = [
    "hc_oaid"    => $hc_oaid,
    "hc_type"    => $hc_type,
    "hc_bgimage" => $hc_bgimage,
    "hc_title"   => $hc_title,
    "hc_image"   => $hc_image,
    "hc_order"   => $hc_order,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("h5scene_content",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);