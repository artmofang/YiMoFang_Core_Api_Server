<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/7
 * Time: 16:36
 * name:机构助力活动助力记录删除
 * url:/organization/delete_help_activity_help_record
 */

//获取参数
$hahr_id = $route->bodyParams["hahr_id"];//主键ID

$whereArr = [
    "hahr_id" => $hahr_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("help_activity_help_record",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );