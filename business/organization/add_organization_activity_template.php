<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/17
 * Time: 15:26
 */

$oat_type         = $route->bodyParams["oat_type"];
$oat_name         = $route->bodyParams["oat_name"];
$oat_title_image  = $route->bodyParams["oat_title_image"];
$oat_cid          = $route->bodyParams["oat_cid"];

$oat_scene                  = $regexpObj->bodyV($response,$route,'oat_scene','NUMBER');
$oat_music_id               = $regexpObj->bodyV($response,$route,'oat_music_id','NUMBER');
$oat_skin_id                = $regexpObj->bodyV($response,$route,'oat_skin_id','NUMBER');
$oat_title_image_text       = $regexpObj->bodyV($response,$route,'oat_title_image_text','NORMAL');
$oat_title_image_text_style = $regexpObj->bodyV($response,$route,'oat_title_image_text_style','NORMAL');
$oat_explain                = $regexpObj->bodyV($response,$route,'oat_explain','NORMAL');

//写入数组
$insertArr = [
    "oat_type"                     => $oat_type,
    "oat_name"                     => $oat_name ,
    "oat_title_image"             => $oat_title_image,
    "oat_cid"                      => $oat_cid,

    "oat_scene"                    => $oat_scene,
    "oat_music_id"                 => $oat_music_id,
    "oat_skin_id"                  => $oat_skin_id,
    "oat_title_image_text"        => $oat_title_image_text,
    "oat_title_image_text_style" => $oat_title_image_text_style,
    "oat_explain"                  => $oat_explain,

    "oat_create_time"              => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_activity_template",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);