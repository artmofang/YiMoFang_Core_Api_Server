<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/8/9
 * Time: 16:33
 * name:更新机构工具申请使用记录
 * url:/organization/update_organization_tools_apply
 */

//获取参数
$ota_id = $route->bodyParams["ota_id"];//主键ID

//更新条件
$whereArr = [
    "ota_id" => $ota_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_tools_apply",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );