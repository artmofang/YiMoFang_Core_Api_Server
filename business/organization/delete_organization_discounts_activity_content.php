<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/19
 * Time: 17:55
 */
//获取参数
$dac_oaid = $route->bodyParams["dac_oaid"];//主键ID

$whereArr = [
    "dac_oaid" => $dac_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("discounts_activity_content",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );