<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 17:36
 * name:添加课程内容
 * url:/organization/add_organization_course_content
 */

//获取参数
$occ_ocid       = $route->bodyParams['occ_ocid'];                           //通知的内容
$occ_content    = $route->bodyParams['occ_content'];                        //通知标题
$occ_type       = $regexpObj->bodyV($response,$route,'occ_type','NORMAL');  //通知标题图
$occ_order      = $regexpObj->bodyV($response,$route,'occ_order','NORMAL');  //通知标题图

//写入数组
$insertArr = [
    'occ_ocid'         => $occ_ocid,
    'occ_content'     => $occ_content,
    'occ_type'         => setDefaultValue($occ_type,0),
    'occ_order'        => setDefaultValue($occ_order,0),
    'pn_create_time'  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_course_content",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );