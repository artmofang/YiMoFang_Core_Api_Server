<?php

//获取参数
$apur_id = $route->bodyParams["apur_id"];//主键ID

//更新条件
$whereArr = [
    "apur_id" => $apur_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("art_product_use_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );