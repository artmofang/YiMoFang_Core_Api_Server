<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/19
 * Time: 10:16
 * name:添加财务账户信息
 * url:/organization/add_organization_finance_info
 */

//获取参数
$ofi_oid            = $route->bodyParams["ofi_oid"];              //机构ID
$ofi_account_type   = $route->bodyParams["ofi_account_type"];    //账户类型 0-个人 1-公司
$ofi_legal          = $route->bodyParams["ofi_legal"];           //法人名称
$ofi_account_name   = $route->bodyParams["ofi_account_name"];    //账户名称
$ofi_bank           = $route->bodyParams["ofi_bank"];            //开户行
$ofi_bank_number    = $route->bodyParams["ofi_bank_number"];    //银行帐号
$ofi_prove_image    = $route->bodyParams["ofi_prove_image"];     //提款证明信息图
$ofi_branch         = $route->bodyParams["ofi_branch"];           //支行


//写入数组
$insertArr = [
    "ofi_oid"           => $ofi_oid,
    "ofi_account_type" => $ofi_account_type,
    "ofi_legal"         => $ofi_legal,
    "ofi_account_name" => $ofi_account_name,
    "ofi_bank"          => $ofi_bank,
    "ofi_bank_number"  => $ofi_bank_number,
    "ofi_prove_image"  =>$ofi_prove_image,
    "ofi_branch"  =>$ofi_branch,
    "ofi_create_time"  =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_finance_info",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);