<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/21
 * Time: 15:34
 * name:促销活动参加(免费)
 *
 */

//获取参数信息
$_u_id      = $route->bodyParams['_u_id'];   //用户ID
$_a_id      = $route->bodyParams['_a_id'];   //活动ID
$_o_id      = $route->bodyParams['_o_id'];   //活动ID
$dau_name      = $route->bodyParams['dau_name'];   //参加人姓名
$dau_phone      = $route->bodyParams['dau_phone'];   //参加人电话

//构造存储过程参数
$proceArr = [
    $_u_id,
    $_a_id,
    $_o_id,
    $dau_name,
    $dau_phone
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_DISCOUTS_JOIN_FREE", $proceArr);

//返回成功结果
$response->responseData( true);