<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/23
 * Time: 17:37
 */
$latc_oaid   = $route->bodyParams["latc_oaid"];//关联的活动id
$latc_type    = $route->bodyParams["latc_type"];//'0-正面 1-反面',
$latc_bgimage = $route->bodyParams["latc_bgimage"];//背景
$latc_content   = $route->bodyParams["latc_content"];//图片类型为1 ，文字类型为0 \n例如：[{type:1,value:”http://www.v.png”,"style":"......."},{type:0,”我是文字”}]

//写入数组
$insertArr = [
    "latc_oaid"    => $latc_oaid,
    "latc_type"    => $latc_type,
    "latc_bgimage" => $latc_bgimage,
    "latc_content"   => $latc_content,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("leaflet_activity_content",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);