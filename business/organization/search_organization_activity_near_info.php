<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 11:11
 */
//获取字段筛选参数
// $fields = $route->bodyParams['fields'];

// //默认条件
// $route->restfulParams['is_delete'] = 0;

// $sum = 0;

// //判断是否需要查询总记录数
// if(isset($route->restfulParams['count'])){

//     unset($route->restfulParams['count']);
//     //删除limit之前先行查询
//     $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_organization_acivity_orgnization",true);

//     //删除不能与count同时使用的条件
//     unset($route->restfulParams['skip']);
//     unset($route->restfulParams['limit']);
//     //获取总条数
//     $sum = $db->mysqlDB->params($route->restfulParams)->count("art_organization_acivity_orgnization",true);


// }
// //根据条件做相应查询
// if(!$rsData){
//     $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_organization_acivity_orgnization",true);
// }

// //拼接后
// $rs = array( "count" => $sum , "data" => $rsData );


// //返回成功结果
// $response->responseData( true, $rs );


//获取字段筛选参数
$fields = "*";
if(isset($route->bodyParams['fields'])){
    $fields = $route->bodyParams['fields'];
}

//默认条件
$route->restfulParams['is_delete'] = 0;
$route->restfulParams['oa_is_end'] = 0;

$lng = $route->restfulParams['lng'];
$lat = $route->restfulParams['lat'];

$skip = 0;
if(isset($route->restfulParams['skip'])){
  $skip = $route->restfulParams['skip'];
}

$limit = 20;
if(isset($route->restfulParams['limit'])){
  $limit = $route->restfulParams['limit'];
}

$oa_is_home = $route->bodyParams["oa_is_home"];

$keywords_str = "";
if(isset($route->bodyParams['keywords']) && $route->bodyParams['keywords'] != 'undefined'){
  $keywords_str = "and oa_category_text like '%".$route->bodyParams['keywords']."%'";
}

$o_area = "";
if(isset($route->bodyParams['o_area']) && $route->bodyParams['o_area'] != '0'){
  $o_area = "and o_area = ".$route->bodyParams['o_area'];
}

$nearValue = $route->bodyParams['nearValue'];



$sql = "SELECT  
$fields,(
    6371 * acos (  
      cos ( radians($lat) )  
      * cos( radians( o_latitude ) )  
      * cos( radians( o_longitude ) - radians($lng) )  
      + sin ( radians($lat) )  
      * sin( radians( o_latitude ) )  
    )  
) AS distance
FROM v_art_organization_acivity_orgnization WHERE oa_is_end = 0 and oa_type = 5 $keywords_str $o_area
HAVING distance < $nearValue
ORDER BY distance LIMIT $skip,$limit;";


$re= $db->mysqlDB->query($sql);

$res = array( "data" => $re);

// //返回成功结果
$response->responseData( true,$res  );