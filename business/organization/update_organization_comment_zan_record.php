<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 20:52
 * name:更新课程评论点赞记录
 * url:/organization/update_organization_comment_zan_record
 */

//获取参数
$oczr_id = $route->bodyParams["oczr_id"];//主键ID

//更新条件
$whereArr = [
    "oczr_id" => $oczr_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("organization_comment_zan_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );