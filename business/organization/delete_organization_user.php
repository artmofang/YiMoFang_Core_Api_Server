<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/21
 * Time: 12:24
 * name:删除机构管理员
 * url:/organization/delete_organization_user
 */

//获取参数
$ou_id = $route->bodyParams["ou_id"];//主键ID

$whereArr = [
    "ou_id" => $ou_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_user",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );