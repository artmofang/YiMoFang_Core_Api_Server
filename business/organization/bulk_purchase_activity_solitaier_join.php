<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/21
 * Time: 17:10
 * name:拼团活动参团
 * url:/organization/bulk_purchase_activity_fast_join
 */

//获取参数信息
$u_id          = $route->bodyParams['u_id'];   //用户ID
$oa_id         = $route->bodyParams['oa_id'];   //活动ID
$order_NO      = $route->bodyParams['order_NO'];   //订单号
$is_validation = $route->bodyParams['is_validation'];   //是否验证

//构造存储过程参数
$proceArr = [
    $u_id,
    $oa_id,
    $order_NO,
    $is_validation
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_BULK_PURCHASE_SOLITAIRE_ACTIVITY_JOIN", $proceArr);

//返回成功结果
$response->responseData( true);