<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 17:52
 * name:机构课程添加
 * url:/organization/add_organization_course
 */
//获取参数
$oc_title        = $route->bodyParams["oc_title"];                            //课程名称
$oc_title_image  = $route->bodyParams["oc_title_image"];                     //课程标题图
$oc_old_price    = $route->bodyParams["oc_old_price"];                       //原价
$oc_new_price    = $route->bodyParams["oc_new_price"];                       //现价
$oc_course_count = $route->bodyParams["oc_course_count"];                    //课时数
$oc_count        = $route->bodyParams["oc_count"];                           //上课人数
$oc_oid          = $route->bodyParams["oc_oid"];                             //关联的机构
$oc_cid          = $route->bodyParams["oc_cid"];                             //所属的类别ID
$oc_type         = $route->bodyParams["oc_type"];                            //课程类型 0-普通课程 1-约课课程
$oc_appointment  = $route->bodyParams["oc_appointment"];                    //预约信息
$oc_switching    = $route->bodyParams["oc_switching"];                       //调课说明
$oc_refund       = $route->bodyParams["oc_refund"];                          //退款说明
$oc_other_info   = $route->bodyParams["oc_other_info"];                      //其他说明
$oc_prompt       = $route->bodyParams["oc_prompt"];                          //温馨提示
$oc_age          = $route->bodyParams["oc_age"];                             //适合人群
$oc_base         = $route->bodyParams["oc_base"];                            //适合基础
$oc_desc         = $route->bodyParams["oc_desc"];                            //课程描述
$oc_content         = $route->bodyParams["oc_content"];                            //课程描述







//写入数组
$insertArr = [
    "oc_title"          => $oc_title,
    "oc_title_image"   => $oc_title_image,
    "oc_old_price"     => $oc_old_price,
    "oc_new_price"     => $oc_new_price,
    "oc_course_count"  => $oc_course_count,
    "oc_count"          => $oc_count,
    "oc_oid"            => $oc_oid,
    "oc_cid"            => $oc_cid,
    "oc_type"           => $oc_type,
    "oc_age"            => $oc_age,
    "oc_base"           => $oc_base,
    "oc_desc"           => $oc_desc,
    "oc_create_time"   => time(),
    "oc_appointment"   =>$oc_appointment,
    "oc_switching"     =>$oc_switching,
    "oc_refund"        =>$oc_refund,
    "oc_other_info"    =>$oc_other_info,
    "oc_prompt"        =>$oc_prompt,
    "oc_content"        =>$oc_content
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_course",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);