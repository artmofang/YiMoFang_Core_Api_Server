<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 17:39
 * name:机构评论添加
 * url:/organization/add_organization_comment
 */
//获取参数
$oc_oid                     = $route->bodyParams["oc_oid"];                                               //所属机构
$oc_content                 = $route->bodyParams["oc_content"];                                           //内容
$oc_uid                     = $route->bodyParams["oc_uid"];                                               //评论人ID
$oc_score_level               = $route->bodyParams["oc_score_level"];                                     //评分
$oc_is_reply                = $regexpObj->bodyV($response,$route,'oc_is_reply','NORMAL');                 //是否为一次回复
$oc_is_organization_commnet = $regexpObj->bodyV($response,$route,'oc_is_organization_commnet','NORMAL');//是否为机构回复 0-否 1-是


//写入数组
$insertArr = [
    "oc_score_level"               =>$oc_score_level,
    "oc_oid"                        => $oc_oid,
    "oc_content"                    => $oc_content,
    "oc_uid"                        => $oc_uid,
    "oc_is_reply"                   => $oc_is_reply,
    "oc_is_organization_commnet"  => $oc_is_organization_commnet,
    "oc_create_time"                => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_comment",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);