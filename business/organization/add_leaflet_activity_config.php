<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/21
 * Time: 19:17
 */
$lac_oaid             = $route->bodyParams["lac_oaid"];//关联的模板拼团活动ID
$lac_logo             = $route->bodyParams["lac_logo"];//logo地址1
$lac_title            = $route->bodyParams["lac_title"];//正面标题1
$lac_content          = $route->bodyParams["lac_content"];//发面内容1
$lac_qr_code          = $route->bodyParams["lac_qr_code"];//二维码地址
$lac_address          = $route->bodyParams["lac_address"];//地址
$lac_phone            = $route->bodyParams["lac_phone"];//联系电话
$lac_front_bg_image   = $route->bodyParams["lac_front_bg_image"];//正面背景
$lac_reverse_bg_image = $route->bodyParams["lac_reverse_bg_image"];//反面面背景
$lac_style_url        = $route->bodyParams["lac_style_url"];//所有内容样式url模板
//写入数组
$insertArr = [
    "lac_oaid"             => $lac_oaid,
    "lac_logo"             => $lac_logo,
    "lac_content"          => $lac_content,
    "lac_title"            => $lac_title,
    "lac_qr_code"          => $lac_qr_code,
    "lac_address"          => $lac_address,
    "lac_phone"            => $lac_phone,
    "lac_front_bg_image"   => $lac_front_bg_image,
    "lac_reverse_bg_image" => $lac_reverse_bg_image,
    "lac_style_url"        => $lac_style_url,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("leaflet_activity_config",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);