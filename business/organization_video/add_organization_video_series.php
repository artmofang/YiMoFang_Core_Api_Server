<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 16:59
 * 添加视频课程系列课程表
 */

//获取参数
$ovs_ovid             = $route->bodyParams['ovs_ovid'];                //关联机构视频课程主键id
$ovs_title            = $route->bodyParams['ovs_title'];               //视频标题
$ovs_order            = $route->bodyParams['ovs_order'];               //排序
$ovs_video_address    = $route->bodyParams['ovs_video_address'];      //视频地址
$ovs_video_time       = $route->bodyParams['ovs_video_time'];         //视频时长
$ovs_create_time      = time();                                           //创建时间
//写入数组
$insertArr = [
    'ovs_ovid'               => $ovs_ovid,
    'ovs_title'              => $ovs_title,
    'ovs_order'              => $ovs_order,
    'ovs_video_address'     => $ovs_video_address,
    'ovs_video_time'        => $ovs_video_time,
    'ovs_create_time'        => $ovs_create_time
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_video_series",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );