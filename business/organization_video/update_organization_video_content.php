<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 16:57
 */

//获取参数
$ovc_id = $route->bodyParams["ovc_id"];

//更新条件
$whereArr = [
    "ovc_id" => $ovc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("organization_video_content",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );