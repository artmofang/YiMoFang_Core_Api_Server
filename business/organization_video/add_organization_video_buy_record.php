<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2019/1/24
 * Time: 17:16
 * name:添加视频课视购买记录
 * url:/organization_video/add_organization_video_buy_record
 */

$ovbr_uid      = $route->bodyParams["ovbr_uid"];        //购买人的id
$ovbr_ovid     = $route->bodyParams["ovbr_ovid"];       //关联视频课程id
$ovbr_oid      = $route->bodyParams["ovbr_oid"];        //关联的机构id
$ovbr_order_id = $route->bodyParams["ovbr_order_id"];   //关联的订单id

$ovbr_name = $route->bodyParams["ovbr_name"];
$ovbr_phone = $route->bodyParams["ovbr_phone"];
$ovbr_age = $route->bodyParams["ovbr_age"];



//写入数组
$insertArr = [
    "ovbr_uid"         => $ovbr_uid,
    "ovbr_ovid"        => $ovbr_ovid,
    "ovbr_oid"         => $ovbr_oid,
    "ovbr_order_id"    => $ovbr_order_id,
    "ovbr_create_time" => time(),
    "ovbr_name"        => $ovbr_name,
    "ovbr_phone"       => $ovbr_phone,
    "ovbr_age"         => $ovbr_age,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_video_buy_record",$insertArr);

//返回成功结果
$response->responseData( true,$rsData);