<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 16:53
 * 添加视频课程内容表
 */



//获取参数
$ovc_ovid        = $route->bodyParams['ovc_ovid'];                     //关联视频id
$ovc_type        = $route->bodyParams['ovc_type'];                     //内容类型 0-文本 1-图片 2-视频
$ovc_order       = $route->bodyParams['ovc_order'];                    //排序
$ovc_content     = $route->bodyParams['ovc_content'];                  //内容
//写入数组
$insertArr = [
    'ovc_ovid'        => $ovc_ovid,
    'ovc_type'        => $ovc_type,
    'ovc_order'       => $ovc_order,
    'ovc_content'    => $ovc_content
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_video_content",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );