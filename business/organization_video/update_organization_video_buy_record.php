<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2019/1/24
 * Time: 17:24
 * name:更新视频课购买记录
 * url:/organization_video/update_organization_video_buy_record
 */

//获取参数
$ovbr_id = $route->bodyParams["ovbr_id"];

//更新条件
$whereArr = [
    "ovbr_id" => $ovbr_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("organization_video_buy_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );