<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 16:59
 * 添加视频课程评论表
 */

//获取参数
$ovd_uid             = $route->bodyParams['ovd_uid'];                //评论人的id
$ovd_ovid            = $route->bodyParams['ovd_ovid'];               //关联的视频课程主键id
$ovd_content         = $route->bodyParams['ovd_content'];            //评论内容
$ovd_level           = $route->bodyParams['ovd_level'];              //评分
$ovd_create_time     = time();                                          //创建时间
//写入数组
$insertArr = [
    'ovd_uid'           => $ovd_uid,
    'ovd_ovid'          => $ovd_ovid,
    'ovd_content'      => $ovd_content,
    'ovd_level'        => $ovd_level,
    'ovd_create_time'  => $ovd_create_time
];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_video_discuss",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );