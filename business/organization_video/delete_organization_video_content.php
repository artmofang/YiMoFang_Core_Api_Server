<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 16:58
 */


//获取参数

$ovc_ovid = $route->bodyParams["ovc_ovid"];  //主键ID

$whereArr = [
    "ovc_ovid" => $ovc_ovid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_video_content",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );