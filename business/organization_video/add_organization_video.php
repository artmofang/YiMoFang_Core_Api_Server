<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2019/1/24
 * Time: 16:43
 * name:添加视频课
 * url:/organization_video/add_organization_video
 */

$ov_skin_id                 = $route->bodyParams["ov_skin_id"];                                     //皮肤ID
$ov_title_image             = $route->bodyParams["ov_title_image"];                                 //封面图
$ov_title_imgae_th          = $route->bodyParams["ov_title_imgae_th"];                              //封面缩略图（带文字）
$ov_title_image_text        = $route->bodyParams["ov_title_image_text"];                            //封面文字
$ov_title_image_text_style  = $route->bodyParams["ov_title_image_text_style"];                      //封面文字样式
$ov_title                   = $route->bodyParams["ov_title"];                                       //视频标题
$ov_old_price               = $route->bodyParams["ov_old_price"];                                   //视频原价
$ov_now_price               = $route->bodyParams["ov_now_price"];                                   //视频现价
$ov_category_text           = $route->bodyParams["ov_category_text"];                               //类别名称
$ov_category_id             = $route->bodyParams["ov_category_id"];                                 //类别名称id
$ov_course_style             = $route->bodyParams["ov_course_style"];                                 //课程类型
$ov_course_type             = $route->bodyParams["ov_course_type"];                                 //课程类型
//$ov_tags                    = $route->bodyParams["ov_tags"];                                        //标签，多个用|隔开
$ov_oid                     = $route->bodyParams["ov_oid"];                                         //所属机构id
$ov_coupon_id               = $regexpObj->bodyV($response,$route,'ov_coupon_id','NORMAL');          //代金劵的ID
$ov_coupon_type             = $regexpObj->bodyV($response,$route,'ov_coupon_type','NORMAL');        //代金劵的类型 0-代金劵 1-约课劵 2-兑换卷
$ov_video_address           = $regexpObj->bodyV($response,$route,'ov_video_address','NORMAL');      //视频地址（单次课的时候用）
$ov_series_time             = $regexpObj->bodyV($response,$route,'ov_series_time','NORMAL');        //系列课次数 （仅仅系列课程用）
$ov_video_time              = $regexpObj->bodyV($response,$route,'ov_video_time','NORMAL');         //视频时间长度（仅单次课用）
$ov_is_stop                 = $regexpObj->bodyV($response,$route,'ov_is_stop','NORMAL');            //是否暂停  0否 1 是
$ov_teacher_name                 = $regexpObj->bodyV($response,$route,'ov_teacher_name','NORMAL'); 


//写入数组
$insertArr = [
    "ov_skin_id"                 => $ov_skin_id,
    "ov_title_image"             => $ov_title_image,
    "ov_title_imgae_th"          => $ov_title_imgae_th,
    "ov_title_image_text"        => $ov_title_image_text,
    "ov_title_image_text_style"  => $ov_title_image_text_style,
    "ov_title"                   => $ov_title,
    "ov_old_price"               => $ov_old_price,
    "ov_now_price"               => $ov_now_price,
    "ov_category_text"           => $ov_category_text,
    "ov_category_id"             => $ov_category_id,
    "ov_course_style"             => $ov_course_style,
    "ov_course_type"             => $ov_course_type,
//  "ov_tags"                    => $ov_tags,
    "ov_oid"                     => $ov_oid,
    "ov_coupon_id"               => $ov_coupon_id,
    "ov_coupon_type"             => $ov_coupon_type,
    "ov_video_address"           => $ov_video_address,
    "ov_series_time"             => $ov_series_time,
    "ov_video_time"              => $ov_video_time,
    "ov_is_stop"                 => $ov_is_stop,
    "ov_create_time"             => time(),
    "ov_teacher_name"            => $ov_teacher_name

];

//执行写入语句
$rsData = $db->mysqlDB->insert("organization_video",$insertArr);

//返回成功结果
$response->responseData( true,$rsData);