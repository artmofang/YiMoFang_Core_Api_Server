<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 17:04
 */

//获取参数

$ovd_id = $route->bodyParams["ovd_id"];  //主键ID

$whereArr = [
    "ovd_id" => $ovd_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_video_discuss",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );