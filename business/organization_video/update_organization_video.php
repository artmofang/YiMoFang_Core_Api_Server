<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2019/1/24
 * Time: 17:13
 * name:更新视频课
 * url:/organization_video/update_organization_video
 */

//获取参数
$ov_id = $route->bodyParams["ov_id"];

//更新条件
$whereArr = [
    "ov_id" => $ov_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("organization_video",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );