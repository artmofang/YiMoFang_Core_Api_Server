<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2019/1/24
 * Time: 17:11
 * name:删除视频课
 * url:/organization_video/delete_organization_video
 */

//获取参数

$ov_id = $route->bodyParams["ov_id"];  //主键ID

$whereArr = [
    "ov_id" => $ov_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_video",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );