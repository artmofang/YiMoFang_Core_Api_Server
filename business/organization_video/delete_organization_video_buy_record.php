<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2019/1/24
 * Time: 17:19
 * name:删除视频课视购买记录
 * url:/organization_video/delete_organization_video_buy_record
 */

//获取参数

$ovbr_id = $route->bodyParams["ovbr_id"];  //主键ID

$whereArr = [
    "ovbr_id" => $ovbr_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_video_buy_record",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );