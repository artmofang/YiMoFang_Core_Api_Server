<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 16:57
 */

//获取参数
$ovs_id= $route->bodyParams["ovs_id"];

//更新条件
$whereArr = [
    "ovs_id" => $ovs_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("organization_video_series",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );