<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/21
 * Time: 9:26
 * name:更新消费码
 * url:/order/update_consume_num
 */

//获取参数

$cn_id = $route->bodyParams["cn_id"]; //主键ID

//更新条件
$whereArr = [
    "cn_id" => $cn_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("consume_num",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );