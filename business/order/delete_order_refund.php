<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 14:26
 * name:
 * url:/order/delete_order_refund
 */

//获取参数

$of_id = $route->bodyParams["of_id"];  //主键ID

$whereArr = [
    "of_id" => $of_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("order_refund",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );