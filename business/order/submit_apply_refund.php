<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 17:35
 * 退款申请提交接口
 */
//获取参数信息
$of_refund_cash     = $route->bodyParams['of_refund_cash'];                        //订单价格
$of_refund_reason       = $route->bodyParams['of_refund_reason'];                  //退款原因
$of_dec     = $route->bodyParams['of_dec'];                                         //退款描述
$of_oid     = $route->bodyParams['of_oid'];                                         //订单id
$of_ono     = $route->bodyParams['of_ono'];                                         //订单编号


//构造存储过程参数
$proceArr = [
    $of_refund_cash,
    $of_refund_reason,
    $of_dec,
    $of_oid,
    $of_ono,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_SUBMIT_APPLY_REFUND", $proceArr);


//返回成功结果
$response->responseData(true);