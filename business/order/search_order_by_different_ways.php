<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/3
 * Time: 10:44
 * name:查询订单列表
 * url:/order/search_order_by_different_ways
 */

$skip = $route->bodyParams['skip'];//第几条

$limit = $route->bodyParams['limit'];//查询多少条

$w_status=$route->bodyParams['w_status']; //订单状态3全部2已支付已验证1已支付未验证0未支付

$o_no = $regexpObj->bodyV($response,$route,'o_no','NORMAL');//订单号
$o_oid = $regexpObj->bodyV($response,$route,'o_oid','NORMAL');//机构编号
$o_phone = $regexpObj->bodyV($response,$route,'o_phone','NORMAL');//手机号码

$start_time = $regexpObj->bodyV($response,$route,'start_time','NORMAL'); //开始时间
$end_time = $regexpObj->bodyV($response,$route,'end_time','NORMAL'); //结束时间


//查询条件
if($o_no){
    $order_no_condition ="WHERE `o_no` = $order_no";
}else if($o_oid){
    $order_no_condition ="WHERE `o_oid` = $o_oid";
}else if($o_phone){
    $order_no_condition ="WHERE `o_phone` = $o_phone";
}else{
    $order_no_condition = "";
}
//$response->responseData( true, array ('o_pay_status'=>$w_status) );
if($order_no_condition){
    $head='AND';
}else{
    $head='WHERE';
}
// 订单状态3全部2已支付已验证1已支付未验证0未支付
if($w_status==0){
    $order_status=$head." `o_pay_status` = 0";
}elseif ($w_status==1){
    $order_status=$head." `o_pay_status` = 1";
}elseif ($w_status==2){
    $order_status=$head."( `o_pay_status` = 3 OR `o_pay_status` = 7)";
}else{
    $order_status="";
}

if(empty($order_status) && empty($order_no_condition)){
    $head1='WHERE';
}else{
    $head1='AND';
}
//开始时间 or 结束时间
if($start_time!='' && $end_time!='' && $start_time!='undefined' && $end_time!='undefined'){
    $time_condition = $head1." `o_create_time` BETWEEN $start_time AND $end_time";
}else{
    $time_condition = "";
}

//提取数据数量
if($skip!= ''&& $limit!=''){
    $sql = "SELECT `o_create_time`,`o_course_name`,`o_no`,`o_old_price`,`o_pay_money`,`o_money`,`o_pay_status`,`o_oid` FROM `art_order`  $order_no_condition $order_status $time_condition ORDER BY o_create_time desc LIMIT $skip,$limit ";
}else{
    $sql = "SELECT `o_create_time`,`o_course_name`,`o_no`,`o_old_price`,`o_pay_money`,`o_money`,`o_pay_status`,`o_oid` FROM `art_order`  $order_no_condition $order_status $time_condition ORDER BY o_create_time desc LIMIT 0,10 ";

}
$countSql="SELECT count(*) as dataCount FROM `art_order` $order_no_condition $order_status $time_condition";

$dataCount=$db->mysqlDB->query($countSql);

$re = $db->mysqlDB->query($sql);

$rsData = array("data" => $re ,"dataCount" =>$dataCount[0]['dataCount']);

$response->responseData( true, $rsData );