<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/2
 * Time: 15:11
 * name:生成订单
 * url:/order/add_order
 */

$o_coupon_id = 0;
$o_oid                  = $route->bodyParams["o_oid"];                               //机构ID
$o_uid                  = $route->bodyParams["o_uid"];                               //用户ID
$o_name                 = $route->bodyParams["o_name"];                              //下单人姓名
$o_phone                = $route->bodyParams["o_phone"];                             //下单人电话
$o_age                  = $route->bodyParams["o_age"];                                //下单人年龄***
$o_sex                  = $route->bodyParams["o_sex"];                                //下单人性别 0-男 1-女 2-保密
$o_address              = $route->bodyParams["o_address"];                           //详细地址
$o_type                 = $route->bodyParams["o_type"];                              //订单的类型 0-机构活动 1-论坛活动 2-课程\r\n' 3-视频课程
$o_money                = $route->bodyParams["o_money"];                             //订单金额
// $o_pay_money            = $route->bodyParams["o_pay_money"];                        //实际支付金额
$o_pay_money            = $route->bodyParams["o_wechat_pay"];
$o_aoc_id               = $route->bodyParams["o_aoc_id"];                            //关联的活动或者课程id
$o_preferential_money   = $route->bodyParams["o_preferential_money"];              //优惠金额
$o_wechat_pay           = $route->bodyParams["o_wechat_pay"];                       //微信支付金额
$o_balance_pay          = $route->bodyParams["o_balance_pay"];                      //余额支付金额
$o_course_pic           = $route->bodyParams["o_course_pic"];                       //课程或活动的图片
$o_course_name          = $route->bodyParams["o_course_name"];                      //课程或活动的名称
$o_organization_name    = $route->bodyParams["o_organization_name"];               //机构名称
$o_old_price            = $route->bodyParams["o_old_price"];                        //原价
$o_new_price            = $route->bodyParams["o_new_price"];                        //现价
$o_pay_type            = $route->bodyParams["o_pay_type"];                        //支付类型  0-微信支付 1-余额支付 2-混合方式 3-其他',


$o_note                 = $regexpObj->bodyV($response,$route,'o_note','NORMAL');           //备注信息
$o_coupon_id            = $regexpObj->bodyV($response,$route,'o_coupon_id','NORMAL');     //关联的优惠券id
$o_activity_type        = $regexpObj->bodyV($response,$route,'o_activity_type','NUMBER');//如果为机构活动， 0-拼团 1-投票 2-砍价 3-H5海报 4-助力 5-促销活动 6-微传单
$o_custom               = $regexpObj->bodyV($response,$route,'o_custom','NORMAL');

if($o_preferential_money != 0){
    $allmoney = floatval($o_wechat_pay) + floatval($o_balance_pay);
    if(floatval($o_money) < floatval($allmoney)){
        return $response->responseData( false, "支付金额有误" );
    }
}

//写入数组
$insertArr = [
    "o_oid"                     => $o_oid,
    "o_uid"                     => $o_uid,
    "o_name"                    => $o_name,
    "o_phone"                   => $o_phone,
    "o_age"                     => $o_age,
    "o_sex"                     => $o_sex,
    "o_address"                 => $o_address,
    "o_type"                    => $o_type,
    "o_aoc_id"                  => $o_aoc_id,
    "o_money"                   => $o_money,
    "o_note"                    => $o_note,
    "o_coupon_id"              => $o_coupon_id,
    "o_activity_type"         => $o_activity_type,
    "o_preferential_money"   => $o_preferential_money,
    "o_pay_money"             => $o_pay_money,
    "o_balance_pay"          => $o_balance_pay,
    "o_wechat_pay"           =>$o_wechat_pay,
    "o_course_pic"           =>$o_course_pic,
    "o_course_name"          =>$o_course_name,
    "o_organization_name"   =>$o_organization_name,
    "o_old_price"            =>$o_old_price,
    "o_new_price"            =>$o_new_price,
    "o_custom"               =>$o_custom,
    "o_create_time"         => time(),
    "o_no"                   => date("Ymd",time()).rand(100000,999999),
    "o_pay_type"            =>$o_pay_type
];
if($o_coupon_id != 0){
    $sql = "update art_user_cash_coupon set ucc_is_used = 1 where ucc_id =".$o_coupon_id." and ucc_is_used = 0";
    $re=$db->mysqlDB->query($sql);
}

//  当为砍价活动的时候，首先的得看看用户有没有下单
if($o_type == 0 && $o_activity_type = 2 ){
    $sql1 = "select o_id from art_order where o_uid = $o_uid and o_aoc_id = $o_aoc_id and is_hide = 0 and o_pay_status = 0";
    $res  = $db->mysqlDB->query($sql1);
    $a = $res[0]["o_id"];
    //echo $a;
    if($a){
        $name = $o_name == "" ? "o_name = ''":"o_name = '$o_name '";
        $phone = $o_phone == "" ? "o_phone = ''": "o_phone = $o_phone";
        $age = $o_age == "" ? "o_age = ''": "o_age = $o_age";
        $sex= $o_sex == "" ? "o_sex = ''": "o_sex = $o_sex";
        $address = $o_address == "" ? "o_address = ''": "o_address = '$o_address '";
        $coupon_id = $o_coupon_id == "" ? "o_coupon_id = ''": "o_coupon_id = $o_coupon_id";
        $preferential_money = $o_preferential_money == "" ? "o_preferential_money = ''": "o_preferential_money = $o_preferential_money";
        $wechat_pay = $o_wechat_pay == "" ? "o_wechat_pay = ''": "o_wechat_pay = $o_wechat_pay";
        $note = $o_note == "" ? "o_note = ''": "o_note = '$o_note'";
        $sql2 = "update art_order set $name,$phone,$age,$sex,$address,$coupon_id,$preferential_money,$wechat_pay,$note WHERE o_id = $a";
     //echo $sql2;
//        exit();
        $result =  $res  = $db->mysqlDB->query($sql2);
        $response->responseData( true, $a);
    }
}
$rsData = $db->mysqlDB->insert("order",$insertArr);
$id = $rsData;
if($o_type == 0 && $o_activity_type = 2 ){
    $sql = "update art_bargain_join set bj_is_order = 1,bj_oid = $id where bj_uid =".$o_uid." and bj_oaid =".$o_aoc_id." and bj_is_order = 0";
    $re = $db->mysqlDB->query($sql);
}

//返回成功结果
$response->responseData( true, $rsData );