<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/2
 * Time: 15:22
 * name:删除订单
 * url:/order/delete_order
 */

//获取参数

$o_id = $route->bodyParams["o_id"];  //主键ID

$whereArr = [
    "o_id" => $o_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("order",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );
