<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/8
 * Time: 16:11
 * name:消费码信息更新
 * url:/order/update_verification_code
 */

//获取参数

$vc_id = $route->bodyParams["vc_id"]; //主键ID

//更新条件
$whereArr = [
    "vc_id" => $vc_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("verification_code",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );