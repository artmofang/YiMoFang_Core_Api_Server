<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/2
 * Time: 15:25
 * name:订单信息更新
 * url:/order/update_order
 */

//获取参数

$o_no = $route->bodyParams["o_no"]; //主键ID

//更新条件
$whereArr = [
    "o_no" => $o_no,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("order",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );