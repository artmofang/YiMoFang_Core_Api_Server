<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/21
 * Time: 9:23
 * name:删除消费码
 * url:/order/delete_consume_num
 */

//获取参数
$cn_id = $route->bodyParams["cn_id"];  //主键ID

$whereArr = [
    "cn_id" => $cn_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("consume_num",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );