<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 15:35
 * 机构财务管理
 */
$o_oid = $route->bodyParams['o_oid'];  //机构id

$year = $regexpObj->bodyV($response,$route,'year','NUMBER');    //年
$mouth = $regexpObj->bodyV($response,$route,'mouth','NUMBER');    //月

//查年列表
$sql1 = "select SUM(o_pay_money) AS `total`,
                    FROM_UNIXTIME(o_create_time,'%Y') AS `time`,
                    COUNT(*) AS `count`
                    FROM art_order
                    WHERE (o_pay_status = 1)
                    AND o_oid = ".$o_oid ."
                    GROUP BY YEAR(FROM_UNIXTIME(o_create_time,'%Y-%m-%d %H:%i:%S'))";

//查月列表
$sql2 = "select SUM(o_pay_money) AS `total`,
                    FROM_UNIXTIME(o_create_time,'%m') AS `time`,
                    COUNT(*) AS `count`
                    FROM art_order
                    WHERE (o_pay_status = 1)
                    AND o_oid = ".$o_oid ."
                    AND FROM_UNIXTIME(o_create_time,'%Y') = ".$year."
                    GROUP BY MONTH(FROM_UNIXTIME(o_create_time,'%Y-%m-%d %H:%i:%S'))";
//查天列表
$sql3 = "select SUM(o_pay_money) AS `total`,
                    FROM_UNIXTIME(o_create_time,'%d') AS `time`,
                    COUNT(*) AS `count`
                    FROM art_order
                    WHERE (o_pay_status = 1)
                    AND o_oid = ".$o_oid ."
                    AND FROM_UNIXTIME(o_create_time,'%m') = ".$mouth."
                    AND FROM_UNIXTIME(o_create_time,'%Y') = ".$year."
                    GROUP BY DAY(FROM_UNIXTIME(o_create_time,'%Y-%m-%d %H:%i:%S'))";

if($year && $mouth){
    $sql = $sql3;
}elseif($year){
    $sql = $sql2;
}else{
    $sql = $sql1;
}

$re=$db->mysqlDB->query($sql);

$res=array("data"=>$re);

$response->responseData(true, $res);