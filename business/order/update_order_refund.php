<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 14:47
 * name:更新退款信息
 * url:/order/update_order_refund
 */

//获取参数

$of_id = $route->bodyParams["of_id"]; //主键ID

//更新条件
$whereArr = [
    "of_id" => $of_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("order_refund",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );