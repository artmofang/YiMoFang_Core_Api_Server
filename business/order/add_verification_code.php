<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/8
 * Time: 16:02
 * name:生成消费码
 * url:/order/add_verification_code
 */

$rc_oid      = $route->bodyParams["vc_oid"];                              //关联机构ID
$rc_uid      = $route->bodyParams["vc_uid"];                              //关联用户ID
$rc_order_id      = $route->bodyParams["vc_order_id"];                    //关联订单ID



//写入数组
$insertArr = [
    "vc_oid"             => $rc_oid,
    "vc_uid"             => $rc_uid,
    "vc_order_id"       => $rc_order_id,
    "vc_create_time"    => time(),
    "vc_verification"   =>"ART".rand(000000,999999)
];

//执行写入语句
$rsData = $db->mysqlDB->insert("verification_code",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );