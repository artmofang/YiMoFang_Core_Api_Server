<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/6/1
 * Time: 21:05
 * name:余额退款
 * url:/order/balance_refund
 */

//获取参数信息
$_uid               = $route->bodyParams['_uid'];                  //用户id
$_blance_pay_money  = $route->bodyParams['_blance_pay_money'];   //余额支付金额

//构造存储过程参数
$proceArr = [
    $_uid,
    $_blance_pay_money
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_BALANCE_REFUND", $proceArr);


//返回成功结果
$response->responseData(true);