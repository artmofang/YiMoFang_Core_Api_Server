<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/8
 * Time: 16:09
 * name:删除消费码
 * url:/order/delete_verification_code
 */

//获取参数

$vc_id = $route->bodyParams["vc_id"];  //主键ID

$whereArr = [
    "vc_id" => $vc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("verification_code",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );