<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/21
 * Time: 9:16
 * name:生成消费码
 * url:/order/add_consume_num
 */

$cn_uid      = $route->bodyParams["cn_uid"];   //关联的用户id
$cn_oid      = $route->bodyParams["cn_oid"];   //关联的机构id
$cn_coid      = $route->bodyParams["cn_coid"]; //关联的课程id
$cn_ono      = $route->bodyParams["cn_ono"];   //关联的订单编号

//写入数组
$insertArr = [
    "cn_uid"             => $cn_uid,
    "cn_oid"             => $cn_oid,
    "cn_coid"            => $cn_coid,
    "cn_ono"             => $cn_ono,
    "cn_number"          => rand(1000000000,9999999999).date("md",time()),
    "cn_create_time"     => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("consume_num",$insertArr);

$sql = "select cn_number from art_consume_num where cn_id = ".$rsData;

$cn_number = $db->mysqlDB->query($sql);




//返回成功结果
$response->responseData( true,$cn_number[0]["cn_number"]);