<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/23
 * Time: 14:03
 * name:生成退款信息
 * url:/order/add_order_refund
 */

$of_oid           = $route->bodyParams['of_oid'];                                    //关联订单ID
$of_refund_cash   = $route->bodyParams['of_refund_cash'];                           //申请退款金额
$of_refund_reason = $regexpObj->bodyV($response,$route,'of_refund_reason','NORMAL');//退款原因
$of_dec           = $regexpObj->bodyV($response,$route,'of_dec','NORMAL');           //退款备注信息

//写入数组
$insertArr = [
    'of_oid'            => $of_oid,
    'of_refund_cash'   => $of_refund_cash,
    'of_refund_reason' => $of_refund_reason,
    'of_dec'            => $of_dec,
    'of_apply_time'    => time(),

];

//执行写入语句
$rsData = $db->mysqlDB->insert("order_refund",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );