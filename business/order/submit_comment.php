<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 17:35
 * 订单评价提交接口
 */
//获取参数信息
$o_id     = $route->bodyParams['o_id'];                        //订单id
$o_type       = $route->bodyParams['o_type'];                  //订单类型   0 机构活动   2 课程
$o_oid     = $route->bodyParams['o_oid'];                      //机构id
$o_oac_id     = $route->bodyParams['o_oac_id'];                //关联的课程或者活动和id
$o_uid     = $route->bodyParams['o_uid'];                      //评论人的用户id
$level     = $route->bodyParams['level'];                      //评分
$content     = $route->bodyParams['content'];                 //内容


//构造存储过程参数
$proceArr = [
    $o_id,
    $o_type,
    $o_oid,
    $o_oac_id,
    $o_uid,
    $level,
    $content,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_ORDER_SUBMIT_COMMENT", $proceArr);


//返回成功结果
$response->responseData( true);