<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 14:19
 * 更新达人用户信息
 */
$iuc_id = $route->bodyParams["iuc_id"];

//更新条件
$whereArr = [
    "iuc_id" => $iuc_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("intelligent_user_config",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );