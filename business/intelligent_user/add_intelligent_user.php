<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 11:30
 * 添加达人用户
 */
$iuc_uid = $route->bodyParams['iuc_uid'];//关联的用户id
$iuc_article_count = $route->bodyParams['iuc_article_count'];//文章总数
$iuc_fans_count = $route->bodyParams['iuc_fans_count'];//粉丝数
$iuc_collect_count = $route->bodyParams['iuc_collect_count'];//收藏数
$iuc_browse_count = $route->bodyParams['iuc_browse_count'];//浏览数
$iuc_invite_count = $route->bodyParams['iuc_invite_count'];//被邀请数



$iuc_level = $regexpObj->bodyV($response,$route,'iuc_level','NUMBER');//达人等级
$iuc_sign = $regexpObj->bodyV($response,$route,'iuc_sign','NORMAL');//个性签名

//写入数组
$insertArr = [
    'iuc_uid'=> $iuc_uid,
    'iuc_article_count'=> $iuc_article_count,
    'iuc_fans_count'=>$iuc_fans_count,
    'iuc_collect_count'=>$iuc_collect_count,
    'iuc_browse_count'=>$iuc_browse_count,
    'iuc_invite_count'=>$iuc_invite_count,
    'iuc_level'=>$iuc_level,
    'iuc_sign'=>$iuc_sign,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("intelligent_user_config",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );