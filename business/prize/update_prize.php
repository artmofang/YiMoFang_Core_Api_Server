<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 11:04
 * name:更新奖品
 * url:/prize/update_prize
 */

//获取参数

$p_id       = $route->bodyParams["p_id"];                                        //主键ID

//更新条件
$whereArr = [
    "p_id" => $p_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("prize",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );