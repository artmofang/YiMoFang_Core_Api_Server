<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 10:32
 * name:添加奖品
 * url:/prize/add_prize
 */

//获取参数
$p_name         = $route->bodyParams["p_name"];                            //商品名称
$p_price        = $route->bodyParams["p_price"];                           //商品价格
$p_title_image  = $route->bodyParams["p_title_image"];                    //奖品图片
$p_type         = $route->bodyParams["p_type"];                            //奖品类型 0-平台 1-机构
$p_oid          = $route->bodyParams["p_oid"];                             //奖品所属的机构ID
$p_desc         = $regexpObj->bodyV($response,$route,'p_desc','NORMAL');   //奖品的描述


//写入数组
$insertArr = [
    "p_name"            => $p_name,
    "p_price"           => $p_price,
    "p_title_image"    => $p_title_image,
    "p_type"            => $p_type,
    "p_oid"             => $p_oid,
    "p_desc"            => $p_desc,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("prize",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );