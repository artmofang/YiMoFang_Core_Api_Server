<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 10:48
 * name:删除奖品
 * url:/prize/delete_prize
 */

//获取参数

$p_id = $route->bodyParams["p_id"];  //主键ID

$whereArr = [
    "p_id" => $p_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("prize",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );