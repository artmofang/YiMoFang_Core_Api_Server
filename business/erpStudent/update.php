<?php
//获取参数
$os_id = $route->bodyParams["os_id"];//主键ID

//更新条件
$whereArr = [
    "os_id" => $os_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_student",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );