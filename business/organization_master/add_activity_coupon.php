<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/9
 * Time: 14:30
 */

$ac_oid       = $route->bodyParams["ac_oid"];                                     //机构id
$ac_oaid       = $route->bodyParams["ac_oaid"];                                   //活动id
$ac_type      = $route->bodyParams["ac_type"];                                    //券类型 0代金券  1兑换券
$ac_fid       = $route->bodyParams["ac_fid"];                                     //券id
$ac_oatype       = $route->bodyParams["ac_oatype"];                                     //券id



//写入数组
$insertArr = [
    "ac_oid"             => $ac_oid,
    "ac_oaid"            => $ac_oaid,
    "ac_type"            => $ac_type,
    "ac_fid"             => $ac_fid,
    "ac_oatype"          => $ac_oatype,
    
];

//执行写入语句
$rsData = $db->mysqlDB->insert("activity_coupon",$insertArr);

//返回成功结果
$response->responseData( true,$rsData);
