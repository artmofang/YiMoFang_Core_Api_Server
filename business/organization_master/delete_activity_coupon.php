<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/9
 * Time: 15:19
 */
//获取参数
$ac_oaid = $route->bodyParams["ac_oaid"];//主键ID

$whereArr = [
    "ac_oaid" => $ac_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("activity_coupon",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );