<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/18
 * Time: 14:52
 * name:验证订单更新订单状态
 * url:/organization_master/order_verification
 */

//获取参数信息
$_o_no           = $route->bodyParams['_o_no'];                  //订单编号
$_cn_number      = $route->bodyParams['_cn_number'];            //订单消费码
$oid    = $route->bodyParams['_oid'];            //机构id

//构造存储过程参数
$proceArr = [
    $_o_no,
    $_cn_number,
    $oid,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_ORDER_VERIFICATION", $proceArr);


//返回成功结果
$response->responseData( true);

