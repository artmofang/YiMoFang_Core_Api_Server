<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/18
 * Time: 15:45
 * name:订单退款审核
 * url:/organization_master/order_refund
 */

//获取参数信息
$_o_id           = $route->bodyParams['_o_id'];                  //订单ID
$_o_no           = $route->bodyParams['_o_no'];                  //订单编号
$_uid            = $route->bodyParams['_uid'];                  //用户id

//构造存储过程参数
$proceArr = [
    $_o_id,
    $_o_no,
    $_uid
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_AGREE_REFUND", $proceArr);


//返回成功结果
$response->responseData( true);