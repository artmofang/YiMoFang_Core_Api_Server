<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:15
 * name:机构砍价活动配置删除
 * url:/bargain/delete_bargain_activity_config
 */

//获取参数
$bc_id = $route->bodyParams['bc_id'];//主键ID

//拼接删除条件
$whereArr = [
    'bc_id'=>$bc_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("bargain_config",$whereArr,false);

//返回结果
$response->responseData(true,$reData);