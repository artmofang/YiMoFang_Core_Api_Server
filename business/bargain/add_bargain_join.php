<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:22
 * name:砍价活动参与者添加
 * url:/bargain/bargain_join
 */

$bj_uid               = $route->bodyParams['bj_uid'];                                        //参与者ID
$bj_oaid               = $route->bodyParams['bj_oaid'];                                      //关联活动ID

//写入数组
$insertArr = [
    'bj_uid'=> $bj_uid,
    'bj_oaid'=> $bj_oaid
];

//执行写入语句
$rsData = $db->mysqlDB->insert("bargain_join",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );
