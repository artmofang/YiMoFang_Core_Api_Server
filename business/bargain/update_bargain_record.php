<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:35
 * name:机构砍价活动砍价记录更新
 * url:/bargain/update_bargain_record
 */

//获取参数
$br_id       = $route->bodyParams["br_id"];  //主键ID

//更新条件
$whereArr = [
    "br_id" => $br_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("bargain_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );