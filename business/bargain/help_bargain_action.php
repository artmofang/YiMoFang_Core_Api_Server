<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 17:35
 */
//获取参数信息
$bj_id   = $route->bodyParams['bj_id'];
$oa_id   = $route->bodyParams['oa_id'];
$cut_uid = $route->bodyParams['cut_uid'];                        


//构造存储过程参数
$proceArr = [
    $bj_id,
    $oa_id,
    $cut_uid
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_HRLP_BARGAIN", $proceArr,"DATA");

//返回成功结果
$response->responseData( true,$data);