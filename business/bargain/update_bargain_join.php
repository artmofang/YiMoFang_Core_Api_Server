<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:29
 * name:机构砍价活动参与者更新
 * url:/bargain/update_bargain_join
 */

//获取参数
$bj_id       = $route->bodyParams["bj_id"];  //主键ID

//更新条件
$whereArr = [
    "bj_id" => $bj_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("bargain_join",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );