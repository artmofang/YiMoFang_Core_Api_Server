<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:26
 * name:机构砍价活动参与者删除
 * url:/bargain/delete_bargain_join
 */

//获取参数
$bj_id = $route->bodyParams['bj_id']; //主键ID

//拼接删除条件
$whereArr = [
    'bj_id'=>$bj_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("bargain_join",$whereArr,false);

//返回结果
$response->responseData(true,$reData);