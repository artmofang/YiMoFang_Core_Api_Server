<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:19
 * name:机构砍价活动配置更新
 * url:/bargain/update_bargain_activity_config
 */

//获取参数
$bc_id       = $route->bodyParams["bc_id"];  //主键ID

//更新条件
$whereArr = [
    "bc_id" => $bc_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("bargain_config",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );