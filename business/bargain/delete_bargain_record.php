<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:34
 * name:机构砍价活动砍价记录删除
 * url:/bargain/delete_bargain_record
 */

//获取参数
$br_id = $route->bodyParams['br_id'];//主键ID

//拼接删除条件
$whereArr = [
    'br_id'=>$br_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("bargain_record",$whereArr,false);

//返回结果
$response->responseData(true,$reData);