<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/18
 * Time: 17:35
 */
//获取参数信息
$oa_id   = $route->bodyParams['oa_id'];
$u_id = $route->bodyParams['u_id'];                        
$bj_phone = $route->bodyParams['bj_phone'];
//$bj_phone = 18629267064;

//构造存储过程参数
$proceArr = [
    $oa_id,
    $u_id,
    $bj_phone
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_JOIN_BARGAIN_ACTIVITY", $proceArr);

//返回成功结果
$response->responseData( true);