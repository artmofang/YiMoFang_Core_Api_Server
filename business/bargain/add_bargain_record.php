<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/9
 * Time: 14:31
 * name:机构砍价活动砍价记录添加
 * url:/bargain/add_bargain_record
 */

$br_fid               = $route->bodyParams['br_fid'];          //参与用户ID
$br_cut_id            = $route->bodyParams['br_cut_id'];       //砍价用户ID
$br_cut_price         = $route->bodyParams['br_cut_price'];    //此次砍价金额

//写入数组
$insertArr = [
    'br_fid'        => $br_fid,
    'br_cut_id'     => $br_cut_id,
    'br_cut_price'  => $br_cut_price,
    'br_cut_time'   => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("bargain_record",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );