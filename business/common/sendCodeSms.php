<?php
require_once("tool/Network.class.php");
use tool\NetworkClass\Network;

$netObj = new Network();

$phone = $route->bodyParams['phone'];
$type  = $route->bodyParams['type'];

$sign     = "好校长";
$tempCode = "";

//注册短信
if($type == 1){


    $tempCode = "SMS_134170123";

    //获取6位随机数字
    $randStr = rand(pow(10,(6-1)), pow(10,6)-1);

    //过期时间
    $endTiem = time() + 600; //10分钟

    //查询该手机号是否已经存在
    $sql = "select count(*) as sum from art_sms where s_phone = " . $phone;

    $rsData = $db->mysqlDB->query($sql);

    $sum = $rsData[0]['sum'];

    if($sum > 0){

        //更新操作
        $sql = "update art_sms set s_code = '" .$randStr . "', s_end_time = " .$endTiem . " where s_phone = '" .$phone . "'";

        $rsData = $db->mysqlDB->query($sql);

    }else{

        //新增
        $sql = "insert into art_sms values(NULL,$randStr,$phone,$endTiem)";

        $rsData = $db->mysqlDB->query($sql);

    }

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendSms.php?phone=$phone&tempSign=$sign&tempCode=$tempCode&randStr=$randStr";
    $data = $netObj->GET($sendUrl);

    echo $data;

//退款短信通知
}else if($type == 2){

    $tempCode = "SMS_136398452";

    $money = $route->bodyParams['money'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/refundSuccess.php?phone=$phone&tempSign=$sign&tempCode=$tempCode&money=$money";
    $data = $netObj->GET($sendUrl);

    echo $data;

//拼团成功通知
}else if($type == 3){

    $tempCode = "SMS_166868615";

    $activity_name = mb_strcut($route->bodyParams['activity_name'],0,17).'...';
    $order_no = $route->bodyParams['order_no'];
    $consumer_code = $route->bodyParams['consumer_code'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendSpellGroupSuccess.php?phone=$phone&tempSign=$sign&tempCode=$tempCode&activity_name=$activity_name&order_no=$order_no&consumer_code=$consumer_code";
    $data = $netObj->GET($sendUrl);

    echo $data;

//约课成功通知
}else if($type == 4){

//  $tempCode = "SMS_136860291";
    $tempCode = "SMS_172888338";

    $o_name = $route->bodyParams['o_name'];
	$o_phone = $route->bodyParams['o_phone'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendAboutClassSuccess.php?phone=$phone&tempSign=$sign&tempCode=$tempCode&o_name=$o_name&o_phone=$o_phone";
    $data = $netObj->GET($sendUrl);

    echo $data;

//砍价到底的短信通知    
}else if($type == 5){

    $tempCode = "SMS_166868611";

    $activity_name = mb_strcut($route->bodyParams['activity_name'],0,17).'...';
    $money = $route->bodyParams['money'];
    $end_time = $route->bodyParams['end_time'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendBargainingSuccess.php?phone=$phone&tempSign=$sign&tempCode=$tempCode&activity_name=$activity_name&money=$money&end_time=$end_time";
    $data = $netObj->GET($sendUrl);

    echo $data;

//支付成功的短信通知    
}else if($type == 6){

    $tempCode = "SMS_138063316";

    $money = $route->bodyParams['money'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendPaySuccess.php?phone=$phone&tempSign=$sign&tempCode=$tempCode&money=$money";
    $data = $netObj->GET($sendUrl);

    echo $data;

//机构版内容迁移通知
}else if($type == 7){

    $tempCode = "SMS_142153408";

    $username = $route->bodyParams['username'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendOrganizationMigrationSuccess.php?phone=$phone&username=$username&tempSign=$sign&tempCode=$tempCode";
    
    $data = $netObj->GET($sendUrl);

    echo $data;
    
//支付成功（带机构名和消费码的短信）    
}else if($type == 8){

    $tempCode = "SMS_153991436";

    $name = $route->bodyParams['name'];
    $oname = $route->bodyParams['oname'];
    $money = $route->bodyParams['money'];
    $number = $route->bodyParams['number'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendPaySuccessNew.php?phone=$phone&name=$name&oname=$oname&tempSign=$sign&tempCode=$tempCode&money=$money&number=$number";
    
    $data = $netObj->GET($sendUrl);

    echo $data;

//好校长购买成功通知短信    
}else if($type == 9){

    $tempCode = "SMS_159780226";

    $username = $route->bodyParams['username'];
    $password = $route->bodyParams['password'];

    $sendUrl = "http://localhost/YiMoFang_Service/AliyunSMS/sendArtProductPaySuccess.php?phone=$phone&username=$username&password=$password&tempSign=$sign&tempCode=$tempCode";
    
    $data = $netObj->GET($sendUrl);

    echo $data;

}



