<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 18:55
 * Name: 添加艺术分类
 */
//获取参数
$c_fid = $route->bodyParams['c_fid'];//所属的父类ID 0-为根类别
$c_name = $route->bodyParams['c_name'];//类别名称
$c_path = $route->bodyParams['c_path'];//类别名称

$c_icon  = $regexpObj->bodyV($response,$route,'c_icon','NORMAL');
$c_order  = $regexpObj->bodyV($response,$route,'c_order','NUMBER');
//写入数组
$insertArr = [
    'c_fid' =>$c_fid,
    'c_name' =>$c_name,
    'c_path' =>$c_path,
    'c_icon' =>$c_icon,
    'c_order' =>$c_order,
];
//执行写入语句
$rsData = $db->mysqlDB->insert("category",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );