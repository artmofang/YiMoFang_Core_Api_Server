<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 19:39
 * 删除艺术分类
 */
//获取参数值
$cid = $route->bodyParams['cid'];

//拼接删除条件
$whereArr = [
    'cid'=>$cid
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("category",$whereArr,false);

//返回结果
$response->responseData(true,$reData);