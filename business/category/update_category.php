<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 19:57
 * 修改艺术分类信息
 */
$cid = $route->bodyParams["cid"];

//更新条件
$whereArr = [
    "cid" => $cid
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("category",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );