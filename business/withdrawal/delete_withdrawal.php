<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/20
 * Time: 11:42
 * 删除提现信息
 */
$w_id = $route->bodyParams['w_id'];

//拼接删除条件
$whereArr = [
    'w_id'=>$w_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("withdrawal",$whereArr,false);

//返回结果
$response->responseData(true,$reData);