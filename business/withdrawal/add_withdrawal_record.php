<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/20
 * Time: 12:00
 *添加体现记录
 */


$wdr_oid = $route->bodyParams['wdr_oid'];//机构id
$wdr_money = $route->bodyParams['wdr_money'];//提现金额
$wdr_practical_money = $route->bodyParams['wdr_practical_money'];//机构实收金额
$wdr_create_time  = time();//提现时间
$wdr_desc       = $regexpObj->bodyV($response,$route,'wdr_desc','NORMAL');          //机构标签用 | 隔开

//写入数组
$insertArr = [
    'wdr_oid'=>$wdr_oid,
    'wdr_money'=>$wdr_money,
    'wdr_status'=>1,
    'wdr_desc'=>$wdr_desc,
    'wdr_practical_money'=>$wdr_practical_money,
    'wdr_create_time'=>$wdr_create_time
];

//执行写入语句
$rsData = $db->mysqlDB->insert('withdraw_deposit_record',$insertArr);

//获取返回值
$response->responseData( true, $rsData );