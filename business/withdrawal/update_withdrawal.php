<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/20
 * Time: 11:47
 * 更新提现信息
 */
$w_id = $route->bodyParams["w_id"];

//更新条件
$whereArr = [
    "w_id" => $w_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("withdrawal",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );