<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/20
 * Time: 10:59
 * 添加提现申请
 */
$w_oid = $route->bodyParams['w_oid'];//机构id
$w_organization_name = $route->bodyParams['w_organization_name'];//机构名称
$w_house_name = $route->bodyParams['w_house_name'];//户主姓名
$w_bank_number = $route->bodyParams['w_bank_number'];//银行卡号
$w_bank_name = $route->bodyParams['w_bank_name'];//银行名称
$w_bank_branch = $route->bodyParams['w_bank_branch'];//支行名称
$w_bank_phone = $route->bodyParams['w_bank_phone'];//提现联系手机号
$w_withdrawal_amount = $route->bodyParams['w_withdrawal_amount'];//提现金额
$w_amount_payable = $w_withdrawal_amount * 0.92;
$w_pay_amount = $route->bodyParams['w_pay_amount'];//实付金额
$w_status = $route->bodyParams['w_status'];//审核状态 0-未审核 1-已通过 2-未通过',
$w_uid = $route->bodyParams['w_uid'];//关联用户ID',



//写入数组
$proceArr = [
   $w_oid,
   $w_withdrawal_amount,
   $w_organization_name,
   $w_house_name,
   $w_bank_number,
   $w_bank_name ,
   $w_bank_branch,
   $w_bank_phone,
   $w_amount_payable ,
   $w_pay_amount ,
   $w_uid ,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_WITHDEAW_ACTION", $proceArr);

//返回成功结果
$response->responseData( true);

