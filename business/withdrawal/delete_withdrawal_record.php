<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/23
 * Time: 9:10
 *
 * 删除提现记录
 */
$wdr_id = $route->bodyParams['wdr_id'];

//拼接删除条件
$whereArr = [
    'wdr_id'=>$wdr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("withdraw_deposit_record",$whereArr,false);

//返回结果
$response->responseData(true,$reData);