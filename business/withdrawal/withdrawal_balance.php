<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/20
 * Time: 12:00
 * 获取机构的可提现额度
 */


//获取参数信息
$wdr_oid = $route->bodyParams['wdr_oid'];

//查询该机构本身可提现的总余额
$sql_1 = "select sum(o_pay_money) as _totalMoney from art_order where (o_pay_status = 3 OR o_pay_status = 7) AND o_oid = $wdr_oid";

$res_1 = $db->mysqlDB->query($sql_1);

$totalMoney = $res_1[0]['_totalMoney'];

//查询该机构本身已提出了多少钱

$sql_2 = "select sum(wdr_money) as _withdeawEndMoney from art_withdraw_deposit_record where wdr_status = 1  AND wdr_oid = $wdr_oid";

$res_2 = $db->mysqlDB->query($sql_2);

$withdeawEndMoney = $res_2[0]['_withdeawEndMoney'];

//已申请，未处理的金额

$sql_3 = "SELECT SUM(`w_withdrawal_amount`) AS `apply_money` FROM `art_withdrawal` WHERE `w_status` = 0 AND `w_oid` = $wdr_oid";

$apply = $db->mysqlDB->query($sql_3);

$apply_money = $apply[0]['apply_money'];

//判断剩余额度
$balance = $totalMoney - $withdeawEndMoney - $apply_money;

$diff = sprintf("%.2f", $balance);

$moneyData = array(
    "totalMoney" => $totalMoney,
    "withdeawEndMoney" => $withdeawEndMoney,
    "diff" => $diff
);

$res = array("data" => $moneyData);

$response->responseData(true, $res);

