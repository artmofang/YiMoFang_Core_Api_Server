<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/20
 * Time: 9:14
 * name:省市区添加
 * url:/location/add
 */
//获取参数
$l_fid       = $route->bodyParams["l_fid"];                               //父ID
$l_name      = $route->bodyParams["l_name"];                             //名称
$l_type      = $route->bodyParams["l_type"];                             //0-省份 1-城市 2-地区
$is_view     = $regexpObj->bodyV($response,$route,'is_view','NUMBER');   //是否显示 0-否 1-是

//写入数组
$insertArr = [
    "l_fid"      => $l_fid,
    "l_name"     => $l_name,
    "l_type"     => $l_type,
    "is_view"    => $is_view,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("location",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );
