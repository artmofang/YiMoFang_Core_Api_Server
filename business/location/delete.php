<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/20
 * Time: 9:22
 * name:省市区删除
 * url:/location/delete
 */
//获取参数

$l_id        = $route->bodyParams["l_id"];  //主键ID

$whereArr = [
    "l_fid" => $l_id
];
$where = [
    "l_id" => $l_id
];
$info = $db->mysqlDB->params($where,$route->restfulParams)->select("location"); //查询所传主键ID是否有数据匹配
if($info){
    $exists = $db->mysqlDB->params($whereArr,$route->restfulParams)->select("location"); //查询是否存在下级单位

    if($exists){
        //下级单位存在，返回错误信息
        $response->responseData( false,'请先删除下级单位' );
    }else{
        //下级单位不存在，执行写入语句
        $rsData = $db->mysqlDB->delete("location",$where,false);

        //返回成功结果
        $response->responseData( true, $rsData );
    }
}else{
    $response->responseData( false,'数据不存在' );
}
