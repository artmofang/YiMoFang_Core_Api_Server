<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/20
 * Time: 9:23
 * name:省市区信息更新
 * url:/location/update
 */
$l_id       = $route->bodyParams["l_id"];                                        //主键ID

//更新条件
$whereArr = [
    "l_id" => $l_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("location",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );