<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 10:25
 * 添加兑换券信息
 */
$ec_name = $route->bodyParams['ec_name'];//兑换券名称
$ec_start_time = $route->bodyParams['ec_start_time'];//兑换券有效期开始时间
$ec_end_time = $route->bodyParams['ec_end_time'];//兑换券有效期结束时间
$ec_type = $route->bodyParams['ec_type'];//兑换券类型 0-平台兑换劵 1-机构兑换劵
$ec_oid = $route->bodyParams['ec_oid'];//兑换券所属机构
$ec_max_count = $route->bodyParams['ec_max_count'];//最大领取数量
$ec_pid = $route->bodyParams['ec_pid'];//奖品名称

$ec_create_time = time();//创建时间
$ec_desc = $regexpObj->bodyV($response,$route,'ec_desc','NORMAL');//兑换券描述信息


//写入数组
$insertArr = [
    'ec_name'=>$ec_name,
    'ec_start_time'=>$ec_start_time,
    'ec_end_time' => $ec_end_time,
    'ec_type'=>$ec_type,
    'ec_oid'=>$ec_oid,
    'ec_max_count'=>$ec_max_count,
    'ec_pid'=>$ec_pid,
    'ec_create_time'=>$ec_create_time,
    'ec_desc'=>$ec_desc,
];
//执行写入语句
$rsData = $db->mysqlDB->insert("exchange_coupon",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );