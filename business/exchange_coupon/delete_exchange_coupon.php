<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 11:04
 * 删除兑换券
 */
//获取参数值
$ec_id = $route->bodyParams['ec_id'];

//拼接删除条件
$whereArr = [
    'ec_id'=>$ec_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("exchange_coupon",$whereArr,false);

//返回结果
$response->responseData(true,$reData);
