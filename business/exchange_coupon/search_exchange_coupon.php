<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 10:58
 * 查询兑换券信息
 */
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){
    
    unset($route->restfulParams['count']);
    
    //获取到记录的总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count('exchange_coupon');
    
}

//根据相应的条件查询


if(isset($route->restfulParams['is_all'])){
    unset($route->restfulParams['is_all']);
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("exchange_coupon");
}else{
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("exchange_coupon");
    $arr = array();
    for($i = 0;$i<count($rsData);$i++){
        if($rsData[$i]["ec_end_time"]>time()){
            array_push($arr,$rsData[$i]);
        }
    }
}




//拼接获得数据
$rs = array( "count" => $sum , "data" => $arr );


//返回成功结果
$response->responseData( true, $rs );