<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 11:01
 * 修改兑换券信息
 */
$ec_id = $route->bodyParams["ec_id"];

//更新条件
$whereArr = [
    "ec_id" => $ec_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("exchange_coupon",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );