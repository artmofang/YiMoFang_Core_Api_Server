<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 19:58
 */

//获取参数
$u_username      = $route->bodyParams["u_username"];                             //帐号
$u_password      = $route->bodyParams["u_password"];                             //密码
$u_nickname      = $regexpObj->bodyV($response,$route,'u_nickname','');            //昵称
$u_type          = $regexpObj->bodyV($response,$route,'u_type','NUMBER');        //用户类型 0-普通用户 1-商户 2-老师  3-达人
$u_realname      = $regexpObj->bodyV($response,$route,'u_realname','NORMAL');    //真实姓名
$u_birthday      = $regexpObj->bodyV($response,$route,'u_birthday','NORMAL');    //生日
$u_sex           = $regexpObj->bodyV($response,$route,'u_sex','NUMBER');         //性别 0-男 1-女 2-保密
$u_phone         = $regexpObj->bodyV($response,$route,'u_phone','NUMBER');       //手机电话
$u_address       = $regexpObj->bodyV($response,$route,'u_address','NORMAL');     //详细地址
$u_sign          = $regexpObj->bodyV($response,$route,'u_sign','NORMAL');        //个性签名
$u_province      = $regexpObj->bodyV($response,$route,'u_province','NUMBER');    //所属省份ID
$u_city          = $regexpObj->bodyV($response,$route,'u_city','NUMBER');        //所属城市ID
$u_area          = $regexpObj->bodyV($response,$route,'u_area','NUMBER');        //所属区域ID
$u_balance       = $regexpObj->bodyV($response,$route,'u_balance','MONEY');     //余额
$u_integration   = $regexpObj->bodyV($response,$route,'u_integration','NUMBER'); //积分
$u_status        = $regexpObj->bodyV($response,$route,'u_status','NUMBER');        //0-未审核 1-审核通过 2-禁用
$openid          = $regexpObj->bodyV($response,$route,'openid','NORMAL');        //微信
$u_header_url    = $regexpObj->bodyV($response,$route,'u_header_url','NORMAL');        //头像
$u_oid           = $regexpObj->bodyV($response,$route,'u_oid','NORMAL');        //关联机构id

//写入数组
$insertArr = [
    "u_username"     => $u_username,
    "u_password"     => md5($u_password),
    "u_nickname"     => $u_nickname,
    "u_type"         => $u_type,
    "u_realname"     => $u_realname,
    "u_birthday"     => $u_birthday,
    "u_sex"          => setDefaultValue($u_sex,0),
    "u_phone"        => $u_phone,
    "u_address"      => $u_address,
    "u_sign"         => $u_sign,
    "u_province"     => setDefaultValue($u_province,0),
    "u_city"         => setDefaultValue($u_city,0),
    "u_area"         => setDefaultValue($u_area,0),
    "u_balance"      => $u_balance,
    "u_integration"  => $u_integration,
    "u_status"        => $u_status,
    "u_create_time"  => time(),
    "openid"          => $openid,
    "u_oid"           => $u_oid,
    "u_header_url"   => $u_header_url
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );

