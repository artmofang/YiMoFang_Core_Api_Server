<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 18:18
 */
$ubc_id                = $route->bodyParams["ubc_id"];

//更新条件
$whereArr = [
    "ubc_id" => $ubc_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_bank_card",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );