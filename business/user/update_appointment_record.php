<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/27
 * Time: 15:42
 */

$ar_id      = $route->bodyParams["ar_id"];                                        //用户账号

//更新条件
$whereArr = [
    "ar_id" => $ar_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("appointment_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );