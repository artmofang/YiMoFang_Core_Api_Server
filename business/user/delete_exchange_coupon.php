<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 16:38
 */
//获取参数

$uec_id = $route->bodyParams["uec_id"];//主键ID

$whereArr = [
    "uec_id" => $uec_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_exchange_coupon",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );
