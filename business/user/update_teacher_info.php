<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 14:36
 * name:更新身份为教师的用户信息
 * url:/user/update_teacher_info
 */

//获取参数

$uti_id       = $route->bodyParams["uti_id"];                                        //主键ID

//更新条件
$whereArr = [
    "uti_id" => $uti_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_teacher_info",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );