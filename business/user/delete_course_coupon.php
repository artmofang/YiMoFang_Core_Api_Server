<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 15:44
 */
$ucc_id = $route->bodyParams["ucc_id"];//主键ID

$whereArr = [
    "ucc_id" => $ucc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_course_coupon",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );