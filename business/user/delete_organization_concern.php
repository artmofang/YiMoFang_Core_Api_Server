<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 11:43
 * name:删除用户关注机构
 * url:/user/delete_organization_concern
 */
//获取参数

// $uoc_id = $route->bodyParams["uoc_id"];  //主键ID
$uoc_oid = $route->bodyParams["uoc_oid"];
$uoc_uid = $route->bodyParams["uoc_uid"];

$whereArr = [
    "uoc_oid" => $uoc_oid,
    "uoc_uid" => $uoc_uid
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_organization_concern",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );