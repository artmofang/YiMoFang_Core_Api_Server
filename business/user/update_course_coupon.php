<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 21:12
 */
$ucc_id = $route->bodyParams["ucc_id"];             //主键ID

//更新条件
$whereArr = [
    "ucc_id" => $ucc_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_course_coupon",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );