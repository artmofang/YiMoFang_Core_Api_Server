<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/23
 * Time: 11:39
 * name:添加用户关注机构
 * url:/user/add_organization_concern
 */

//获取参数
$uoc_oid      = $route->bodyParams["uoc_oid"];                              //机构ID
$uoc_uid     = $route->bodyParams["uoc_uid"];                             //用户ID


//写入数组
$insertArr = [
    "uoc_oid"           => $uoc_oid,
    "uoc_uid"           => $uoc_uid,
    "uoc_create_time"   => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_organization_concern",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );