<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/28
 * Time: 15:06
 * name:增加用户浏览课程记录
 * url:/user/add_organization_course_browsing_history
 */

//获取参数
$uocbh_ocid      = $route->bodyParams["uocbh_ocid"];                          //课程ID
$uocbh_uid     = $route->bodyParams["uocbh_uid"];                             //用户ID


//写入数组
$insertArr = [
    "uocbh_ocid"           => $uocbh_ocid,
    "uocbh_uid"           => $uocbh_uid,
    "uocbh_create_time"   => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_organization_course_browsing_history",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );