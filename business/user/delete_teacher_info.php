<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 14:32
 * name:删除身份为教师的用户信息
 * url:/user/delete_teacher_info
 */

//获取参数

$uti_id = $route->bodyParams["uti_id"];  //主键ID

$whereArr = [
    "uti_id" => $uti_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_teacher_info",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );