<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 21:24
 * name:添加收藏
 * url:/user/add_collect
 */

//获取参数
$uc_uid        = $route->bodyParams["uc_uid"];      //用户ID
$uc_fid        = $route->bodyParams["uc_fid"];      //收藏对象的主键ID
$uc_type       = $route->bodyParams["uc_type"];     //收藏的类型 0-课程收藏 1-活动收藏 2-论坛文章收藏 3-论坛活动收藏 4-论坛回答收藏


//查询是否已经收藏过了
$sql = "select count(*) as sum from art_user_collect where uc_type = $uc_type and uc_fid = $uc_fid and uc_uid = $uc_uid";
$rs  = $db->mysqlDB->query($sql);
$sum = $rs[0]['sum'];

if($sum > 0){
    $response->responseData( false, "已收藏过该信息了" );
    exit();
}

//写入数组
$insertArr = [
    "uc_uid"          => $uc_uid,
    "uc_fid"          => $uc_fid,
    "uc_type"         => $uc_type,
    "uc_create_time" => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_collect",$insertArr);

//返回成功结果
$response->responseData( true );