<?php
/**
 * name:增加用户浏览机构记录
 */

//获取参数
$uosbh_oid      = $route->bodyParams["uosbh_oid"];                          //学校ID
$uosbh_uid     = $route->bodyParams["uosbh_uid"];                             //用户ID


//写入数组
$insertArr = [
    "uosbh_oid"           => $uosbh_oid,
    "uosbh_uid"           => $uosbh_uid,
    "uosbh_create_time"   => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_organization_school_browsing_history",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );