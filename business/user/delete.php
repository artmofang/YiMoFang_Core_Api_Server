<?php

/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 20:07
 */

//获取参数

$u_id = $route->bodyParams["u_id"];  //主键ID

$whereArr = [
    "u_id" => $u_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );


