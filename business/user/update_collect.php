<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 16:24
 * name:更新收藏
 * url:/user/update_collect
 */

//获取参数
$uc_id       = $route->bodyParams["uc_id"];   //主键ID

//更新条件
$whereArr = [
    "uc_id" => $uc_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_collect",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );