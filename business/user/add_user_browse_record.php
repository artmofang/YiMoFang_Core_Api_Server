<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/10
 * Time: 11:32
 * name:添加收藏记录
 * url:/user/add_user_browse_record
 */

//获取参数
$ubr_uid            = $route->bodyParams["ubr_uid"];             //用户ID
$ubr_browse_type    = $route->bodyParams["ubr_browse_type"];    //浏览类型 0-课程 1-机构 2-论坛 3-活动 4-文章
$ubr_fid            = $route->bodyParams["ubr_fid"];             //对应类型主键ID

//写入数组
$insertArr = [
    "ubr_uid"           => $ubr_uid,
    "ubr_browse_type"  => $ubr_browse_type,
    "ubr_fid"           => $ubr_fid,
    "ubr_browse_time"  => time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_browse_record",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );