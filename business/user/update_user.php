<?php

/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 20:08
 * name:用户信息更新
 * url:/user/update_user
 */

//获取参数

$u_id       = $route->bodyParams["u_id"];    //用户id

//更新条件
$whereArr = [
    "u_id" => $u_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );