<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 14:23
 * name:添加身份为教师的用户信息
 * url:/user/add_teacher_info
 */

$uti_name            = $route->bodyParams["uti_name"];              //姓名
$uti_photo           = $route->bodyParams["uti_photo"];             //照片
$uti_major           = $route->bodyParams["uti_major"];             //授课专业
$uti_education       = $route->bodyParams["uti_education"];        //教育背景
$uti_work_experience = $route->bodyParams["uti_work_experience"]; //工作经历
$uit_qualification   = $route->bodyParams["uit_qualification"];    //资质
$uti_honor           = $route->bodyParams["uti_honor"];             //获得的荣誉
$uti_seniority       = $route->bodyParams["uti_seniority"];         //教龄
$uti_uid             = $route->bodyParams["uti_uid"];                //关联的用户id
$uti_oid             = $route->bodyParams["uti_oid"];                //关联的用户id


//写入数组
$insertArr = [
    "uti_name"              => $uti_name,
    "uti_photo"             => $uti_photo,
    "uti_major"             => $uti_major,
    "uti_education"         => $uti_education,
    "uti_work_experience"  => $uti_work_experience,
    "uit_qualification"    => $uit_qualification,
    "uti_honor"             => $uti_honor,
    "uti_seniority"         => $uti_seniority,
    "uti_uid"                => $uti_uid,
    "uti_oid"                => $uti_oid,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_teacher_info",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );