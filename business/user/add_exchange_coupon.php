<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 16:27
 */
//获取参数
$uec_uid      = $route->bodyParams["uec_uid"];                              //用户ID
$uec_ecid     = $route->bodyParams["uec_ecid"];                             //关联兑换券ID
$uec_oid     = $route->bodyParams["uec_oid"];                             //机构id
$uec_exchange_code =  substr(md5(time()+rand(10000,99999)),0,8);               //生成的兑换码

$uec_get_time = time();
//写入数组
$insertArr = [
    "uec_uid"           => $uec_uid,
    "uec_ecid"          => $uec_ecid,
    "uec_get_time"          => $uec_get_time,
    "uec_oid"          => $uec_oid,
    "uec_exchange_code"  => $uec_exchange_code
    
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_exchange_coupon",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );