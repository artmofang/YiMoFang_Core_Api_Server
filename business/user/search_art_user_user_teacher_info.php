<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/27
 * Time: 15:49
 * name:用户身份为教师带教师信息
 * url:/user/search_art_user_user_teacher_info
 */

//获取字段筛选参数
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数


if(isset($route->restfulParams['count'])){

    unset($route->restfulParams['count']);

    //删除limit之前先行查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_user_user_teacher_info",true);

    //删除不能与count同时使用的条件
    unset($route->restfulParams['skip']);
    unset($route->restfulParams['limit']);

    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count("art_user_user_teacher_info",true);


}

//根据条件做相应查询
if(!$rsData){
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("art_user_user_teacher_info",true);
}


//拼接后
$rs = array( "count" => $sum , "data" => $rsData);
//返回成功结果
$response->responseData( true, $rs );