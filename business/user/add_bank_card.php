<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 17:35
 */
//获取参数
$ubc_uid               = $route->bodyParams["ubc_uid"];                                  //用户ID
$ubc_house_name        = $route->bodyParams["ubc_house_name"];                          //开户银行
$ubc_bank_number       = $route->bodyParams["ubc_bank_number"];                         //银行卡号
$ubc_bank_name         = $regexpObj->bodyV($response,$route,'ubc_bank_name','NORMAL');  //开户行名称
$ubc_apply_time        = $regexpObj->bodyV($response,$route,'ubc_apply_time','NORMAL'); //申请时间
$ubc_bank_image        = $regexpObj->bodyV($response,$route,'ubc_bank_image','NORMAL'); //开户行图片
$ubc_note              = $regexpObj->bodyV($response,$route,'ubc_note','NORMAL');        //理由备注

//写入数组
$insertArr = [
    "ubc_uid"                   => $ubc_uid,
    "ubc_house_name"           => $ubc_house_name,
    "ubc_bank_number"          => $ubc_bank_number,
    "ubc_bank_name"            => $ubc_bank_name,
    "ubc_apply_time"           => $ubc_apply_time,
    "ubc_bank_image"           => $ubc_bank_image,
    "ubc_note"                  => $ubc_note,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_bank_card",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );