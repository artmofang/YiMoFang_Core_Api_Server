<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 9:59
 */
//获取参数
$ucc_uid      = $route->bodyParams["ucc_uid"];                              //用户ID
$ucc_ccid     = $route->bodyParams["ucc_ccid"];                             //关联的约课劵ID
$ucc_is_used  = $regexpObj->bodyV($response,$route,'ucc_is_used','NORMAL'); //是否使用 0-否 1-是


//写入数组
$insertArr = [
    "ucc_uid"           => $ucc_uid,
    "ucc_ccid"          => $ucc_ccid,
    "ucc_is_used"       => $ucc_is_used,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_course_coupon",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );