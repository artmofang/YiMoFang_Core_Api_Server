<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/18
 * Time: 19:50
 */
//获取参数

$ot_id = $route->bodyParams["ot_id"];  //主键ID

$whereArr = [
    "ot_id" => $ot_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("organization_teacher",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );