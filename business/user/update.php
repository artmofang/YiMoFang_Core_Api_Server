<?php

/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 20:08
 * name:用户信息更新
 * url:/user/update
 */

//获取参数

$u_username       = $route->bodyParams["u_username"];                                        //用户账号

//更新条件
$whereArr = [
    "u_username" => $u_username,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );