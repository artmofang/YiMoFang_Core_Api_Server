<?php
/**
 * name:删除用户浏览机构记录
 */

//获取参数

$uosbh_id = $route->bodyParams["uosbh_id"];  //主键ID

$whereArr = [
    "uosbh_id" => $uosbh_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_organization_school_browsing_history",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );