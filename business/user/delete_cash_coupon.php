<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 20:51
 */
$ucc_id = $route->bodyParams["ucc_id"];//主键ID

$whereArr = [
    "ucc_id" => $ucc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_cash_coupon",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );