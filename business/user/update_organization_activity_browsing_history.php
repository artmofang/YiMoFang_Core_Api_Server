<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/28
 * Time: 15:17
 * name:修改用户浏览活动记录
 * url:/user/update_organization_activity_browsing_history
 */

//获取参数
$ubh_id       = $route->bodyParams["ubh_id"];   //主键ID

//更新条件
$whereArr = [
    "ubh_id" => $ubh_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_organization_activity_browsing_history",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );