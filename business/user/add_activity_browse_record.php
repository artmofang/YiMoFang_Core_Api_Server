<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/13
 * Time: 10:44
 * 增加活动浏览记录
 */
//获取参数
$abr_uid      = $route->bodyParams["abr_uid"];                             //用户id
$abr_oaid      = $route->bodyParams["abr_oaid"];                           //活动id
$abr_oid      = $route->bodyParams["abr_oid"];                             //机构id


//写入数组
$insertArr = [
    "abr_uid"     => $abr_uid,
    "abr_oaid"     => $abr_oaid,
    "abr_oid"     => $abr_oid,
    "abr_create_time" => time(),

];

//执行写入语句
$rsData = $db->mysqlDB->insert("activity_browse_record",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );