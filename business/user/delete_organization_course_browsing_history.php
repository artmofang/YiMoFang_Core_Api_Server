<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/28
 * Time: 15:14
 * name:删除用户浏览课程记录
 * url:/user/delete_organization_course_browsing_history
 */

//获取参数

$uocbh_id = $route->bodyParams["uocbh_id"];  //主键ID

$whereArr = [
    "uocbh_id" => $uocbh_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_organization_course_browsing_history",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );