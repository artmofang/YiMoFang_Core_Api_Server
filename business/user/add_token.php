<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 16:58
 */
//获取参数
$ut_token           = $route->bodyParams["ut_token"];
$ut_username        = $route->bodyParams["ut_username"];

//写入数组
$insertArr = [
    "ut_token"              => $ut_token,
    "ut_username"           => $ut_username,
    "ut_end_time"           => strtotime('+1 month', time()),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_token",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );
