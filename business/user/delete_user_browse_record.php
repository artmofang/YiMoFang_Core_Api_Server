<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/10
 * Time: 11:36
 * name:删除收藏记录
 * url:/user/delete_user_browse_record
 */

//获取参数
$ubr_id = $route->bodyParams["ubr_id"];//主键ID

$whereArr = [
    "ubr_id" => $ubr_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_browse_record",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );