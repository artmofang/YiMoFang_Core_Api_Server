<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/28
 * Time: 15:15
 * name:增加用户浏览活动记录
 * url:/user/add_organization_activity_browsing_history
 */

//获取参数
$ubh_oaid    = $route->bodyParams["ubh_oaid"];                          //活动ID
$ubh_uid     = $route->bodyParams["ubh_uid"];                           //用户ID


//写入数组
$insertArr = [
    "ubh_oaid"          => $ubh_oaid,
    "ubh_uid"           => $ubh_uid,
    "ubh_create_time"  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_organization_activity_browsing_history",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );