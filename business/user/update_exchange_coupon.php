<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 16:52
 */
$uec_id = $route->bodyParams["uec_id"];                  //主键ID

//更新条件
$whereArr = [
    "uec_id" => $uec_id
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_exchange_coupon",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );