<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/13
 * Time: 11:04
 * 更新用户浏览活动记录
 */
//获取参数

$abr_id       = $route->bodyParams["abr_id"];                                        //用户账号

//更新条件
$whereArr = [
    "abr_id" => $abr_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("activity_browse_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );