<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/10
 * Time: 11:46
 * name:更新收藏记录(收藏时间)
 * url:/user/update_user_browse_record
 */

//获取参数
$ubr_browse_type    = $route->bodyParams["ubr_browse_type"]; //浏览类型 0-课程 1-机构 2-论坛 3-活动 4-文章
$ubr_fid            = $route->bodyParams["ubr_fid"];                 //对应类型主键ID

//更新条件
$whereArr = [
    "ubr_browse_type" => $ubr_browse_type,
    "ubr_fid" => $ubr_fid,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_browse_record",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );