<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 21:46
 */
//获取字段筛选参数
$fields = $route->bodyParams['fields'];

//默认条件
//$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){

    unset($route->restfulParams['count']);

    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count("user_collect");


}

//根据条件做相应查询
$rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("user_collect");

//拼接后
$rs = array( "count" => $sum , "data" => $rsData );


//返回成功结果
$response->responseData( true, $rs );