<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/24
 * Time: 19:26
 * name:更新用户关注机构
 * url:/user/update_organization_concern
 */

//获取参数
$uoc_id       = $route->bodyParams["uoc_id"];   //主键ID

//更新条件
$whereArr = [
    "uoc_id" => $uoc_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_organization_concern",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );