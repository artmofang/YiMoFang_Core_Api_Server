<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/5
 * Time: 14:33
 * 添加约课记录
 */

//获取参数
$ar_uid               = $route->bodyParams["ar_uid"];    //用户ID
$ar_name              = $route->bodyParams["ar_name"];  //用户姓名
$ar_phone             = $route->bodyParams["ar_phone"]; //电话号码
$ar_cid               = $route->bodyParams["ar_cid"];   //课程id
$ar_oid               = $route->bodyParams["ar_oid"];   //机构id
$ar_create_time       = time();
$ar_des               = $regexpObj->bodyV($response,$route,'ar_des','NORMAL');  //备注

//写入数组
$insertArr = [
    "ar_uid"                    => $ar_uid,
    "ar_name"                   => $ar_name,
    "ar_phone"                  => $ar_phone,
    "ar_cid"                    => $ar_cid,
    "ar_oid"                    => $ar_oid,
    "ar_create_time"           => $ar_create_time,
    "ar_des"                    => $ar_des,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("appointment_record",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );
