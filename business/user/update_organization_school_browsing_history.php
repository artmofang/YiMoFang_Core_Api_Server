<?php
/**
 * name:修改用户浏览机构记录
 */

//获取参数
$uosbh_id       = $route->bodyParams["uosbh_id"];   //主键ID

//更新条件
$whereArr = [
    "uosbh_id" => $uosbh_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_organization_school_browsing_history",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );