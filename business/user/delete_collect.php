<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 21:52
 */
$uc_id = $route->bodyParams["uc_id"];//主键ID

$whereArr = [
    "uc_id" => $uc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_collect",$whereArr,false);

//返回成功结果
$response->responseData( true, $rsData );