<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/28
 * Time: 15:21
 * name:删除用户浏览活动记录
 * url:/user/delete_organization_activity_browsing_history
 */

//获取参数

$ubh_id = $route->bodyParams["ubh_id"];  //主键ID

$whereArr = [
    "ubh_id" => $ubh_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_organization_activity_browsing_history",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );
