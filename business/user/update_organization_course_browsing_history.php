<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/28
 * Time: 15:09
 * name:修改用户浏览课程记录
 * url:/user/update_organization_course_browsing_history
 */

//获取参数
$uocbh_id       = $route->bodyParams["uocbh_id"];   //主键ID

//更新条件
$whereArr = [
    "uocbh_id" => $uocbh_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("user_organization_course_browsing_history",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );