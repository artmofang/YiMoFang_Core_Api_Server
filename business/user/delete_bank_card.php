<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/17
 * Time: 18:05
 */
//获取参数

$ubc_id = $route->bodyParams["ubc_id"];//主键ID

$whereArr = [
    "ubc_id" => $ubc_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("user_bank_card",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );