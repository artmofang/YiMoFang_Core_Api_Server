<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/4/16
 * Time: 20:37
 */
$ucc_uid      = $route->bodyParams["ucc_uid"];                              //用户ID
$ucc_ccid     = $route->bodyParams["ucc_ccid"];                             //关联的代金卷ID

$ucc_is_used  = $regexpObj->bodyV($response,$route,'ucc_is_used','NORMAL'); //是否使用 0-否 1-是
$ucc_from_type  = $regexpObj->bodyV($response,$route,'ucc_from_type','NORMAL'); //代金券来源
$ucc_oid  = $regexpObj->bodyV($response,$route,'ucc_oid','NORMAL'); //机构id


//写入数组
$insertArr = [
    "ucc_uid"           => $ucc_uid,
    "ucc_ccid"          => $ucc_ccid,
    "ucc_from_type"     => $ucc_from_type,
    "ucc_is_used"       => $ucc_is_used,
    "ucc_oid"=>$ucc_oid,
    "ucc_get_time"     =>time()
];

//执行写入语句
$rsData = $db->mysqlDB->insert("user_cash_coupon",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );