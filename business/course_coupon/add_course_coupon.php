<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 20:04
 *添加约课券
 */
$cc_name = $route->bodyParams['cc_name'];//约课券名称
$cc_start_time = $route->bodyParams['cc_start_time'];//有效期开始时间
$cc_end_time = $route->bodyParams['cc_end_time'];//有效期结束时间
$cc_oid = $route->bodyParams['cc_oid'];//所属机构ID
$cc_ocid = $route->bodyParams['cc_ocid'];//所属课程ID
$cc_max_count = $route->bodyParams['cc_max_count'];//最大领取数量


$cc_desc = $regexpObj->bodyV($response,$route,'cc_desc','NORMAL');//劵说明
$cc_create_time  = time();//创建时间
//写入数组
$insertArr = [
    'cc_name' =>$cc_name,
    'cc_start_time' =>$cc_start_time,
    'cc_end_time' =>$cc_end_time,
    'cc_oid' =>$cc_oid,
    'cc_ocid' =>$cc_ocid,
    'cc_max_count' =>$cc_max_count,
    'cc_desc' =>$cc_desc,
    'cc_create_time' =>$cc_create_time,
];
//执行写入语句
$rsData = $db->mysqlDB->insert("course_coupon",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );