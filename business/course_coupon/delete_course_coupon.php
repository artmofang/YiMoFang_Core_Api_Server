<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 20:25
 * 删除约课券信息
 */
$cc_id = $route->bodyParams['cc_id'];

//拼接删除条件
$whereArr = [
    'cc_id'=>$cc_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("course_coupon",$whereArr,false);

//返回结果
$response->responseData(true,$reData);