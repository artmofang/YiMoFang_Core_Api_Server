<?php

    //新增学生//

    /*

    //获取POST参数
    $name = $route->bodyParams['name'];
    $age = $route->bodyParams['age'];

    //构建新增的数组
    $insertArr = [
        "name" => $name,
        "age"  => $age
    ];

    //写入数据库
    $rsData = $db->mysqlDB->insert("student",$insertArr);
    
    //返回成功结果
    $response->responseData( true, $rsData );

    */

    //更新学生//

    /*
    $id   = $route->bodyParams["id"];

    //更新条件
    $whereArr = [
        "id" => $id
    ];

    //拼接更新条件
    $updateArr = deleteArrData($whereArr,$route->bodyParams);

    //执行更新语句
    $rsData = $db->mysqlDB->update("student",$whereArr,$updateArr);

    //返回成功结果
    $response->responseData( true, $rsData );

    */

    //删除学生
    /*
    $id = $route->bodyParams['id'];

    //拼接删除条件
    $whereArr = [
        'id' => $id
    ];

    //执行逻辑删除语句
    $rsData = $db->mysqlDB->delete("student",$whereArr,false);

    //返回结果
    $response->responseData(true,$reData);
    */

    //查询学生
    $fields = $route->bodyParams['fields'];

    //默认条件
    $route->restfulParams['is_delete'] = 0;

    $sum = 0;

    //判断是否需要查询总记录数
    if(isset($route->restfulParams['count'])){
        
        unset($route->restfulParams['count']);
        
        //获取到记录的总条数
        $sum = $db->mysqlDB->params($route->restfulParams)->count('student');
        
    }

    //根据相应的条件查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("student");


    //拼接获得数据
    $rs = array( "count" => $sum , "data" => $rsData );

    //返回成功结果
    $response->responseData( true, $rs );


?>