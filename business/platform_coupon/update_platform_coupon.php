<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/16
 * Time: 11:15
 * 更新平台优惠券
 */
//获取参数

$pc_id       = $route->bodyParams["pc_id"];                                        //主键ID

//更新条件
$whereArr = [
    "pc_id" => $pc_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("platform_coupon",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );