<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/17
 * Time: 9:28
 * 领取优惠券
 */
//获取参数信息
$pc_id       = $route->bodyParams['pc_id'];                 //平台发放的优惠券id
$pc_type     = $route->bodyParams['pc_type'];               //优惠券类型 0-约课券  1-代金券
$ucc_uid     = $route->bodyParams['ucc_uid'];               //领取优惠券的用户id


//构造存储过程参数
$proceArr = [
    $pc_id,
    $pc_type,
    $ucc_uid,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_RECEIVE_PLATFORM_COUPON", $proceArr);


//返回成功结果
$response->responseData( true);