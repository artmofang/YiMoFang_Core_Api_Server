<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/17
 * Time: 9:28
 * 领取新手礼包
 */
//获取参数信息

$ucc_uid     = $route->bodyParams['ucc_uid'];               //领取优惠券的用户id


//构造存储过程参数
$proceArr = [
    $ucc_uid,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_RECEIVE_NEW_BAG", $proceArr);


//返回成功结果
$response->responseData( true);