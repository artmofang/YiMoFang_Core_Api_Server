<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/16
 * Time: 10:57
 * 添加平台券
 */

//获取参数
$pc_type         = $route->bodyParams["pc_type"];                          //券类型
$pc_price        = $route->bodyParams["pc_price"];                         //券价格
$pc_name         = $route->bodyParams["pc_name"];                          //券名称
$pc_stock        = $route->bodyParams["pc_stock"];                         //剩余数量
$pc_start_time   = $route->bodyParams["pc_start_time"];                   //开始时间
$pc_end_time     = $route->bodyParams["pc_end_time"];                     //结束时间



//写入数组
$insertArr = [
    "pc_type"            => $pc_type,
    "pc_price"           => $pc_price,
    "pc_name"            => $pc_name,
    "pc_stock"           => $pc_stock,
    "pc_start_time"     => $pc_start_time,
    "pc_end_time"       => $pc_end_time,
];

//执行写入语句
$rsData = $db->mysqlDB->insert("platform_coupon",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );