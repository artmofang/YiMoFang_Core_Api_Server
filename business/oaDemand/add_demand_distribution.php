<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 15:27
 * name: 添加分配
 * url:/oaDemand/add_demand_distribution
 */

//获取参数
$dd_did      = $route->bodyParams['dd_did'];        //关联需求ID
$dd_uid      = $route->bodyParams['dd_uid'];        //关联员工ID
$dd_end_time = $route->bodyParams['dd_end_time'];  //需求到期时间

//写入数组
$insertArr = [
    "dd_did"                => $dd_did,
    "dd_uid"                => $dd_uid,
    "dd_end_time"           => $dd_end_time,
    "dd_create_time"        => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_demand_distribution",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );