<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 14:24
 * name:添加需求
 * url:/oaDemand/add_demand
 */

//获取参数
$d_title          = $route->bodyParams['d_title'];                                       //需求标题
$d_content        = $route->bodyParams['d_content'];                                     //需求内容
$d_create_user    = $route->bodyParams['d_create_user'];                                //需求提出者姓名
$d_change_process = $regexpObj->bodyV($response,$route,'d_change_process','NORMAL');   //变更过程以‘，’隔开
$d_progress       = $regexpObj->bodyV($response,$route,'d_progress','NORMAL');          //工作进度
$d_status         = $regexpObj->bodyV($response,$route,'d_status','NUMBER');            //需求状态 0-待分配 1-开发中 2-已完成 3-停用 4-已变更
$d_change_times   = $regexpObj->bodyV($response,$route,'d_change_times','NUMBER');     //更新次数

//写入数组
$insertArr = [
    "d_title"           => $d_title,
    "d_content"         => $d_content,
    "d_create_user"     => $d_create_user,
    "d_change_process" => $d_change_process,
    "d_progress"        => $d_progress,
    "d_status"          => $d_status,
    "d_change_times"    => $d_change_times,
    "d_create_time"     => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_demand",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );