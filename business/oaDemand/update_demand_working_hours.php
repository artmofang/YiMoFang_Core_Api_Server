<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/24
 * Time: 15:12
 * name:更新工作时间
 * url:/oaDemand/update_demand_working_hours
 */

$dwh_id       = $route->bodyParams["dwh_id"];  //主键ID

//更新条件
$whereArr = [
    "dwh_id" => $dwh_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_demand_working_hours",$whereArr,$updateArr,false);

//返回成功结果
$response->responseData( true, $rsData );