<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 14:56
 * name:更新需求
 * url:/oaDemand/update_demand
 */

$d_id       = $route->bodyParams["d_id"];  //主键ID

//更新条件
$whereArr = [
    "d_id" => $d_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_demand",$whereArr,$updateArr,false);

//返回成功结果
$response->responseData( true, $rsData );
