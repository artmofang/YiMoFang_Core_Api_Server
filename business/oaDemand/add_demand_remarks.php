<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 15:41
 * name:添加分配
 * url:/oaDemand/add_demand_remarks
 */

//获取参数
$dr_did         = $route->bodyParams['dr_did'];             //关联需求ID
$dr_remark      = $route->bodyParams['dr_remark'];          //备注
$dr_user        = $route->bodyParams['dr_user'];            //备注人姓名

//写入数组
$insertArr = [
    "dr_did"          => $dr_did,
    "dr_user"         => $dr_user,
    "dr_remark"       => $dr_remark,
    "dr_create_time"  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_demand_remarks",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );