<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 15:32
 * name:删除分配
 * url:/oaDemand/delete_demand_distribution
 */

//获取参数
$dd_id = $route->bodyParams['dd_id'];

//拼接删除条件
$whereArr = [
    'dd_id'=>$dd_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_demand_distribution",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);