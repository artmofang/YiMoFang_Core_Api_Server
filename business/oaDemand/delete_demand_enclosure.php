<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/25
 * Time: 9:27
 * name:删除附件
 * url:/oaDemand/delete_demand_enclosure
 */

//获取参数
$de_id = $route->bodyParams['de_id'];

//拼接删除条件
$whereArr = [
    'de_id'=>$de_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_demand_enclosure",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);