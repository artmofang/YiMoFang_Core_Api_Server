<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/24
 * Time: 15:09
 * name:删除工作时间
 * url:/oaDemand/delete_demand_working_hours
 */

//获取参数
$dwh_id = $route->bodyParams['dwh_id'];

//拼接删除条件
$whereArr = [
    'dwh_id'=>$dwh_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_demand_working_hours",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);