<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/24
 * Time: 15:11
 * name:查询工作时间
 * url:/oaDemand/search_demand_working_hours
 *
 */
//获取字段筛选参数
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if (isset($route->restfulParams['count'])) {

    unset($route->restfulParams['count']);
    //删除limit之前先行查询
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->selectTable("oa_demand_working_hours");

    //删除不能与count同时使用的条件
    unset($route->restfulParams['skip']);
    unset($route->restfulParams['limit']);
    //获取总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->countTable("oa_demand_working_hours");


}
//根据条件做相应查询
if (!$rsData) {
    $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->selectTable("oa_demand_working_hours");
}

//拼接后
$rs = array("count" => $sum, "data" => $rsData);



//返回成功结果
$response->responseData(true, $rs);