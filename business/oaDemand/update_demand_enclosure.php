<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/25
 * Time: 9:30
 * name:更新附件
 * url:/oaDemand/update_demand_enclosure
 */


$de_id       = $route->bodyParams["de_id"];  //主键ID

//更新条件
$whereArr = [
    "de_id" => $de_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_demand_enclosure",$whereArr,$updateArr,false);

//返回成功结果
$response->responseData( true, $rsData );