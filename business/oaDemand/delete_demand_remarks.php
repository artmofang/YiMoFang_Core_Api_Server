<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 15:43
 * name:删除备注
 * url:/oaDemand/delete_demand_remarks
 */

//获取参数
$dr_id = $route->bodyParams['dr_id'];

//拼接删除条件
$whereArr = [
    'dr_id'=>$dr_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_demand_remarks",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);