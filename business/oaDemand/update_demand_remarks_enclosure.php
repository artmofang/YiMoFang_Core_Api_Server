<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 17:53
 * name:更新备注附件
 * url:/oaDemand/update_demand_remarks_enclosure
 */

$dre_id       = $route->bodyParams["dre_id"];  //主键ID

//更新条件
$whereArr = [
    "dre_id" => $dre_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_demand_remarks_enclosure",$whereArr,$updateArr,false);

//返回成功结果
$response->responseData( true, $rsData );