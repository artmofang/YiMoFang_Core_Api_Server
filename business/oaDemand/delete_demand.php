<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 14:35
 * name;删除需求
 * url:/oaDemand/delete_demand
 */

//获取参数
$d_id = $route->bodyParams['d_id'];

//拼接删除条件
$whereArr = [
    'd_id'=>$d_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_demand",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);