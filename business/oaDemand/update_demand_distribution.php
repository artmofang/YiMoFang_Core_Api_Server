<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 15:34
 * name:更新分配
 * url:/oaDemand/update_demand_distribution
 */

$dd_id       = $route->bodyParams["dd_id"];  //主键ID

//更新条件
$whereArr = [
    "dd_id" => $dd_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_demand_distribution",$whereArr,$updateArr,false);

//返回成功结果
$response->responseData( true, $rsData );