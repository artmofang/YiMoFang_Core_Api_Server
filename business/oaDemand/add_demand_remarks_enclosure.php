<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 17:49
 * name:添加备注附件
 * url:/oaDemand/add_demand_remarks_enclosure
 */

//获取参数
$dre_drid         = $route->bodyParams['dre_drid'];       //备注id
$dre_enclosure   = $route->bodyParams['dre_enclosure']; //附件路径
$dre_file_type   = $route->bodyParams['dre_file_type']; //附件类型
$dre_file_size   = $route->bodyParams['dre_file_size']; //附件大小

//写入数组
$insertArr = [
    "dre_drid"          => $dre_drid,
    "dre_enclosure"   => $dre_enclosure,
    "dre_file_type"   => $dre_file_type,
    "dre_file_size"   => $dre_file_size,
    "dre_create_time" => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_demand_remarks_enclosure",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );