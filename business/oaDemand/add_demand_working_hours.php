<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/24
 * Time: 15:07
 * name:添加工作时间
 * url:/oaDemand/add_demand_working_hours
 */

//获取参数
$dwh_uid     = $route->bodyParams['dwh_uid'];                                  //员工ID
$dwh_date    = $route->bodyParams['dwh_date'];                                //工作时间

//写入数组
$insertArr = [
    "dwh_uid"        => $dwh_uid,
    "dwh_date"       => $dwh_date
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_demand_working_hours",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );