<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/27
 * Time: 17:51
 * name:删除备注附件
 * url:/oaDemand/delete_demand_remarks_enclosure
 */

//获取参数
$dre_id = $route->bodyParams['dre_id'];

//拼接删除条件
$whereArr = [
    'dre_id'=>$dre_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_demand_remarks_enclosure",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);