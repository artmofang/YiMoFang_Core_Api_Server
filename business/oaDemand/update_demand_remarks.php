<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 15:45
 * name:更新备注
 * url:/oaDemand/update_demand_remarks
 */

$dr_id       = $route->bodyParams["dr_id"];  //主键ID

//更新条件
$whereArr = [
    "dr_id" => $dr_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_demand_remarks",$whereArr,$updateArr,false);

//返回成功结果
$response->responseData( true, $rsData );