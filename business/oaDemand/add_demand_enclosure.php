<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/25
 * Time: 9:23
 * name:添加附件
 * url:/oaDemand/add_demand_enclosure
 */

//获取参数
$de_did         = $route->bodyParams['de_did'];                                     //需求id
$de_enclosure   = $route->bodyParams['de_enclosure'];                              //附件路径
$de_file_type   = $route->bodyParams['de_file_type'];                              //附件类型
$de_file_size   = $route->bodyParams['de_file_size'];                              //附件大小

//写入数组
$insertArr = [
    "de_did"          => $de_did,
    "de_enclosure"   => $de_enclosure,
    "de_file_type"   => $de_file_type,
    "de_file_size"   => $de_file_size,
    "de_create_time" => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_demand_enclosure",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );