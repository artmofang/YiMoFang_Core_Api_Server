<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/23
 * Time: 14:38
 * name:查询需求
 * url:/oaDemand/search_demand
 */

if ($route->restfulParams['list'] == "") {

    //获取字段筛选参数
    $fields = $route->bodyParams['fields'];

//默认条件
    $route->restfulParams['is_delete'] = 0;

    $sum = 0;

//判断是否需要查询总记录数
    if (isset($route->restfulParams['count'])) {

        unset($route->restfulParams['count']);
        //删除limit之前先行查询
        $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->selectTable("oa_demand");

        //删除不能与count同时使用的条件
        unset($route->restfulParams['skip']);
        unset($route->restfulParams['limit']);
        //获取总条数
        $sum = $db->mysqlDB->params($route->restfulParams)->countTable("oa_demand");


    }
//根据条件做相应查询
    if (!$rsData) {
        $rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->selectTable("oa_demand");
    }

//拼接后
    $rs = array("count" => $sum, "data" => $rsData);


//返回成功结果
    $response->responseData(true, $rs);

} else {

    $limit = $route->restfulParams['limit'];

    $skip = $route->restfulParams['skip'];

    $count = $route->restfulParams['count'];

    if ($limit != "" && $skip != "") {

        $sql = "SELECT * FROM `oa_demand` WHERE `d_status` <> 4 LIMIT $skip,$limit";

    } else {

        $sql = "SELECT * FROM `oa_demand` WHERE `d_status` <> 4 LIMIT 0,20";

    }

    if($count!=""){

        $countSql = "SELECT COUNT(*) AS COUNT FROM `oa_demand` WHERE `d_status` <> 4 ";

        $count = $db->mysqlDB->query($countSql);

    }else{

        $count = 0;

    }

    $re = $db->mysqlDB->query($sql);

    $rsData = array("count" => $count[0]['COUNT'] ,"data" => $re);

    $response->responseData(true, $rsData);

}
