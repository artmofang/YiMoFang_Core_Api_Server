<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 17:15
 * name:删除平台消息通知
 * url:/platform/delete_platform_notification
 */

//获取参数

$pn_id = $route->bodyParams["pn_id"];  //主键ID

$whereArr = [
    "pn_id" => $pn_id
];

//执行写入语句
$rsData = $db->mysqlDB->delete("platform_notification",$whereArr,false);

//返回成功结果

$response->responseData( true, $rsData );