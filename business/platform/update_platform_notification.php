<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 17:24
 * name:更新平台消息通知
 * url:/platform/update_platform_notification
 */

//获取参数
$pn_id = $route->bodyParams["pn_id"];

//更新条件
$whereArr = [
    "pn_id" => $pn_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("platform_notification",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );