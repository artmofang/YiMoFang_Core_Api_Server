<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/16
 * Time: 16:50
 * name:添加平台消息通知
 * url:/platform/add_platform_notification
 */

//获取参数
$pn_content   = $route->bodyParams['pn_content'];                           //通知的内容
$pn_title     = $route->bodyParams['pn_title'];                             //通知标题
$pn_type      = $route->bodyParams['pn_type'];                              //通知类型 0-用户 1-机构
$pn_image_url   = $regexpObj->bodyV($response,$route,'pn_image_url','NORMAL');  //通知标题图

//写入数组
$insertArr = [
    'pn_content'        => $pn_content,
    'pn_title'          => $pn_title,
    'pn_type'           => $pn_type,
    'pn_create_time'  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("platform_notification",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );