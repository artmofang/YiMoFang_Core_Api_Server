<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/24
 * Time: 15:30
 * name:删除员工
 * url:/oaUser/delete_user
 */

//获取参数
$u_id = $route->bodyParams['u_id'];

//拼接删除条件
$whereArr = [
    'u_id'=>$u_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("oa_user",$whereArr,false,false);

//返回结果
$response->responseData(true,$rsData);