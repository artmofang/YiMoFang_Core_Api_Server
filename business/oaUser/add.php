<?php

$u_username = $route->bodyParams["u_username"];
$u_password = $route->bodyParams["u_password"];
$u_phone    = $route->bodyParams["u_phone"];
$u_age      = $route->bodyParams["u_age"]; 
$u_realname = $route->bodyParams["u_realname"]; 
$u_sex      = $route->bodyParams["u_sex"]; 
$u_role_id  = $route->bodyParams["u_role_id"]; 

//写入数组
$insertArr = [
    "u_username"     => $u_username,
    "u_password"     => $u_password,
    "u_phone"        => $u_phone,
    "u_age"          => $u_age,
    "u_realname"     => $u_realname,
    "u_sex"          => $u_sex,
    "u_role_id"      => $u_role_id,
    "u_create_time"  => time(),
];

//执行写入语句
$rsData = $db->mysqlDB->insert("oa_user",$insertArr,false);

//返回成功结果
$response->responseData( true , $rsData );