<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/7/24
 * Time: 15:31
 * name:更新员工
 * url:/oaUser/update_user
 */

$u_id       = $route->bodyParams["u_id"];  //主键ID

//更新条件
$whereArr = [
    "u_id" => $u_id,
];

$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行写入语句
$rsData = $db->mysqlDB->update("oa_user",$whereArr,$updateArr,false);

//返回成功结果
$response->responseData( true, $rsData );