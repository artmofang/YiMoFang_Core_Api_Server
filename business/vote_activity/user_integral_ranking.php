<?php
/**
 * Created by PhpStorm.
 * User: R.Ice
 * Date: 2018/5/4
 * Time: 9:11
 */

$vau_uid = $route->bodyParams['vau_uid'];  //参与者ID
$vau_oaid = $route->bodyParams['vau_oaid'];  //活动的ID

$sql = "SELECT * FROM (SELECT vau.vau_uid,vau.vau_poll,vau.vau_oaid,@rownum := @rownum + 1 AS rownum
FROM
    (
        SELECT
            vau_uid,
            vau_poll,
            vau_oaid
        FROM
            `art_vote_activity_user` 
                where vau_oaid = $vau_oaid
        ORDER BY
        vau_poll DESC
    ) AS vau,
    (SELECT @rownum := 0) r) as d where vau_uid = $vau_uid";

$re=$db->mysqlDB->query($sql);

$res=array("data"=>$re);

$response->responseData(true, $res);