<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 16:30
 * 添加投票活动的配置信息
 */
//获取参数
//$vac_type                      = $route->bodyParams['vac_type'];                        //投票活动的类型 0-图片 1-视频
$vac_oaid                      = $route->bodyParams['vac_oaid'];                        //关联活动ID
//$is_vote_verify                = $route->bodyParams['is_vote_verify'];                 //是否开启验证投票 0-关 1-开
//$is_vote_limit                 = $route->bodyParams['is_vote_limit'];                  //是否开启投票限制 0-关 1-开
//$everyday_vote_count           = $route->bodyParams['everyday_vote_count'];           //是否开启投票限制 0-关 1-开
//$is_vote_most_count_every_hour = $route->bodyParams['is_vote_most_count_every_hour'];//是否限制每个参赛者每小时可得最大票数 0-关 1-开
$everyday_vote_count           = $route->bodyParams['everyday_vote_count'];           //是否限制每个参赛者每小时可得最大票数 0-关 1-开
//$vac_bg_color                  = $route->bodyParams['vac_bg_color'];                   //是否限制每个参赛者每小时可得最大票数 0-关 1-开
$vac_max_count                  = $route->bodyParams['vac_max_count'];                   //是否限制每个参赛者每小时可得最大票数 0-关 1-开
$vac_enlist_start_time          = $route->bodyParams['vac_enlist_start_time'];                   //是否限制每个参赛者每小时可得最大票数 0-关 1-开
$vac_enlist_end_time                 = $route->bodyParams['vac_enlist_end_time'];                   //是否限制每个参赛者每小时可得最大票数 0-关 1-开
$vac_vote_start_time                 = $route->bodyParams['vac_vote_start_time'];                   //是否限制每个参赛者每小时可得最大票数 0-关 1-开
$vac_vote_end_time                = $route->bodyParams['vac_vote_end_time'];                   //是否限制每个参赛者每小时可得最大票数 0-关 1-开
//$vac_type                         = $route->bodyParams['vac_type'];                   //投票类型 0 图片  1视频

//写入数组
$insertArr = [
    'vac_type'                      => 0,
    'vac_oaid'                      => $vac_oaid,
    'vac_max_count'                => $vac_max_count,
    'vac_enlist_start_time'       => $vac_enlist_start_time,
    'vac_enlist_end_time'         => $vac_enlist_end_time,
    'vac_vote_end_time'            => $vac_vote_end_time,
    'vac_vote_start_time'          => $vac_vote_start_time,
//    'is_vote_limit'                 => $is_vote_limit,
//    'is_vote_most_count_every_hour' => $is_vote_most_count_every_hour,
    'everyday_vote_count'          => $everyday_vote_count,
//    'vac_bg_color'                  => $vac_bg_color,
];

//执行写入语句
$rsData = $db->mysqlDB->insert('vote_activity_config',$insertArr);

//获取返回值
$response->responseData( true, $rsData );