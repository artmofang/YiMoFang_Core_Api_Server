<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/13
 * Time: 23:26
 * 更新投票活动优惠券信息
 */
 
$vac_id = $route->bodyParams["vac_id"];

//更新条件
$whereArr = [
    "vr_id" => $vr_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("vote_reward",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );