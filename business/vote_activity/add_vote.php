<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/10
 * Time: 16:23
 * 添加投票接口
 */
//获取参数信息
$vaur_uid       = $route->bodyParams['vaur_uid'];             //投票者用户ID
$vaur_vote_uid  = $route->bodyParams['vaur_vote_uid'];       //被投票者的用户ID
$vaur_oaid      = $route->bodyParams['vaur_oaid'];            //投票活动id


//构造存储过程参数
$proceArr = [
    $vaur_uid,
    $vaur_vote_uid,
    $vaur_oaid,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_USER_JOIN_VOTE_ACTIVITY_RECORD", $proceArr);


//返回成功结果
$response->responseData( true);
