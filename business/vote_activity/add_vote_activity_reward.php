<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/20
 * Time: 22:36
 */
$vr_oaid        = $route->bodyParams["vr_oaid"];//关联的模板促销活动ID
$vr_rank        = $route->bodyParams["vr_rank"];//排名起
$vr_ranknum     = $route->bodyParams["vr_ranknum"];//排名至
$vr_name        = $route->bodyParams["vr_name"];//奖品名称
$vr_coupon_id   = $route->bodyParams["vr_coupon_id"];//优惠券id
$vr_coupon_type = $route->bodyParams["vr_coupon_type"];//优惠券类型
//写入数组
$insertArr = [
    "vr_oaid"        => $vr_oaid,
    "vr_rank"        => $vr_rank,
    "vr_ranknum"     => $vr_ranknum,
    "vr_name"        => $vr_name,
    "vr_coupon_id"   => $vr_coupon_id,
    "vr_coupon_type" => $vr_coupon_type,

];

//执行写入语句
$rsData = $db->mysqlDB->insert("vote_reward",$insertArr);

//返回成功结果
$response->responseData( true,  $rsData);