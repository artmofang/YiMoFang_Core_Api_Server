<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 18:06
 * 更新投票活动参与用户信息
 */
$vau_id = $route->bodyParams["vau_id"];

//更新条件
$whereArr = [
    "vau_id" => $vau_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("vote_activity_user",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );