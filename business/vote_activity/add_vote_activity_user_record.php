<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 21:03
 * 添加用户投票记录
 */
$vaur_id = $route->bodyParams['vaur_id'];//投票用户ID
$vaur_vote_uid = $route->bodyParams['vaur_vote_uid'];//被投票用户id
$vaur_oaid = $route->bodyParams['vaur_oaid'];//活动id

$vaur_create_time  = time();//创建时间

//写入数组
$insertArr = [
    'vaur_id'=>$vaur_id,
    'vaur_vote_uid'=>$vaur_vote_uid,
    'vaur_oaid'=>$vaur_oaid,
    'vaur_create_time'=>$vaur_create_time,
];

//执行写入语句
$rsData = $db->mysqlDB->insert('vote_activity_user_record',$insertArr);

//获取返回值
$response->responseData( true, $rsData );
