<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 17:29
 * 添加投票参与用户
 */

//获取参数信息
$vau_uid     = $route->bodyParams['vau_uid'];                        //参加者用户id
$vau_name       = $route->bodyParams['vau_name'];                    //参赛者姓名
$vau_declaration      = $route->bodyParams['vau_declaration'];      //参赛宣言
$vau_oaid     = $route->bodyParams['vau_oaid'];                      //参加的活动id
$vau_content  = $route->bodyParams['vau_content'];                   //参赛内容
$vau_phone  = $route->bodyParams['vau_phone'];                   //参赛内容


//构造存储过程参数
$proceArr = [
    $vau_oaid,
    $vau_uid,
    $vau_name,
    $vau_declaration,
    $vau_content,
    $vau_phone,
];

//构建存储过程所需参数
$db->mysqlDB->setProceduerParams( $proceArr, "C" );

//执行创建代理
$data = $db->mysqlDB->execProceduer( "ART_USER_JOIN_VOTE_ACTIVITY", $proceArr);


//返回成功结果
$response->responseData( true);
