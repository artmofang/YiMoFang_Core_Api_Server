<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 16:59
 * 更新投票活动信息
 */
$vac_id = $route->bodyParams["vac_id"];

//更新条件
$whereArr = [
    "vac_id" => $vac_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("vote_activity_config",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );