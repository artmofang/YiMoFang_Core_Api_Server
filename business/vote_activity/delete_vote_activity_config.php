<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/19
 * Time: 16:51
 * 删除投票活动
 */
//获取参数值
$vac_id = $route->bodyParams['vac_id'];

//拼接删除条件
$whereArr = [
    'vac_id'=>$vac_id
];

//执行逻辑删除语句
$rsData = $db->mysqlDB->delete("vote_activity_config",$whereArr,false);

//返回结果
$response->responseData(true,$reData);