
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/21
 * Time: 20:59
 * 查询短信验证码
 */
$fields = $route->bodyParams['fields'];

//默认条件
$route->restfulParams['is_delete'] = 0;

$sum = 0;

//判断是否需要查询总记录数
if(isset($route->restfulParams['count'])){
    
    unset($route->restfulParams['count']);
    
    //获取到记录的总条数
    $sum = $db->mysqlDB->params($route->restfulParams)->count('sms');
    
}

//根据相应的条件查询
$rsData = $db->mysqlDB->field($fields)->params($route->restfulParams)->select("sms");

//拼接获得数据
$rs = array( "count" => $sum , "data" => $rsData );


//返回成功结果
$response->responseData( true, $rs );