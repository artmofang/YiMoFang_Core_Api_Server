<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 10:45
 */
//获取参数
$bpac_name = $route->bodyParams['bpac_name']; //拼团活动名称
$bpac_desc = $route->bodyParams['bpac_desc']; //拼团活动详情
$bpac_old_price = $route->bodyParams['bpac_old_price']; //原价
$bpac_new_price = $route->bodyParams['bpac_new_price']; //拼团价
$bpac_min_count = $route->bodyParams['bpac_min_count'];  //最小参与成团人数
$bpac_open_price = $route->bodyParams['bpac_open_price'];//开团价格
$bpac_participation_price = $route->bodyParams['bpac_participation_price'];//参团价格

$bpac_appointment_info  = $regexpObj->bodyV($response,$route,'bpac_appointment_info','NORMAL'); //预约信息
$bpac_class_adjust_info  = $regexpObj->bodyV($response,$route,'bpac_class_adjust_info','NORMAL'); //调课说明
$bpac_refund_info  = $regexpObj->bodyV($response,$route,'bpac_refund_info','NORMAL'); //退款说明
$bpac_other_info  = $regexpObj->bodyV($response,$route,'bpac_other_info','NORMAL'); //其他说明
$bpac_builder_reward  = $regexpObj->bodyV($response,$route,'bpac_builder_reward','NORMAL'); //团长奖励
$bpac_participant_reward  = $regexpObj->bodyV($response,$route,'bpac_participant_reward','NORMAL'); //参团奖励
$bpac_prompt  = $regexpObj->bodyV($response,$route,'bpac_prompt','NORMAL'); //温馨提示

//写入数组
$insertArr = [
    'bpac_name'=> $bpac_name,
    'bpac_desc'=> $bpac_desc,
    'bpac_old_price'=> $bpac_old_price,
    'bpac_new_price'=> $bpac_new_price,
    'bpac_min_count'=> $bpac_min_count,
    'bpac_open_price'=> $bpac_open_price,
    'bpac_participation_price'=> $bpac_participation_price,
    'bpac_appointment_info'=> $bpac_appointment_info,
    'bpac_class_adjust_info'=> $bpac_class_adjust_info,
    'bpac_refund_info'=> $bpac_refund_info,
    'bpac_other_info'=> $bpac_other_info,
    'bpac_builder_reward'=> $bpac_builder_reward,
    'bpac_participant_reward'=> $bpac_participant_reward,
    'bpac_prompt'=>$bpac_prompt
];

//执行写入语句
$rsData = $db->mysqlDB->insert("bulk_purchase_activity_config",$insertArr);
//返回成功结果
$response->responseData( true, $rsData );