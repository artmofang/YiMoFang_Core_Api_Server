<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 15:05
 */
$bpac_id = $route->bodyParams["bpac_id"];

//更新条件
$whereArr = [
    "bpac_id" => $bpac_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("bulk_purchase_activity_config",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );