<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/4
 * Time: 3:59
 */
$bpl_oaid = $route->bodyParams['bpl_oaid']; //活动id
$bpl_fid= $route->bodyParams['bpl_fid']; //团长id
$bpl_uid= $route->bodyParams['order_NO']; //订单编号
$create_time = time();

//写入数组
$insertArr = [
    'bpl_oaid'=>$bpl_oaid,
    'bpl_fid'=>$bpl_fid,
    'bpl_uid'=>$bpl_uid,
    'create_time'=>$create_time
];

//执行语句
$rsData = $db->mysqlDB->insert("bulk_purchase_lock",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );