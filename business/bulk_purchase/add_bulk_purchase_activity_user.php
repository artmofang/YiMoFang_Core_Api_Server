<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 15:18
 */
//获取数据
$bpau_uid = $route->bodyParams['bpau_uid']; //参与用户ID
$bpau_oaid= $route->bodyParams['bpau_oaid']; //活动ID
$bpau_fid = $regexpObj->bodyV($response,$route,'bpau_fid','NUMBER'); //当前用户所属的团主键ID，如果为0，则为开团者

//写入数组
$insertArr = [
    'bpau_uid'=>$bpau_uid,
    'bpau_oaid'=>$bpau_oaid,
    'bpau_fid'=>$bpau_fid,
    'bpau_join_time'=>time()
];

//执行语句
$rsData = $db->mysqlDB->insert("bulk_purchase_activity_user",$insertArr);

//返回成功结果
$response->responseData( true, $rsData );