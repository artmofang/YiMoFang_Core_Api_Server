<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 16:02
 */
$bpl_id= $route->bodyParams["bpl_id"];

$bpl_lock  = $regexpObj->bodyV($response,$route,'bpl_lock','NUMBER'); //活动ID


//更新条件
$whereArr = [
    "bpl_id" => $bpl_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("bulk_purchase_lock",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );