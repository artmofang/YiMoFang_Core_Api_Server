<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/18
 * Time: 16:02
 */
$bpau_id= $route->bodyParams["bpau_id"];
$bpau_user_count  = $regexpObj->bodyV($response,$route,'bpau_user_count','NUMBER'); //在fid为0时，当前团的参与人数，否则无效
$bpau_fid  = $regexpObj->bodyV($response,$route,'bpau_fid','NUMBER'); //当前用户所属的开团者ID，如果为0，则为开团者
$bpau_uid  = $regexpObj->bodyV($response,$route,'bpau_uid','NUMBER'); //参与用户ID
$bpau_oaid  = $regexpObj->bodyV($response,$route,'bpau_oaid','NUMBER'); //活动ID


//更新条件
$whereArr = [
    "bpau_id" => $bpau_id
];

//拼接更新条件
$updateArr = deleteArrData($whereArr,$route->bodyParams);

//执行更新语句
$rsData = $db->mysqlDB->update("bulk_purchase_activity_user",$whereArr,$updateArr);

//返回成功结果
$response->responseData( true, $rsData );