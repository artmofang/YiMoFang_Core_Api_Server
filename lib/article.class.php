<?php

/*
 * 要求 文件名 当前接口文件夹名.Class.php
 * 	  类名	当前接口文件夹名
 *    调取方式 $当前接口文件夹名->XX();
 */

class article
{

    //私有资源【必写】
    private $db;

    /*     * 自动加载资源【必写】* */

    function __construct( $db )
    {
        $this->db = $db;
    }

    function getArticleType( $pid = 0 )
    {
        $sql = "select * from ms_article_type where pid=$pid and is_delete=0 order by sort_order asc ";

        $rsData = $this->db->mysqlDB->query( $sql );

        if( is_array( $rsData ) )
        {
            foreach( $rsData as $v )
            {
                $arr[ $v[ 'type_id' ] ][ 'type_name' ] = $v[ 'type_name' ];
                $arr[ $v[ 'type_id' ] ][ 'type_id' ] = $v[ 'type_id' ];
				$arr[ $v[ 'type_id' ] ][ 'child' ] = $rsData;
                $arr[ $v[ 'type_id' ] ][ 'son' ] = $this->getArticleType( $v[ 'type_id' ] );
            }
        }
        return $arr;
    }

}

?>