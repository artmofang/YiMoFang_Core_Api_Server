<?php
/*
*用户公共类
*
*
*/

class usercommen
{

    //私有资源【必写】
    private $db;

    /*     * 自动加载资源【必写】* */

    function __construct( $db )
    {
        $this->db = $db;
    }
    
    //LAYER_NUM //获得用户节点层级LAYER_NUM
    public function connection_index($user_name){
        $db=$this->db;
        $where['USER_NAME']=$user_name;
        $list=$db->mysqlDB->where($where)->findTable('cl_member_connection_relations');
        if($list){
            return $list['LAYER_NUM'];
        }else{
            return false;
        }
    }

   //判断两个用户是否为同一节点
    function connection($user_name1,$user_name2)
    {
        if( !$user_name1_layer = $this->connection_index( $user_name1 ) )
        {
            \coreLib\ErrorClass\Error::Result( 'BUSINESS_ERROR', '用户节点层级获取失败' );
        }
        
        if( !$user_name2_layer = $this->connection_index( $user_name2 ) )
        {
            \coreLib\ErrorClass\Error::Result( 'BUSINESS_ERROR', '用户节点层级获取失败' );
        }
        
        return $user_name1_layer == $user_name2_layer;
    }

    //递归获得验证节点 
    /*
    *$yuanuser 上层用户名
    *$user_name 下层用户名
    *
    */
    public function connection_recursion($yuanuser,$user_name)
    {
        if( $this->connection( $yuanuser, $user_name ) )
        {
            return false;
        }
        
    	$db=$this->db;
    	$where['user_name']=$user_name;
    	$list=$db->mysqlDB->field('CONTACT_MAN')->where($where)->find('users');
    	if($list['CONTACT_MAN']==$yuanuser){
    		return true;
    	}elseif($list['CONTACT_MAN']=='Company'){
    		return false;

    	}else{
    		connection_recursion($yuanuser,$list['CONTACT_MAN']);
    	}


    }
    
    //二叉树验证节点关系
    /*
    *$yuanuser 上层用户名
    *$user_name 下层用户名
    *
    */
    public function connection_twice($yuanuser,$user_name){
        if( $this->connection( $yuanuser, $user_name ) )
        {
            return false;
        }
        
    	$sql="SELECT * FROM cl_member_connection_relations WHERE "
    	."LEFT_INDEX >= ( SELECT LEFT_INDEX FROM cl_member_connection_relations WHERE USER_NAME = '".$yuanuser."') "
    	." and  RIGHT_INDEX<=( SELECT RIGHT_INDEX FROM cl_member_connection_relations WHERE USER_NAME='".$yuanuser."')"
    	." and  USER_NAME='".$user_name."'";
    	$db=$this->db;
    	$re=$db->mysqlDB->query($sql);
    	if(is_array($re)){

    		return true;
    	}else{
    		return false;
    	}

    }
    
    //二叉树验证推荐关系
    /*
    *$yuanuser 上层用户名
    *$user_name 下层用户名
    *
    */
    public function recommend_twice($yuanuser,$user_name){
    	$sql="SELECT * FROM cl_member_recommend_relations WHERE "
    	."LEFT_INDEX >= ( SELECT LEFT_INDEX FROM cl_member_recommend_relations WHERE USER_NAME = '".$yuanuser."') "
    	." and  RIGHT_INDEX<=( SELECT RIGHT_INDEX FROM cl_member_recommend_relations WHERE USER_NAME='".$yuanuser."')"
    	." and  USER_NAME='".$user_name."'";
    	$db=$this->db;
    	$re=$db->mysqlDB->query($sql);
    	if(is_array($re)){

    		return true;
    	}else{
    		return false;
    	}


    }



}  













?>