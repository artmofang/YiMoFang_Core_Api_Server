<?php
include './thirdSDK/RongLibSDK/rongcloud.php';

class friends{

	public  $rCloud;
    
    /**自动加载资源【必写】**/
    function __construct(){

    	//配置相关融云信息
		$appKey    = $GLOBALS['third']['rongLibAppKey'];
		$appSecret = $GLOBALS['third']['rongLibAppSecret'];

		//初始化融云
    	$RongCloud 	  = new RongCloud($appKey,$appSecret);
    	$this->rCloud = $RongCloud;
    
    }
    
    public function getRongCloud(){
    	return $this->rCloud;
    }
    
}
?>