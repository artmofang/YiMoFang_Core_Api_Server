<?php
/**
 * 密保卡
 */
class Security {

	private $db;

	private $image_url = 'files/image/';
	// 构造函数初始化$db
	function __construct($db) {
		$this->db = $db;
	}
	// 密保卡绑定
	function user_binding() {
		// 生成随机横坐标
		$rand_str = $this->rand_str ( 9 );
		$arr = array ();
		for($k = 0; $k < strlen ( $rand_str ); $k ++) {
			for($i = 1; $i < 10; $i ++) {
				$rand = $this->rand_num ( 2 );
				// 赋给所有code的容器
				$arr [$rand_str {$k} . $i] = $rand;
			}
		}
		$array = array ();
		$array ['code'] = serialize ( $arr ); // 序列化后将信息入库
		$array ['letter'] = $rand_str; // 横坐标
		return $array;
	}
	
	// 生成密保卡资源
	public function make_ob($code, $letter, $key) {
		$codes = unserialize ( $code );
		// 图片初始值
		$bit = 2; // 密保卡位数
		$height = 361; // 图片高度
		$width = 570; // 图片宽度
		$im = imagecreatetruecolor ( $width, $height ); // 创建
		$linecolor = imagecolorallocate ( $im, 168, 34, 133 );
		$fontcolor = imagecolorallocate ( $im, 0, 0, 0 );
		$top_rectangle_color = imagecolorallocate ( $im, 246, 90, 135 ); // 上标题颜色
		$top_letter_color = imagecolorallocate ( $im, 255, 255, 255 ); // abcd
		$left_rectangle_color = imagecolorallocate ( $im, 246, 90, 135 );
		$left_num_color = imagecolorallocate ( $im, 255, 255, 192 );
		$logo_str_color = imagecolorallocate ( $im, 255, 255, 255 );
		$top_rectangle_color2 = imagecolorallocate ( $im, 168, 34, 133 ); // 头颜色
		imagefill ( $im, 0, 0, imagecolorallocate ( $im, 248, 235, 240 ) ); // 图片背景色
	    $font = $image_url . 'arial.ttf'; // 字体
		$font_en = $image_url . 'CANDARAB.TTF'; // 英文字体
		$font2 = $image_url . 'msyh.ttf'; // 密保卡上方黑体
		                                      // 添加水印
		                                      // $dst = imagecreatefromjpeg("./mibao.jpg");
		                                      // imagecopymerge($im,$dst,120,15,0,0,193,55,100);
		imageline ( $im, 10, 72, $width - 10, 72, $linecolor );
		$ltext = "38妇乐密保卡";
		// 头颜色添加
		imagefilledrectangle ( $im, 10, 10, $width - 10, 72, $top_rectangle_color2 );
		if (! imagettftext ( $im, 18, 0, 220, 47, $logo_str_color, $font2, $ltext )) {
			exit ( 'error' );
		}
		
		// 写入卡号
		$p = $key;
		// 序列号位置
		$x = 40;
		$y = 95;
		
		imagettftext ( $im, 13, 0, $x, $y, $color, $font2, '序列号：' );
		imagettftext ( $im, 13, 0, $x + 80, $y, $color, $font_en, $p );
		// 颜色框
		imagefilledrectangle ( $im, 10, 106, $width - 10, 128, $top_rectangle_color );
		imagefilledrectangle ( $im, 10, 129, 65, $height - 16, $left_rectangle_color );
		// 写入最上排英文字母及竖线
		for($i = 1; $i <= 10; $i ++) {
			$x = $i * 55 + 35;
			$y = 123;
			$float_size = 15; // 字母位置参数
			imagettftext ( $im, $float_size, 0, $x, $y, $top_letter_color, $font_en, $letter {$i - 1} ); // 写入最上排英文字母
		}
		
		for($i = 0; $i <= 10; $i ++) {
			
			$linex = $i * 55 + 65;
			$liney = 105;
			$liney2 = $height - 15; // 竖线位置参数
			
			imageline ( $im, $linex, $liney, $linex, $liney2, $linecolor ); // 划入竖线
		}
		
		// 写入竖排数字及填入矩阵数据 划横线
		
		for($j = 0; $j < 9; $j ++) {
			
			$jj = $j + 1;
			
			$x = 35;
			$y = ($jj * 24) + 123; // 左排数字及横线位置参数
			
			imagettftext ( $im, $float_size, 0, $x, $y, $left_num_color, $font_en, $jj ); // 写入左排数字
			
			for($i = 1; $i <= 9; $i ++) {
				
				$float_size2 = 13;
				$x = $i * 55 + 27;
				$sy = $y; // 填入矩阵数据位置参数
				
				$s = $letter {$i - 1};
				$s .= $j + 1;
				@imagettftext ( $im, $float_size2, 0, $x, $sy, $fontcolor, $font_en, $codes [$s] ); // 写入矩阵数据
			}
		}
		
		for($j = 0; $j < 11; $j ++) { // 横线数
			
			$line_x = 10;
			$line_x2 = $width - 10;
			$y = $j * 24 + 105; // 横线位置参数 y坐标数据同上
			
			imageline ( $im, $line_x, $y, $line_x2, $y, $linecolor ); // 划入横线
		}
		
		// 外框边线
		
		imageline ( $im, 10, 10, $width - 10, 10, $linecolor ); // 横线
		                                                        
		// imageline($im,10,$height-15,$width-10,$height-10,$linecolor);
		
		imageline ( $im, 10, 10, 10, $height - 16, $linecolor ); // 竖线
		
		imageline ( $im, $width - 10, 10, $width - 10, $height - 16, $linecolor );
		
		// 生成图片
		
		ob_clean ();
		return $im;
	}
	// 生成大写字母
	private function rand_str($i = "") {
		$strstr = "ABCDEFGHI";
		// $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";//动态生成
		/*
		 * $finalStr = array();
		 * for($j=0;$j<$i;$j++) {
		 * $strTemp = substr($str,rand(0,strlen($str)-1),1);
		 * $finalStrArray[$j] = $strTemp;
		 * $str = str_replace($strTemp, "", $str);
		 * }
		 * sort($finalStrArray);
		 * $arrstr = $finalStrArray;
		 * $strstr="";
		 * for($h=0;$h<$i;$h++){
		 * $strstr .= $arrstr[$h];
		 * }
		 */
		return $strstr;
	}
	// 生成数字
	private function rand_num($num) {
		$array = array (
				0,
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				8,
				9 
		);
		$key = array_rand ( $array, $num );
		$str = "";
		for($i = 0; $i < $num; $i ++) {
			$str .= $array [$key [$i]];
		}
		return $str;
	}
	// 生成密保卡号
	public function rand_key() {
		// 写入卡号
		$str = '';
		for($i = 1; $i <= 4; $i ++) {
			$str .= $this->rand_num ( 4 ) . ' ';
		}
		return $str;
	}
	
	// 下载图片
	/*
	 * $im 密保卡资源
	 *
	 */
	public function down($im) {
		header ( "Content-Type:text/html;charset=utf-8" );
		header ( "Content-type: image/jpeg" );
		header ( "Content-Disposition: attachment;filename=demo.jpg");
		$s = imagejpeg ( $im );
		imagedestroy ( $im );
	}
}